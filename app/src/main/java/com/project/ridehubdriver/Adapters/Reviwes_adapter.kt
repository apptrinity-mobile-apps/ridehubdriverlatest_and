package com.project.ridehubdriver.Adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.RatingBar
import android.widget.TextView
import com.project.ridehubdriver.PojoResponse.Reviwes_Pojo
import com.project.ridehubdriver.R
import java.util.*

/**
 * Created by user88 on 11/6/2015.
 */
class Reviwes_adapter(private val context: Activity, private val data: ArrayList<Reviwes_Pojo>) :
    BaseAdapter() {
    private val mInflater: LayoutInflater
    private val check: String? = null
    var Srating = ""

    init {
        mInflater = LayoutInflater.from(context)
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    inner class ViewHolder {
        internal var rating_text: TextView? = null
        internal var rating: RatingBar? = null

    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder

        if (convertView == null) {
            view = mInflater.inflate(R.layout.item_rating, parent, false)
            holder = ViewHolder()
            holder.rating_text = view.findViewById<View>(R.id.rating_page_tv) as TextView
            holder.rating =
                view.findViewById<View>(R.id.rating_page_behaviour_rating_bar) as RatingBar

            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }

        holder.rating!!.onRatingBarChangeListener =
            RatingBar.OnRatingBarChangeListener { ratingBar, rating, fromUser ->
                Srating = rating.toString()
                data[position].ratings_count = Srating
            }

        holder.rating_text!!.text = data[position].options_title

        return view
    }
}
