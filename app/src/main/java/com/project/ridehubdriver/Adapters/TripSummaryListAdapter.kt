package com.project.ridehubdriver.Adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

import com.project.ridehubdriver.PojoResponse.Ride
import com.project.ridehubdriver.PojoResponse.TripDetails
import com.project.ridehubdriver.R

import java.util.ArrayList

/**
 * Created by user14 on 9/22/2015.
 */
class TripSummaryListAdapter(private val context: Activity) : BaseAdapter() {

    private var data: ArrayList<Ride>? = null
    private val mInflater: LayoutInflater
    private val check: String? = null

    init {
        mInflater = LayoutInflater.from(context)
    }

    fun setDao(tripDetails: TripDetails) {
        data = tripDetails.response!!.rides as ArrayList<Ride>
    }

    override fun getCount(): Int {
        return data!!.size
    }

    override fun getItem(position: Int): Any {
        return data!![position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewTypeCount(): Int {
        return 1
    }


    inner class ViewHolder {
        internal var trip_summery_tvaddress: TextView? = null
        internal var trip_summery_tvprice: TextView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.trip_summary_list, parent, false)
            holder = ViewHolder()
            holder.trip_summery_tvaddress =
                view.findViewById<View>(R.id.trip_summary_list_address) as TextView
            holder.trip_summery_tvprice =
                view.findViewById<View>(R.id.trip_summary_list_date_and_time) as TextView
            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }
        holder.trip_summery_tvaddress!!.text = data!![position].pickup
        holder.trip_summery_tvprice!!.text = data!![position].rideTime
        return view
    }

}