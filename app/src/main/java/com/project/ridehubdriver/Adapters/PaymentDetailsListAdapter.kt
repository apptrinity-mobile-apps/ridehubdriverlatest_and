package com.project.ridehubdriver.Adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

import com.project.ridehubdriver.PojoResponse.PaymentDetailsListPojo
import com.project.ridehubdriver.R

import java.util.ArrayList

/**
 * Created by user14 on 9/22/2015.
 */
class PaymentDetailsListAdapter(
    private val context: Activity,
    private val data: ArrayList<PaymentDetailsListPojo>
) : BaseAdapter() {
    private val mInflater: LayoutInflater
    private val check: String? = null

    init {
        mInflater = LayoutInflater.from(context)
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewTypeCount(): Int {
        return 1
    }


    inner class ViewHolder {
        internal var rideid: TextView? = null
        internal var amount: TextView? = null
        internal var ridedate: TextView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder
        val data1 = " "
        if (convertView == null) {
            view = mInflater.inflate(R.layout.item_payment_summary_detailed, parent, false)

            holder = ViewHolder()
            holder.rideid = view.findViewById<View>(R.id.pmt_dtl_list_rideid) as TextView
            holder.amount = view.findViewById<View>(R.id.pmt_dtl_list_amount) as TextView
            holder.ridedate = view.findViewById<View>(R.id.pmt_dtl_list_date) as TextView
            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }
        holder.rideid!!.text = data[position].getride_id()
        holder.amount!!.text = data[position].getamount()
        holder.ridedate!!.text = data[position].getride_date()
        return view
    }


}

