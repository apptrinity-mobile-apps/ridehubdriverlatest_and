package com.project.ridehubdriver.Adapters

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.location.Location
import android.media.MediaPlayer
import android.os.Build
import android.os.CountDownTimer
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import at.grabner.circleprogress.CircleProgressView
import at.grabner.circleprogress.TextMode
import com.android.volley.Request
import com.project.ridehubdriver.Activities.ArrivedTrip
import com.project.ridehubdriver.Activities.DriverAlertActivity
import com.project.ridehubdriver.Activities.DriverMapActivity
import com.project.ridehubdriver.Helper.DBHelper
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceManager
import com.project.ridehubdriver.PojoResponse.ChatResponsePojo
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.PkDialog
import com.project.ridehubdriver.Utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import kotlin.math.abs

/**
 * Created by Administrator on 11/23/2015.
 */

@SuppressLint("SetTextI18n")
class ContinuousRequestAdapter(
    private val context: Activity,
    private val listview: LinearLayout
) {
    private var sm: SessionManager? = null
    private val mInflater: LayoutInflater
    var req_count: Int = 0
    private val mCircleView: CircleProgressView? = null
    private val timer: CountDownTimer? = null
    private val KEY1 = "key1"
    private val KEY2 = "key2"
    private val KEY3 = "key3"
    private val KEY4 = "key4"
    private var rider_id = ""
    private var dialog: Dialog? = null
    private val seconds = 0
    private val mediaPlayer: MediaPlayer? = null
    private val myLocation: Location? = null
    private var timerCompletCallback: DriverAlertActivity.TimerCompletCallback? = null
    private val mHandler = Handler()
    lateinit var dbHelper: DBHelper


    private val acceptBtnlistener = View.OnClickListener { view ->
        val holder = view.tag as ViewHolder
        val jsonParams = HashMap<String, String>()
        val userDetails = sm!!.userDetails
        val driverId = userDetails[SessionManager.KEY_DRIVERID]
        rider_id = getDataForPosition(holder.count, KEY1, holder.data)
        //String address = getDataForPosition(i,KEY3);
        jsonParams["ride_id"] = "" + rider_id
        jsonParams["driver_id"] = "" + driverId!!
        if (DriverMapActivity.myLocation != null) {
            jsonParams["driver_lat"] = "" + DriverMapActivity.myLocation!!.latitude
            jsonParams["driver_lon"] = "" + DriverMapActivity.myLocation!!.longitude
            println("rideid---------$rider_id")
            println("driver_id---------$driverId")
            println("driver_lat---------" + DriverMapActivity.myLocation!!.latitude)
            println("driver_lon---------" + DriverMapActivity.myLocation!!.longitude)
        }
        val manager = ServiceManager(context, acceptServicelistener)
        manager.makeServiceRequest(
            ServiceConstant.ACCEPTING_RIDE_REQUEST,
            Request.Method.POST,
            jsonParams
        )
        println("acceptrideurl------------" + ServiceConstant.ACCEPTING_RIDE_REQUEST)
        showDialog()
        stopPlayer()
    }

    internal var acceptServicelistener: ServiceManager.ServiceListener =
        object : ServiceManager.ServiceListener {
            override fun onCompleteListener(`object`: Any) {
                dismissDialog()
                var Sstatus = ""
                var SResponse = ""
                var Str_Username = ""
                val Str_UserId = ""
                var Str_User_email = ""
                var Str_Userphoneno = ""
                var Str_Userrating = ""
                var Str_userimg = ""
                var Str_pickuplocation = ""
                var Str_pickuplat = ""
                var Str_pickup_long = ""
                var Str_pickup_time = ""
                var Str_message = ""
                var Str_droplat = ""
                var Str_droplon = ""
                var str_drop_location = ""
                var str_notefordriver = ""

                if (`object` is String) {
                    println("Responseaccept---------$`object`")

                    Log.e("accept", `object`)

                    try {
                        val object1 = JSONObject(`object`)
                        Sstatus = object1.getString("status")
                        if (Sstatus.equals("1", ignoreCase = true)) {
                            println("status-----------$Sstatus")
                            val jobject = object1.getJSONObject("response")
                            val jobject2 = jobject.getJSONObject("user_profile")
                            Str_message = jobject.getString("message")
                            Str_Username = jobject2.getString("user_name")

                            //Str_UserId = jobject2.getString("user_id");
                            userID = jobject2.getString("user_id")

                            Str_User_email = jobject2.getString("user_email")
                            Str_Userphoneno = jobject2.getString("phone_number")
                            Str_Userrating = jobject2.getString("user_review")
                            Str_pickuplocation = jobject2.getString("pickup_location")
                            Str_pickuplat = jobject2.getString("pickup_lat")
                            Str_pickup_long = jobject2.getString("pickup_lon")
                            Str_pickup_time = jobject2.getString("pickup_time")
                            Str_userimg = jobject2.getString("user_image")
                            Str_droplat = jobject2.getString("drop_lat")
                            Str_droplon = jobject2.getString("drop_lon")
                            str_drop_location = jobject2.getString("drop_loc")
                            str_notefordriver = jobject2.getString("note")
                            // System.out.println("userid-------------"+Str_UserId);

                        }
                        if (Sstatus.equals("1", ignoreCase = true)) {

                            // intent for notification
                            // BUNDLE_NOTIFICATION_ID += 1
                            val bundle_notification_id = "$BUNDLE_NOTIFICATION_ID"

                            val resultIntent = Intent(context, ArrivedTrip::class.java)
                            resultIntent.putExtra("address", Str_pickuplocation)
                            resultIntent.putExtra("rideId", rider_id)
                            resultIntent.putExtra("pickuplat", Str_pickuplat)
                            resultIntent.putExtra("pickup_long", Str_pickup_long)
                            resultIntent.putExtra("username", Str_Username)
                            resultIntent.putExtra("userrating", Str_Userrating)
                            resultIntent.putExtra("phoneno", Str_Userphoneno)
                            resultIntent.putExtra("userimg", Str_userimg)
                            resultIntent.putExtra("UserId", userID)
                            resultIntent.putExtra("drop_lat", Str_droplat)
                            resultIntent.putExtra("drop_lon", Str_droplon)
                            resultIntent.putExtra("drop_location", str_drop_location)
                            resultIntent.putExtra("note_for_driver", str_notefordriver)
                            resultIntent.putExtra("bundle_notification_id", bundle_notification_id)

                            if (str_notefordriver == "" || str_notefordriver == "null" || str_notefordriver.isEmpty()) {
                                // don't show notification
                            } else {
                                showNotification(
                                    str_notefordriver,
                                    resultIntent,
                                    bundle_notification_id
                                )
                            }

                            val intent = Intent(context, ArrivedTrip::class.java)
                            intent.putExtra("address", Str_pickuplocation)
                            intent.putExtra("rideId", rider_id)
                            intent.putExtra("pickuplat", Str_pickuplat)
                            intent.putExtra("pickup_long", Str_pickup_long)
                            intent.putExtra("username", Str_Username)
                            intent.putExtra("userrating", Str_Userrating)
                            intent.putExtra("phoneno", Str_Userphoneno)
                            intent.putExtra("userimg", Str_userimg)
                            intent.putExtra("UserId", userID)
                            intent.putExtra("drop_lat", Str_droplat)
                            intent.putExtra("drop_lon", Str_droplon)
                            intent.putExtra("drop_location", str_drop_location)
                            intent.putExtra("note_for_driver", str_notefordriver)
                            intent.putExtra("bundle_notification_id", bundle_notification_id)

                            context.startActivity(intent)
                            context.finish()

                        } else {
                            SResponse = object1.getString("response")
                            Alert(context.getString(R.string.alert_sorry_label_title), SResponse)
                            val mdialog = PkDialog(context)
                            mdialog.setDialogTitle(context.getString(R.string.alert_sorry_label_title))
                            mdialog.setDialogMessage(SResponse)
                            mdialog.setPositiveButton(
                                context.getString(R.string.alert_label_ok),
                                View.OnClickListener {
                                    mdialog.dismiss()
                                    //--------remove list after
                                    listview.removeViewAt(0)
                                    context.finish()
                                }
                            )
                            mdialog.show()
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onErrorListener(error: Any) {
                dismissDialog()
                context.finish()
            }
        }

    inner class ViewHolder {
        var count: Int = 0
        var accept: TextView? = null
        var decline: TextView? = null
        var circularProgressBar: CircleProgressView? = null
        var cabily_alert_address: TextView? = null
        var cabily_alert_to: TextView? = null
        var cabily_alert_destaddress: TextView? = null
        internal var Ll_ride_Requst_layout: RelativeLayout? = null
        var data: JSONObject? = null
    }

    init {
        sm = SessionManager(context)
        mInflater = LayoutInflater.from(context)
    }

    fun setTimerCompleteCallBack(callBack: DriverAlertActivity.TimerCompletCallback) {
        this.timerCompletCallback = callBack
    }

    @SuppressLint("WrongViewCast")
    fun getView(i: Int, data: JSONObject): View {
        val view: View
        val holder: ViewHolder
        val data1 = " "
        // JSONObject data = (JSONObject) getItem(i);
        view = mInflater.inflate(R.layout.driver_alert_2, null, false)
        holder = ViewHolder()
        holder.data = data
        holder.count = i
        holder.accept = view.findViewById(R.id.cabily_driver_alert_accept_btn) as TextView
        holder.decline = view.findViewById(R.id.cabily_driver_alert_reject_btn) as TextView
        holder.cabily_alert_address = view.findViewById(R.id.cabily_alert_address) as TextView
        // holder.cabily_alert_to = (TextView) view.findViewById(R.id.cabily_alert_to);
        dbHelper = DBHelper(context)
        holder.cabily_alert_destaddress =
            view.findViewById(R.id.cabily_alert_destaddress) as TextView
        holder.circularProgressBar = view.findViewById(R.id.timer_circleView) as CircleProgressView
        holder.Ll_ride_Requst_layout =
            view.findViewById(R.id.Ll_ride_request_layout) as RelativeLayout
        view.tag = holder
        val user = sm!!.requestCount
        req_count = user[SessionManager.KEY_COUNT]!!
        println("---------------req_count top-------------------$req_count")

        holder.accept!!.tag = holder
        holder.decline!!.tag = holder
        holder.accept!!.setOnClickListener(acceptBtnlistener)
        holder.decline!!.setOnClickListener(DeclineBtnListener(i))
        var destination_value = getDataForPosition(i, KEY4, data)
        if (destination_value.equals("", ignoreCase = true)) {
            destination_value = ""
            holder.cabily_alert_destaddress!!.visibility = View.GONE
            // holder.cabily_alert_to.setVisibility(View.GONE);
        } else {
            //holder.cabily_alert_to.setVisibility(View.VISIBLE);
            holder.cabily_alert_destaddress!!.visibility = View.VISIBLE
            holder.cabily_alert_destaddress!!.text = destination_value

        }
        holder.cabily_alert_address!!.text = "" + getDataForPosition(i, KEY3, data)

        val chatResponsePojo = ChatResponsePojo()
        chatResponsePojo.RIDER_ID = getDataForPosition(i, KEY1, data)
        chatResponsePojo.RECEIVED_STATUS = ""
        chatResponsePojo.RECEIVED_MESSAGE = getDataForPosition(i, KEY2, data)
        chatResponsePojo.SENT_STATUS = ""
        chatResponsePojo.SENT_MESSAGE = getDataForPosition(i, KEY4, data)
        dbHelper.insert(chatResponsePojo)
        println(
            "---------------Source and destination top-------------------" +
                    getDataForPosition(i, KEY3, data) + "&&" +
                    getDataForPosition(i, KEY4, data)
        )
        holder.circularProgressBar!!.isEnabled = false
        val position = getDataForPosition(i, KEY3, data)
        holder.circularProgressBar!!.isFocusable = false
        holder.circularProgressBar!!.setMaxValue(
            Integer.parseInt(
                getDataForPosition(
                    i,
                    KEY2,
                    data
                )
            ).toFloat()
        )
        holder.circularProgressBar!!.setValueAnimated(0F)
        holder.circularProgressBar!!.textSize = 30
        holder.circularProgressBar!!.isAutoTextSize = true
        holder.circularProgressBar!!.textScale = 0.6f
        holder.circularProgressBar!!.textColor =
            ContextCompat.getColor(context, R.color.progress_ripplecolor)
        mHandler.post(CircularHandler(holder, Integer.parseInt(getDataForPosition(i, KEY2, data))))
        return view
    }

    // code for generating app level notifications without fireBase
    var BUNDLE_NOTIFICATION_ID = 100
    val BUNDLE_CHANNEL_ID = "RIDEHUB_DRIVER_APP"
    val BUNDLE_CHANNEL_NAME = "RIDEHUB_DRIVER"
    private fun showNotification(message: String, intent: Intent, bundle_notification_id: String) {

        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val groupChannel = NotificationChannel(
                BUNDLE_CHANNEL_ID,
                BUNDLE_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(groupChannel)
        }
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
        val resultPendingIntent =
            PendingIntent.getActivity(
                context,
                BUNDLE_NOTIFICATION_ID,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )

        val notificationBuilder = NotificationCompat.Builder(context, BUNDLE_CHANNEL_ID)
            .setGroup(bundle_notification_id)
            .setGroupSummary(true)
            .setSmallIcon(R.mipmap.ic_launcher_blue_round)
            .setContentTitle("Message from rider")
            .setContentText(message)
            .setDefaults(NotificationCompat.DEFAULT_ALL)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setAutoCancel(true)
            .setContentIntent(resultPendingIntent)

        notificationManager.notify(BUNDLE_NOTIFICATION_ID, notificationBuilder.build())
    }

    private inner class CircularHandler(internal var holder: ViewHolder, internal var value: Int?) :
        Runnable {
        internal var isRunning: Boolean = false

        init {
            isRunning = true
        }

        override fun run() {
            if (isRunning) {
                value = value!! - 1

                holder.circularProgressBar!!.setText(abs(value!!).toString())
                holder.circularProgressBar!!.setTextMode(TextMode.TEXT)
                holder.circularProgressBar!!.setValueAnimated(value!!.toFloat(), 500)
                mHandler.postDelayed(this, 1000)

                if (value == 0) {
                    mHandler.removeCallbacks(this)
                    if (timerCompletCallback != null) {
                        holder.Ll_ride_Requst_layout!!.visibility = View.GONE

                        println("requestcount2 above------------------$req_count")

                        req_count -= 1
                        val session = SessionManager(context)
                        session.setRequestCount(req_count)

                        println("requestcount2 below------------------$req_count")

                        if (req_count == 0) {

                            println("activity  finished------------------$req_count")
                            sm!!.setRequestCount(0)
                            context.finish()
                            stopPlayer()
                            context.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        }
                    }
                    isRunning = false
                }
            }
        }
    }

    private fun getDataForPosition(i: Int, key: String, data: JSONObject?): String {
        try {
            return data!!.get(key) as String
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return ""
    }

    private fun showDialog() {
        dialog = Dialog(context)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
    }

    fun dismissDialog() {
        try {
            if (dialog != null)
                dialog!!.dismiss()
        } catch (e: Exception) {
        }

    }


    private fun stopPlayer() {
        try {
            if (DriverAlertActivity.mediaPlayer != null && DriverAlertActivity.mediaPlayer!!.isPlaying()) {
                DriverAlertActivity.mediaPlayer!!.stop()
            }
        } catch (e: Exception) {
        }

    }


    //--------------Alert Method-----------
    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(context)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            context.getString(R.string.alert_label_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    inner class DeclineBtnListener internal constructor(internal var mPosition: Int) :
        View.OnClickListener {
        override fun onClick(v: View) {

            val holder = v.tag as ViewHolder
            try {

                if (timerCompletCallback != null) {
                    holder.count = holder.count - 1

                    val user = sm!!.requestCount
                    var req_count = user[SessionManager.KEY_COUNT]!!
                    req_count = req_count - 1

                    println("----------inside declineBtnListener req_count----------------$req_count")

                    sm!!.setRequestCount(req_count)
                    holder.Ll_ride_Requst_layout!!.visibility = View.GONE

                    if (req_count == 0) {
                        sm!!.setRequestCount(0)
                        context.finish()
                        if (DriverAlertActivity.mediaPlayer != null && DriverAlertActivity.mediaPlayer!!.isPlaying()) {
                            DriverAlertActivity.mediaPlayer!!.stop()
                        }
                    }
                    //req_count=req_count-1;
                    // sm.setRequestCount(req_count);
                    // timerCompletCallback.timerCompleteCallBack(holder);
                }
            } catch (e: Exception) {
            }

        }
    }

    companion object {
        lateinit var userID: String
    }


}
