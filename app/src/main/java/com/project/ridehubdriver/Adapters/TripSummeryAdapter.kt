package com.project.ridehubdriver.Adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.project.ridehubdriver.PojoResponse.TripSummaryPojo
import com.project.ridehubdriver.R
import java.util.*

/**
 * Created by user88 on 10/23/2015.
 */
class TripSummeryAdapter(
    private val context: Activity,
    private val data: ArrayList<TripSummaryPojo>
) : BaseAdapter() {
    private val mInflater: LayoutInflater
    private val check: String? = null

    init {
        mInflater = LayoutInflater.from(context)
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewTypeCount(): Int {
        return 1
    }


    inner class ViewHolder {
        internal var trip_summery_tvaddress: TextView? = null
        internal var tv_ride_destination: TextView? = null
       // internal var time_imageview: ImageView? = null
    }


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.item_trip_summary, parent, false)
            holder = ViewHolder()
            holder.trip_summery_tvaddress =
                view.findViewById(R.id.trip_summary_list_address)
            holder.tv_ride_destination =
                view.findViewById(R.id.tv_ride_destination)
//            holder.time_imageview = view.findViewById(R.id.time_imageview) as ImageView
            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }

      //  val recent_main_layout = view.findViewById(R.id.recent_main_layout) as LinearLayout
        /* if (position % 2 == 1) {*/
      //  recent_main_layout.setBackgroundColor(context.resources.getColor(R.color.white))
//        holder.trip_summery_tvaddress!!.setTextColor(context.resources.getColor(R.color.app_color))
        //holder.trip_summery_tvdate!!.setTextColor(context.resources.getColor(R.color.light_grey))
        // holder.time_imageview.setImageDrawable(context.getResources().getDrawable(R.drawable.your_trip_blue));
        /*} else {
            recent_main_layout.setBackgroundColor(context.getResources().getColor(R.color.app_color));
            holder.trip_summery_tvaddress.setTextColor(context.getResources().getColor(R.color.white));
            holder.trip_summery_tvdate.setTextColor(context.getResources().getColor(R.color.blue_ridehub));
            holder.time_imageview.setImageDrawable(context.getResources().getDrawable(R.drawable.carwhite_tripsummary_ridehub));
        }*/

        holder.trip_summery_tvaddress!!.text = data[position].getpickup()
        holder.tv_ride_destination!!.text = data[position].getdrop()
        //holder.trip_summery_tvdate!!.setText(data[position].getdatetime())
        return view
    }


}
