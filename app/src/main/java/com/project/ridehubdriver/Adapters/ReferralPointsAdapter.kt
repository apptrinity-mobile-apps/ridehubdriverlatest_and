package com.project.ridehubdriver.Adapters

import android.app.Activity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.project.ridehubdriver.PojoResponse.ReferralPointsPojo
import com.project.ridehubdriver.R
import java.util.*

/**
 * Created by user14 on 9/22/2015.
 */
class ReferralPointsAdapter(
    private val context: Activity,
    private val data: ArrayList<ReferralPointsPojo>
) : BaseAdapter() {
    private val mInflater: LayoutInflater
    private val check: String? = null

    init {
        mInflater = LayoutInflater.from(context)
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewTypeCount(): Int {
        return 1
    }


    inner class ViewHolder {
        internal var tv_refer_name: TextView? = null
        internal var tv_refer_transaction_type: TextView? = null
        internal var tv_refer_rideid: TextView? = null
        internal var tv_refer_level: TextView? = null
        internal var tv_refer_points: TextView? = null
        internal var tv_datereferal: TextView? = null
        internal var tv_refer_type: TextView? = null
        internal var track_your_ride_driverimage: ImageView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder
        val data1 = " "
        if (convertView == null) {
            view = mInflater.inflate(R.layout.item_referral_points, parent, false)
            holder = ViewHolder()
            holder.tv_refer_name = view.findViewById<View>(R.id.tv_refer_name) as TextView
            // holder.tv_refer_transaction_type = (CustomTextView) view.findViewById(R.id.tv_refer_transaction_type);
            holder.tv_refer_rideid = view.findViewById<View>(R.id.tv_refer_rideid) as TextView
            holder.tv_refer_level = view.findViewById<View>(R.id.tv_refer_level) as TextView
            holder.tv_refer_points = view.findViewById<View>(R.id.tv_refer_points) as TextView
            holder.tv_datereferal = view.findViewById<View>(R.id.tv_datereferal) as TextView
            holder.tv_refer_type = view.findViewById<View>(R.id.tv_refer_type) as TextView
            holder.track_your_ride_driverimage =
                view.findViewById<View>(R.id.track_your_ride_driverimage) as ImageView

            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }

        Log.e("POINTS_REFERRAL", data[position].points!!)
        holder.tv_refer_name!!.text = data[position].referrer_name
        // holder.tv_refer_transaction_type.setText(data.get(position).getTrans_type() +"-");
        holder.tv_refer_rideid!!.text = data[position].ride_id!!
        holder.tv_refer_level!!.text =  data[position].level!!
        holder.tv_refer_points!!.text = data[position].trans_type + " - " + data[position].points
        holder.tv_datereferal!!.text = data[position].date!!
        holder.tv_refer_type!!.text =  data[position].type!!

        /*if(data.get(position).getType().equals("driver")){
            holder.track_your_ride_driverimage.setImageDrawable(context.getResources().getDrawable(R.drawable.referral_driver));
        }*/

        /* if(data.get(position).getTrans_type().equals("CR")){
            holder.tv_refer_transaction_type.setText(data.get(position).getTrans_type());
            holder.tv_refer_transaction_type.setTextColor(context.getResources().getColor(R.color.green_alert_dialog_text));
        }else {
            holder.tv_refer_transaction_type.setText(data.get(position).getTrans_type());
            holder.tv_refer_transaction_type.setTextColor(context.getResources().getColor(R.color.alert_red));
        }*/

        return view
    }


}
