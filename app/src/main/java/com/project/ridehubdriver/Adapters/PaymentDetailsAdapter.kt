package com.project.ridehubdriver.Adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.project.ridehubdriver.PojoResponse.PaymentdetailsPojo
import com.project.ridehubdriver.R
import java.util.*

/**
 * Created by user14 on 9/22/2015.
 */
class PaymentDetailsAdapter(
    private val context: Activity,
    private val data: ArrayList<PaymentdetailsPojo>
) : BaseAdapter() {
    private val mInflater: LayoutInflater
    private val check: String? = null

    init {
        mInflater = LayoutInflater.from(context)
    }

    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getViewTypeCount(): Int {
        return 1
    }


    inner class ViewHolder {
        internal var payment: TextView? = null
        internal var amount: TextView? = null
        internal var receiveddate: TextView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view: View
        val holder: ViewHolder
        var data1 = " "
        if (convertView == null) {
            view = mInflater.inflate(R.layout.item_payment_summary, parent, false)
            holder = ViewHolder()
            holder.payment = view.findViewById(R.id.pmt_stmt_payment) as TextView
            holder.amount = view.findViewById(R.id.pmt_stmt_amount) as TextView
            holder.receiveddate = view.findViewById(R.id.pmt_stmt_received_date) as TextView

            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }

        data1 = data[position].getpay_duration_from() + " to " + data[position].getpay_duration_to()

        holder.payment!!.text = data1
        holder.amount!!.setText(data[position].getamount())
        holder.receiveddate!!.setText(data[position].getpay_date())

        return view
    }


}
