package com.project.ridehubdriver.Adapters

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.project.ridehubdriver.PojoResponse.CancelReasonPojo
import com.project.ridehubdriver.R
import java.util.*

/**
 * Created by user88 on 10/28/2015.
 */
class CancelReasonAdapter(
    private val context: Activity,
    private val data: ArrayList<CancelReasonPojo>
) : BaseAdapter() {
    private val mInflater: LayoutInflater
    private val check: String? = null

    init {
        mInflater = LayoutInflater.from(context)
    }


    override fun getCount(): Int {
        return data.size
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    inner class ViewHolder {
        internal var cancel_reason: TextView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val view: View
        val holder: ViewHolder
        if (convertView == null) {
            view = mInflater.inflate(R.layout.cancelreason_single, parent, false)
            holder = ViewHolder()
            holder.cancel_reason = view.findViewById(R.id.cancel_reasonTv) as TextView
            view.tag = holder
        } else {
            view = convertView
            holder = view.tag as ViewHolder
        }
        holder.cancel_reason!!.setText(data[position].reason)

        return view
    }
}
