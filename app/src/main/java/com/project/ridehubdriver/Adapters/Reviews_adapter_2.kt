package com.project.ridehubdriver.Adapters

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.project.ridehubdriver.PojoResponse.Reviwes_Pojo
import com.project.ridehubdriver.R
import com.project.ridehubdriver.customRatingBar.BaseRatingBar
import com.project.ridehubdriver.customRatingBar.RotationRatingBar
import java.util.*

class Reviews_adapter_2(context: Context, list: ArrayList<Reviwes_Pojo>) :
    RecyclerView.Adapter<Reviews_adapter_2.ViewHolder>() {

    private var mContext: Context? = null
    private var mList: ArrayList<Reviwes_Pojo> = ArrayList()
    private var setselected = 0
    var Srating = ""

    init {
        this.mContext = context
        this.mList = list
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_rating, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {

        return mList.size

    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.rating.setOnRatingChangeListener(object : BaseRatingBar.OnRatingChangeListener {
            override fun onRatingChange(
                ratingBar: BaseRatingBar,
                rating: Float,
                fromUser: Boolean
            ) {
                Srating = rating.toString()
                mList[position].ratings_count = Srating
            }
        })
       /* holder.rating.onRatingBarChangeListener =
            RatingBar.OnRatingBarChangeListener { ratingBar, rating, fromUser ->
                Srating = rating.toString()
                mList[position].ratings_count = Srating
            }*/
        holder.rating_text.text = mList[position].options_title

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var rating: RotationRatingBar
        var rating_text: TextView

        init {
            rating_text = view.findViewById(R.id.rating_page_tv)
            rating = view.findViewById(R.id.rating_page_behaviour_rating_bar)
        }
    }


}




