package com.project.ridehubdriver.customRatingBar

import android.os.Parcel
import android.os.Parcelable
import android.view.View

internal class SavedState : View.BaseSavedState {

    var rating: Float = 0.toFloat()

    /**
     * Constructor called from [BaseRatingBar.onSaveInstanceState]
     */
    constructor(superState: Parcelable) : super(superState)

    /**
     * Constructor called from [.CREATOR]
     */
    private constructor(`in`: Parcel) : super(`in`) {
        rating = `in`.readFloat()
    }

    override fun writeToParcel(out: Parcel, flags: Int) {
        super.writeToParcel(out, flags)
        out.writeFloat(rating)
    }

    /*companion object {

        val CREATOR: Parcelable.Creator<SavedState> = object : Parcelable.Creator<SavedState> {
            override fun createFromParcel(`in`: Parcel): SavedState {
                return SavedState(`in`)
            }

            override fun newArray(size: Int): Array<SavedState> {
                return arrayOfNulls(size)
            }
        }
    }*/

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SavedState> {
        override fun createFromParcel(parcel: Parcel): SavedState {
            return SavedState(parcel)
        }

        override fun newArray(size: Int): Array<SavedState?> {
            return arrayOfNulls(size)
        }
    }
}
