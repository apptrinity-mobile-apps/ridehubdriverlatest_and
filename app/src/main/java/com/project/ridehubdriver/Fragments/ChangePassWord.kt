package com.project.ridehubdriver.Fragments

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.*
import android.view.animation.AlphaAnimation
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.project.ridehubdriver.Activities.NavigationDrawer
import com.project.ridehubdriver.Activities.SignUpSignIn
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.ConnectionDetector
import com.project.ridehubdriver.Utils.PkDialog
import com.project.ridehubdriver.Utils.SessionManager
import org.json.JSONObject
import java.util.*

/**
 * Created by user88 on 12/21/2015.
 */
class ChangePassWord : Fragment() {

    private var Et_currrentpassword: EditText? = null
    private var Et_new_password: EditText? = null
    private var Et_new_confirm_password: EditText? = null
    private var dialog: Dialog? = null
    private var layout_done: LinearLayout? = null
    private var mRequest: ServiceRequest? = null

    private val postrequest: StringRequest? = null
    private var driver_id: String? = ""
    internal lateinit var session: SessionManager
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var drawer_layout: ImageView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        try {

            if (rootview != null) {
                val parent = rootview!!.parent as ViewGroup?
                parent?.removeView(rootview)
            }
            rootview = inflater.inflate(R.layout.activity_change_password, container, false)

        } catch (e: InflateException) {
            /* map is already there, just return view as it is */
        }

        init(rootview!!)

        drawer_layout!!.setOnClickListener {
            NavigationDrawer.openDrawer()
        }

        layout_done!!.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            layout_done!!.startAnimation(buttonClick)
            cd = ConnectionDetector(activity!!)
            isInternetPresent = cd!!.isConnectingToInternet
            if (Et_currrentpassword!!.length() == 0) {
                erroredit(
                    Et_currrentpassword!!,
                    resources.getString(R.string.changepassword_currentpwd_label)
                )
            } else if (Et_new_password!!.length() == 0) {
                erroredit(
                    Et_new_password!!,
                    resources.getString(R.string.changepassword_newpwd_label)
                )
            } else if (Et_new_confirm_password!!.length() == 0) {
                erroredit(
                    Et_new_confirm_password!!,
                    resources.getString(R.string.changepassword_confirmpwd_label)
                )
            } else if (Et_new_password!!.text.toString() != Et_new_confirm_password!!.text.toString()) {
                erroredit(
                    Et_new_confirm_password!!,
                    resources.getString(R.string.changepassword_matchpwd_label)
                )
            } else {
                if (isInternetPresent!!) {
                    changepassword_PostRequest(ServiceConstant.changepassword)
                    println("changepwd-----------" + ServiceConstant.changepassword)
                } else {
                    Alert(
                        resources.getString(R.string.alert_sorry_label_title),
                        resources.getString(R.string.alert_nointernet)
                    )
                }
            }
        }

        return rootview

    }

    private fun init(rootview: View) {

        session = SessionManager(activity!!)

        Et_currrentpassword =
            rootview.findViewById(R.id.loginpage_currentPwd_edittext_label)
        Et_new_password =
            rootview.findViewById(R.id.loginpage_new_Pwd_edittext_label)
        Et_new_confirm_password =
            rootview.findViewById(R.id.loginpage_confirm_new_Pwd_edittext_label)
        layout_done = rootview.findViewById(R.id.layout_changepassword_done)
        drawer_layout = rootview.findViewById(R.id.ham_home)

        val user = session.userDetails
        driver_id = user[SessionManager.KEY_DRIVERID]

    }

    private fun erroredit(editname: EditText, msg: String) {
        val shake = AnimationUtils.loadAnimation(activity, R.anim.shake)
        editname.startAnimation(shake)
        val fgcspan = ForegroundColorSpan(Color.parseColor("#CC0000"))
        val ssbuilder = SpannableStringBuilder(msg)
        ssbuilder.setSpan(fgcspan, 0, msg.length, 0)
        editname.error = ssbuilder
    }

    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(activity!!)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            resources.getString(R.string.alert_label_ok),
            View.OnClickListener {
                mDialog.dismiss()
                val test = Intent(activity, SignUpSignIn::class.java)
                startActivity(test)
            })
        mDialog.show()
    }

    //--------------------------code for post forgot password-----------------------
    private fun changepassword_PostRequest(Url: String) {
        dialog = Dialog(activity!!)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------password----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id!!
        jsonParams["password"] = Et_currrentpassword!!.text.toString()
        jsonParams["new_password"] = Et_new_password!!.text.toString()

        println("--------------driver_id-------------------" + driver_id!!)
        println("--------------password-------------------" + Et_currrentpassword!!.text.toString())
        println("--------------new_password-------------------" + Et_currrentpassword!!.text.toString())

        mRequest = ServiceRequest(activity!!)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    println("changepwdresponse---------$response")
                    var Str_status = ""
                    var Str_response = ""
                    try {
                        val `object` = JSONObject(response)
                        Str_status = `object`.getString("status")
                        Str_response = `object`.getString("response")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (Str_status.equals("1", ignoreCase = true)) {
                        Alert(
                            resources.getString(R.string.label_pushnotification_cashreceived),
                            Str_response
                        )
                    } else {
                        Alert(resources.getString(R.string.alert_sorry_label_title), Str_response)
                    }
                    dialog!!.dismiss()
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }
            })
    }

    companion object {
        private var rootview: View? = null
    }


}
