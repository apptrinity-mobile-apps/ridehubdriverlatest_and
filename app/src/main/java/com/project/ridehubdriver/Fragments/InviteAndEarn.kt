package com.project.ridehubdriver.Fragments

import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.Telephony
import android.view.*
import android.view.animation.AlphaAnimation
import android.widget.ImageView
import android.widget.TextView
import com.android.volley.Request
import com.project.ridehubdriver.Activities.NavigationDrawer
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.Hockeyapp.FragmentHockeyApp
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.ConnectionDetector
import com.project.ridehubdriver.Utils.CurrencySymbolConverter
import com.project.ridehubdriver.Utils.PkDialog
import com.project.ridehubdriver.Utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*


/**
 * Created by Prem Kumar and Anitha on 10/13/2015.
 */
class InviteAndEarn : FragmentHockeyApp(), View.OnClickListener {
    internal val PERMISSION_REQUEST_CODE = 111
    internal var sCurrencySymbol: String? = ""
    internal lateinit var ll_shareinvitecode: TextView
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    private var Tv_referral_code: TextView? = null
    private var mRequest: ServiceRequest? = null
    private var UserID: String? = ""
    private var isdataPresent = false
    private var Sstatus = ""
    private var friend_earn_amount = ""
    private var you_earn_amount = ""
    private var friends_rides = ""
    private var ScurrencyCode = ""
    private var referral_code = ""
    private var sShareLink = ""
    private val package_name = "com.project.ridehubdriver"
    private var drawer_layout: ImageView? = null
    private var parentView: View? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        try {
            if (parentView != null) {
                val parent = parentView!!.parent as ViewGroup
                parent.removeView(parentView)
            }
            parentView = inflater.inflate(R.layout.activity_free_rides, container, false)

        } catch (e: InflateException) {
        }

        initialize(parentView!!)
        drawer_layout!!.setOnClickListener {
            NavigationDrawer.openDrawer()
        }

        return parentView
    }

    private fun initialize(rootview: View) {
        session = SessionManager(activity!!)
        cd = ConnectionDetector(activity!!)
        isInternetPresent = cd!!.isConnectingToInternet
        drawer_layout = rootview.findViewById(R.id.ham_home) as ImageView
        Tv_referral_code =
            rootview.findViewById(R.id.invite_earn_referral_code_textview) as TextView
        ll_shareinvitecode = rootview.findViewById(R.id.ll_shareinvitecode) as TextView

        ll_shareinvitecode.setOnClickListener(this)

        ll_shareinvitecode.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            ll_shareinvitecode.startAnimation(buttonClick)
            val text =
                resources.getString(R.string.invite_earn_label_share_messgae1) + " " + sCurrencySymbol + "" + friend_earn_amount + " " + getResources().getString(
                    R.string.invite_earn_label_share_messgae2
                ) + " " + referral_code + " to ride free.Enjoy! " + "https://play.google.com/store/apps/details?id=" + package_name + "&hl=en"
            whatsApp_sendMsg(text)
        }

        // get user data from session
        val user = session!!.userDetails
        UserID = user[SessionManager.KEY_DRIVERID]

        if (isInternetPresent!!) {
            displayInvite_Request(ServiceConstant.inviteandEarn_Url)
        } else {
            Alert(
                getResources().getString(R.string.alert_label_title),
                getResources().getString(R.string.alert_nointernet)
            )
        }

    }

    override fun onClick(v: View) {
        if (isdataPresent) {
            val text =
                getResources().getString(R.string.invite_earn_label_share_messgae1) + " " + sCurrencySymbol + "" + friend_earn_amount + " " + getResources().getString(
                    R.string.invite_earn_label_share_messgae2
                ) + " " + referral_code + " to ride free.Enjoy!" + "https://play.google.com/store/apps/details?id=" + package_name + "&hl=en"

        } else {
            Alert(
                getResources().getString(R.string.alert_label_title),
                getResources().getString(R.string.invite_earn_label_problem_server)
            )
        }
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(activity!!)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(
            getResources().getString(R.string.action_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()

    }

    //--------Sending message on WhatsApp Method------
    private fun whatsApp_sendMsg(text: String) {
        val pm = getActivity()!!.getPackageManager()
        try {
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(Intent.EXTRA_TEXT, text)
            sendIntent.type = "text/plain"
            Intent.createChooser(sendIntent, "Share via")
            startActivity(sendIntent)
        } catch (e: Exception) {
            Alert(
                getResources().getString(R.string.alert_label_title),
                getResources().getString(R.string.invite_earn_label_whatsApp_not_installed)
            )
        }

    }

    //-----------------------Display Invite Amount Post Request-----------------
    private fun displayInvite_Request(Url: String) {
        val dialog = Dialog(activity!!)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.setText(this@InviteAndEarn.getResources().getString(R.string.action_pleasewait))

        println("-------------displayInvite_Request Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = UserID!!

        mRequest = ServiceRequest(activity!!)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("-------------displayInvite_Request Response----------------$response")

                    try {

                        val `object` = JSONObject(response)
                        Sstatus = `object`.getString("status")
                        if (`object`.length() > 0) {
                            val response_Object = `object`.getJSONObject("response")
                            if (response_Object.length() > 0) {
                                val detail_object = response_Object.getJSONObject("details")
                                if (detail_object.length() > 0) {
                                    friend_earn_amount =
                                        detail_object.getString("friends_earn_amount")
                                    you_earn_amount = detail_object.getString("your_earn_amount")
                                    friends_rides = detail_object.getString("your_earn")
                                    referral_code = detail_object.getString("referral_code")
                                    ScurrencyCode = detail_object.getString("currency")
                                    sShareLink = detail_object.getString("url")

                                    isdataPresent = true
                                } else {
                                    isdataPresent = false
                                }
                            } else {
                                isdataPresent = false
                            }
                        } else {
                            isdataPresent = false
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    if (isdataPresent) {
                        sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(ScurrencyCode)

                        val earn_text =
                            "<font color=#1d3b6f>" + getResources().getString(R.string.invite_earn_label_friends_earn) + "</font>" + "<font color=#ef5e27>" + " " + sCurrencySymbol + "" + you_earn_amount + "</font>"

                        val code_text =
                            "<font color=#1d3b6f>" + getResources().getString(R.string.invite_earn_label_friends_ride) + ", " + getResources().getString(
                                R.string.invite_earn_label_friend_ride
                            ) + "</font>" + "<font color=#ef5e27>" + sCurrencySymbol + "" + you_earn_amount + "</font>"
                        Tv_referral_code!!.text = referral_code
                    }
                    dialog.dismiss()
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                val text =
                    getResources().getString(R.string.invite_earn_label_share_messgae1) + " " + sCurrencySymbol + "" + friend_earn_amount + " " + getResources().getString(
                        R.string.invite_earn_label_share_messgae2
                    ) + " " + referral_code + " " + getResources().getString(R.string.invite_earn_label_share_messgae3)

                var defaultSmsPackageName: String? = null //Need to change the build to API 19
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(activity)
                }

                val sendIntent = Intent(Intent.ACTION_SEND)
                sendIntent.type = "text/plain"
                sendIntent.putExtra(Intent.EXTRA_TEXT, text)
                if (defaultSmsPackageName != null) {
                    sendIntent.setPackage(defaultSmsPackageName)
                }
                startActivity(sendIntent)
            } else {
            }
        }
    }


    //-----------------Move Back on pressed phone back button------------------
    fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            // nothin
            true
        } else false
    }

}