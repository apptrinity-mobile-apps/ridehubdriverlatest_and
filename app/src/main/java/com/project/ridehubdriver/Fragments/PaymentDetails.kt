package com.project.ridehubdriver.Fragments

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.project.ridehubdriver.Activities.NavigationDrawer
import com.project.ridehubdriver.Activities.PaymentDetailsList
import com.project.ridehubdriver.Adapters.PaymentDetailsAdapter
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.Hockeyapp.FragmentHockeyApp
import com.project.ridehubdriver.PojoResponse.PaymentdetailsPojo
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.ConnectionDetector
import com.project.ridehubdriver.Utils.PkDialog
import com.project.ridehubdriver.Utils.SessionManager
import org.json.JSONObject
import java.text.NumberFormat
import java.util.*

/**
 * Created by user14 on 9/22/2015.
 */
class PaymentDetails : FragmentHockeyApp() {

    private var parentView: View? = null
    internal lateinit var payment_list: ListView
    internal var postrequest: StringRequest? = null
    private var session: SessionManager? = null
    internal var driver_id: String? = ""
    internal var payid: String? = null
    private var paymentstatementList: ArrayList<PaymentdetailsPojo>? = null
    private var adapter: PaymentDetailsAdapter? = null
    private var dialog: Dialog? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private val actionBar: ActionBar? = null
    private var empty_Tv: TextView? = null
    private var show_progress_status = false
    private var drawer_layout: ImageView? = null

    private var mRequest: ServiceRequest? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        try{
            parentView = inflater.inflate(R.layout.activity_payment_details, container, false)
        }catch (e:Exception){

        }

        /*ActionBarActivity actionBarActivity = (ActionBarActivity) getActivity();
        actionBar = actionBarActivity.getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.hide();*/
        initialize(parentView!!)

        drawer_layout!!.setOnClickListener {
            NavigationDrawer.openDrawer()
        }

        /* parentView!!.findViewById<View>(R.id.ham_home).setOnClickListener {
             if (resideMenu != null) {
                 resideMenu.openMenu(ResideMenu.DIRECTION_LEFT)
             }
         }*/

        payment_list.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val intent = Intent(activity, PaymentDetailsList::class.java)
                intent.putExtra("Payid", paymentstatementList!![position].getpay_id())

                startActivity(intent)
                activity!!.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }
        setUpViews()
        return parentView
    }

    private fun setUpViews() {
        val parentActivity = activity as NavigationDrawer?
        //resideMenu = parentActivity!!.getResideMenu()
    }

    private fun initialize(rootview: View) {
        paymentstatementList = ArrayList()
        session = SessionManager(activity!!)
        payment_list = rootview.findViewById<View>(R.id.listView_paymentdetails) as ListView
        empty_Tv = rootview.findViewById<View>(R.id.payment_no_textview) as TextView
        drawer_layout = rootview.findViewById(R.id.ham_home) as ImageView

        payment_list.isVerticalScrollBarEnabled = false

        val user = session!!.userDetails
        driver_id = user[SessionManager.KEY_DRIVERID]


        cd = ConnectionDetector(activity!!)
        isInternetPresent = cd!!.isConnectingToInternet

        if (isInternetPresent!!) {
            PostRequest(ServiceConstant.paymentdetails_url)
            println("payment------------------" + ServiceConstant.paymentdetails_url)
        } else {
            Alert(
                resources.getString(R.string.alert_sorry_label_title),
                resources.getString(R.string.alert_nointernet)
            )
        }

    }

    //--------------Alert Method-----------
    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(activity!!)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            resources.getString(R.string.alert_label_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    //-----------------------Code for payment details post request-----------------
    private fun PostRequest(Url: String) {
        dialog = Dialog(activity!!)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------paymentdetails----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id!!
        println("--------------driver_id-------------------" + driver_id!!)

        mRequest = ServiceRequest(activity!!)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {

                override fun onCompleteListener(response: String) {
                    Log.e("paymrnt", response)

                    var status = ""
                    var Str_currency_code = ""
                    var Str_response = ""
                    try {
                        val `object` = JSONObject(response)
                        status = `object`.getString("status")

                        if (status.equals("1", ignoreCase = true)) {

                            val jsonObject = `object`.getJSONObject("response")

                            Str_currency_code = jsonObject.getString("currency")

                            println("currency--------------$Str_currency_code")

                            val currencycode = Currency.getInstance(getLocale(Str_currency_code))

                            val jarry = jsonObject.getJSONArray("payments")

                            if (jarry.length() > 0) {

                                for (i in 0 until jarry.length()) {

                                    val jobject = jarry.getJSONObject(i)

                                    val item = PaymentdetailsPojo()

                                    item.setamount(currencycode.symbol + jobject.getString("amount"))
                                    item.setpay_date(jobject.getString("pay_date"))
                                    item.setpay_duration_from(jobject.getString("pay_duration_from"))
                                    item.setpay_duration_to(jobject.getString("pay_duration_to"))
                                    item.setpay_id(jobject.getString("pay_id"))

                                    println("pay_id--------" + jobject.getString("pay_id"))

                                    paymentstatementList!!.add(item)
                                }
                                show_progress_status = true

                            } else {
                                show_progress_status = false
                            }

                        } else {
                            Str_response = `object`.getString("response")
                        }

                    } catch (e: Exception) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    if (status.equals("1", ignoreCase = true)) {
                        adapter = PaymentDetailsAdapter(activity!!, paymentstatementList!!)
                        payment_list.adapter = adapter
                        dialog!!.dismiss()

                        if (show_progress_status) {
                            empty_Tv!!.visibility = View.GONE
                        } else {
                            empty_Tv!!.visibility = View.VISIBLE
                            payment_list.emptyView = empty_Tv
                        }

                    } else {

                        Alert(resources.getString(R.string.alert_sorry_label_title), Str_response)
                    }
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }

            })
    }

    //method to convert currency code to currency symbol
    private fun getLocale(strCode: String): Locale? {

        for (locale in NumberFormat.getAvailableLocales()) {
            val code = NumberFormat.getCurrencyInstance(locale).currency!!.currencyCode
            if (strCode == code) {
                return locale
            }
        }
        return null
    }


}


