package com.project.ridehubdriver.Fragments

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.*
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.project.ridehubdriver.Activities.NavigationDrawer
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.Hockeyapp.FragmentHockeyApp
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.ConnectionDetector
import com.project.ridehubdriver.Utils.PkDialog
import com.project.ridehubdriver.Utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 */
class BankDetails : FragmentHockeyApp() {
    private val postrequest: StringRequest? = null
    private var session: SessionManager? = null
    private var driver_id: String? = ""
    private val parentView: View? = null
    private var holder_name: EditText? = null
    private var holder_address: EditText? = null
    private var account_no: EditText? = null
    private var bankname: EditText? = null
    private var branchname: EditText? = null
    private var branchaddress: EditText? = null
    private var ifsccode: EditText? = null
    private var routingno: EditText? = null
  //  private var save_btn: Button? = null
    internal var context: Context? = null
    internal lateinit var dialog: Dialog
    private var drawer_layout: ImageView? = null
    private var mRequest: ServiceRequest? = null

    private var actionBar: ActionBar? = null
    private val mCustomView: View? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private val colorDrawable = ColorDrawable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        try {
            if (rootview != null) {
                val parent = rootview!!.parent as ViewGroup?
                parent?.removeView(rootview)
            }
            rootview = inflater.inflate(R.layout.bank_account, container, false)


        } catch (e: InflateException) {
            /* map is already there, just return view as it is */
        }

        /*val actionBarActivity = getActivity() as ActionBarActivity
        actionBar = actionBarActivity.getSupportActionBar()
        actionBar!!.setDisplayShowTitleEnabled(false)
        actionBar!!.setDisplayShowTitleEnabled(true)
        actionBar!!.setHomeButtonEnabled(true)
        actionBar!!.hide()*/
        initialize(rootview!!)

        /*rootview!!.findViewById<View>(R.id.ham_home).setOnClickListener {
            if (resideMenu != null) {
                resideMenu!!.openMenu(ResideMenu.DIRECTION_LEFT)
            }
        }*/

        drawer_layout!!.setOnClickListener {
            NavigationDrawer.openDrawer()
        }

        /*save_btn!!.setOnClickListener {
            if (holder_name!!.length() == 0) {
                erroredit(
                    holder_name!!,
                    getResources().getString(R.string.action_alert_bank_Username)
                )
            } else if (holder_address!!.length() == 0) {
                erroredit(
                    holder_address!!,
                    getResources().getString(R.string.action_alert_bank_address)
                )
            } else if (account_no!!.length() == 0) {
                erroredit(
                    account_no!!,
                    getResources().getString(R.string.action_alert_bank_accountno)
                )
            } else if (bankname!!.length() == 0) {
                erroredit(bankname!!, getResources().getString(R.string.action_alert_bank_name))
            } else if (branchname!!.length() == 0) {
                erroredit(branchname!!, getResources().getString(R.string.action_alert_branch_name))
            } else if (branchaddress!!.length() == 0) {
                erroredit(
                    branchaddress!!,
                    getResources().getString(R.string.action_alert_branch_address)
                )
            } else if (ifsccode!!.length() == 0) {
                erroredit(ifsccode!!, getResources().getString(R.string.action_alert_bank_ifs_code))
            } else if (routingno!!.length() == 0) {
                erroredit(
                    routingno!!,
                    getResources().getString(R.string.action_alert_bank_routingno)
                )
            } else {
                cd = ConnectionDetector(activity!!)
                isInternetPresent = cd!!.isConnectingToInternet
                if (isInternetPresent!!) {
                    postRequest_savebank(ServiceConstant.saveBankDetails)
                    System.out.println("bank------------------" + ServiceConstant.saveBankDetails)
                } else {
                    Alert(
                        getResources().getString(R.string.alert_sorry_label_title),
                        getResources().getString(R.string.alert_nointernet)
                    )
                }
            }
        }*/

        setUpViews()
        return rootview
    }

    private fun setUpViews() {
        val parentActivity = getActivity() as NavigationDrawer
       // resideMenu = parentActivity.getResideMenu()
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(activity!!)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            getResources().getString(R.string.alert_label_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    private fun initialize(rootview: View) {
        session = SessionManager(activity!!)

        holder_name = rootview.findViewById(R.id.bank_ac_holder_name)
        holder_address = rootview.findViewById(R.id.bank_ac_holder_address)
        account_no = rootview.findViewById(R.id.bank_ac_account_number)
        bankname = rootview.findViewById(R.id.bank_ac_bank_name)
        branchname = rootview.findViewById(R.id.bank_ac_branch_name)
        branchaddress = rootview.findViewById(R.id.bank_ac_branch_address)
        ifsccode = rootview.findViewById(R.id.bank_ac_ifsc_code)
        routingno = rootview.findViewById(R.id.bank_ac_routing_number)
//        save_btn = rootview.findViewById(R.id.bank_ac_save_button) as Button
        drawer_layout = rootview.findViewById(R.id.ham_home) as ImageView

        val user = session!!.userDetails
        driver_id = user[SessionManager.KEY_DRIVERID]

        cd = ConnectionDetector(activity!!)
        isInternetPresent = cd!!.isConnectingToInternet

        if (isInternetPresent!!) {
            postRequest_getbank(ServiceConstant.getBankDetails)
            System.out.println("bankget------------------" + ServiceConstant.getBankDetails)
        } else {
            Alert(
                getResources().getString(R.string.alert_sorry_label_title),
                getResources().getString(R.string.alert_nointernet)
            )
        }

    }

    private fun erroredit(editname: TextView, msg: String) {
        val shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake)
        editname.startAnimation(shake)
        val fgcspan = ForegroundColorSpan(Color.parseColor("#CC0000"))
        val ssbuilder = SpannableStringBuilder(msg)
        ssbuilder.setSpan(fgcspan, 0, msg.length, 0)
        editname.error = ssbuilder
    }

    //-----------------------Post Request-----------------
    private fun postRequest_savebank(Url: String) {
        dialog = Dialog(activity!!)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_loading))

        println("-------------savebank----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id!!
        jsonParams["acc_holder_name"] = holder_name!!.text.toString()
        jsonParams["acc_holder_address"] = holder_address!!.text.toString()
        jsonParams["acc_number"] = account_no!!.text.toString()
        jsonParams["bank_name"] = bankname!!.text.toString()
        jsonParams["branch_name"] = branchname!!.text.toString()
        jsonParams["branch_address"] = branchaddress!!.text.toString()
        jsonParams["swift_code"] = ifsccode!!.text.toString()
        jsonParams["routing_number"] = routingno!!.text.toString()

        println("driver_id----------" + driver_id!!)

        println("acc_holder_name----------" + holder_name!!.text.toString())

        println("acc_holder_address----------" + holder_address!!.text.toString())

        println("acc_number----------" + account_no!!.text.toString())

        println("bank_name----------" + bankname!!.text.toString())

        println("branch_address----------" + branchaddress!!.text.toString())

        println("swift_code----------" + ifsccode!!.text.toString())

        println("routing_number----------" + routingno!!.text.toString())
        mRequest = ServiceRequest(activity!!)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {

                override fun onCompleteListener(response: String) {

                    var Strstatus = ""
                    var Sresponse = ""
                    var Str_accountholder_name = ""
                    var Str_accountholder_address = ""
                    var Str_account_number = ""
                    var Str_bank_name = ""
                    var Str_branch_name = ""
                    var Str_branch_address = ""
                    var Str_swift_code = ""
                    var Str_routing_number = ""

                    println("bank-----------------$response")

                    try {
                        val `object` = JSONObject(response)
                        Strstatus = `object`.getString("status")

                        if (Strstatus.equals("1", ignoreCase = true)) {

                            val jsonObject = `object`.getJSONObject("response")
                            val jobjct = jsonObject.getJSONObject("banking")

                            Str_accountholder_name = jobjct.getString("acc_holder_name")
                            Str_accountholder_address = jobjct.getString("acc_holder_address")
                            Str_account_number = jobjct.getString("acc_number")
                            Str_bank_name = jobjct.getString("bank_name")
                            Str_branch_name = jobjct.getString("branch_name")
                            Str_branch_address = jobjct.getString("branch_address")
                            Str_swift_code = jobjct.getString("swift_code")
                            Str_routing_number = jobjct.getString("routing_number")
                        } else {
                            Sresponse = `object`.getString("response")
                        }
                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    if (Strstatus.equals("1", ignoreCase = true)) {
                        Toast.makeText(
                            getActivity(),
                            getResources().getString(R.string.alertsaved_label_title),
                            Toast.LENGTH_LONG
                        ).show()
                        holder_name!!.setText(Str_accountholder_name)
                        holder_address!!.setText(Str_accountholder_address)
                        account_no!!.setText(Str_account_number)
                        bankname!!.setText(Str_bank_name)
                        branchname!!.setText(Str_branch_name)
                        ifsccode!!.setText(Str_swift_code)
                        routingno!!.setText(Str_routing_number)

                    } else {
                        Alert(getResources().getString(R.string.alert_sorry_label_title), Sresponse)
                    }
                    dialog.dismiss()
                }

                override fun onErrorListener() {

                    dialog.dismiss()

                }

            })


    }


    //-----------------------Post Request-----------------
    private fun postRequest_getbank(Url: String) {
        dialog = Dialog(activity!!)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_loading))

        println("-------------dashboard----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id!!
        println("driver_id----------" + driver_id!!)

        mRequest = ServiceRequest(activity!!)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener{

                override fun onCompleteListener(response: String) {
                    var Strstatus = ""
                    var Smessage = ""
                    var Str_accountholder_name = ""
                    var Str_accountholder_address = ""
                    var Str_account_number = ""
                    var Str_bank_name = ""
                    var Str_branch_name = ""
                    var Str_branch_address = ""
                    var Str_swift_code = ""
                    var Str_routing_number = ""
                    println("bank-----------------$response")

                    try {
                        val `object` = JSONObject(response)
                        Strstatus = `object`.getString("status")

                        if (Strstatus.equals("1", ignoreCase = true)) {
                            val jsonObject = `object`.getJSONObject("response")
                            val jobjct = jsonObject.getJSONObject("banking")

                            Str_accountholder_name = jobjct.getString("acc_holder_name")
                            Str_accountholder_address = jobjct.getString("acc_holder_address")
                            Str_account_number = jobjct.getString("acc_number")
                            Str_bank_name = jobjct.getString("bank_name")
                            Str_branch_name = jobjct.getString("branch_name")
                            Str_branch_address = jobjct.getString("branch_address")
                            Str_swift_code = jobjct.getString("swift_code")
                            Str_routing_number = jobjct.getString("routing_number")

                        } else {
                            Smessage = `object`.getString("response")
                        }

                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                        dialog.dismiss()
                    }

                    if (Strstatus.equals("1", ignoreCase = true)) {
                        holder_name!!.setText(Str_accountholder_name)
                        holder_address!!.setText(Str_accountholder_address)
                        account_no!!.setText(Str_account_number)
                        bankname!!.setText(Str_bank_name)
                        branchname!!.setText(Str_branch_name)
                        ifsccode!!.setText(Str_swift_code)
                        routingno!!.setText(Str_routing_number)
                        branchaddress!!.setText(Str_branch_address)

                    } else {
                        Alert(getResources().getString(R.string.alert_sorry_label_title), Smessage)
                    }

                    dialog.dismiss()


                }

                override fun onErrorListener() {

                    dialog.dismiss()

                }

            })
    }

    companion object {
        private var rootview: View? = null
    }


}
