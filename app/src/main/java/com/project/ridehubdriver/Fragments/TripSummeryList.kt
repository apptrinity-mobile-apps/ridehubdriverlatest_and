package com.project.ridehubdriver.Fragments

import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.core.content.ContextCompat
import com.android.volley.Request
import com.project.ridehubdriver.Activities.NavigationDrawer
import com.project.ridehubdriver.Activities.TripSummaryDetail
import com.project.ridehubdriver.Adapters.TripSummeryAdapter
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.Hockeyapp.FragmentHockeyApp
import com.project.ridehubdriver.PojoResponse.TripSummaryPojo
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.ConnectionDetector
import com.project.ridehubdriver.Utils.PkDialog
import com.project.ridehubdriver.Utils.SessionManager
import org.json.JSONObject
import java.util.*

/**
 * Created by user88 on 10/23/2015.
 */
class TripSummeryList : FragmentHockeyApp() {
    internal var context: Context? = null
    private var session: SessionManager? = null
    private var driver_id: String? = ""
    private val check: String? = null
    private var all_layout: LinearLayout? = null
    private var onride_layout: LinearLayout? = null
    private var complete_layout: LinearLayout? = null
    private var cancelled_layout: LinearLayout? = null
    private var Tv_all: TextView? = null
    private var Tv_onride: TextView? = null
    private var Tv_complete: TextView? = null
    private var Tv_cancelled: TextView? = null
    private var listview: ListView? = null
    private var empty_Tv: TextView? = null
    internal lateinit var dialog: Dialog
    private var tripsummaryListall: ArrayList<TripSummaryPojo>? = null
    private var tripsummaryListonride: ArrayList<TripSummaryPojo>? = null
    private var tripsummaryListcompleted: ArrayList<TripSummaryPojo>? = null
    private var tripsummaryListcancelled: ArrayList<TripSummaryPojo>? = null
    private var adapter: TripSummeryAdapter? = null
    private var no_trip_summary: TextView? = null

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var show_progress_status = false

    private var mRequest: ServiceRequest? = null
    private var drawer_layout: ImageView? = null

    internal lateinit var receiver: BroadcastReceiver
    private var StabSelectedCheck = "All"

    lateinit var fade_in_anim: Animation

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        try {
            if (rootview != null) {
                val parent = rootview!!.parent as ViewGroup?
                parent?.removeView(rootview)
            }
            rootview = inflater.inflate(R.layout.activity_trip_summary, container, false)
        } catch (e: InflateException) {
            Log.d("InflateException", e.toString())
        }

        /*val actionBarActivity = activity as ActionBarActivity?
        actionBar = actionBarActivity!!.getSupportActionBar()
        actionBar!!.setDisplayShowTitleEnabled(false)
        actionBar!!.setDisplayShowTitleEnabled(true)
        actionBar!!.setHomeButtonEnabled(true)
        actionBar!!.hide()*/
        context = activity
        initialize(rootview!!)

        //Code for broadcat receive
        val filter = IntentFilter()
        filter.addAction("com.finish.tripsummerylist")

        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.action == "com.finish.tripsummerylist") {
                    activity!!.finish()
                }
            }
        }
        activity!!.registerReceiver(receiver, filter)

        drawer_layout!!.setOnClickListener {
            NavigationDrawer.openDrawer()
        }

        /*rootview!!.findViewById<View>(R.id.ham_home).setOnClickListener {
            if (resideMenu != null) {
                resideMenu!!.openMenu(ResideMenu.DIRECTION_LEFT)
            }
        }*/
        listview!!.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                if (StabSelectedCheck.equals("All", ignoreCase = true)) {
                    val intent = Intent(activity, TripSummaryDetail::class.java)
                    intent.putExtra("ride_id", tripsummaryListall!![position].getride_id())
                    startActivity(intent)
                    activity!!.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                } else if (StabSelectedCheck.equals("Onride", ignoreCase = true)) {
                    val intent = Intent(activity, TripSummaryDetail::class.java)
                    intent.putExtra("ride_id", tripsummaryListonride!![position].getride_id())
                    startActivity(intent)
                    activity!!.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                } else if (StabSelectedCheck.equals("Completed", ignoreCase = true)) {
                    val intent = Intent(activity, TripSummaryDetail::class.java)
                    intent.putExtra("ride_id", tripsummaryListcompleted!![position].getride_id())
                    startActivity(intent)
                    activity!!.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                } else {
                    val intent = Intent(activity, TripSummaryDetail::class.java)
                    intent.putExtra("ride_id", tripsummaryListcancelled!![position].getride_id())
                    startActivity(intent)
                    activity!!.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                }

            }

        all_layout!!.setOnClickListener {
            StabSelectedCheck = "All"
            listview!!.setSelectionAfterHeaderView()           // scroll to top of list
            all_layout!!.startAnimation(fade_in_anim)
            all_layout!!.background =
                ContextCompat.getDrawable(activity!!, R.drawable.button_blue_selected_bg)
            onride_layout!!.background = null
            complete_layout!!.background = null
            cancelled_layout!!.background = null
            Tv_all!!.setTextColor(ContextCompat.getColor(activity!!, R.color.white))
            Tv_onride!!.setTextColor(ContextCompat.getColor(activity!!, R.color.title_header))
            Tv_complete!!.setTextColor(ContextCompat.getColor(activity!!, R.color.title_header))
            Tv_cancelled!!.setTextColor(ContextCompat.getColor(activity!!, R.color.title_header))

            adapter = TripSummeryAdapter(activity!!, tripsummaryListall!!)
            listview!!.adapter = adapter
            adapter!!.notifyDataSetChanged()
            if (tripsummaryListall!!.size > 0) {
                empty_Tv!!.visibility = View.GONE
            } else {
                empty_Tv!!.visibility = View.VISIBLE
                listview!!.emptyView = empty_Tv
            }
        }

        onride_layout!!.setOnClickListener {
            StabSelectedCheck = "Onride"
            listview!!.setSelectionAfterHeaderView()
            onride_layout!!.startAnimation(fade_in_anim)
            all_layout!!.background = null
            onride_layout!!.background =
                ContextCompat.getDrawable(activity!!, R.drawable.button_blue_selected_bg)
            complete_layout!!.background = null
            cancelled_layout!!.background = null
            Tv_all!!.setTextColor(ContextCompat.getColor(activity!!, R.color.title_header))
            Tv_onride!!.setTextColor(ContextCompat.getColor(activity!!, R.color.white))
            Tv_complete!!.setTextColor(ContextCompat.getColor(activity!!, R.color.title_header))
            Tv_cancelled!!.setTextColor(ContextCompat.getColor(activity!!, R.color.title_header))

            adapter = TripSummeryAdapter(activity!!, tripsummaryListonride!!)
            listview!!.adapter = adapter
            adapter!!.notifyDataSetChanged()
            if (tripsummaryListonride!!.size > 0) {
                empty_Tv!!.visibility = View.GONE
            } else {
                empty_Tv!!.visibility = View.VISIBLE
                listview!!.emptyView = empty_Tv
            }
        }
        complete_layout!!.setOnClickListener {
            StabSelectedCheck = "Completed"
            listview!!.setSelectionAfterHeaderView()
            complete_layout!!.startAnimation(fade_in_anim)
            all_layout!!.background = null
            onride_layout!!.background = null
            complete_layout!!.background =
                ContextCompat.getDrawable(activity!!, R.drawable.button_blue_selected_bg)
            cancelled_layout!!.background = null
            Tv_all!!.setTextColor(ContextCompat.getColor(activity!!, R.color.title_header))
            Tv_onride!!.setTextColor(ContextCompat.getColor(activity!!, R.color.title_header))
            Tv_complete!!.setTextColor(ContextCompat.getColor(activity!!, R.color.white))
            Tv_cancelled!!.setTextColor(ContextCompat.getColor(activity!!, R.color.title_header))

            adapter = TripSummeryAdapter(activity!!, tripsummaryListcompleted!!)
            listview!!.adapter = adapter
            adapter!!.notifyDataSetChanged()
            if (tripsummaryListcompleted!!.size > 0) {
                empty_Tv!!.visibility = View.GONE
            } else {
                empty_Tv!!.visibility = View.VISIBLE
                listview!!.emptyView = empty_Tv
            }
        }
        cancelled_layout!!.setOnClickListener {
            StabSelectedCheck = "Cancelled"
            listview!!.setSelectionAfterHeaderView()
            cancelled_layout!!.startAnimation(fade_in_anim)
            all_layout!!.background = null
            onride_layout!!.background = null
            complete_layout!!.background = null
            cancelled_layout!!.background =
                ContextCompat.getDrawable(activity!!, R.drawable.button_blue_selected_bg)
            Tv_all!!.setTextColor(ContextCompat.getColor(activity!!, R.color.title_header))
            Tv_onride!!.setTextColor(ContextCompat.getColor(activity!!, R.color.title_header))
            Tv_complete!!.setTextColor(ContextCompat.getColor(activity!!, R.color.title_header))
            Tv_cancelled!!.setTextColor(ContextCompat.getColor(activity!!, R.color.white))

            adapter = TripSummeryAdapter(activity!!, tripsummaryListcancelled!!)
            listview!!.adapter = adapter
            adapter!!.notifyDataSetChanged()
            if (tripsummaryListcancelled!!.size > 0) {
                empty_Tv!!.visibility = View.GONE
            } else {
                empty_Tv!!.visibility = View.VISIBLE
                listview!!.emptyView = empty_Tv
            }
        }
        return rootview
    }

    private fun initialize(rootview: View) {
        tripsummaryListall = ArrayList()
        tripsummaryListonride = ArrayList()
        tripsummaryListcompleted = ArrayList()
        tripsummaryListcancelled = ArrayList()
        session = SessionManager(activity!!)
        all_layout = rootview.findViewById(R.id.trip_summary_all_layout) as LinearLayout
        onride_layout = rootview.findViewById(R.id.trip_summary_onride_layout) as LinearLayout
        complete_layout = rootview.findViewById(R.id.trip_summary_completed_layout) as LinearLayout
        cancelled_layout = rootview.findViewById(R.id.trip_summary_cancelled_layout) as LinearLayout
        listview = rootview.findViewById(R.id.trip_summary_listview) as ListView
        no_trip_summary = rootview.findViewById(R.id.no_trip_summary) as TextView
        Tv_all = rootview.findViewById(R.id.trip_summary_all) as TextView
        Tv_onride = rootview.findViewById(R.id.trip_summary_onride) as TextView
        Tv_complete = rootview.findViewById(R.id.trip_summary_completed_button) as TextView
        Tv_cancelled = rootview.findViewById(R.id.trip_summary_cancelled_button) as TextView
        drawer_layout = rootview.findViewById(R.id.ham_home) as ImageView
        empty_Tv = rootview.findViewById(R.id.no_trip_summary) as TextView
        val user = session!!.userDetails
        driver_id = user[SessionManager.KEY_DRIVERID]
        //all_layout.setBackgroundColor(getResources().getColor(R.color.blue_ridehub));
        // Tv_all!!.setTextColor(resources.getColor(R.color.blue_ridehub))
        cd = ConnectionDetector(activity!!)
        isInternetPresent = cd!!.isConnectingToInternet
        if (isInternetPresent!!) {
            PostRequest(ServiceConstant.tripsummery_list_url)
            println("triplists------------------" + ServiceConstant.tripsummery_list_url)
        } else {
            Alert(
                resources.getString(R.string.alert_sorry_label_title),
                resources.getString(R.string.alert_nointernet)
            )
        }
        fade_in_anim = AnimationUtils.loadAnimation(context, R.anim.fade_in)
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(activity!!)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            resources.getString(R.string.alert_label_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }

    //-----------------------Code for my rides post request-----------------

    private fun PostRequest(Url: String) {
        dialog = Dialog(activity!!)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------triplist----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id!!
        jsonParams["trip_type"] = "all"

        mRequest = ServiceRequest(activity!!)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    println("--------------reponse-------------------$response")
                    Log.e("trip", response)
                    var status = ""
                    var total_rides = ""
                    val type_group = ""
                    var Str_response = ""

                    try {
                        val `object` = JSONObject(response)
                        status = `object`.getString("status")

                        println("triplist--status----$status")

                        if (status.equals("1", ignoreCase = true)) {

                            val jsonObject = `object`.getJSONObject("response")

                            total_rides = jsonObject.getString("total_rides")
                            val jarry = jsonObject.getJSONArray("rides")

                            if (jarry.length() > 0) {

                                for (i in 0 until jarry.length()) {
                                    val jobjct = jarry.getJSONObject(i)
                                    val items = TripSummaryPojo()
                                    items.setpickup(jobjct.getString("pickup"))
                                    items.setdrop(jobjct.getString("drop"))
                                    items.setdatetime(jobjct.getString("datetime"))
                                    items.setride_id(jobjct.getString("ride_id"))
                                    tripsummaryListall!!.add(items)

                                    if (jobjct.getString("group").equals(
                                            "completed",
                                            ignoreCase = true
                                        )
                                    ) {
                                        tripsummaryListcompleted!!.add(items)
                                    } else if (jobjct.getString("group").equals(
                                            "onride",
                                            ignoreCase = true
                                        )
                                    ) {
                                        tripsummaryListonride!!.add(items)
                                    } else if (jobjct.getString("group").equals(
                                            "cancelled",
                                            ignoreCase = true
                                        )
                                    ) {
                                        tripsummaryListcancelled!!.add(items)
                                        println("triplist----------Cancleed---" + jobjct.getString("group"))
                                    }
                                }
                                show_progress_status = true
                            } else {
                                show_progress_status = false
                            }
                        } else {
                            Str_response = `object`.getString("response")
                            println("triplist----------response---$Str_response")
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    dialog.dismiss()

                    if (status.equals("1", ignoreCase = true)) {
                        adapter = TripSummeryAdapter(activity!!, tripsummaryListall!!)
                        listview!!.adapter = adapter

                        if (show_progress_status) {
                            empty_Tv!!.visibility = View.GONE
                        } else {
                            empty_Tv!!.visibility = View.VISIBLE
                            listview!!.emptyView = empty_Tv
                        }
                    } else {
                        Alert(resources.getString(R.string.alert_sorry_label_title), Str_response)
                    }
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }

            })

    }

    companion object {
        private var rootview: View? = null
    }

}
