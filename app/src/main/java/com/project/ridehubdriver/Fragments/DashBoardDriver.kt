package com.project.ridehubdriver.Fragments

import android.app.Dialog
import android.content.Intent
import android.content.IntentSender
import android.content.res.Resources
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.animation.AlphaAnimation
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.project.ridehubdriver.Activities.DriverMapActivity
import com.project.ridehubdriver.Activities.NavigationDrawer
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceManager
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.Helper.xmpp.ChatingService
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.*
import com.project.ridehubdriver.customRatingBar.RotationRatingBar
import com.squareup.picasso.Picasso
import org.json.JSONObject
import java.text.NumberFormat
import java.util.*

/**
 */
class DashBoardDriver : Fragment(), View.OnClickListener,
    com.google.android.gms.location.LocationListener, GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback {
    private var session: SessionManager? = null
    private var user_img: ImageView? = null
    private var driver_img: String? = ""
    private var driver_name: String? = ""
    private var vehicle_name: String? = ""
    private var vehicle_no: String? = ""
    private var dialog: Dialog? = null
    private val postrequest: StringRequest? = null
    private var driver_id: String? = ""
    private var chauffl_id: String? = ""
    private var mRequest: ServiceRequest? = null
    private var Str_currencglobal = ""
    private var currency_code: Currency? = null
    private var sCurrencySymbol = ""

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var Bt_Go_Online: LinearLayout? = null
    private val Emty_Text: TextView? = null

    private var actionBar: ActionBar? = null

    private var isLastTripAvailable = false
    private var isTodayEarningsAvailable = false
    private var isTodayTipsAvailable = false

    private val currencycode1: Currency? = null
    private val currencycode2: Currency? = null
    private val currencycode: Currency? = null

    internal var gps: GPSTracker? = null
    private var mMap: GoogleMap? = null
    private var MyCurrent_lat = 0.0
    private var MyCurrent_long = 0.0
    private val alert_layout: RelativeLayout? = null
    private val alert_textview: TextView? = null
    internal lateinit var markerOptions: MarkerOptions

    private val show_progress_status = false
    private val Str_currency_code = ""

    private var Tv_driver_name: TextView? = null
    private var Tv_Driver_Vechile_no: TextView? = null
    private var Tv_Driver_vechile_model: TextView? = null
    private var Tv_car_category: TextView? = null
    private var Tv_driver_chauffl_no: TextView? = null

    private var Tv_lasttrip_ridetime: TextView? = null
    private var drawer_layout: ImageView? = null
    private var Tv_lasttrip_ridedate: TextView? = null
    private var Tv_lasttrip_earnings: TextView? = null
    private var Tv_today_earnings_onlinehours: TextView? = null
    private var Tv_todayearnigs_trips: TextView? = null
    private var Tv_todayearnings_earnings: TextView? = null
    private var Tv_todaytips_trips: TextView? = null
    private var Tv_todaytips_tips: TextView? = null
    private val Im_driver_img: RoundedImageView? = null
    private var strlat: Double = 0.toDouble()
    private var strlon: Double = 0.toDouble()
    private var driver_rating: RotationRatingBar? = null

    internal lateinit var mLocationRequest: LocationRequest
    internal var mGoogleApiClient: GoogleApiClient? = null
    internal lateinit var result: PendingResult<LocationSettingsResult>
    // internal lateinit var tv_driver_rating: TextView


    internal lateinit var windowManager: WindowManager
    internal lateinit var imageView: ImageView
    lateinit var update_dialog: Dialog

    private var parentView: View? = null

    private val updateAvailablityServiceListener = object : ServiceManager.ServiceListener {
        override fun onCompleteListener(`object`: Any) {
            try {
                dismissDialog()
                val response = `object` as String

                val object1 = JSONObject(response)
                if (object1.length() > 0) {
                    val status = object1.getString("status")
                    println("DRIVER STATUSDASHBOARD-------$status")
                    val response = object1.getString("response")
                    if (status.equals("1", ignoreCase = true)) {
                        isOnline = true
                        val i = Intent(activity, DriverMapActivity::class.java)
                        startActivity(i)
                    } else {
                        //
                        showLocationSearchDialog(response)
                    }

                    val login_id = object1.getString("login_id")
                    val login_time = object1.getString("login_time")
                    Log.e("loginTime", "$login_id==$login_time")
                    if (login_id.equals("", ignoreCase = true) && login_time.equals(
                            "",
                            ignoreCase = true
                        )
                    ) {

                    } else {
                        session!!.createSessionlogin_details(login_id, login_time)
                    }
                    println("login_id-------$login_id")
                    println("onlineresponse-------$login_time")
                }


                println("onlineresponse-------$response")


            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        override fun onErrorListener(obj: Any) {
            dismissDialog()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        try {
            if (parentView != null) {
                val parent = parentView!!.parent as ViewGroup?
                parent?.removeView(parentView)
            }

            parentView = inflater.inflate(R.layout.driver_dash_board_ride_hub, container, false)

        } catch (e: InflateException) {
        }

        try {
            val home = activity!!.resources.getString(R.string.home)
            activity!!.title = "" + home
        } catch (e: Exception) {
        }

        setUpViews()
        initialize(parentView!!)
        initilizeMap()

        drawer_layout!!.setOnClickListener {
            NavigationDrawer.openDrawer()
        }

        val buttonClick = AlphaAnimation(1F, 0.8f)
        Bt_Go_Online!!.setOnClickListener {
            Bt_Go_Online!!.startAnimation(buttonClick)
            gps = GPSTracker(activity!!)
            if (gps!!.isgpsenabled() && gps!!.canGetLocation()) {
                cd = ConnectionDetector(activity!!)
                isInternetPresent = cd!!.isConnectingToInternet
                if (isInternetPresent!!) {
                    session!!.createSessionOnline("1")
                    session!!.createSessionOnline("1")
                    ChatingService.startDriverAction(activity!!)
                    showDialog(resources.getString(R.string.action_loading))
                    val jsonParams = HashMap<String, String>()
                    val userDetails = session!!.userDetails
                    val onlinedetails = session!!.onlineDetails
                    val driverId = userDetails["driverid"]
                    jsonParams["driver_id"] = "" + driverId!!
                    jsonParams["availability"] = "" + "Yes"
                    println("availability-----" + "Yes")
                    println("driver_id-----$driverId")
                    val manager = ServiceManager(activity!!, updateAvailablityServiceListener)
                    manager.makeServiceRequest(
                        ServiceConstant.UPDATE_AVAILABILITY,
                        Request.Method.POST,
                        jsonParams
                    )
                    println("go_onlineurl-----" + ServiceConstant.UPDATE_AVAILABILITY)
                } else {
                    Alert(
                        resources.getString(R.string.alert_sorry_label_title),
                        resources.getString(R.string.alert_nointernet)
                    )
                }
            } else {
                enableGpsService()
            }
            //showNotification("Login",BUNDLE_NOTIFICATION_ID.toString())
        }
        setLocationRequest()
        buildGoogleApiClient()
        return parentView
    }

    private fun setUpViews() {
        val parentActivity = activity as NavigationDrawer?
        // resideMenu = parentActivity!!.getResideMenu()
    }


    fun showDialog(message: String) {
        dialog = Dialog(activity!!)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
    }

    private fun setLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = 10000
        mLocationRequest.fastestInterval = 5000
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    protected fun startLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient!!.isConnected) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this
            )
        }
    }


    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(activity!!)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
    }

    private fun initialize(rootview: View) {
        session = SessionManager(activity!!)
        gps = GPSTracker(activity!!)
        val user = session!!.userDetails
        driver_id = user[SessionManager.KEY_DRIVERID]
        chauffl_id = user[SessionManager.KEY_CHAUFFL_NO]
        driver_img = user[SessionManager.KEY_DRIVER_IMAGE]
        driver_name = user[SessionManager.KEY_DRIVER_NAME]
        vehicle_no = user[SessionManager.KEY_VEHICLENO]
        vehicle_name = user[SessionManager.KEY_VEHICLE_MODEL]
        Tv_lasttrip_ridetime = rootview.findViewById(R.id.dashboard_ride_time) as TextView
        drawer_layout = rootview.findViewById(R.id.ham_home) as ImageView

        Tv_lasttrip_ridedate = rootview.findViewById(R.id.dashboard_last_trip_ride_date) as TextView
        Tv_lasttrip_earnings = rootview.findViewById(R.id.netAmount_price_last_trips) as TextView
        Tv_today_earnings_onlinehours =
            rootview.findViewById(R.id.dashboard_today_earnings_onlinetime) as TextView
        Tv_todayearnigs_trips =
            rootview.findViewById(R.id.dashboard_today_earnings_trips) as TextView
        Tv_todayearnings_earnings =
            rootview.findViewById(R.id.netAmount_price_today_earnings) as TextView
        Tv_todaytips_trips = rootview.findViewById(R.id.dashboard_todays_trips) as TextView
        Tv_todaytips_tips = rootview.findViewById(R.id.netAmount_price_today_tips) as TextView
        Bt_Go_Online = rootview.findViewById(R.id.Bt_gonlinebutton) as LinearLayout
        Tv_driver_name = rootview.findViewById(R.id.home_user_name) as TextView
        Tv_Driver_vechile_model = rootview.findViewById(R.id.home_car_name) as TextView
        user_img = rootview.findViewById(R.id.dasboard_driverimg) as ImageView
        Tv_Driver_Vechile_no = rootview.findViewById(R.id.home_car_no) as TextView
        Tv_car_category = rootview.findViewById(R.id.home_car_category) as TextView
        Tv_driver_chauffl_no = rootview.findViewById(R.id.home_driver_shuffleid) as TextView
        //tv_driver_rating = rootview.findViewById(R.id.tv_driver_rating) as TextView
        driver_rating = rootview.findViewById(R.id.driver_dashboard_ratting)
        Picasso.with(activity).load(driver_img).placeholder(R.drawable.no_profile_image_avatar_icon)
            .into(user_img)

        //val actionBarActivity = activity as AppCompatActivity?
        /*actionBar = actionBarActivity!!.getSupportActionBar()
        actionBar!!.setHomeButtonEnabled(true)
        actionBar!!.setCustomView(R.layout.action_bar_home)
        actionBar!!.setBackgroundDrawable(ColorDrawable(Color.parseColor("#31c3e7")))
        actionBar!!.setDisplayShowCustomEnabled(true)
        actionBar!!.hide()*/

        cd = ConnectionDetector(activity!!)
        isInternetPresent = cd!!.isConnectingToInternet

        if (isInternetPresent!!) {
            driverdashboard_PostRequest(ServiceConstant.driver_dashboard)
            Log.e("Dashchauffl_id", chauffl_id + "-----" + driver_id)
            Tv_driver_chauffl_no!!.setText("Chauffl No : " + chauffl_id)
            System.out.println("driverdashboardurl------------" + ServiceConstant.driver_dashboard)
        } else {
            Alert(
                resources.getString(R.string.alert_sorry_label_title),
                resources.getString(R.string.alert_nointernet)
            )
        }
    }

    override fun onPause() {
        super.onPause()
        System.gc()
    }

    private fun initilizeMap() {

        if (mMap == null) {
            val mapFragment =
                childFragmentManager.findFragmentById(R.id.driver_dashboradsmain_map) as SupportMapFragment
            mapFragment.getMapAsync(this)
            if (mMap == null) {
                /*Toast.makeText(getActivity(), "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                    .show();*/

            }
        }
        //mMap.setMapType(mMap.MAP_TYPE_NORMAL);

        //markerOptions = MarkerOptions()
        // onMapReady(mMap!!)
    }

    override fun onMapReady(googleMap: GoogleMap) {


        try {
            mMap = googleMap
            mMap!!.mapType = R.raw.map_silver_json
            Log.e("MapsActivityRaw", "Can't find style.")

            mMap!!.uiSettings.setAllGesturesEnabled(true)
            mMap!!.uiSettings.isMapToolbarEnabled = false
            mMap!!.isIndoorEnabled = true
            mMap!!.isBuildingsEnabled = true
            mMap!!.uiSettings.isZoomControlsEnabled = true
            mMap!!.isIndoorEnabled = true
            mMap!!.isMyLocationEnabled = true
            mMap!!.uiSettings.isMyLocationButtonEnabled = false
            mMap!!.isBuildingsEnabled = true
            mMap!!.uiSettings.isZoomControlsEnabled = false
            if (gps != null && gps!!.canGetLocation()) {
                val Dlatitude = gps!!.getLatitude()
                val Dlongitude = gps!!.getLongitude()
                MyCurrent_lat = Dlatitude
                MyCurrent_long = Dlongitude
                val cameraPosition =
                    CameraPosition.Builder().target(LatLng(Dlatitude, Dlongitude)).zoom(18f).build()
                mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

            } else {
                if (alert_layout != null && alert_textview != null) {
                    alert_layout.visibility = View.VISIBLE
                    alert_textview.text = resources.getString(R.string.alert_gpsEnable)
                }
            }


        } catch (e: Resources.NotFoundException) {
            Log.e("MapsActivityRaw", "Can't find style.", e)
        }

    }


    fun dismissDialog() {
        if (dialog != null)
            dialog!!.dismiss()
    }


    //Enabling Gps Service
    private fun enableGpsService() {
        mLocationRequest = LocationRequest.create()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = (30 * 1000).toLong()
        mLocationRequest.fastestInterval = (5 * 1000).toLong()
        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest)
        builder.setAlwaysShow(true)
        result =
            LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())
        result.setResultCallback { result ->
            val status = result.status
            //final LocationSettingsStates state = result.getLocationSettingsStates();
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                    // Location settings are not satisfied. But could be fixed by showing the user
                    // a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(activity, REQUEST_LOCATION)
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }

                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }
        }
    }

    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(activity!!)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            resources.getString(R.string.alert_label_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    private fun driverdashboard_PostRequest(Url: String) {
        dialog = Dialog(activity!!)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        /* TextView dialog_title = (TextView) dialog.findViewById(R.id.custom_loading_textview);
        dialog_title.setText(getResources().getString(R.string.action_loading));*/

        println("-------------dashboard----------------$Url ++++++ $driver_id")

        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id!!

        mRequest = ServiceRequest(activity!!)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {

                override fun onCompleteListener(response: String) {

                    Log.e("dashboards", response)
                    println("dashboards---------$response")
                    var Str_status = ""
                    var Str_response = ""
                    var Str_driver_status = ""
                    var Str_driver_name = ""
                    var Str_driver_img = ""
                    var Str_driver_category = ""
                    var Str_vechile_no = ""
                    var Str_vechile_model = ""
                    var Str_driver_id = ""
                    var Str_driver_lattitude = ""
                    var Str_driver_longitude = ""
                    var Str_driver_ratting = ""
                    var Str_lasttrip_ridetime = ""
                    var Str_lasttrip_ridedate = ""
                    var Str_lasttrip_earnings = ""
                    var Str_lasttrip_currencycode = ""
                    var Str_todayearnings_onlinehours = ""
                    var Str_todayearnings_trips = ""
                    var Str_todayearnings_earnings = ""
                    var Str_todayearnings_currencycode = ""
                    var Str_todaytips_trips = ""
                    var Str_todaytips_tips = ""
                    var Str_todaytips_currencycode = ""

                    try {

                        val jobject = JSONObject(response)

                        Str_status = jobject.getString("status")

                        if (Str_status.equals("1", ignoreCase = true)) {
                            val `object` = jobject.getJSONObject("response")

                            Str_currencglobal = `object`.getString("currency")
                            currency_code = Currency.getInstance(getLocale(Str_currencglobal))

                            Str_driver_id = `object`.getString("driver_id")
                            Str_driver_status = `object`.getString("driver_status")
                            Str_driver_name = `object`.getString("driver_name")
                            Str_vechile_no = `object`.getString("vehicle_number")
                            Str_vechile_model = `object`.getString("vehicle_model")
                            Str_driver_img = `object`.getString("driver_image")
                            Str_driver_ratting = `object`.getString("driver_review")
                            Str_driver_category = `object`.getString("driver_category")

                            Str_driver_lattitude = `object`.getString("driver_lat")
                            Str_driver_longitude = `object`.getString("driver_lon")
                            strlat = java.lang.Double.parseDouble(Str_driver_lattitude)
                            strlon = java.lang.Double.parseDouble(Str_driver_longitude)

                            val check_last_trip_object = `object`.get("last_trip")
                            if (check_last_trip_object is JSONObject) {

                                val jobject1 = `object`.getJSONObject("last_trip")
                                if (jobject1.length() > 0) {
                                    Str_lasttrip_ridetime = jobject1.getString("ride_time")
                                    Str_lasttrip_ridedate = jobject1.getString("ride_date")
                                    Str_lasttrip_currencycode = jobject1.getString("currency")
                                    // currencycode = Currency.getInstance(getLocale(Str_lasttrip_currencycode));

                                    sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(
                                        Str_lasttrip_currencycode
                                    ).toString()

                                    Str_lasttrip_earnings =
                                        sCurrencySymbol + jobject1.getString("earnings")
                                    isLastTripAvailable = true

                                    println("ridetim------$Str_lasttrip_ridetime")
                                    println("ridedate------$Str_lasttrip_ridedate")
                                    println("amount------$Str_lasttrip_earnings")

                                } else {
                                    isLastTripAvailable = false
                                }
                            } else {
                                isLastTripAvailable = false
                            }

                            val check_today_earnings_object = `object`.get("today_earnings")
                            if (check_today_earnings_object is JSONObject) {
                                val jobject2 = `object`.getJSONObject("today_earnings")
                                if (jobject2.length() > 0) {
                                    Str_todayearnings_onlinehours =
                                        jobject2.getString("online_hours")
                                    Str_todayearnings_trips = jobject2.getString("trips")
                                    Str_todayearnings_currencycode = jobject2.getString("currency")

                                    // currencycode1= Currency.getInstance(getLocale(Str_todayearnings_currencycode));

                                    sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(
                                        Str_todayearnings_currencycode
                                    ).toString()

                                    Str_todayearnings_earnings =
                                        sCurrencySymbol + jobject2.getString("earnings")
                                    isTodayEarningsAvailable = true
                                } else {
                                    isTodayEarningsAvailable = false
                                }
                            } else {
                                isTodayEarningsAvailable = false
                            }

                            val check_today_tips_object = `object`.get("today_tips")
                            if (check_today_tips_object is JSONObject) {
                                val jobject3 = `object`.getJSONObject("today_tips")
                                if (jobject3.length() > 0) {
                                    Str_todaytips_trips = jobject3.getString("trips")
                                    Str_todaytips_currencycode = jobject3.getString("currency")
                                    //currencycode2 = Currency.getInstance(getLocale(Str_todaytips_currencycode));

                                    sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(
                                        Str_todaytips_currencycode
                                    ).toString()

                                    Str_todaytips_tips =
                                        sCurrencySymbol + jobject3.getString("tips")
                                    isTodayTipsAvailable = true
                                } else {
                                    isTodayTipsAvailable = false
                                }
                            } else {
                                isTodayTipsAvailable = false
                            }

                        } else {
                            Str_response = jobject.getString("response")
                        }

                        if (Str_status.equals("1", ignoreCase = true)) {
                            Tv_Driver_vechile_model!!.text = Str_vechile_model
                            Tv_driver_name!!.text = Str_driver_name
                            Tv_Driver_Vechile_no!!.text = Str_vechile_no
                            //tv_driver_rating.text = Str_driver_ratting
                            driver_rating!!.rating = java.lang.Float.parseFloat(Str_driver_ratting)
                            Tv_car_category!!.text = Str_driver_category

                            if (isLastTripAvailable) {
                                Tv_lasttrip_ridetime!!.text = Str_lasttrip_ridetime
                                Tv_lasttrip_ridedate!!.text = Str_lasttrip_ridedate
                                Tv_lasttrip_earnings!!.text = Str_lasttrip_earnings
                            } else {
                                Tv_lasttrip_ridetime!!.text =
                                    resources.getString(R.string.lasttrip_emtpy_label)
                                Tv_lasttrip_ridedate!!.text =
                                    resources.getString(R.string.lasttrip_emtpy_label)
                                Tv_lasttrip_earnings!!.text = sCurrencySymbol + "0.00"
                            }

                            if (isTodayEarningsAvailable) {
                                Tv_today_earnings_onlinehours!!.text = Str_todayearnings_onlinehours
                                if (Str_todayearnings_trips == "1") {
                                    Tv_todayearnigs_trips!!.text = "$Str_todayearnings_trips Trip"
                                } else {
                                    Tv_todayearnigs_trips!!.text = "$Str_todayearnings_trips Trips"
                                }
                                Tv_todayearnings_earnings!!.text = Str_todayearnings_earnings
                            } else {
                                Tv_today_earnings_onlinehours!!.text =
                                    resources.getString(R.string.todayearning_no_online_label)
                                Tv_todayearnigs_trips!!.text =
                                    resources.getString(R.string.todayearning_no_trips_label)
                                Tv_todayearnings_earnings!!.text = sCurrencySymbol + "0.00"
                            }

                            if (isTodayTipsAvailable) {
                                Tv_todaytips_tips!!.text = Str_todaytips_tips
                                if (Str_todaytips_trips == "1") {
                                    Tv_todaytips_trips!!.text = "$Str_todaytips_trips Tip"
                                } else {
                                    Tv_todaytips_trips!!.text = "$Str_todaytips_trips Tips"
                                }
                            } else {
                                Tv_todaytips_tips!!.text = sCurrencySymbol + "0.00"
                                Tv_todaytips_trips!!.text =
                                    resources.getString(R.string.todayearning_no_tips_label)
                            }

                            //---------------map marker--------------
                            mMap!!.addMarker(
                                MarkerOptions()
                                    .position(LatLng(strlat, strlon))
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.maps))
                            )
                            // Move the camera to last position with a zoom level
                            val cameraPosition =
                                CameraPosition.Builder().target(LatLng(strlat, strlon)).zoom(12f)
                                    .build()
                            mMap!!.animateCamera(
                                CameraUpdateFactory.newCameraPosition(
                                    cameraPosition
                                )
                            )


                            if (Str_driver_status.equals("Yes", ignoreCase = true)) {
                                session!!.createSessionOnline("1")
                                session!!.createSessionOnline("1")
                                ChatingService.startDriverAction(activity!!)
                                //showDialog(getResources().getString(R.string.action_loading));
                                val jsonParams = HashMap<String, String>()
                                val userDetails = session!!.userDetails
                                val onlinedetails = session!!.onlineDetails
                                val driverId = userDetails["driverid"]
                                jsonParams["driver_id"] = "" + driverId!!
                                jsonParams["availability"] = "" + "Yes"
                                println("availability-----" + "Yes")
                                println("driver_id-----$driverId")
                                val manager =
                                    ServiceManager(activity!!, updateAvailablityServiceListener)
                                manager.makeServiceRequest(
                                    ServiceConstant.UPDATE_AVAILABILITY,
                                    Request.Method.POST,
                                    jsonParams
                                )
                                System.out.println("go_onlineurl-----" + ServiceConstant.UPDATE_AVAILABILITY)

                            }

                        } else {
                            Alert(
                                resources.getString(R.string.alert_sorry_label_title),
                                Str_response
                            )
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    dialog!!.dismiss()

                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }
            })


    }


    override fun onConnected(bundle: Bundle?) {

    }

    override fun onConnectionSuspended(i: Int) {

    }

    override fun onLocationChanged(location: Location) {

    }

    override fun onClick(v: View) {

    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

    }

    companion object {
        internal val REQUEST_LOCATION = 199

        val currentActivty: FragmentActivity?
            @Throws(Exception::class)
            get() {
                val activityThreadClass = Class.forName("android.app.ActivityThread")
                val activityThread =
                    activityThreadClass.getMethod("currentActivityThread").invoke(null)
                val activitiesField = activityThreadClass.getDeclaredField("mActivities")
                activitiesField.isAccessible = true
                val activities = activitiesField.get(activityThread) as HashMap<*, *>
                for (activityRecord in activities.values) {
                    val activityRecordClass = activityRecord.javaClass
                    val pausedField = activityRecordClass.getDeclaredField("paused")
                    pausedField.isAccessible = true
                    if (!pausedField.getBoolean(activityRecord)) {
                        val activityField = activityRecordClass.getDeclaredField("activity")
                        activityField.isAccessible = true
                        return activityField.get(activityRecord) as FragmentActivity
                    }
                }
                return null
            }
        var isOnline = false


        //method to convert currency code to currency symbol
        private fun getLocale(strCode: String): Locale? {

            for (locale in NumberFormat.getAvailableLocales()) {
                val code = NumberFormat.getCurrencyInstance(locale).currency!!.currencyCode
                if (strCode == code) {
                    return locale
                }
            }
            return null
        }
    }

    // custom notification
    /*var BUNDLE_NOTIFICATION_ID = 100
    val BUNDLE_CHANNEL_ID = "RIDEHUB_DRIVER_APP"
    val BUNDLE_CHANNEL_NAME = "RIDEHUB_DRIVER"
    private fun showNotification(message: String, bundle_notification_id: String) {
       *//* val contentView =  RemoteViews(activity!!.packageName, R.layout.custom_notification)
        contentView.setTextViewText(R.id.textView1,"RideHub")
        contentView.setTextViewText(R.id.textView2,"RideHub")
        contentView.setTextViewText(R.id.textView3,"Jason is arriving in WheelChair Taxi Cab555")
        contentView.setImageViewResource(R.id.imageView,R.mipmap.ic_launcher_blue_round)*//*
        val notificationManager =
            activity!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val groupChannel = NotificationChannel(
                BUNDLE_CHANNEL_ID,
                BUNDLE_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(groupChannel)
        }
        val intent = Intent()
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
        val resultPendingIntent =
            PendingIntent.getActivity(
                activity!!,
                BUNDLE_NOTIFICATION_ID,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
        val notificationBuilder = NotificationCompat.Builder(activity!!, BUNDLE_CHANNEL_ID)
            .setGroup(bundle_notification_id)
            .setGroupSummary(true)
            .setSmallIcon(R.mipmap.ic_launcher_blue_round)
            .setContentTitle("Message from rider")
            .setContentText(message)
            .setDefaults(NotificationCompat.DEFAULT_ALL)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setAutoCancel(true)
//            .setContent(contentView)
            .setContentIntent(resultPendingIntent)
            .setShowWhen(true)
//            .setTimeoutAfter(1000)

        notificationManager.notify(BUNDLE_NOTIFICATION_ID, notificationBuilder.build())
    }*/


    private fun showLocationSearchDialog(source_or_destination: String) {
        update_dialog = Dialog(activity!!)
        update_dialog.window
        update_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        update_dialog.setContentView(R.layout.goonline_dialog)
        update_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        update_dialog.setCanceledOnTouchOutside(false)
        update_dialog.window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        update_dialog.show()
        val ll_ok = update_dialog!!.findViewById(R.id.ll_ok) as LinearLayout
        val tv_message = update_dialog!!.findViewById(R.id.tv_message) as TextView
        tv_message.setText(source_or_destination)
        ll_ok.setOnClickListener { update_dialog.dismiss() }

    }


}



