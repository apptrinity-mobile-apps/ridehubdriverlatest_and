package com.project.ridehubdriver.Fragments

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.project.ridehubdriver.Activities.NavigationDrawer
import com.project.ridehubdriver.Adapters.ReferralPointsAdapter
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.Hockeyapp.FragmentHockeyApp
import com.project.ridehubdriver.PojoResponse.ReferralPointsPojo
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.ConnectionDetector
import com.project.ridehubdriver.Utils.PkDialog
import com.project.ridehubdriver.Utils.SessionManager
import org.json.JSONObject
import java.text.NumberFormat
import java.util.*

/**
 * Created by user14 on 9/22/2015.
 */
class ReferralPointsActivity : FragmentHockeyApp() {

    private var parentView: View? = null
    internal lateinit var listView_referralpoints: ListView
    internal lateinit var refer_points_subtotal_textView: TextView
    internal var postrequest: StringRequest? = null
    private var session: SessionManager? = null
    internal var driver_id: String? = ""
    internal var payid: String? = null
    private var referralpointslist: ArrayList<ReferralPointsPojo>? = null
    private var adapter: ReferralPointsAdapter? = null
    private var dialog: Dialog? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var actionBar: ActionBar? = null
    private var empty_Tv: TextView? = null
    private val show_progress_status = false
    private var drawer_layout: ImageView? = null
    private var mRequest: ServiceRequest? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        try{
            parentView = inflater.inflate(R.layout.activity_referral_points, container, false)
        }catch (e:Exception){

        }
        initialize(parentView!!)

        drawer_layout!!.setOnClickListener {
            NavigationDrawer.openDrawer()
        }

        return parentView
    }

    private fun initialize(rootview: View) {
        session = SessionManager(activity!!)
        listView_referralpoints = rootview.findViewById(R.id.listView_referralpoints) as ListView
        refer_points_subtotal_textView =
            rootview.findViewById(R.id.refer_points_subtotal_textView) as TextView
        drawer_layout = rootview.findViewById(R.id.ham_home) as ImageView
        empty_Tv = rootview.findViewById<View>(R.id.payment_no_textview) as TextView

        val user = session!!.userDetails
        driver_id = user[SessionManager.KEY_DRIVERID]


        cd = ConnectionDetector(activity!!)
        isInternetPresent = cd!!.isConnectingToInternet

        if (isInternetPresent!!) {
            PostRequest(ServiceConstant.referralpoints_url)
            println("Referral------------------" + ServiceConstant.referralpoints_url)
        } else {

            Alert(
                resources.getString(R.string.alert_sorry_label_title),
                resources.getString(R.string.alert_nointernet)
            )

        }

    }

    //--------------Alert Method-----------
    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(activity!!)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            resources.getString(R.string.alert_label_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    //-----------------------Code for payment details post request-----------------
    private fun PostRequest(Url: String) {
        dialog = Dialog(activity!!)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------paymentdetails----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id!!
        println("--------------driver_id-------------------" + driver_id!!)

        mRequest = ServiceRequest(activity!!)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {

                override fun onCompleteListener(response: String) {
                    Log.e("REFERRALPOINTS", response)

                    var status = ""
                    var total_points = ""
                    var Str_response = ""
                    try {
                        val `object` = JSONObject(response)
                        status = `object`.getString("status")
                        total_points = `object`.getString("total_points")

                        Log.e("REFERRALPOINTSSTATUS", "$status-----$total_points")
                        if (status.equals("1", ignoreCase = true)) {

                            val jarry = `object`.getJSONArray("data")
                            referralpointslist = ArrayList<ReferralPointsPojo>()
                            println("ARRAYLENGTH--------" + jarry.length())
                            if (jarry.length() > 0) {
                                for (i in 0 until jarry.length()) {
                                    val item = ReferralPointsPojo()
                                    val jobject = jarry.getJSONObject(i)
                                    item.referrer_name = (jobject.getString("referrer_name"))
                                    item.date = (jobject.getString("date"))
                                    item.refer_from = (jobject.getString("refer_from"))
                                    item.refer_to = (jobject.getString("refer_to"))
                                    item.type= (jobject.getString("type"))
                                    item.ride_id= (jobject.getString("ride_id"))
                                    item.trans_type = (jobject.getString("trans_type"))
                                    item.level = (jobject.getString("level"))
                                    item.points = (jobject.getString("points"))
                                    println("referrer_name--------" + jobject.getString("referrer_name"))

                                    referralpointslist!!.add(item)
                                }
                                refer_points_subtotal_textView.setText("$"+total_points)
                                //  show_progress_status = true;

                            } else {
                                //  show_progress_status = false;
                            }

                        } else {
                            Str_response = `object`.getString("response")
                        }

                    } catch (e: Exception) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    if (status.equals("1", ignoreCase = true)) {
                        adapter = ReferralPointsAdapter(activity!!, referralpointslist!!)
                        listView_referralpoints.adapter = adapter
                        dialog!!.dismiss()

                        if (show_progress_status) {
                            empty_Tv!!.visibility = View.GONE
                        } else {
                            empty_Tv!!.visibility = View.VISIBLE
                            listView_referralpoints.emptyView = empty_Tv
                        }

                    } else {

                        Alert(resources.getString(R.string.alert_sorry_label_title), Str_response)
                    }
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }

            })
    }


    //method to convert currency code to currency symbol
    private fun getLocale(strCode: String): Locale? {

        for (locale in NumberFormat.getAvailableLocales()) {
            val code = NumberFormat.getCurrencyInstance(locale).currency!!.currencyCode
            if (strCode == code) {
                return locale
            }
        }
        return null
    }


}


