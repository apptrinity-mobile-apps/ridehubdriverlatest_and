package com.project.ridehubdriver.Hockeyapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager

import androidx.fragment.app.Fragment

import com.project.ridehubdriver.Helper.ServiceConstant

import net.hockeyapp.android.CrashManager
import net.hockeyapp.android.UpdateManager


/**
 * Created by Prem Kumar and Anitha on 11/12/2015.
 */
open class FragmentHockeyApp : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreate(savedInstanceState)
        activity!!.window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        checkForUpdates()
        return null
    }

    override fun onResume() {
        super.onResume()
        checkForCrashes()
    }

    override fun onPause() {
        super.onPause()
        unregisterManagers()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterManagers()
    }

    private fun checkForCrashes() {
        CrashManager.register(activity, APP_ID)
    }

    private fun checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(activity, APP_ID)
    }

    private fun unregisterManagers() {
        UpdateManager.unregister()
        // unregister other managers if necessary...
    }

    companion object {


        private val APP_ID = ServiceConstant.ACTION_ACTION_HOCKYAPPID
    }
}

