package com.project.ridehubdriver.Hockeyapp

import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.project.ridehubdriver.Helper.ServiceConstant
import net.hockeyapp.android.CrashManager
import net.hockeyapp.android.UpdateManager

open class ActivityHockeyApp : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        checkForUpdates()
    }

    override fun onResume() {
        super.onResume()
        checkForCrashes()
    }

    override fun onPause() {
        super.onPause()
        unregisterManagers()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterManagers()
    }

    private fun checkForCrashes() {
        CrashManager.register(this, APP_ID)
    }

    private fun checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this, APP_ID)
    }

    private fun unregisterManagers() {
        UpdateManager.unregister()
        // unregister other managers if necessary...
    }

    companion object {

        private val APP_ID = ServiceConstant.ACTION_ACTION_HOCKYAPPID
    }


}