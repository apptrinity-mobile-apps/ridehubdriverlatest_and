package com.project.ridehubdriver.parser

import com.project.ridehubdriver.PojoResponse.ViewTripDetails

import org.json.JSONException
import org.json.JSONObject

/**
 * Created by user88 on 10/23/2015.
 */
class ViewTripParser : BaseParser() {

    internal lateinit var details: ViewTripDetails
    override fun parse(response: String): Any {
        details = ViewTripDetails()
        try {
            val `object` = JSONObject(response)
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return details
    }
}
