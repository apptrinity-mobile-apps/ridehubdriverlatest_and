package com.project.ridehubdriver.parser

import org.json.JSONException

/**
 * Created by user88 on 10/23/2015.
 */
abstract class BaseParser {

    @Throws(JSONException::class)
    abstract fun parse(response: String): Any

}
