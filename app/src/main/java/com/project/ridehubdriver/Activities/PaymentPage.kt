package com.project.ridehubdriver.Activities

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AlphaAnimation
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.Helper.xmpp.ChatingService
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.ConnectionDetector
import com.project.ridehubdriver.Utils.PkDialog
import com.project.ridehubdriver.Utils.SessionManager
import com.project.ridehubdriver.subclass.SubclassActivity
import me.drakeet.materialdialog.MaterialDialog
import org.json.JSONObject
import java.util.*

/**
 * Created by Manibabu on 11/4/2015.
 */
class PaymentPage : SubclassActivity() {

    private var driver_id: String? = ""
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private val context: Context? = null
    private var session: SessionManager? = null
    private var Tv_amount: TextView? = null
    private var Bt_receivecash: LinearLayout? = null
    private var Str_amount: String? = ""
    private var Str_rideid: String? = ""
    private var Str_currencycode: String? = ""
    internal lateinit var dialog: Dialog
    internal var postrequest: StringRequest? = null

    private var mRequest: ServiceRequest? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.activity_receive_cash)
        /*val config = resources.configuration
        if (config.smallestScreenWidthDp >= 600) {
            setContentView(R.layout.receive_cash)
        } else {
            setContentView(R.layout.receive_cash)
        }*/

        initialize()
        //Starting Xmpp service
        ChatingService.startDriverAction(this@PaymentPage)

        Bt_receivecash!!.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            Bt_receivecash!!.startAnimation(buttonClick)
            cd = ConnectionDetector(this@PaymentPage)
            isInternetPresent = cd!!.isConnectingToInternet
            if (isInternetPresent!!) {
                PostRequest(ServiceConstant.receivedbill_amounr_cash_url)
                System.out.println("end------------------" + ServiceConstant.receivedbill_amounr_cash_url)
            } else {
                Alert(
                    resources.getString(R.string.alert_sorry_label_title),
                    resources.getString(R.string.alert_nointernet)
                )
            }
            Toast.makeText(
                applicationContext,
                resources.getString(R.string.waitfortransaction_label_title),
                Toast.LENGTH_SHORT
            ).show()
        }

    }

    private fun initialize() {
        session = SessionManager(this@PaymentPage)
        // get user data from session
        val user = session!!.userDetails
        driver_id = user[SessionManager.KEY_DRIVERID]
        val i = intent
        Str_amount = i.getStringExtra("amount")
        Str_rideid = i.getStringExtra("rideid")
        Str_currencycode = i.getStringExtra("CurrencyCode")

        println("rideid---paymneinituliz-------------" + Str_rideid!!)


        println("amount-------------" + Str_amount!!)
        Tv_amount = findViewById(R.id.Receive_cash_amount) as TextView
        Bt_receivecash = findViewById(R.id.Receive_cash_receive_btn) as LinearLayout
        Tv_amount!!.text = Str_amount

    }


    //--------------Alert Method------------------
    private fun Alert(title: String, alert: String) {
        val dialogs = MaterialDialog(this@PaymentPage)
        dialogs.setTitle(title)
            .setMessage(alert)
            .setPositiveButton(
                "OK"
            ) { dialogs.dismiss() }
            .show()
    }

    //-----------------------Code for begin trip post request-----------------

    private fun PostRequest(Url: String) {
        dialog = Dialog(this@PaymentPage)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------PaymentPage----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id!!
        jsonParams["ride_id"] = Str_rideid!!
        jsonParams["amount"] = Str_amount!!

        println("--------------postdriver_id-------------------" + driver_id!!)

        println("--------------posstdriver_id-------------------" + Str_amount!!)

        println("--------------postrideid-------------------" + Str_rideid!!)

        mRequest = ServiceRequest(this@PaymentPage)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    Log.e("recev", response)

                    println("responsepayment---------$response")

                    var Str_status = ""
                    var Str_response = ""
                    var Str_ride_type = ""

                    try {
                        val `object` = JSONObject(response)
                        Str_status = `object`.getString("status")
                        Str_response = `object`.getString("response")
                        Str_ride_type = `object`.getString("ride_type")

                        println("response----------" + `object`.getString("response"))

                        println("status----------" + `object`.getString("status"))

                        if (Str_status.equals("1", ignoreCase = true)) {

                            val mdialog = PkDialog(this@PaymentPage)
                            mdialog.setDialogTitle(resources.getString(R.string.action_loading_sucess))
                            mdialog.setDialogMessage(Str_response)
                            mdialog.setPositiveButton(
                                "OK", View.OnClickListener {
                                    mdialog.dismiss()

                                    val broadcastIntent_otp = Intent()
                                    broadcastIntent_otp.action = "com.finish.OtpPage"
                                    sendBroadcast(broadcastIntent_otp)

                                    val broadcastIntent = Intent()
                                    broadcastIntent.action = "com.finish.EndTrip"
                                    sendBroadcast(broadcastIntent)
                                    finish()
                                    if(Str_ride_type.equals("offline")){

                                        val intent = Intent(this@PaymentPage, NavigationDrawer::class.java)
                                       // intent.putExtra("rideid", Str_rideid)
                                        startActivity(intent)
                                    }else{
                                        val intent = Intent(this@PaymentPage, RatingsPage::class.java)
                                        intent.putExtra("rideid", Str_rideid)
                                        startActivity(intent)
                                    }


                                    //onBackPressed();
                                }
                            )
                            mdialog.show()
                        } else {
                            val mdialog = PkDialog(this@PaymentPage)
                            mdialog.setDialogTitle(resources.getString(R.string.alert_sorry_label_title))
                            mdialog.setDialogMessage(Str_response)
                            mdialog.setCancelOnTouchOutside(false)
                            mdialog.setPositiveButton(
                                resources.getString(R.string.alert_label_ok),
                                View.OnClickListener { mdialog.dismiss() }
                            )
                            mdialog.show()

                        }

                    } catch (e: Exception) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    dialog.dismiss()
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }

            })

    }

    companion object {

        var EXTRA = "EXTRA"
    }

}
