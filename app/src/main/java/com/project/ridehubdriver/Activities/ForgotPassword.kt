package com.project.ridehubdriver.Activities

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.ConnectionDetector
import com.project.ridehubdriver.Utils.PkDialog
import org.json.JSONObject
import java.util.*

/**
 * Created by user88 on 12/21/2015.
 */
class ForgotPassword : AppCompatActivity() {

    private var Et_email: EditText? = null
    private var Rl_layout_email_send: LinearLayout? = null
    private var layout_forgot_pwd_back: ImageView? = null
    private var dialog: Dialog? = null
    private val postrequest: StringRequest? = null
    private var mRequest: ServiceRequest? = null

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        val config = resources.configuration
        if (config.smallestScreenWidthDp >= 600) {
            setContentView(R.layout.forgot_password)
        } else {
            setContentView(R.layout.forgot_password)
        }


        initilize()

        Rl_layout_email_send!!.setOnClickListener {
            cd = ConnectionDetector(this@ForgotPassword)
            isInternetPresent = cd!!.isConnectingToInternet

            if (isInternetPresent!!) {
                forgotpassword_PostRequest(ServiceConstant.forgotpassword)
                System.out.println("forgotpwd-----------" + ServiceConstant.forgotpassword)
            } else {

                Alert(
                    resources.getString(R.string.alert_sorry_label_title),
                    resources.getString(R.string.alert_nointernet)
                )
            }
        }


        layout_forgot_pwd_back!!.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

    }


    private fun initilize() {

        Et_email = findViewById(R.id.editText_email_forgotpwd) as EditText
        Rl_layout_email_send = findViewById(R.id.settings_forgotpwd_button) as LinearLayout
        layout_forgot_pwd_back = findViewById(R.id.imageView10) as ImageView
    }


    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(this@ForgotPassword)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            resources.getString(R.string.alert_label_ok),
            View.OnClickListener {
                mDialog.dismiss()
                if (!title.equals("Sorry!", ignoreCase = true)) {
                    onBackPressed()
                }
            })
        mDialog.show()
    }


    //--------------------------code for post forgot password-----------------------
    private fun forgotpassword_PostRequest(Url: String) {
        dialog = Dialog(this@ForgotPassword)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------forgotpwd----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["email"] = Et_email!!.text.toString()

        println("--------------email-------------------" + Et_email!!.text.toString())

        mRequest = ServiceRequest(this@ForgotPassword)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    Log.e("forgotpwd", response)

                    println("forgotpwdresponse---------$response")

                    var Str_status = ""
                    var Str_response = ""

                    try {
                        val `object` = JSONObject(response)
                        Str_status = `object`.getString("status")
                        Str_response = `object`.getString("response")
                    } catch (e: Exception) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    if (Str_status.equals("1", ignoreCase = true)) {
                        Alert(
                            resources.getString(R.string.label_pushnotification_cashreceived),
                            Str_response
                        )

                    } else {
                        if (Str_response.equals(
                                "Some parameters are missing.",
                                ignoreCase = true
                            )
                        ) {
                            Str_response = "Enter Email ID"
                        } else if (Str_response.equals(
                                "Authentication failed.",
                                ignoreCase = true
                            )
                        ) {
                            Str_response = "Please enter valid Email-ID"
                        }

                        Alert(resources.getString(R.string.alert_sorry_label_title), Str_response)
                    }
                    dialog!!.dismiss()
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }
            })

    }


}
