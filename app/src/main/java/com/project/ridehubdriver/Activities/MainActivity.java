package com.project.ridehubdriver.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.project.ridehubdriver.R;
import com.project.ridehubdriver.Utils.SessionManagerChat;

import java.util.HashMap;

//import android.support.v7.app.AlertDialog;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "WebViewLog";
    private static String url = "http://apbiotechkisan.in/abt.html"; //my favourite site! //"https://webkit.lemonway.fr/ejemplo/?moneyintoken=shidotoken"; //
    private ProgressDialog progressBar;
    private WebView webview;
    private EditText editText;
    private RelativeLayout back;
    private Button button;
    String data;

    private SessionManagerChat session;
    private String driver_id = "";
    private String driver_name = "";
    private String driver_image = "";
    private String driver_email = "";
    private String Str_RideId = "";
    private String Str_address = "";
    private String Str_pickUp_Lat = "";
    private String Str_pickUp_Long = "";
    private String Str_username = "";
    private String Str_user_rating = "";
    private String Str_user_phoneno = "";
    private String Str_user_img = "";
    private String Str_droplat = "";
    private String Str_droplon = "";
    private String str_drop_location = "";
    private String Suser_Id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //DRIVER SESSION DETAILS
        session = new SessionManagerChat(MainActivity.this);
        HashMap<String, String> user = session.getUserDetails();
        driver_id = user.get(SessionManagerChat.KEY_DRIVERID);
        driver_name = user.get(SessionManagerChat.KEY_DRIVER_NAME);
        driver_image = user.get(SessionManagerChat.KEY_DRIVER_IMAGE);
        driver_email = user.get(SessionManagerChat.KEY_EMAIL);


        webview = (WebView) findViewById(R.id.webView);
        editText = (EditText) findViewById(R.id.editText);
        button = (Button) findViewById(R.id.button);
        back = (RelativeLayout) findViewById(R.id.my_rides_chat_header_back_layout);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editText.setText("");
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


//USER SESSION DETAILS

        Intent i = getIntent();
        Str_address = i.getStringExtra("address");
        Str_RideId = i.getStringExtra("rideId");
        Str_pickUp_Lat = i.getStringExtra("pickuplat");
        Str_pickUp_Long = i.getStringExtra("pickup_long");
        Str_username = i.getStringExtra("username");
        Str_user_rating = i.getStringExtra("userrating");
        Str_user_phoneno = i.getStringExtra("phoneno");
        Str_user_img = i.getStringExtra("userimg");
        Str_droplat = i.getStringExtra("drop_lat");
        Str_droplon = i.getStringExtra("drop_lon");
        str_drop_location = i.getStringExtra("drop_location");
        System.out.println("KKKKKKK---------" + Str_droplat);
        Suser_Id = i.getStringExtra("UserId");


        data = "<script>\n" +
                "(function(t,a,l,k,j,s){\n" +
                "s=a.createElement('script');s.async=1;s.src=\"https://cdn.talkjs.com/talk.js\";a.head.appendChild(s)\n" +
                ";k=t.Promise;t.Talk={v:1,ready:{then:function(f){if(k)return new k(function(r,e){l.push([f,r,e])});l\n" +
                ".push([f])},catch:function(){return k&&new k()},c:l}};})(window,document,[]);\n" +
                "</script>\n" +
                "\n" +
                "<!-- container element in which TalkJS will display a chat UI -->\n" +
                "<div id=\"talkjs-container\" style=\"width: 100%; margin: 0px; height: 500px\"><i>Loading chat...</i></div>\n" +
                "\n" +
                "<!-- TalkJS initialization code, which we'll customize in the next steps -->\n" +
                "<script>\n" +
                "Talk.ready.then(function() {\n" +
                "    var me = new Talk.User({\n" +
                "        id: '"+ driver_id +"',\n" +
                "        name: '"+driver_name+"',\n" +
                "        role: \"user\",\n"+
                "        email: '"+driver_email+"',\n" +
                "        photoUrl: '"+driver_image+"',\n" +
                "        welcomeMessage: \"Hey there! How are you? :-)\"\n" +
                "    });\n" +
                "    window.talkSession = new Talk.Session({\n" +
                "        appId: \"M5iybBK9\",\n" +
                "        me: me\n" +
                "    });\n" +
                "    var other = new Talk.User({\n" +
                "        id: '"+ Suser_Id +"',\n" +
                "        name: '"+Str_username+"',\n" +
                "        role: \"user\",\n"+
                "        email: '"+driver_email+"',\n" +
                "        photoUrl: '"+Str_user_img+"',\n" +
                "        welcomeMessage: \"Hey, how can I help?\"\n" +
                "    });\n" +
                "\n" +
                "    var conversation = talkSession.getOrCreateConversation(Talk.oneOnOneId(me, other))\n" +
                "    conversation.setParticipant(me);\n" +
                "    conversation.setParticipant(other);\n" +
                "    var inbox = talkSession.createInbox({selected: conversation});\n" +
                "    inbox.mount(document.getElementById(\"talkjs-container\"));\n" +
                "});\n" +
                "</script>\n";

        /*data = "<script>\n" +
                "(function(t,a,l,k,j,s){\n" +
                "s=a.createElement('script');s.async=1;s.src=\"https://cdn.talkjs.com/talk.js\";a.head.appendChild(s)\n" +
                ";k=t.Promise;t.Talk={v:1,ready:{then:function(f){if(k)return new k(function(r,e){l.push([f,r,e])});l\n" +
                ".push([f])},catch:function(){return k&&new k()},c:l}};})(window,document,[]);\n" +
                "</script>\n" +
                "\n" +
                "<!-- container element in which TalkJS will display a chat UI -->\n" +
                "<div id=\"talkjs-container\" style=\"width: 100%; margin: 0px; height: 500px\"><i>Loading chat...</i></div>\n" +
                "\n" +
                "<!-- TalkJS initialization code, which we'll customize in the next steps -->\n" +
                "<script>\n" +
                "Talk.ready.then(function() {\n" +
                "    var me = new Talk.User({\n" +
                "        id: \"654321\",\n" +
                "        name: \"Sebastian\",\n" +
                "        role: \"user\",\n"+
                "        email: \"Sebastian@example.com\",\n" +
                "        photoUrl: \"https://demo.talkjs.com/img/sebastian.jpg\",\n" +
                "        welcomeMessage: \"Hey, how can I help?\"\n" +
                "    });\n" +
                "    window.talkSession = new Talk.Session({\n" +
                "        appId: \"w4zp7QVv\",\n" +
                "        me: me\n" +
                "    });\n" +
                "    var other = new Talk.User({\n" +
                "        id: \"123456\",\n" +
                "        name: \"Alice\",\n" +
                "        role: \"user\",\n"+
                "        email: \"alice@example.com\",\n" +
                "        photoUrl: \"https://demo.talkjs.com/img/alice.jpg\",\n" +
                "        welcomeMessage: \"Hey there! How are you? :-)\"\n" +
                "    });\n" +
                "\n" +
                "    var conversation = talkSession.getOrCreateConversation(Talk.oneOnOneId(me, other))\n" +
                "    conversation.setParticipant(me);\n" +
                "    conversation.setParticipant(other);\n" +
                "    var inbox = talkSession.createInbox({selected: conversation});\n" +
                "    inbox.mount(document.getElementById(\"talkjs-container\"));\n" +
                "});\n" +
                "</script>\n";*/


        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        webview.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        progressBar = ProgressDialog.show(MainActivity.this, "WebView Android", "Loading...");

        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i(TAG, "shouldOverrideUrlLoading() DEPRECATED Processing webview url click...");
                writeToLog("Processing webview url " + url);
                if (url.contains("moneyintoken")) {
                    Log.i(TAG, "****contiene token ");
                }

                view.loadUrl(url);

                return true;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    writeToLog("shouldOverrideUrlLoading() Processing webview url " + request.getUrl().toString());

                    if (request.getUrl().toString().contains("moneyintoken")) {
                        Log.i(TAG, "2 ****contiene token ");
                    }

                } else {
                    writeToLog("Processing webview url " + request.toString());

                    if (request.toString().contains("moneyintoken")) {
                        Log.i(TAG, "2 ****contiene token ");
                    }
                }


                return true;
            }

            public void onPageFinished(WebView view, String url) {
                Log.i(TAG, "Finished loading URL: " + url);
                writeToLog("Finished loading URL: " + url);
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Log.e(TAG, "onReceivedError DEPRECATED Error: " + description);
                Toast.makeText(getApplicationContext(), "Oh no! " + description, Toast.LENGTH_SHORT).show();
                writeToLog("Error: " + description, 1);
            }


            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                String newUrl = view.getUrl();
                Log.i(TAG, "onPageStarted() URL: " + url + " newURL: " + newUrl);
                writeToLog("onPageStarted() URL: " + url + " newURL: " + newUrl);
            }

            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                String message = "SSL Certificate error.";
                switch (error.getPrimaryError()) {
                    case SslError.SSL_UNTRUSTED:
                        message = "The certificate authority is not trusted.";
                        break;
                    case SslError.SSL_EXPIRED:
                        message = "The certificate has expired.";
                        break;
                    case SslError.SSL_IDMISMATCH:
                        message = "The certificate Hostname mismatch.";
                        break;
                    case SslError.SSL_NOTYETVALID:
                        message = "The certificate is not yet valid.";
                        break;
                }
                message += "\"SSL Certificate Error\" Do you want to continue anyway?.. YES";

                writeToLog(message, 1);
                handler.proceed();

                Log.e(TAG, "onReceivedError DEPRECATED Error: " + message);
            }

        });
        // webview.loadUrl(url);

        webview.loadDataWithBaseURL("https://talkjs.com/dashboard/login", data, "text/html", "UTF-8", "");
    }

    private void writeToLog(String msg) {
        writeToLog(msg, 0);
    }

    private void writeToLog(String msg, int error) {
        String color = "#00AA00";
        if (error == 1) color = "#00AA00";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            editText.setText(Html.fromHtml("<font color=" + color + "> • " + msg + "</font><br>" + editText.getText().toString() + "<br>", Html.FROM_HTML_MODE_LEGACY));
        } else {
            editText.setText(Html.fromHtml("<font color=" + color + "> • " + msg + "</font><br>" + editText.getText().toString() + "<br>"));
        }
    }

}
