package com.project.ridehubdriver.Activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ProgressBar
import com.android.volley.Request
import com.project.ridehubdriver.Fragments.DashBoardDriver
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.ConnectionDetector
import com.project.ridehubdriver.Utils.PkDialog
import com.project.ridehubdriver.Utils.SessionManager
import com.project.ridehubdriver.Utils.SessionManager_Applaunch
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * Created by user88 on 6/3/2016.
 */
class Splash : Activity() {

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    internal lateinit var context: Context
    private var mRequest: ServiceRequest? = null
    private var progress: ProgressBar? = null

    internal var sPendingRideId = ""
    internal var sRatingStatus = ""
    private var isAppInfoAvailable = false

    private var session_launch: SessionManager_Applaunch? = null

    private var session: SessionManager? = null
    private var driver_id: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.splash)
        context = applicationContext
        cd = ConnectionDetector(this@Splash)
        isInternetPresent = cd!!.isConnectingToInternet
        session = SessionManager(this@Splash)

        session_launch = SessionManager_Applaunch(this@Splash)
        progress = findViewById<ProgressBar>(R.id.progress_splash)

        cd = ConnectionDetector(this@Splash)
        isInternetPresent = cd!!.isConnectingToInternet

        val user = session!!.userDetails
        driver_id = user[SessionManager.KEY_DRIVERID]

        if (isInternetPresent!!) {
            postRequest_applaunch(ServiceConstant.app_launching_url)
            println("applaunch------------------" + ServiceConstant.app_launching_url)
        } else {
            Alert(
                resources.getString(R.string.alert_sorry_label_title),
                resources.getString(R.string.alert_nointernet)
            )
        }

        val timerThread = object : Thread() {
            override fun run() {
                try {
                    sleep(3000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                } finally {
                    val intent = Intent(this@Splash, SignUpSignIn::class.java)
                    startActivity(intent)
                }
            }
        }
        timerThread.start()
    }

    override fun onPause() {
        super.onPause()
        finish()
    }

    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(this@Splash)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            resources.getString(R.string.alert_label_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }

    //-----------------------App Information Post Request-----------------
    private fun postRequest_applaunch(Url: String) {

        println("-------------Splash App Information Url----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["user_type"] = "driver"
        jsonParams["id"] = driver_id!!
        mRequest = ServiceRequest(this@Splash)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("-------------Splash App Information Response----------------$response")

                    var Str_status = ""
                    var sContact_mail = ""
                    var sCustomerServiceNumber = ""
                    var sSiteUrl = ""
                    var sXmppHostUrl = ""
                    var sHostName = ""
                    var sFacebookId = ""
                    var sGooglePlusId = ""
                    var sPhoneMasking = ""
                    try {
                        val `object` = JSONObject(response)
                        Str_status = `object`.getString("status")
                        if (Str_status.equals("1", ignoreCase = true)) {
                            val response_object = `object`.getJSONObject("response")
                            if (response_object.length() > 0) {
                                val info_object = response_object.getJSONObject("info")
                                if (info_object.length() > 0) {
                                    sContact_mail = info_object.getString("site_contact_mail")
                                    sCustomerServiceNumber =
                                        info_object.getString("customer_service_number")
                                    sSiteUrl = info_object.getString("site_url")
                                    sXmppHostUrl = info_object.getString("xmpp_host_url")
                                    sHostName = info_object.getString("xmpp_host_name")
                                    sFacebookId = info_object.getString("facebook_app_id")
                                    sGooglePlusId = info_object.getString("google_plus_app_id")
                                    sPhoneMasking = info_object.getString("phone_masking_status")

                                    isAppInfoAvailable = true
                                } else {
                                    isAppInfoAvailable = false
                                }

                                sPendingRideId = response_object.getString("pending_rideid")
                                sRatingStatus = response_object.getString("ride_status")

                            } else {
                                isAppInfoAvailable = false
                            }
                        } else {
                            isAppInfoAvailable = false
                        }

                        if (Str_status.equals("1", ignoreCase = true) && isAppInfoAvailable) {

                            session = SessionManager(this@Splash)
                            if (session!!.isLoggedIn) {
                                val i = Intent(this@Splash, DashBoardDriver::class.java)
                                startActivity(i)
                                finish()
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                            } else {
                                val i = Intent(this@Splash, SignUpSignIn::class.java)
                                startActivity(i)
                                finish()
                                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                            }
                        } else {
                            /*Alert(
                                resources.getString(R.string.alert_sorry_label_title),
                                resources.getString(R.string.fetchdatatoast)
                            )*/
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }

                override fun onErrorListener() {}
            })
    }

}
