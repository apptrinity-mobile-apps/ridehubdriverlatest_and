package com.project.ridehubdriver.Activities

import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AlphaAnimation
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.fragment.app.FragmentActivity
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.ConnectionDetector
import com.project.ridehubdriver.Utils.PkDialog
import com.project.ridehubdriver.Utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by user88 on 11/17/2015.
 */
class EndTrip_EnterDetails : FragmentActivity() {
    private var driver_id: String? = ""
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    private var dialog: Dialog? = null
    private var Et_hours: EditText? = null
    private var Et_minuts: EditText? = null
    private var Et_distance: EditText? = null
    private var Et_drop_location: EditText? = null
    private var Et_drop_time: EditText? = null
    private var Bt_shimmer_endtrip: LinearLayout? = null
    internal var initialX: Float = 0.toFloat()
    internal var initialY: Float = 0.toFloat()
    private var mRequest: ServiceRequest? = null
    private var Str_status = ""
    private var Str_pickup_time: String? = ""
    private var Str_pickup_lat: String? = ""
    private var Str_pickup_long: String? = ""
    private var Str_pickup_location: String? = ""
    private var Str_drop_location: String? = ""
    private var Str_response = ""
    private var Str_ridefare = ""
    private var Str_timetaken = ""
    private var Str_waitingtime = ""
    private var Str_need_payment = ""
    private var Str_currency = ""
    private var Str_ride_distance = ""
    private var str_recievecash = ""
    private val postrequest: StringRequest? = null
    private var Str_rideid: String? = ""
    private var Rl_layout_back: ImageView? = null
    private val googlerequestcode = 100
    private var Slattitude: String? = ""
    private var Slongitude: String? = ""
    private var totalDistanceTravel: String? = null
    private var mins: String? = null
    private val secs: String? = null
    private var hours: String? = null
    private var wait_time = ""
    private var distanceURL = ServiceConstant.DISTANCE_MATRIX_API
    private val ORIGIN_STRING = "ORIGIN_STRING"
    private val DESTINATION_STRING = "DESTINATION_STRING"
    private var progresswheel: ProgressBar? = null

    // private SimpleDateFormat mFormatter = new SimpleDateFormat("MMM/dd,hh:mm aa");
    private val mFormatter = SimpleDateFormat("ddMMM,yyyy hh:mm aa")

    private val coupon_mFormatter = SimpleDateFormat("yyyy-MM-dd hh:mm aa")

    private val coupon_time_mFormatter = SimpleDateFormat("hh:mm aa")
    private val mTime_Formatter = SimpleDateFormat("HH")
    internal lateinit var receiver: BroadcastReceiver

    //----------------DatePicker Listener------------
    /*private val listener = object : SlideDateTimeListener() {
        fun onDateTimeSet(date: Date) {

            val cal = Calendar.getInstance()
            cal.add(Calendar.HOUR, 1)
            val d = cal.time
            val currenttime = mTime_Formatter.format(d)
            var selecedtime = mTime_Formatter.format(date)
            val displaytime = coupon_mFormatter.format(date)

            Toast.makeText(
                getApplicationContext(),
                coupon_mFormatter.format(date),
                Toast.LENGTH_SHORT
            ).show()

            println("-----------------current date---------------------$currenttime")
            println("-----------------selected date---------------------$selecedtime")

            if (selecedtime.equals("00", ignoreCase = true)) {
                selecedtime = "24"
            }

            if (Integer.parseInt(currenttime) >= Integer.parseInt(selecedtime)) {
                Et_drop_time!!.setText(displaytime)

                println("droptime-----------$displaytime")

            } else {
                Alert(
                    getApplicationContext().getResources().getString(R.string.alert_label_trip_title),
                    getApplicationContext().getResources().getString(R.string.alert_label_trip_content)
                )
            }
        }

        // Optional cancel listener
        fun onDateTimeCancel() {
            Toast.makeText(getApplicationContext(), "Canceled", Toast.LENGTH_SHORT).show()
        }
    }*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.end_trip_detail)
        initialize()

        //Code for broadcat receive
        val filter = IntentFilter()
        filter.addAction("com.finish.endtripenterdetail")

        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.action == "com.finish.endtripenterdetail") {
                    finish()
                }
            }
        }
        registerReceiver(receiver, filter)


        Rl_layout_back!!.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        Bt_shimmer_endtrip!!.setOnClickListener {
            mins = Et_minuts!!.text.toString()
            hours = Et_hours!!.text.toString()

            println("hour----------------" + hours!!)
            println("mins----------------" + mins!!)

            wait_time = (("" + hours + ":"
                    + mins) + ":"
                    + 0)

            println("waittime----------------$wait_time")
            cd = ConnectionDetector(getApplicationContext())
            isInternetPresent = cd!!.isConnectingToInternet
            if (isInternetPresent!!) {
                postRequest_EnterTripdetails(ServiceConstant.endtrip_url)
                System.out.println("enterdetails------------------" + ServiceConstant.endtrip_url)
            } else {
                Alert(
                    getResources().getString(R.string.alert_sorry_label_title),
                    getResources().getString(R.string.alert_nointernet)
                )
            }
            Log.d(TAG, "Left to Right swipe performed")
        }

        Et_drop_location!!.setOnClickListener {
            val intent = Intent(this@EndTrip_EnterDetails, GooglePlaceSearch::class.java)
            startActivityForResult(intent, googlerequestcode)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }


        /*Et_drop_time!!.setOnClickListener {
            SlideDateTimePicker.Builder(getSupportFragmentManager())
                .setListener(listener)
                .setInitialDate(Date())
                .build()
                .show()
        }*/

    }


    private fun initialize() {
        session = SessionManager(this@EndTrip_EnterDetails)
        // get user data from session
        val user = session!!.userDetails
        driver_id = user[SessionManager.KEY_DRIVERID]

        val i = getIntent()
        Str_rideid = i.getStringExtra("rideid")
        Str_pickup_time = i.getStringExtra("pickuptime")
        Str_pickup_location = i.getStringExtra("pickup_location")
        Str_pickup_lat = i.getStringExtra("pickup_lat")
        Str_pickup_long = i.getStringExtra("pickup_long")
        Str_drop_location = i.getStringExtra("drop_location")
        Slattitude = i.getStringExtra("DropLatitude")
        Slongitude = i.getStringExtra("DropLongitude")


        Et_hours = findViewById(R.id.arrived_tripdetail_waittime_Et_hours) as EditText
        Et_minuts = findViewById(R.id.arrived_tripdetail_waittime_Et_mins) as EditText
        Et_distance = findViewById(R.id.arrived_ridedistance_Et) as EditText
        Et_distance!!.isEnabled = false
        Et_drop_location = findViewById(R.id.arrived_tripdetail_droplocation_Et) as EditText
        Et_drop_time = findViewById(R.id.arrived_tripdetail_droptime_Et) as EditText
        Rl_layout_back = findViewById(R.id.layout_arrivd_detail_back) as ImageView
        Bt_shimmer_endtrip = findViewById(R.id.btn_endtrip) as LinearLayout
        progresswheel = findViewById(R.id.locationplace_progressBar) as ProgressBar

        Et_drop_location!!.setText(Str_drop_location)

        println("pickuptime------------------$Str_pickup_lat==$Str_pickup_long==$Str_pickup_location==$Str_drop_location==$Slattitude==$Slongitude")


        getDistanceBetweenTwoPoints(Str_pickup_location, Str_drop_location)

    }


     override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == googlerequestcode) {
            try {
                val Saddress = data!!.getStringExtra("address")
                Slattitude = data!!.getStringExtra("Lattitude")
                Slongitude = data!!.getStringExtra("Longitude")
                println("msggoogleplace-------------" + Saddress!!)
                Et_drop_location!!.setText(Saddress)
            } catch (e: Exception) {
            }

        }
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(this@EndTrip_EnterDetails)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            getResources().getString(R.string.alert_label_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }

    //-------------------Show Summery fare  Method--------------------
    private fun showfaresummerydetails() {

        val dialog = Dialog(this@EndTrip_EnterDetails)
        val view = LayoutInflater.from(this@EndTrip_EnterDetails)
            .inflate(R.layout.fare_summary, null)
        val Tv_reqest = view.findViewById<View>(R.id.requst) as TextView
        val tv_fare_totalamount =
            view.findViewById<View>(R.id.fare_summery_total_amount) as TextView
        val tv_ridedistance =
            view.findViewById<View>(R.id.fare_summery_ride_distance_value) as TextView
        val tv_timetaken =
            view.findViewById<View>(R.id.fare_summery_ride_timetaken_value) as TextView
        val tv_waittime = view.findViewById<View>(R.id.fare_summery_wait_time_value) as TextView
        val layout_request_payment =
            view.findViewById<View>(R.id.layout_faresummery_requstpayment) as RelativeLayout
        val layout_receive_cash =
            view.findViewById<View>(R.id.fare_summery_receive_cash_layout) as RelativeLayout
        tv_fare_totalamount.text = Str_ridefare
        tv_ridedistance.text = Str_ride_distance
        tv_timetaken.text = Str_timetaken
        tv_waittime.text = Str_waitingtime
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setContentView(view)
        dialog.show()


        //if (Str_need_payment.equalsIgnoreCase("YES")){

        layout_receive_cash.visibility = View.VISIBLE
        layout_request_payment.visibility = View.VISIBLE
        Tv_reqest.setText(this@EndTrip_EnterDetails.getResources().getString(R.string.lbel_fare_summery_requestpayment))

        //}else{
        //  layout_receive_cash.setVisibility(View.GONE);
        // Tv_reqest.setText(EndTrip.this.getResources().getString(R.string.alert_label_ok));

        //}

        layout_receive_cash.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            layout_receive_cash.startAnimation(buttonClick)
            val intent = Intent(this@EndTrip_EnterDetails, OtpPage::class.java)
            intent.putExtra("rideid", Str_rideid)
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        layout_request_payment.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            layout_request_payment.startAnimation(buttonClick)
            cd = ConnectionDetector(this@EndTrip_EnterDetails)
            isInternetPresent = cd!!.isConnectingToInternet
            if (isInternetPresent!!) {

                if (Tv_reqest.text.toString().equals(
                        this@EndTrip_EnterDetails.getResources().getString(
                            R.string.lbel_fare_summery_requestpayment
                        ), ignoreCase = true
                    )
                ) {
                    postRequest_Reqqustpayment(ServiceConstant.request_paymnet_url)
                    System.out.println("arrived------------------" + ServiceConstant.request_paymnet_url)
                } else {
                    val intent = Intent(this@EndTrip_EnterDetails, RatingsPage::class.java)
                    startActivity(intent)
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                }

            } else {
                Alert(
                    getResources().getString(R.string.alert_sorry_label_title),
                    getResources().getString(R.string.alert_nointernet)
                )
            }
        }

    }

    private fun showfaresummerydetails1() {

        val dialog = Dialog(this@EndTrip_EnterDetails)
        val view = LayoutInflater.from(this@EndTrip_EnterDetails)
            .inflate(R.layout.fare_summary, null)
        val Tv_reqest = view.findViewById<View>(R.id.requst) as TextView
        val tv_fare_totalamount =
            view.findViewById<View>(R.id.fare_summery_total_amount) as TextView
        val tv_ridedistance =
            view.findViewById<View>(R.id.fare_summery_ride_distance_value) as TextView
        val tv_timetaken =
            view.findViewById<View>(R.id.fare_summery_ride_timetaken_value) as TextView
        val tv_waittime = view.findViewById<View>(R.id.fare_summery_wait_time_value) as TextView
        val layout_request_payment =
            view.findViewById<View>(R.id.layout_faresummery_requstpayment) as RelativeLayout
        val layout_receive_cash =
            view.findViewById<View>(R.id.fare_summery_receive_cash_layout) as RelativeLayout
        tv_fare_totalamount.text = Str_ridefare
        tv_ridedistance.text = Str_ride_distance
        tv_timetaken.text = Str_timetaken
        tv_waittime.text = Str_waitingtime
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setContentView(view)
        dialog.show()


        // if (Str_need_payment.equalsIgnoreCase("YES")){

        layout_receive_cash.visibility = View.GONE
        layout_request_payment.visibility = View.VISIBLE
        Tv_reqest.setText(this@EndTrip_EnterDetails.getResources().getString(R.string.lbel_fare_summery_requestpayment))


        layout_request_payment.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            layout_request_payment.startAnimation(buttonClick)

            cd = ConnectionDetector(this@EndTrip_EnterDetails)
            isInternetPresent = cd!!.isConnectingToInternet
            if (isInternetPresent!!) {

                if (Tv_reqest.text.toString().equals(
                        this@EndTrip_EnterDetails.getResources().getString(
                            R.string.lbel_fare_summery_requestpayment
                        ), ignoreCase = true
                    )
                ) {
                    postRequest_Reqqustpayment(ServiceConstant.request_paymnet_url)
                    System.out.println("arrived------------------" + ServiceConstant.request_paymnet_url)
                } else {
                    val intent = Intent(this@EndTrip_EnterDetails, RatingsPage::class.java)
                    startActivity(intent)
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                }

            } else {
                Alert(
                    getResources().getString(R.string.alert_sorry_label_title),
                    getResources().getString(R.string.alert_nointernet)
                )
            }
        }

    }


    private fun showfaresummerydetails2() {

        val dialog = Dialog(this@EndTrip_EnterDetails)
        val view = LayoutInflater.from(this@EndTrip_EnterDetails)
            .inflate(R.layout.fare_summary, null)
        val Tv_reqest = view.findViewById<View>(R.id.requst) as TextView
        val tv_fare_totalamount =
            view.findViewById<View>(R.id.fare_summery_total_amount) as TextView
        val tv_ridedistance =
            view.findViewById<View>(R.id.fare_summery_ride_distance_value) as TextView
        val tv_timetaken =
            view.findViewById<View>(R.id.fare_summery_ride_timetaken_value) as TextView
        val tv_waittime = view.findViewById<View>(R.id.fare_summery_wait_time_value) as TextView
        val layout_request_payment =
            view.findViewById<View>(R.id.layout_faresummery_requstpayment) as RelativeLayout
        val layout_receive_cash =
            view.findViewById<View>(R.id.fare_summery_receive_cash_layout) as RelativeLayout
        tv_fare_totalamount.text = Str_ridefare
        tv_ridedistance.text = Str_ride_distance
        tv_timetaken.text = Str_timetaken
        tv_waittime.text = Str_waitingtime
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setContentView(view)
        dialog.show()


        // if (Str_need_payment.equalsIgnoreCase("YES")){

        layout_receive_cash.visibility = View.GONE
        layout_request_payment.visibility = View.VISIBLE
        Tv_reqest.setText(this@EndTrip_EnterDetails.getResources().getString(R.string.lbel_notification_ok))



        layout_request_payment.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            layout_request_payment.startAnimation(buttonClick)
            cd = ConnectionDetector(this@EndTrip_EnterDetails)
            isInternetPresent = cd!!.isConnectingToInternet
            if (isInternetPresent!!) {


                val intent = Intent(this@EndTrip_EnterDetails, RatingsPage::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)


            } else {
                Alert(
                    getResources().getString(R.string.alert_sorry_label_title),
                    getResources().getString(R.string.alert_nointernet)
                )
            }
        }

    }


    //-----------------------Post Request Enter Trip Details Details-----------------
    private fun postRequest_EnterTripdetails(Url: String) {
        dialog = Dialog(this@EndTrip_EnterDetails)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_loading))

        println("-------------endtripenter----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id!!
        jsonParams["ride_id"] = Str_rideid!!
        jsonParams["drop_lat"] = Slattitude!!
        jsonParams["drop_lon"] = Slongitude!!
        jsonParams["interrupted"] = "YES"
        jsonParams["drop_loc"] = Et_drop_location!!.text.toString()
        jsonParams["distance"] = Et_distance!!.text.toString()
        jsonParams["wait_time"] = wait_time
        jsonParams["drop_time"] = Et_drop_time!!.text.toString()

        println("driver_id----------" + driver_id!!)

        println("drop_loc----------" + Et_drop_location!!.text.toString())

        println("distance----------" + Et_distance!!.text.toString())

        println("drop_time----------" + Et_drop_time!!.text.toString())

        println("wait_time----------$wait_time")

        println("drop_lat----------" + Slattitude!!)

        println("drop_lon----------" + Slongitude!!)

        println("interrupted----------" + "YES")

        println("ride_id----------" + Str_rideid!!)


        mRequest = ServiceRequest(this@EndTrip_EnterDetails)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {

                override fun onCompleteListener(response: String) {
                    Log.e("endtripresponse", response)

                    println("response-----------------$response")

                    try {
                        val `object` = JSONObject(response)
                        Str_status = `object`.getString("status")

                        println("status--------------$Str_status")

                        if (Str_status.equals("1", ignoreCase = true)) {
                            val jsonObject = `object`.getJSONObject("response")
                            val jobject = jsonObject.getJSONObject("fare_details")

                            Str_need_payment = jsonObject.getString("need_payment")
                            str_recievecash = jsonObject.getString("receive_cash")

                            Str_currency = jobject.getString("currency")

                            val currencycode = Currency.getInstance(getLocale(Str_currency))
                            Str_ridefare = currencycode.symbol + jobject.getString("ride_fare")
                            Str_timetaken = jobject.getString("ride_duration")
                            Str_waitingtime = jobject.getString("waiting_duration")
                            Str_ride_distance = jobject.getString("ride_distance")
                            Str_need_payment = jobject.getString("need_payment")
                        } else {

                            Str_response = `object`.getString("response")

                        }

                    } catch (e: Exception) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    dialog!!.dismiss()
                    if (Str_status.equals("1", ignoreCase = true)) {

                        //  endTripHandler.removeCallbacks(endTripRunnable);


                        if (Str_need_payment.equals("YES", ignoreCase = true)) {
                            println("sucess------------$Str_need_payment")
                            if (str_recievecash.matches("Enable".toRegex())) {
                                showfaresummerydetails()
                            } else {
                                showfaresummerydetails1()
                            }

                        } else {
                            showfaresummerydetails2()
                        }

                    } else {
                        Alert(
                            getResources().getString(R.string.alert_sorry_label_title),
                            Str_response
                        )
                    }


                    dialog!!.dismiss()
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()

                }

            })

    }


    fun getDistanceBetweenTwoPoints(origin: String?, destination: String?) {
        var origin = origin
        var destination = destination
        origin = origin!!.replace(" ".toRegex(), "%20")
        destination = destination!!.replace(" ".toRegex(), "%20")
        distanceURL = distanceURL.replace(ORIGIN_STRING, origin)
        distanceURL = distanceURL.replace(DESTINATION_STRING, destination)
        Log.e("distanceURL", distanceURL)
        Distance_route_calculation(distanceURL)
        //new DistanceMatrixTask().execute("");
    }

    private fun Distance_route_calculation(Url: String) {

        progresswheel!!.visibility = View.VISIBLE
        println("--------------Distance url-------------------$Url")

        mRequest = ServiceRequest(this@EndTrip_EnterDetails)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.GET,
            null,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("--------------Distance   reponse-------------------$response")
                    var status = ""
                    try {
                        val object1 = JSONObject(response)
                        if (object1.length() > 0) {

                            status = object1.getString("status")

                            if ("OK".equals(status, ignoreCase = true)) {
                                val rowsArray = object1.getJSONArray("rows")
                                val `object` = rowsArray.get(0) as JSONObject
                                val elementsArray = `object`.getJSONArray("elements")
                                val distance = elementsArray.get(0) as JSONObject
                                val distanceObject = distance.get("distance") as JSONObject
                                totalDistanceTravel = distanceObject.get("text") as String
                                val splited = totalDistanceTravel!!.split("\\s+".toRegex())
                                    .dropLastWhile { it.isEmpty() }.toTypedArray()
                                /* JSONObject duration = (JSONObject) elementsArray.get(1);
                            JSONObject durationObject  =  (JSONObject) distance.get("duration");
                            String totalDurationInMinute = (String) distance.get("text");*/
                                Et_distance!!.setText(splited[0].trim { it <= ' ' })
                                progresswheel!!.visibility = View.INVISIBLE
                                CloseKeyboard(Et_distance!!)
                            } else {
                            }
                        }

                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }


                }

                override fun onErrorListener() {
                    progresswheel!!.visibility = View.INVISIBLE
                }
            })
    }


    //-----------------------Code for arrived post request-----------------
    private fun postRequest_Reqqustpayment(Url: String) {
        dialog = Dialog(this@EndTrip_EnterDetails)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_loading))

        println("-------------endtripenter----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id!!
        jsonParams["ride_id"] = Str_rideid!!

        println("--------------driver_id-------------------" + driver_id!!)
        println("--------------ride_id-------------------" + Str_rideid!!)

        mRequest = ServiceRequest(this@EndTrip_EnterDetails)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    Log.e("requestpayment", response)

                    println("response---------$response")

                    var Str_status = ""
                    var Str_response = ""
                    val Str_currency = ""
                    val Str_rideid = ""
                    val Str_action = ""

                    try {
                        val `object` = JSONObject(response)
                        Str_response = `object`.getString("response")
                        Str_status = `object`.getString("status")

                    } catch (e: Exception) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    if (Str_status.equals("0", ignoreCase = true)) {
                        Alert(
                            getResources().getString(R.string.alert_sorry_label_title),
                            Str_response
                        )

                    } else {

                        Alert(
                            getResources().getString(R.string.label_pushnotification_cashreceived),
                            Str_response
                        )

                    }

                }

                override fun onErrorListener() {

                    dialog!!.dismiss()

                }


            })


    }

    private fun CloseKeyboard(edittext: EditText) {
        val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(
            edittext.applicationWindowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }

    companion object {
        private val TAG = "swipe"
        //method to convert currency code to currency symbol
        private fun getLocale(strCode: String): Locale? {

            for (locale in NumberFormat.getAvailableLocales()) {
                val code = NumberFormat.getCurrencyInstance(locale).currency!!.currencyCode
                if (strCode == code) {
                    return locale
                }
            }
            return null
        }
    }
}
