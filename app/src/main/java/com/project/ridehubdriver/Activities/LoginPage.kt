package com.project.ridehubdriver.Activities

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.Settings.Secure
import android.telephony.TelephonyManager
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.appcompat.app.ActionBar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.android.volley.Request
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.project.ridehubdriver.Helper.ServiceConstant.Companion.LOGIN_URL
import com.project.ridehubdriver.Helper.ServiceManager
import com.project.ridehubdriver.Helper.xmpp.ChatingService
import com.project.ridehubdriver.PojoResponse.LoginDetails
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.GPSTracker
import com.project.ridehubdriver.Utils.PkDialog
import com.project.ridehubdriver.Utils.SessionManager
import java.util.*
import java.util.regex.Pattern

class LoginPage : BaseActivity(), View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {


    private var android_id: String? = null
    private var session: SessionManager? = null
    private var emailid: EditText? = null
    private var password: EditText? = null
    private var signin: LinearLayout? = null
    private var GCM_Id: String? = null
    private val actionBar: ActionBar? = null
    internal lateinit var mGoogleApiClient: GoogleApiClient
    internal lateinit var mLocationRequest: LocationRequest
    internal lateinit var result: PendingResult<LocationSettingsResult>
    internal lateinit var gps: GPSTracker

    private var slideUp: Animation? = null
    private var slideLeft: Animation? = null

    private var layout_forgotpassword: RelativeLayout? = null
    // private var tv_donthaveaccount: TextView? = null
    private lateinit var iv_show_password: ImageView
    private var isPasswordShown = false
    val PERMISSION_REQUEST_CODE = 111

    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.activity_login)

        FirebaseInstanceId.getInstance().instanceId
            .addOnSuccessListener(this@LoginPage, object :
                OnSuccessListener<InstanceIdResult> {
                override fun onSuccess(instanceIdResult: InstanceIdResult) {
                    val newToken = instanceIdResult.token
                    GCM_Id = newToken
                    Log.e("newToken", GCM_Id!!)
                }
            })

        initialize()

        mGoogleApiClient = GoogleApiClient.Builder(this@LoginPage)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this).build()
        mGoogleApiClient.connect()


        layout_forgotpassword!!.setOnClickListener {
            val intent = Intent(this@LoginPage, ForgotPassword::class.java)
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }


        iv_show_password.setOnClickListener {
            if (isPasswordShown) {
                iv_show_password.setImageResource(R.drawable.ic_hide)
                password!!.transformationMethod = PasswordTransformationMethod()
                isPasswordShown = false
            } else {
                iv_show_password.setImageResource(R.drawable.ic_visible)
                password!!.transformationMethod = HideReturnsTransformationMethod()
                isPasswordShown = true
            }
        }

        /* tv_donthaveaccount!!.setOnClickListener {
             ///manibabu
             if (ContextCompat.checkSelfPermission(
                     this@LoginPage,
                     Manifest.permission.READ_PHONE_STATE
                 ) !== PackageManager.PERMISSION_GRANTED
             ) {
                 requestCameraAndExternalPermission()
             } else {
                 val telephonyManager =
                     getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                 val android_device_id = telephonyManager.deviceId
                 Log.d(
                     "IMEI NUmber",
                     "" + telephonyManager.deviceId
                 )//+"?device_id="+android_device_id
                 val browserIntent = Intent(
                     Intent.ACTION_VIEW,
                     Uri.parse(ServiceConstant.Register_URL + "?device_id=" + android_device_id)
                 )
                 startActivity(browserIntent)
                 overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
             }
         }*/


    }

    private fun initialize() {
        android_id =
            Secure.getString(applicationContext.contentResolver, Secure.ANDROID_ID)
        session = SessionManager(this)
        gps = GPSTracker(this@LoginPage)
        emailid = findViewById(R.id.email)
        password = findViewById(R.id.password)
        signin = findViewById(R.id.signin_main_button)
        layout_forgotpassword = findViewById(R.id.layout_forgot_password)
        // tv_donthaveaccount = findViewById(R.id.tv_donthaveaccount) as TextView


        val name = getColoredSpanned("Don't have an account?", "#ffffff")
        val surName = getColoredSpanned("SignUp", "#44B2E6")
        //tv_donthaveaccount!!.text = Html.fromHtml("$name $surName")

        password!!.transformationMethod = PasswordTransformationMethod()

        slideUp = AnimationUtils.loadAnimation(this@LoginPage, R.anim.slide_up)
        slideLeft = AnimationUtils.loadAnimation(this@LoginPage, R.anim.slide_left)

        signin!!.setOnClickListener(this)
        iv_show_password = findViewById(R.id.iv_show_password)
        //showDialog(getResources().getString(R.string.lablesigningin_Textview))

    }


    private fun slideLeft() {
        signin!!.startAnimation(slideLeft)
        signin!!.visibility = View.INVISIBLE
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(this@LoginPage)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            getResources().getString(R.string.alert_label_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    //Enabling Gps Service
    private fun enableGpsService() {
        mLocationRequest = LocationRequest.create()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = (30 * 1000).toLong()
        mLocationRequest.fastestInterval = (5 * 1000).toLong()

        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest)
        builder.setAlwaysShow(true)

        result =
            LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())

        result.setResultCallback { result ->
            val status = result.status
            //final LocationSettingsStates state = result.getLocationSettingsStates();
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                    // Location settings are not satisfied. But could be fixed by showing the user
                    // a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(this@LoginPage, REQUEST_LOCATION)
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }

                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }
        }
    }


    override fun onClick(v: View) {
        if (v === signin) {
            val pass = password!!.text.toString()
            val email = emailid!!.text.toString()
            // !isValidEmail(email)
            if (email.length < 10) {
                emailid!!.error = "Mobile Number Invalid"
            } else if (pass.isEmpty()) {
                password!!.error = resources.getString(R.string.action_alert_invalid_password)
            } else if (ContextCompat.checkSelfPermission(
                    this@LoginPage,
                    Manifest.permission.READ_PHONE_STATE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestCameraAndExternalPermission()
            } else {
                gps = GPSTracker(this@LoginPage)
                if (gps.canGetLocation() && gps.isgpsenabled()) {
                    //showDialog(getResources().getString(R.string.lablesigningin_Textview))
                    postRequest(LOGIN_URL)
                } else {
                    enableGpsService()
                }

            }
        }
    }


    private fun isValidEmail(email: String): Boolean {
        val EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
        val pattern = Pattern.compile(EMAIL_PATTERN)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

    // validating password with retype password
    fun isValidPassword(pass: String?): Boolean {
        return if (pass != null && pass.length > 5) {
            true
        } else false
    }

    private fun postRequest(url: String) {

        if (ContextCompat.checkSelfPermission(
                this@LoginPage,
                Manifest.permission.READ_PHONE_STATE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestCameraAndExternalPermission()
        } else {

            val telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
//            val android_device_id = telephonyManager.deviceId
            val android_device_id = Secure.getString(contentResolver, Secure.ANDROID_ID)
            Log.d("IMEI NUmber", "" + /*telephonyManager.deviceId*/ android_device_id)
            val jsonParams = HashMap<String, String>()
            jsonParams["phone"] = emailid!!.text.toString()
            jsonParams["password"] = password!!.text.toString()
            jsonParams["device_id"] = android_device_id
            jsonParams["gcm_id"] = GCM_Id!!
            val manager = ServiceManager(this, mServiceListener)
            manager.makeServiceRequest(url, Request.Method.POST, jsonParams)
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ),
            PERMISSION_REQUEST_CODE
        )
    }

    private val mServiceListener = object : ServiceManager.ServiceListener {
        override fun onCompleteListener(res: Any) {
            Log.e("LOGIN RESPONSE CHAITU", "" + res)
            println("loginresponse-------------------$res")
            //dismissDialog()
            var status: String? = ""
            var driver_img: String? = ""
            var driver_id: String? = ""
            var driver_name: String? = ""
            var email: String? = ""
            var vehicle_number: String? = ""
            var vehicle_model: String? = ""
            var key: String? = ""
            var isalive = ""
            val gcmid = ""
            var sec_key: String? = ""
            var login_time: String? = ""
            var login_id: String? = ""
            var chauffl_no: String? = ""
            if (res is LoginDetails) {
                LoginPage.details = res
                status = res.status
                driver_img = res.driverImage
                driver_id = res.driverId
                driver_name = res.driverName
                email = res.email
                vehicle_number = res.vehicleNumber
                vehicle_model = res.vehicleModel
                sec_key = res.sec_key
                key = res.key
                login_time = res.login_time
                login_id = res.login_id
                chauffl_no = res.chaufflNo

                isalive = res.is_alive_other!!

                println("logintime--------------" + login_time!!)

                println("loginid--------------" + login_id!!)
                println("chauffl_no--------------" + chauffl_no!!)

                println("key--------------" + sec_key!!)

                println("driverid--------------" + driver_id!!)

                println("--------gcm id-------------" + key!!)

            }
            if (status!!.equals("1", ignoreCase = true)) {

                session!!.createLoginSession(
                    driver_img!!,
                    driver_id!!,
                    driver_name!!,
                    email!!,
                    vehicle_number!!,
                    vehicle_model!!,
                    key!!,
                    sec_key!!,
                    key,
                    login_time!!,
                    login_id!!,
                    chauffl_no!!
                )
                session!!.userVehicle = vehicle_model

                if (isalive.equals("Yes", ignoreCase = true)) {

                    SignUpSignIn.homePage.finish()
                    val mDialog = PkDialog(this@LoginPage)
                    mDialog.setDialogTitle(getResources().getString(R.string.app_name))
                    mDialog.setDialogMessage(getResources().getString(R.string.alert_multiple_login))
                    mDialog.setPositiveButton(
                        getResources().getString(R.string.alert_label_ok),
                        View.OnClickListener {
                            mDialog.dismiss()

                            ChatingService.startDriverAction(this@LoginPage)
                            val i = Intent(this@LoginPage, NavigationDrawer::class.java)
                            slideLeft()
                            startActivity(i)
                            finish()
                        })
                    mDialog.show()
                } else {
                    ChatingService.startDriverAction(this@LoginPage)
                    val i = Intent(this@LoginPage, NavigationDrawer::class.java)
                    slideLeft()
                    startActivity(i)
                    finish()
                }
            }
        }

        override fun onErrorListener(obj: Any) {
            if (obj is LoginDetails) {
                val status = obj.status
                if (status!!.equals("0", ignoreCase = true)) {
                    Alert(
                        getResources().getString(R.string.action_alert_SigninFaild),
                        getResources().getString(R.string.action_alert_signinfaildmsg)
                    )
                }
            }
        }
    }

    override fun onConnected(bundle: Bundle?) {

    }

    override fun onConnectionSuspended(i: Int) {

    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

    }

    private fun requestCameraAndExternalPermission() {
        ActivityCompat.requestPermissions(this, PERMISSIONS_SELFIE, REQUEST_SELFIE)
    }


    private fun getColoredSpanned(text: String, color: String): String {
        return "<font color=$color>$text</font>"
    }

    companion object {
        var details: LoginDetails? = null
        internal val REQUEST_LOCATION = 199
        private val REQUEST_SELFIE = 123
        private val PERMISSIONS_SELFIE = arrayOf(Manifest.permission.READ_PHONE_STATE)
    }


}

