package com.project.ridehubdriver.Activities

import android.content.Intent
import android.os.Bundle
import android.view.Window
import android.view.WindowManager
import android.widget.TextView
import com.project.ridehubdriver.R
import java.text.NumberFormat
import java.util.*

/**
 * Created by user88 on 11/6/2015.
 */
class PushNotificationAlert : BaseActivity() {
    private var Message_Tv: TextView? = null
    private var Textview_Ok: TextView? = null
    private var Textview_alert_header: TextView? = null
    private var message: String? = ""
    private var action: String? = ""
    private var amount: String? = ""
    private var rideid: String? = ""
    private var currency_code: String? = ""
    private var str_amount = ""
    private var Rl_layout_alert_ok: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.pushnotification)
        initialize()

        Rl_layout_alert_ok!!.setOnClickListener {
            println("action-----------" + action!!)

            if (action!!.equals("ride_cancelled", ignoreCase = true)) {
                finish()
            } else if (action!!.equals("receive_cash", ignoreCase = true)) {

                println("inside receive_cash-----------$str_amount")

                val intent = Intent(this@PushNotificationAlert, PaymentPage::class.java)
                intent.putExtra("amount", str_amount)
                intent.putExtra("rideid", rideid)
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                finish()

            } else if (action!!.equals("payment_paid", ignoreCase = true)) {
                val intent = Intent(this@PushNotificationAlert, RatingsPage::class.java)
                intent.putExtra("rideid", rideid)
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                finish()
            }
        }
    }


    private fun initialize() {
        val i = intent
        message = i.getStringExtra("Message")
        action = i.getStringExtra("Action")
        amount = i.getStringExtra("amount")
        rideid = i.getStringExtra("RideId")
        currency_code = i.getStringExtra("Currencycode")

       // Textview_Ok = findViewById(R.id.pushnotification_alert_ok_textview) as TextView
        Message_Tv = findViewById(R.id.pushnotification_alert_messge_textview) as TextView
        Textview_alert_header = findViewById(R.id.pushnotification_alert_messge_label) as TextView
        Rl_layout_alert_ok = findViewById(R.id.pushnotification_alert_ok) as TextView
        Message_Tv!!.text = message

        if (action!!.equals("ride_cancelled", ignoreCase = true)) {
            Textview_alert_header!!.text =
                resources.getString(R.string.label_pushnotification_canceled)
        } else if (action!!.equals("receive_cash", ignoreCase = true)) {
            Textview_alert_header!!.text =
                resources.getString(R.string.label_pushnotification_cashreceived)
        } else if (action!!.equals("payment_paid", ignoreCase = true)) {
            Textview_alert_header!!.text =
                resources.getString(R.string.label_pushnotification_ride_completed)
        }

        if (currency_code != null) {
            val currencycode = Currency.getInstance(getLocale(currency_code))
            str_amount = currencycode.symbol + amount!!
        }
    }


    //method to convert currency code to currency symbol
    private fun getLocale(strCode: String?): Locale? {

        for (locale in NumberFormat.getAvailableLocales()) {
            val code = NumberFormat.getCurrencyInstance(locale).currency!!.currencyCode
            if (strCode != null && strCode == code) {
                return locale
            }
        }
        return null
    }

}
