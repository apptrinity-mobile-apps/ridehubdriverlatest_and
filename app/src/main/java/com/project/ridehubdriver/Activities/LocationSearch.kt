package com.project.ridehubdriver.Activities

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.Request
import com.project.ridehubdriver.Adapters.PlaceSearchAdapter
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.Hockeyapp.ActionBarActivityHockeyApp
import com.project.ridehubdriver.PojoResponse.EstimateDetailPojo
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.ConnectionDetector
import com.project.ridehubdriver.Utils.PkDialog
import org.json.JSONException
import org.json.JSONObject
import java.util.*


/**
 * Created by Prem Kumar and Anitha on 11/12/2015.
 */
class LocationSearch : ActionBarActivityHockeyApp() {

    private var back: RelativeLayout? = null
    private var et_search: EditText? = null
    private var listview: ListView? = null
    private var alert_layout: RelativeLayout? = null
    private var alert_textview: TextView? = null
    private var tv_emptyText: TextView? = null
    private var progresswheel: ProgressBar? = null

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null

    private var mRequest: ServiceRequest? = null
    internal lateinit var context: Context
    internal var itemList_location = ArrayList<String>()
    internal var itemList_placeId = ArrayList<String>()

    internal lateinit var adapter: PlaceSearchAdapter
    private var isdataAvailable = false
    private val isEstimateAvailable = false

    private var Slatitude = ""
    private var Slongitude = ""
    private var Sselected_location = ""

    internal lateinit var dialog: Dialog
    internal var ratecard_list = ArrayList<EstimateDetailPojo>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.place_search)
        context = applicationContext
        initialize()


        listview!!.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                Sselected_location = itemList_location[position]

                cd = ConnectionDetector(this@LocationSearch)
                isInternetPresent = cd!!.isConnectingToInternet
                if (isInternetPresent!!) {
                    LatLongRequest(
                        ServiceConstant.GetAddressFrom_LatLong_url + itemList_placeId[position]
                    )
                } else {
                    alert_layout!!.visibility = View.VISIBLE
                    alert_textview!!.text = resources.getString(R.string.alert_nointernet)
                }
            }

        et_search!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {

                cd = ConnectionDetector(this@LocationSearch)
                isInternetPresent = cd!!.isConnectingToInternet

                if (isInternetPresent!!) {
                    if (mRequest != null) {
                        mRequest!!.cancelRequest()
                    }
                    CitySearchRequest(ServiceConstant.place_search_url + et_search!!.text.toString().toLowerCase())
                } else {
                    alert_layout!!.visibility = View.VISIBLE
                    alert_textview!!.text = resources.getString(R.string.alert_nointernet)
                }

            }
        })

        et_search!!.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(et_search!!)
            }
            false
        }

        back!!.setOnClickListener {
            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            this@LocationSearch.finish()
            this@LocationSearch.overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
        }
    }

    private fun initialize() {
        alert_layout = findViewById(R.id.location_search_alert_layout) as RelativeLayout
        alert_textview = findViewById(R.id.location_search_alert_textView) as TextView
        back = findViewById(R.id.location_search_back_layout) as RelativeLayout
        et_search = findViewById(R.id.location_search_editText) as EditText
        listview = findViewById(R.id.location_search_listView) as ListView
        progresswheel = findViewById(R.id.location_search_progressBar) as ProgressBar
        tv_emptyText = findViewById(R.id.location_search_empty_textview) as TextView

    }

    private fun CloseKeyboard(edittext: EditText) {
        val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(
            edittext.applicationWindowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {
        val mDialog = PkDialog(this@LocationSearch)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(
            resources.getString(android.R.string.ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()

    }

    //-------------------Search Place Request----------------
    private fun CitySearchRequest(Url: String) {

        progresswheel!!.visibility = View.VISIBLE
        println("--------------Search city url-------------------$Url")

        mRequest = ServiceRequest(this@LocationSearch)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.GET,
            null!!,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("--------------Search city  reponse-------------------$response")
                    var status = ""
                    try {
                        val `object` = JSONObject(response)
                        if (`object`.length() > 0) {

                            status = `object`.getString("status")
                            val place_array = `object`.getJSONArray("predictions")
                            if (status.equals("OK", ignoreCase = true)) {
                                if (place_array.length() > 0) {
                                    itemList_location.clear()
                                    itemList_placeId.clear()
                                    for (i in 0 until place_array.length()) {
                                        val place_object = place_array.getJSONObject(i)
                                        itemList_location.add(place_object.getString("description"))
                                        itemList_placeId.add(place_object.getString("place_id"))
                                    }
                                    isdataAvailable = true
                                } else {
                                    itemList_location.clear()
                                    itemList_placeId.clear()
                                    isdataAvailable = false
                                }
                            } else {
                                itemList_location.clear()
                                itemList_placeId.clear()
                                isdataAvailable = false
                            }
                        }
                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    progresswheel!!.visibility = View.INVISIBLE
                    alert_layout!!.visibility = View.GONE
                    if (isdataAvailable) {
                        tv_emptyText!!.visibility = View.GONE
                    } else {
                        tv_emptyText!!.visibility = View.VISIBLE
                    }
                    adapter = PlaceSearchAdapter(this@LocationSearch, itemList_location)
                    listview!!.adapter = adapter
                    adapter.notifyDataSetChanged()
                }

                override fun onErrorListener() {
                    progresswheel!!.visibility = View.INVISIBLE
                    alert_layout!!.visibility = View.GONE

                    // close keyboard
                    CloseKeyboard(et_search!!)
                }
            })
    }


    //-------------------Get Latitude and Longitude from Address(Place ID) Request----------------
    private fun LatLongRequest(Url: String) {

        dialog = Dialog(this@LocationSearch)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = "Processing"
        println("--------------LatLong url-------------------$Url")

        mRequest = ServiceRequest(this@LocationSearch)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.GET,
            null!!,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("--------------LatLong  reponse-------------------$response")
                    var status = ""
                    try {
                        val `object` = JSONObject(response)
                        if (`object`.length() > 0) {

                            status = `object`.getString("status")
                            val place_object = `object`.getJSONObject("result")
                            if (status.equals("OK", ignoreCase = true)) {
                                if (place_object.length() > 0) {
                                    val geometry_object = place_object.getJSONObject("geometry")
                                    if (geometry_object.length() > 0) {
                                        val location_object =
                                            geometry_object.getJSONObject("location")
                                        if (location_object.length() > 0) {
                                            Slatitude = location_object.getString("lat")
                                            Slongitude = location_object.getString("lng")
                                            isdataAvailable = true
                                        } else {
                                            isdataAvailable = false
                                        }
                                    } else {
                                        isdataAvailable = false
                                    }
                                } else {
                                    isdataAvailable = false
                                }
                            } else {
                                isdataAvailable = false
                            }
                        }
                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    if (isdataAvailable) {

                        val returnIntent = Intent()
                        returnIntent.putExtra("Selected_Latitude", Slatitude)
                        returnIntent.putExtra("Selected_Longitude", Slongitude)
                        returnIntent.putExtra("Selected_Location", Sselected_location)
                        setResult(Activity.RESULT_OK, returnIntent)
                        onBackPressed()
                        finish()

                    } else {
                        dialog.dismiss()
                        Alert(resources.getString(R.string.alert_label_title), status)
                    }
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })
    }


    //-----------------Move Back on pressed phone back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {

            // close keyboard
            val mgr = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            mgr.hideSoftInputFromWindow(back!!.windowToken, 0)

            this@LocationSearch.finish()
            this@LocationSearch.overridePendingTransition(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
            return true
        }
        return false
    }
}

