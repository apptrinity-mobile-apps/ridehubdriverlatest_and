package com.project.ridehubdriver.Activities

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.media.MediaPlayer
import android.os.Bundle
import android.os.CountDownTimer
import android.provider.Settings
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.app.ActivityCompat
import at.grabner.circleprogress.CircleProgressView
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.project.ridehubdriver.Adapters.ContinuousRequestAdapter
import com.project.ridehubdriver.Hockeyapp.ActionBarActivityHockeyApp
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.io.UnsupportedEncodingException
import java.net.URLDecoder
import java.util.*


class DriverAlertActivity : ActionBarActivityHockeyApp(){
    private var sessionManager: SessionManager? = null
    private val mCircleView: CircleProgressView? = null
    private val timer: CountDownTimer? = null
    private val seconds = 0
    var dataObject: JSONObject? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLocationRequest: LocationRequest? = null
    private val TAG = "LocationProvider"

    private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    /**
     * Represents a geographical location.
     *
     *
     *
     */
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    /**
     * Represents a geographical location.
     */
    protected var mLastLocation: Location? = null


    private var listView: LinearLayout? = null
    private var continuousRequestAdapter: ContinuousRequestAdapter? = null
    private var count = 0

    private val timerCompletCallback = object : TimerCompletCallback {
        override fun timerCompleteCallBack(holder: ContinuousRequestAdapter.ViewHolder) {
            try {

                val user = sessionManager!!.requestCount
                var req_count = user[SessionManager.KEY_COUNT]!!
                req_count = req_count - 1
                sessionManager!!.setRequestCount(req_count)

                println("------- req_count inside decline--------------$req_count")

                if (req_count == 0) {
                    sessionManager!!.setRequestCount(0)
                    finish()
                    if (DriverAlertActivity.mediaPlayer != null && DriverAlertActivity.mediaPlayer!!.isPlaying) {
                        DriverAlertActivity.mediaPlayer!!.stop()
                    }
                }

                listView!!.removeViewAt(holder.count)

            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.activity_driver_alert)
        listView = findViewById(R.id.linearList)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        //getLastLocation()

        //initView()
    }
    public override fun onStart() {
        super.onStart()


        if (!checkPermissions()) {
            requestPermissions()
        } else {
            getLastLocation()
            initView()
        }

    }
    private fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(this,
            Manifest.permission.ACCESS_COARSE_LOCATION)
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

     fun initView() {
        sessionManager = SessionManager(this)
        if (!sessionManager!!.isLoggedIn) {
            Toast.makeText(
                this,
                resources.getString(R.string.drivernotlogged_label_title),
                Toast.LENGTH_SHORT
            ).show()
        }
        mediaPlayer = MediaPlayer.create(this, Settings.System.DEFAULT_RINGTONE_URI)

        //alertAdapter = new DriverAlertAdapter(sessionManager, this, myLocation);
         //Log.w("mLastLocation", ""+mLastLocation!!.latitude+"--"+mLastLocation!!.longitude)
        continuousRequestAdapter = ContinuousRequestAdapter(this, listView!!)
        continuousRequestAdapter!!.setTimerCompleteCallBack(timerCompletCallback)


        addData(intent)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        addData(intent)
    }

    interface TimerCompletCallback {
        fun timerCompleteCallBack(holder: ContinuousRequestAdapter.ViewHolder)
    }

    private fun addData(intent: Intent?) {
        if (mediaPlayer != null) {
            if (!mediaPlayer!!.isPlaying) {
                mediaPlayer!!.start()
                mediaPlayer!!.isLooping = true

            }
        }

        if (intent != null) {

            val user = sessionManager!!.requestCount
            var req_count = user[SessionManager.KEY_COUNT]!!
            req_count = req_count + 1
            sessionManager!!.setRequestCount(req_count)

            println("=-----------------start req_count---------------$req_count")


            val extra = intent.extras
            if (extra != null) {
                val data = extra.get(EXTRA) as String?
                println("=-----------------Chaitanya test---------------" + data!!)
                try {
                    val decodeData = URLDecoder.decode(data, "UTF-8")

                    count = count + 1
                    val dataObject = JSONObject(decodeData)
                    val params = LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT
                    )
                    listView!!.addView(
                        continuousRequestAdapter!!.getView(count, dataObject),
                        params
                    )
                } catch (e: JSONException) {
                } catch (e: UnsupportedEncodingException) {
                }

            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        return if (id == R.id.action_settings) {
            true
        } else super.onOptionsItemSelected(item)
    }




    override fun onDestroy() {

        if (mediaPlayer != null) {
            if (mediaPlayer!!.isPlaying) {
                mediaPlayer!!.stop()
            }
        }


        super.onDestroy()

    }

    companion object {
        private val details = ArrayList<JSONObject>()
        var EXTRA = "EXTRA"
        var mediaPlayer: MediaPlayer? = null
        var myLocation: Location? = null
    }

   /* private fun getLastLocation() {
        mFusedLocationClient.getLastLocation()
            .addOnCompleteListener(this, object : OnCompleteListener<Location> {
                override fun onComplete(@NonNull task: Task<Location>) {
                    if (task.isSuccessful() && task.getResult() != null) {
                        Log.d("last location", "" + task.getResult())
                        myLocation = task.result
                        Log.d("my location", "" + myLocation)
                    } else {
                        Log.w("getLastLocation", "getLastLocation:exception", task.getException())
                    }
                }
            })
    }*/
   @SuppressLint("MissingPermission")
   private fun getLastLocation() {
       mFusedLocationClient!!.lastLocation
           .addOnCompleteListener(this) { task ->
               if (task.isSuccessful && task.result != null) {
                   mLastLocation = task.result
                   Log.w("mLastLocation", ""+mLastLocation!!.latitude+"--"+mLastLocation!!.longitude)
                 /*  mLatitudeText!!.setText(
                       mLatitudeLabel+":   "+
                               (mLastLocation )!!.latitude)
                   mLongitudeText!!.setText(mLongitudeLabel+":   "+
                           (mLastLocation )!!.longitude)*/
               } else {
                   Log.w("TAG", "getLastLocation:exception", task.exception)
                   //showMessage(getString(R.string.no_location_detected))
               }
           }
   }

    private fun startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(this@DriverAlertActivity,
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
            REQUEST_PERMISSIONS_REQUEST_CODE)
    }

    private fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(this,
            Manifest.permission.ACCESS_COARSE_LOCATION)

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.")

            /*showSnackbar(R.string.permission_rationale, android.R.string.ok,
                View.OnClickListener {
                    // Request permission
                    startLocationPermissionRequest()
                })*/

        } else {
            Log.i(TAG, "Requesting permission")
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest()
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        Log.i(TAG, "onRequestPermissionResult")
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.size <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.")
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLastLocation()
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                /*showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                    View.OnClickListener {
                        // Build intent that displays the App settings screen.
                        val intent = Intent()
                        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                        val uri = Uri.fromParts("package",
                            BuildConfig.APPLICATION_ID, null)
                        intent.data = uri
                        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                        startActivity(intent)
                    })*/
            }
        }
    }
    private fun showSnackbar(mainTextStringId: Int, actionStringId: Int,
                             listener: View.OnClickListener) {

        Toast.makeText(this@DriverAlertActivity, getString(mainTextStringId), Toast.LENGTH_LONG).show()
    }

}
