package com.project.ridehubdriver.Activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AlphaAnimation
import android.widget.LinearLayout
import android.widget.Toast
import com.android.volley.Request
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.Helper.xmpp.ChatingService
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.PkDialog
import com.project.ridehubdriver.subclass.SubclassActivity
import org.json.JSONObject
import java.util.*


/**
 * Created by user115 on 2/18/2016.
 */
class LoadingPage : SubclassActivity() {


    private var mRequest: ServiceRequest? = null

    private var SdriverId: String? = ""

    private var SrideId: String? = ""
    internal lateinit var checkstatuss: LinearLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.waiting)

        checkstatuss = findViewById(R.id.checkstatus)

        ChatingService.startDriverAction(this@LoadingPage)

        val i = intent
        SdriverId = i.getStringExtra("Driverid")
        SrideId = i.getStringExtra("RideId")

        // System.out.println("loading reques------------------" + ServiceConstant.request_paymnet_url);
        checkstatuss.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            checkstatuss.startAnimation(buttonClick)
            postRequest_Reqqustpayment(ServiceConstant.check_trip_status)

            Log.d("TRIP STATUS...", ServiceConstant.check_trip_status)
        }

    }


    //--------------Alert Method-----------
    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(this@LoadingPage)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            resources.getString(R.string.alert_label_ok),
            View.OnClickListener {
                mDialog.dismiss()

                val broadcastIntent_begintrip = Intent()
                broadcastIntent_begintrip.action = "com.finish.com.finish.BeginTrip"
                sendBroadcast(broadcastIntent_begintrip)

                val broadcastIntent_arrivedtrip = Intent()
                broadcastIntent_arrivedtrip.action = "com.finish.ArrivedTrip"
                sendBroadcast(broadcastIntent_arrivedtrip)

                val broadcastIntent_endtrip = Intent()
                broadcastIntent_endtrip.action = "com.finish.EndTrip"
                sendBroadcast(broadcastIntent_endtrip)

                //finish();
            })
        mDialog.show()

    }


    //-----------------------Code for arrived post request-----------------
    private fun postRequest_Reqqustpayment(Url: String) {

        println("-------------endtrip----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = SdriverId!!
        jsonParams["ride_id"] = SrideId!!

        println("--------------driver_id-------------------" + SdriverId!!)


        println("--------------ride_id-------------------" + SrideId!!)

        mRequest = ServiceRequest(this@LoadingPage)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {

                override fun onCompleteListener(response: String) {

                    Log.e("requestpayment", response)

                    println("response---------$response")

                    var Str_status = ""
                    var Str_response = ""
                    val Str_currency = ""
                    val Str_rideid = ""
                    val Str_action = ""

                    try {
                        val `object` = JSONObject(response)
                        Str_response = `object`.getString("response")
                        Str_status = `object`.getString("status")
                        val jb = `object`.getJSONObject("response")
                        val trip_waiting = jb.getString("trip_waiting")
                        val rating_pending = jb.getString("ratting_pending")

                        Log.d("TRIPWAITING", trip_waiting)
                        Log.d("Pending", rating_pending)


                        if (trip_waiting.equals("Yes", ignoreCase = true)) {
                            Toast.makeText(applicationContext, "Please wait", Toast.LENGTH_LONG)
                                .show()

                        } else {
                            if (rating_pending.equals("Yes", ignoreCase = true)) {
                                val i = Intent(this@LoadingPage, RatingsPage::class.java)
                                i.putExtra("rideid", SrideId)
                                startActivity(i)
                                finish()
                            } else {
                                val i = Intent(this@LoadingPage, SignUpSignIn::class.java)
                                startActivity(i)
                                finish()
                            }
                        }

                    } catch (e: Exception) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }
                }

                override fun onErrorListener() {

                }

            })

    }

    //-----------------Move Back on  phone pressed  back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            // nothing
            true
        } else false
    }


}
