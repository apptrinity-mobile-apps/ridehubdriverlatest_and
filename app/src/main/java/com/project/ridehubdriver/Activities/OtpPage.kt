package com.project.ridehubdriver.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.Helper.xmpp.ChatingService
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.ConnectionDetector
import com.project.ridehubdriver.Utils.CurrencySymbolConverter
import com.project.ridehubdriver.Utils.PkDialog
import com.project.ridehubdriver.Utils.SessionManager
import com.project.ridehubdriver.subclass.SubclassActivity
import org.json.JSONObject
import java.text.NumberFormat
import java.util.*

/**
 * Created by user88 on 11/5/2015.
 */
class OtpPage : SubclassActivity() {
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    internal lateinit var Et_otp: EditText
    internal lateinit var BT_otp_confirm: LinearLayout

    internal lateinit var dialog: Dialog
    private var mRequest: ServiceRequest? = null

    internal var postrequest: StringRequest? = null

    internal var Str_otp = ""
    internal var Str_amount = ""
    private var Str_rideId: String? = ""
    private var driver_id: String? = ""

    private var sCurrencySymbol: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.otp_page)
        initialize()

        //Starting Xmpp service
        ChatingService.startDriverAction(this@OtpPage)
        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)
        BT_otp_confirm.setOnClickListener {
            // if(Et_otp.getText().toString().equalsIgnoreCase(Str_otp)) {

            val intent = Intent(this@OtpPage, PaymentPage::class.java)
            intent.putExtra("amount", Str_amount)
            intent.putExtra("rideid", Str_rideId)
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            /* }else{
                    Alert(getResources().getString(R.string.alert_sorry_label_title), "OTP you entered is incorrect");
                }*/
        }

    }

    private fun initialize() {
        session = SessionManager(this@OtpPage)

        val user = session!!.userDetails
        driver_id = user[SessionManager.KEY_DRIVERID]
        Et_otp = findViewById(R.id.otp_enter_code)
        BT_otp_confirm = findViewById(R.id.otp_request_btn)
        val i = intent
        Str_rideId = i.getStringExtra("rideid")

        println("inside-------------" + Str_rideId!!)

        // Et_otp.setSelection(Et_otp.getText().length());

        Et_otp.isFocusable = true

        cd = ConnectionDetector(this@OtpPage)
        isInternetPresent = cd!!.isConnectingToInternet
        if (isInternetPresent!!) {
            PostRequest(ServiceConstant.receivecash_url)
            System.out.println("end------------------" + ServiceConstant.receivecash_url)
        } else {

            Alert(
                resources.getString(R.string.alert_sorry_label_title),
                resources.getString(R.string.alert_nointernet)
            )
        }

    }


    //--------------Alert Method------------------
    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(this@OtpPage)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            resources.getString(R.string.alert_label_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    //-----------------------Code for arrived post request-----------------
    private fun PostRequest(Url: String) {
        dialog = Dialog(this@OtpPage)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------otp----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id!!
        jsonParams["ride_id"] = Str_rideId!!

        println("otp-------driver_id---------" + driver_id!!)

        println("otp-------ride_id---------" + Str_rideId!!)
        mRequest = ServiceRequest(this@OtpPage)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {

                override fun onCompleteListener(response: String) {
                    Log.e("otp", response)

                    println("otp---------$response")

                    var Str_status = ""
                    var Str_response = ""
                    var Str_otp_status = ""
                    var Str_ride_id = ""
                    var Str_currency = ""

                    try {
                        val `object` = JSONObject(response)
                        Str_status = `object`.getString("status")

                        if (Str_status.equals("1", ignoreCase = true)) {

                            Str_response = `object`.getString("response")
                            Str_currency = `object`.getString("currency")
                            //Currency currencycode = Currency.getInstance(getLocale(Str_currency));

                            sCurrencySymbol =
                                CurrencySymbolConverter.getCurrencySymbol(Str_currency)

                            Str_otp_status = `object`.getString("otp_status")
                            Str_otp = `object`.getString("otp")
                            Str_ride_id = `object`.getString("ride_id")
                            Str_amount = sCurrencySymbol!! + `object`.getString("amount")

                            println("otp--------$Str_otp")

                            println("Str_otp_status--------$Str_otp_status")
                            val intent = Intent(this@OtpPage, PaymentPage::class.java)
                            intent.putExtra("amount", Str_amount)
                            intent.putExtra("rideid", Str_rideId)
                            startActivity(intent)
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        } else {

                            Str_response = `object`.getString("response")
                        }
                    } catch (e: Exception) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    dialog.dismiss()


                    if (Str_status.equals("1", ignoreCase = true)) {

                        if (Str_otp_status.equals("development", ignoreCase = true)) {
                            Et_otp.setText(Str_otp)
                        }

                    } else {
                        Alert(resources.getString(R.string.alert_sorry_label_title), Str_response)
                    }
                }

                override fun onErrorListener() {

                    dialog.dismiss()
                }
            })

    }

    //method to convert currency code to currency symbol
    private fun getLocale(strCode: String): Locale? {

        for (locale in NumberFormat.getAvailableLocales()) {
            val code = NumberFormat.getCurrencyInstance(locale).currency!!.currencyCode
            if (strCode == code) {
                return locale
            }
        }
        return null
    }


}
