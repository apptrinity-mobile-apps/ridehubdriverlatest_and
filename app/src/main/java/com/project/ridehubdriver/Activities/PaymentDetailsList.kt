package com.project.ridehubdriver.Activities

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.project.ridehubdriver.Adapters.PaymentDetailsListAdapter
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.Hockeyapp.ActivityHockeyApp
import com.project.ridehubdriver.PojoResponse.PaymentDetailsListPojo
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.ConnectionDetector
import com.project.ridehubdriver.Utils.PkDialog
import com.project.ridehubdriver.Utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.text.NumberFormat
import java.util.*

/**
 *
 */
class PaymentDetailsList : ActivityHockeyApp() {

    internal lateinit var payment: TextView
    internal lateinit var amount: TextView
    internal lateinit var recv_date: TextView
    internal lateinit var list: ListView
    internal var postrequest: StringRequest? = null
    private var session: SessionManager? = null
    internal var driver_id: String? = ""
    private var paymentdetailList: ArrayList<PaymentDetailsListPojo>? = null
    private var adapter: PaymentDetailsListAdapter? = null
    private var payid = ""
    private var layout_back: ImageView? = null
    private var dialog: Dialog? = null
    private var Emty_Text: TextView? = null
    private var mRequest: ServiceRequest? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null

    private var show_progress_status = false
    private var Str_currency_code = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.activity_payment_details_view)
        initialize()
        layout_back!!.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

    }


    private fun initialize() {
        paymentdetailList = ArrayList<PaymentDetailsListPojo>()
        session = SessionManager(this@PaymentDetailsList)
        payment = findViewById(R.id.pmt_detail_payment) as TextView
        amount = findViewById(R.id.pmt_detail_amount) as TextView
        recv_date = findViewById(R.id.pmt_detail_receiveddate) as TextView
        list = findViewById(R.id.pmt_detail_listview) as ListView
        layout_back = findViewById(R.id.layout_payment_list_back) as ImageView
        Emty_Text = findViewById(R.id.payment_lists_no_textview) as TextView

        list.isVerticalScrollBarEnabled = false

        val user = session!!.userDetails
        driver_id = user[SessionManager.KEY_DRIVERID]

        payid = getIntent().getStringExtra("Payid")!!

        cd = ConnectionDetector(this@PaymentDetailsList)
        isInternetPresent = cd!!.isConnectingToInternet

        if (isInternetPresent!!) {
            paymentRequest(ServiceConstant.paymentdetails_lis_url)
            println("paymentlists------------------" + ServiceConstant.paymentdetails_lis_url)
        } else {
            Alert(
                getResources().getString(R.string.alert_sorry_label_title),
                getResources().getString(R.string.alert_nointernet)
            )

        }
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(this@PaymentDetailsList)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            getResources().getString(R.string.alert_label_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }

    //------------------code for post--------------
    private fun paymentRequest(Url: String) {
        dialog = Dialog(this@PaymentDetailsList)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.setText(getResources().getString(R.string.action_loading))

        println("-------------dashboard----------------$Url")

        val jsonParams = HashMap<String, String>()

        println("driver_id------------" + driver_id!!)
        println("pay_id------------$payid")

        jsonParams["driver_id"] = driver_id!!
        jsonParams["pay_id"] = payid

        mRequest = ServiceRequest(this@PaymentDetailsList)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {

                override fun onCompleteListener(response: String) {
                    println("--------------Payment Details reponse-------------------$response")

                    var status = ""
                    val response1 = ""
                    var pay_id = ""
                    var pay_duration_from = ""
                    var pay_duration_to = ""
                    var amount1 = ""
                    var pay_date = ""
                    try {
                        val `object` = JSONObject(response)
                        status = `object`.getString("status")

                        val jobject = `object`.getJSONObject("response")

                        Str_currency_code = jobject.getString("currency")
                        println("curr-----------$Str_currency_code")
                        val currencycode = Currency.getInstance(getLocale(Str_currency_code))

                        val ride_array = jobject.getJSONArray("payments")
                        paymentdetailList!!.clear()

                        if (ride_array.length() > 0) {
                            //Emty_Text.setVisibility(View.GONE);
                            for (k in 0 until ride_array.length()) {
                                val product_object = ride_array.getJSONObject(k)
                                val items1 = PaymentDetailsListPojo()
                                pay_id = product_object.getString("pay_id")
                                pay_duration_from = product_object.getString("pay_duration_from")
                                pay_duration_to = product_object.getString("pay_duration_to")
                                amount1 = currencycode.symbol + product_object.getString("amount")
                                pay_date = product_object.getString("pay_date")
                            }
                            show_progress_status = true
                        }

                        if (jobject.getString("total_payments") == "0") {
                            paymentdetailList!!.clear()
                            show_progress_status = false
                            list.visibility = View.GONE
                            Emty_Text!!.visibility = View.VISIBLE
                        } else {
                            val ride_array1 = jobject.getJSONArray("listsArr")
                            println("length------------------" + ride_array1.length())
                            if (ride_array1.length() > 0) {
                                for (k in 0 until ride_array1.length()) {
                                    val product_object = ride_array1.getJSONObject(k)
                                    val items1 = PaymentDetailsListPojo()
                                    items1.setride_id(product_object.getString("ride_id"))
                                    items1.setamount(
                                        currencycode.symbol + product_object.getString(
                                            "amount"
                                        )
                                    )
                                    items1.setride_date(product_object.getString("ride_date"))
                                    paymentdetailList!!.add(items1)
                                }
                                show_progress_status = true

                            } else {
                                paymentdetailList!!.clear()
                                show_progress_status = false
                                list.visibility = View.GONE
                                Emty_Text!!.visibility = View.VISIBLE
                            }
                        }

                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block

                        println("try------------------$e")

                        e.printStackTrace()
                    }

                    payment.text = "$pay_duration_from to $pay_duration_to"
                    amount.text = amount1
                    recv_date.text = pay_date

                    println("pay-----------------" + paymentdetailList!!)

                    adapter =
                        PaymentDetailsListAdapter(this@PaymentDetailsList, paymentdetailList!!)
                    list.adapter = adapter
                    dialog!!.dismiss()

                    if (show_progress_status) {
                        Emty_Text!!.visibility = View.GONE
                    } else {
                        Emty_Text!!.visibility = View.VISIBLE
                        list.visibility = View.GONE
                        list.emptyView = Emty_Text
                    }
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }

            })

    }

    //method to convert currency code to currency symbol
    private fun getLocale(strCode: String): Locale? {

        for (locale in NumberFormat.getAvailableLocales()) {
            val code = NumberFormat.getCurrencyInstance(locale).currency!!.currencyCode
            if (strCode == code) {
                return locale
            }
        }
        return null
    }


}
