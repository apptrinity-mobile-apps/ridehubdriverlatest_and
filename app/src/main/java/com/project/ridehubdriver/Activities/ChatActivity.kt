package com.project.ridehubdriver.Activities

import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.project.ridehubdriver.Helper.DBHelper
import com.project.ridehubdriver.Helper.xmpp.ChatingService
import com.project.ridehubdriver.PojoResponse.ChatResponsePojo
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.SessionManager
import com.project.ridehubdriver.subclass.RealTimeActivity
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso

class ChatActivity : RealTimeActivity() {

    private var context: Context? = null
    private var session: SessionManager? = null

    private var driver_id: String? = ""
    private var ride_id: String? = ""
    private var rider_name: String? = ""
    private var rider_img: String? = ""
    private var rider_id: String? = ""

    private lateinit var ll_comments: LinearLayout
    private lateinit var iv_user_image: ImageView
    private lateinit var iv_post_comment: ImageView
    private lateinit var tv_user_name: TextView
    private lateinit var et_comments: EditText
    private lateinit var rv_chat: RecyclerView

    lateinit var dbHelper: DBHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        initialize()
        val user = session!!.userDetails
        driver_id = user[SessionManager.KEY_DRIVERID]

        val i = intent
        ride_id = i.getStringExtra("rideId")
        rider_name = i.getStringExtra("username")
        rider_img = i.getStringExtra("userimg")
        rider_id = i.getStringExtra("UserId")

        //Starting Xmpp service
        ChatingService.startDriverAction(this@ChatActivity)

        if (rider_name == "" || rider_name.equals("null")) {
            tv_user_name.text = ""
        } else {
            tv_user_name.text = rider_name
        }

        Picasso.with(context)
            .load(rider_img.toString())
            .placeholder(R.drawable.no_profile_image_avatar_icon)
            .memoryPolicy(MemoryPolicy.NO_CACHE)
            .into(iv_user_image)

        // message history
        val chatResponsePojo = ChatResponsePojo()
        chatResponsePojo.RIDER_ID = ""
        chatResponsePojo.RECEIVED_STATUS = ""
        chatResponsePojo.RECEIVED_MESSAGE = ""
        chatResponsePojo.SENT_STATUS = ""
        chatResponsePojo.SENT_MESSAGE = ""
        dbHelper.insert(chatResponsePojo)

        // from layout
        val fromLayout = LinearLayout(context)
        fromLayout.orientation = LinearLayout.HORIZONTAL
        fromLayout.layoutParams =
            LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
        fromLayout.setPadding(10, 10, 10, 10)
        val textView1 = TextView(context)
        val image = ImageView(context)
        image.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.map_icon))
        image.maxHeight = 15
        image.maxWidth = 15
        textView1.background = ContextCompat.getDrawable(context!!, R.drawable.chat_bg)
        textView1.setTextColor(
            ContextCompat.getColor(
                context!!,
                R.color.black
            )
        ) // hex color 0xAARRGGBB
        // textView1.text = list.get(j).comment
        fromLayout.addView(image)
        fromLayout.addView(textView1)

        ll_comments.addView(fromLayout)


        // to layout
        val toLayout = LinearLayout(context)
        toLayout.orientation = LinearLayout.HORIZONTAL
        val textView2 = TextView(context)
        val layoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        layoutParams.gravity = Gravity.END
        toLayout.setPadding(10, 10, 10, 10) // (left, top, right, bottom)
        toLayout.layoutParams = layoutParams
        val imageView = ImageView(context)
        imageView.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.map_icon))
        imageView.maxHeight = 15
        imageView.maxWidth = 15
        textView2.background = ContextCompat.getDrawable(context!!, R.drawable.chat_bg)
        textView2.setTextColor(
            ContextCompat.getColor(
                context!!,
                R.color.black
            )
        ) // hex color 0xAARRGGBB
        // textView2.text = list.get(j).comment
        toLayout.addView(textView2)
        toLayout.addView(image)

        ll_comments.addView(toLayout)

    }

    private fun initialize() {
        context = this@ChatActivity
        session = SessionManager(this@ChatActivity)
        dbHelper = DBHelper(this@ChatActivity)

        ll_comments = findViewById(R.id.ll_comments)
        iv_user_image = findViewById(R.id.iv_user_image)
        tv_user_name = findViewById(R.id.tv_user_name)
        et_comments = findViewById(R.id.et_comments)
        iv_post_comment = findViewById(R.id.iv_post_comment)
        rv_chat = findViewById(R.id.rv_chat)

        val layoutManager = LinearLayoutManager(this@ChatActivity, RecyclerView.VERTICAL, false)
        rv_chat.layoutManager = layoutManager
    }

}