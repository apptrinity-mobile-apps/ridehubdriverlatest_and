package com.project.ridehubdriver.Activities

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.project.ridehubdriver.Adapters.PlaceSearchAdapter
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * Created by user88 on 11/18/2015.
 */
class GooglePlaceSearch : Activity() {

    internal var itemList_location = ArrayList<String>()
    internal var itemList_placeId = ArrayList<String>()
    internal lateinit var adapter: PlaceSearchAdapter
    private var isdataAvailable = false

    private var alert_layout: RelativeLayout? = null
    private var alert_textview: TextView? = null
    private var tv_emptyText: TextView? = null
    private var back: RelativeLayout? = null

    private var driver_id: String? = ""
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    internal var dialog: Dialog? = null

    internal lateinit var mdialog: Dialog

    private var progresswheel: ProgressBar? = null
    private var list_view: ListView? = null

    private var Slatitude = ""
    private var Slongitude = ""
    private var Sdrop_location = ""

    private var postrequest: StringRequest? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.drop_location)
        initialize()


        list_view!!.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                Sdrop_location = itemList_location[position]

                cd = ConnectionDetector(this@GooglePlaceSearch)
                isInternetPresent = cd!!.isConnectingToInternet
                if (isInternetPresent!!) {
                    LatLongRequest(
                        ServiceConstant.GetAddressFrom_LatLong_url + itemList_placeId[position]
                    )
                } else {
                    alert_layout!!.visibility = View.VISIBLE
                    alert_textview!!.text = resources.getString(R.string.alert_nointernet)
                }
            }


        back!!.setOnClickListener { onBackPressed() }


        Et_search.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {
                cd = ConnectionDetector(this@GooglePlaceSearch)
                isInternetPresent = cd!!.isConnectingToInternet


                try {
                    if (isInternetPresent!!) {
                        if (postrequest != null) {
                            postrequest!!.cancel()
                        }
                        // Intent returnIntent = new Intent();
                        //returnIntent.putExtra("Selected_location", Et_search.getText().toString());
                        //setResult(RESULT_OK, returnIntent);
                        //onBackPressed();
                        // finish();
                        CitySearchRequest(ServiceConstant.place_search_url + Et_search.text.toString().toLowerCase())
                    } else {
                        alert_layout!!.visibility = View.VISIBLE
                        alert_textview!!.text = resources.getString(R.string.alert_nointernet)
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        })

        Et_search.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                CloseKeyboard(Et_search)
            }
            false
        }

    }

    private fun initialize() {
        session = SessionManager(this@GooglePlaceSearch)
        // get user data from session
        val user = session!!.userDetails
        driver_id = user[SessionManager.KEY_DRIVERID]

        progresswheel = findViewById(R.id.droplocation_progressBar) as ProgressBar
        alert_layout = findViewById(R.id.droplocation_alert_layout) as RelativeLayout
        alert_textview = findViewById(R.id.droplocation_alert_textView) as TextView
        back = findViewById(R.id.droplocation_back_layout) as RelativeLayout
        tv_emptyText = findViewById(R.id.estimate_price_empty_textview) as TextView
        list_view = findViewById(R.id.droplocation_listView) as ListView
        Et_search = findViewById(R.id.droplocation_editText) as EditText

    }


    //--------------Alert Method-----------
    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(this@GooglePlaceSearch)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            resources.getString(R.string.alert_label_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    //-------------------Search Place Request----------------
    private fun CitySearchRequest(Url: String) {

        progresswheel!!.visibility = View.VISIBLE
        println("--------------Search city url-------------------$Url")
        postrequest = StringRequest(Request.Method.GET, Url, Response.Listener { response ->
            println("--------------Search city  reponse-------------------$response")
            var status = ""
            try {
                val `object` = JSONObject(response)
                if (`object`.length() > 0) {

                    status = `object`.getString("status")
                    val place_array = `object`.getJSONArray("predictions")
                    if (status.equals("OK", ignoreCase = true)) {
                        if (place_array.length() > 0) {
                            itemList_location.clear()
                            itemList_placeId.clear()
                            for (i in 0 until place_array.length()) {
                                val place_object = place_array.getJSONObject(i)
                                itemList_location.add(place_object.getString("description"))
                                itemList_placeId.add(place_object.getString("place_id"))
                            }
                            isdataAvailable = true
                        } else {
                            itemList_location.clear()
                            itemList_placeId.clear()
                            isdataAvailable = false
                        }
                    } else {
                        itemList_location.clear()
                        itemList_placeId.clear()
                        isdataAvailable = false
                    }
                }
            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

            progresswheel!!.visibility = View.INVISIBLE
            alert_layout!!.visibility = View.GONE
            if (isdataAvailable) {
                tv_emptyText!!.visibility = View.GONE
            } else {
                tv_emptyText!!.visibility = View.VISIBLE
            }
            adapter = PlaceSearchAdapter(this@GooglePlaceSearch, itemList_location)
            list_view!!.adapter = adapter
            adapter.notifyDataSetChanged()
        }, Response.ErrorListener { error ->
            progresswheel!!.visibility = View.INVISIBLE
            alert_layout!!.visibility = View.GONE

            // close keyboard
            CloseKeyboard(Et_search)
            VolleyErrorResponse.VolleyError(this@GooglePlaceSearch, error)
        })
        postrequest!!.retryPolicy = DefaultRetryPolicy(
            20000,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postrequest!!.setShouldCache(false)
        AppController.instance!!.addToRequestQueue(postrequest!!)
    }

    private fun CloseKeyboard(edittext: EditText) {
        val `in` = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(
            edittext.applicationWindowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }

    //-------------------Get Latitude and Longitude from Address(Place ID) Request----------------
    private fun LatLongRequest(Url: String) {
        try {
            mdialog = Dialog(this@GooglePlaceSearch)
            mdialog.window
            mdialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            mdialog.setContentView(R.layout.custom_loading)
            mdialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            mdialog.setCanceledOnTouchOutside(false)
            mdialog.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val dialog_title = mdialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)
        println("--------------LatLong url-------------------$Url")
        postrequest = StringRequest(Request.Method.GET, Url, Response.Listener { response ->
            println("--------------LatLong  reponse-------------------$response")
            var status = ""
            try {
                val `object` = JSONObject(response)
                if (`object`.length() > 0) {

                    status = `object`.getString("status")
                    val place_object = `object`.getJSONObject("result")
                    if (status.equals("OK", ignoreCase = true)) {
                        if (place_object.length() > 0) {
                            val geometry_object = place_object.getJSONObject("geometry")
                            if (geometry_object.length() > 0) {
                                val location_object = geometry_object.getJSONObject("location")
                                if (location_object.length() > 0) {
                                    Slatitude = location_object.getString("lat")
                                    Slongitude = location_object.getString("lng")
                                    isdataAvailable = true
                                } else {
                                    isdataAvailable = false
                                }
                            } else {
                                isdataAvailable = false
                            }
                        } else {
                            isdataAvailable = false
                        }
                    } else {
                        isdataAvailable = false
                    }
                }
            } catch (e: JSONException) {
                // TODO Auto-generated catch block
                e.printStackTrace()
            }

            if (isdataAvailable) {
                val intent = Intent()
                intent.putExtra("Lattitude", Slatitude)
                intent.putExtra("Longitude", Slongitude)
                intent.putExtra("address", Sdrop_location)

                println("msg-------------$Sdrop_location")
                println("Lattitude-------------$Slatitude")
                println("Longitude-------------$Slongitude")

                setResult(Activity.RESULT_OK, intent)
                finish()


            } else {
                mdialog.dismiss()
                Alert(resources.getString(R.string.alert_sorry_label_title), status)
            }
        }, Response.ErrorListener { error ->
            mdialog.dismiss()
            VolleyErrorResponse.VolleyError(this@GooglePlaceSearch, error)
        })
        postrequest!!.retryPolicy = DefaultRetryPolicy(
            20000,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        postrequest!!.setShouldCache(false)
        AppController.instance!!.addToRequestQueue(postrequest!!)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    companion object {
        lateinit var Et_search: EditText
    }
}
