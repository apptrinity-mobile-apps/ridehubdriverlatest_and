package com.project.ridehubdriver.Activities

import android.app.Dialog
import android.os.Bundle
import android.view.Window
import androidx.annotation.Nullable
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Hockeyapp.ActionBarActivityHockeyApp
import com.project.ridehubdriver.R

/**
 */
open class BaseActivity : ActionBarActivityHockeyApp(), ServiceConstant {
    private var dialog: Dialog? = null

    protected override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun showDialog(message: String) {
        dialog = Dialog(this)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
    }

    fun dismissDialog() {
        try {
            if (dialog != null)
                dialog!!.dismiss()
        } catch (ex: IllegalArgumentException) {
            ex.printStackTrace()
        }

    }
}
