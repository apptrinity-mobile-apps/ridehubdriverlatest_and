package com.project.ridehubdriver.Activities

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AlphaAnimation
import android.widget.AdapterView
import android.widget.LinearLayout
import android.widget.ListView
import android.widget.RelativeLayout
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.android.volley.Request
import com.project.ridehubdriver.Adapters.HomeMenuListAdapter
import com.project.ridehubdriver.Fragments.*
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceManager
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.SessionManager
import java.util.*

class NavigationDrawer : BaseActivity() {
    internal lateinit var actionBarDrawerToggle: ActionBarDrawerToggle
    private var mDrawerList: ListView? = null
    internal lateinit var dialog: Dialog
    private var title: Array<String>? = null
    private var icon: IntArray? = null
    private var session: SessionManager? = null

    private var isInternetPresent: Boolean? = false
    //internal var bookmyride: Fragment = Fragment_HomePage()

    lateinit var mFragmentManager: FragmentManager

    @SuppressLint("NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.activity_navigationdrawer)
        session = SessionManager(this@NavigationDrawer)
        context = applicationContext
        drawerLayout = findViewById(R.id.navigation_drawer)
        mDrawer = findViewById(R.id.drawer)
        mDrawerList = findViewById(R.id.drawer_listview)

        mFragmentManager = supportFragmentManager

        if (savedInstanceState == null) {
            val tx = supportFragmentManager.beginTransaction()
            tx.replace(R.id.content_frame, DashBoardDriver())
            tx.commit()
        }

        actionBarDrawerToggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            R.string.app_name,
            R.string.app_name
        )
        drawerLayout.setDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.syncState()

        title = arrayOf(
            "username",
            resources.getString(R.string.nav_home),
            resources.getString(R.string.nav_leasebooking),
            resources.getString(R.string.nav_tripsummary),
            resources.getString(R.string.nav_bankaccount),
            resources.getString(R.string.nav_paymentdetails),
            resources.getString(R.string.nav_referpoints),
            resources.getString(R.string.nav_inviteearn),

            resources.getString(R.string.nav_changepassword),
            resources.getString(R.string.nav_privacypolicy),
            resources.getString(R.string.nav_logout)
        )
        icon = intArrayOf(
            R.drawable.nav_home,
            R.drawable.nav_home,
            R.drawable.offline_ride,
            R.drawable.nav_tripsummary,
            R.drawable.nav_bankaccount,
            R.drawable.nav_paymentdetails,
            R.drawable.nav_referral_points,
            R.drawable.nav_inviteearn,
            R.drawable.nav_changepassword,
            R.drawable.nav_privacy_terms,
            R.drawable.nav_logout
        )
        mMenuAdapter = HomeMenuListAdapter(
            context!!, title!!, icon!!
        )
        mDrawerList!!.adapter =
            mMenuAdapter
        mMenuAdapter!!.notifyDataSetChanged()

        mDrawerList!!.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                try {

                    Log.e("menu_position", "" + position)
                    when (position) {
                        0 -> {
                            changeFragment(DashBoardDriver())
                        }
                        1 -> {
                            changeFragment(DashBoardDriver())
                        }
                        2 -> {
                            changeFragment(LocationSearchLeaseBooking())
                        }
                        3 -> {
                            changeFragment(TripSummeryList())
                        }
                        4 -> {
                            changeFragment(BankDetails())
                        }
                        5 -> {
                            changeFragment(PaymentDetails())
                        }
                        6 -> {
                            changeFragment(ReferralPointsActivity())
                        }
                        7 -> {
                            changeFragment(InviteAndEarn())
                        }
                        8 -> {
                            changeFragment(ChangePassWord())
                        }
                        9 -> {
                            val browserIntent =
                                Intent(Intent.ACTION_VIEW, Uri.parse("https://ridehub.co/policy"))
                            startActivity(browserIntent)
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        }
                        10 -> {
                            showBackPressedDialog(false)
                        }
                    }
                    mDrawerList!!.setItemChecked(position, true)
                    drawerLayout.closeDrawer(
                        mDrawer!!
                    )

                } catch (e: Exception) {
                    Log.e("NAVIEXCEPTION", e.toString())
                }

            }
    }

    fun logoutFromFacebook() {
        // your sharedPrefrence
        val editor = context!!.getSharedPreferences("CASPreferences", Context.MODE_PRIVATE).edit()
        editor.clear()
        editor.commit()
    }

    companion object {
        internal lateinit var drawerLayout: DrawerLayout
        internal var mDrawer: RelativeLayout? = null
        internal var mMenuAdapter: HomeMenuListAdapter? = null

        private var context: Context? = null

        fun openDrawer() {
            drawerLayout.openDrawer(
                mDrawer!!
            )
        }

        fun disableSwipeDrawer() {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
        }

        fun enableSwipeDrawer() {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
        }


        fun navigationNotifyChange() {
            mMenuAdapter!!.notifyDataSetChanged()
        }
    }


    private fun changeFragment(targetFragment: Fragment) {
        //resideMenu.clearIgnoredViewList()
        val mFragmentTransaction = mFragmentManager.beginTransaction()
        mFragmentTransaction.replace(R.id.content_frame, targetFragment)
        mFragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        mFragmentTransaction.commit()
        /*supportFragmentManager
            .beginTransaction()
            .replace(R.id.content_frame, targetFragment, "fragment")
            .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
            .commit()*/
    }

    override fun onBackPressed() {
        showBackPressedDialog(false)
        //super.onBackPressed()
    }

    private fun showBackPressedDialog(isLogout: Boolean) {
        System.gc()

        val dialogView: View
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater

        val config = resources.configuration
        if (config.smallestScreenWidthDp >= 600) {
            dialogView = inflater.inflate(R.layout.logout_alert, null)
        } else {
            dialogView = inflater.inflate(R.layout.logout_alert, null)
        }

        dialogBuilder.setView(dialogView)
        val rl_yes: LinearLayout = dialogView.findViewById(R.id.layout_logout_yes)
        val rl_no: LinearLayout = dialogView.findViewById(R.id.layout_logout_no)
        val b = dialogBuilder.create()
        b.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        b.show()
        rl_yes.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            rl_yes.startAnimation(buttonClick)
            b.dismiss()
            Log.e("LOGOUT", "" + isLogout)
            if (!isLogout) {
                Log.e("LOGOUTffff", "" + isLogout)
                logout()
            }
            finish()
        }
        rl_no.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            rl_yes.startAnimation(buttonClick)
            b.dismiss()
        }

    }

    private fun logout() {

        // session.logoutUser();
        //showDialog("Logout");
        val jsonParams = HashMap<String, String>()
        val userDetails = session!!.userDetails
        val driverId = userDetails[SessionManager.KEY_DRIVERID]
        val login_time = userDetails[SessionManager.KEY_LOGINTIME]
        val login_id = userDetails[SessionManager.KEY_LOGINID]
        Log.e("LOGOUT", "$driverId--$login_time--$login_id")
        jsonParams["driver_id"] = "" + driverId!!
        jsonParams["device"] = "" + "ANDROID"
        jsonParams["login_time"] = "" + login_time!!
        jsonParams["login_id"] = "" + login_id!!
        val manager = ServiceManager(this, updateAvailablityServiceListener)
        manager.makeServiceRequest(ServiceConstant.LOGOUT_REQUEST, Request.Method.POST, jsonParams)

        val thread = Thread(Runnable {
            session!!.logoutUser()

        })

        thread.start()
    }

    private val updateAvailablityServiceListener = object : ServiceManager.ServiceListener {
        override fun onCompleteListener(`object`: Any) {
            //dismissDialog()
        }

        override fun onErrorListener(error: Any) {
            // dismissDialog()
        }
    }

}


