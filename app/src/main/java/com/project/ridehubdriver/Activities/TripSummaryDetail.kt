package com.project.ridehubdriver.Activities

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AlphaAnimation
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.android.volley.Request
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.PojoResponse.CancelReasonPojo
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.*
import com.project.ridehubdriver.customRatingBar.RotationRatingBar
import com.project.ridehubdriver.subclass.SubclassActivity
import com.squareup.picasso.Picasso
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.json.JSONTokener
import java.text.NumberFormat
import java.util.*

/**
 * Created by user14 on 9/22/2015.
 */
class TripSummaryDetail : SubclassActivity(), OnMapReadyCallback {
    override fun onMapReady(googleMap: GoogleMap?) {
        /* mMap = googleMap
         // Changing map type
         mMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL
         // Showing / hiding your current location
         mMap!!.isMyLocationEnabled = false
         // Enable / Disable zooming controls
         mMap!!.uiSettings.isZoomControlsEnabled = false
         // Enable / Disable my location button
         mMap!!.uiSettings.isMyLocationButtonEnabled = false
         // mMap / Disable Compass icon
         mMap!!.uiSettings.isCompassEnabled = false
         // Enable / Disable Rotate gesture
         mMap!!.uiSettings.isRotateGesturesEnabled = true
         // Enable / Disable zooming functionality
         mMap!!.uiSettings.isZoomGesturesEnabled = true
         mMap!!.isMyLocationEnabled = false
         //--------Disabling the map functionality---------
         mMap!!.uiSettings.setAllGesturesEnabled(false)*/


    }

    private var Rl_layout_amount_status: LinearLayout? = null
    private var Tv_tripdetail_address: TextView? = null
    private var Tv_tripdetail_dropaddress: TextView? = null
    private var Tv_trip_view_rating: TextView? = null
    private var Tv_tripdetail_vehicle_number: TextView? = null
    private var Tv_tripdetail_pickup: TextView? = null
    private var Tv_tripdetail_ride_distance: TextView? = null
    private var Tv_tripdetail_timetaken: TextView? = null
    private var Tv_tripdetail_waitingtime: TextView? = null
    private var Tv_tripdetail_total_paid: TextView? = null
    private var Tv_tripdetail_payment_type: TextView? = null
    private var Tv_tripdetail_total_amount_paid: TextView? = null
    private var Tv_trip_status: TextView? = null
    private var Tv_trip_paid_status: TextView? = null
    private var ll_payment_data: LinearLayout? = null
    private var Tv_trip_detail_view_discount: TextView? = null
    private var Tv_trip_detail_view_servicetax: TextView? = null
    private var ridedetail_ratingbar: RotationRatingBar? = null
    internal lateinit var dialog: Dialog
    private var sCurrencySymbol: String? = ""
    private var Str_ride_distance = ""
    private var Str_time_taken = ""
    private var Str_wait_time = ""
    private var Str_totalpaid = ""
    private var Str_totalbill: String? = null
    private var Strtotalbill: String? = null
    private var mRequest: ServiceRequest? = null
    private var Str_continue_ridedetail = ""
    private var Str_rideId: String? = ""
    private var driver_id: String? = ""
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private val context: Context? = null
    private var session: SessionManager? = null
    private var Bt_Cancel_ride: TextView? = null
    private var Bt_Continue_Ride: TextView? = null
    private var Bt_RequestPayment: TextView? = null
    private var mMap: GoogleMap? = null
    internal var gps: GPSTracker? = null
    private var Str_lattitude = ""
    private var Str_logitude = ""
    private var strlat: Double = 0.toDouble()
    private var strlon: Double = 0.toDouble()
    private var Str_loctionaddress = ""
    private var Str_drop = ""
    private var Str_Username = ""
    private var Str_useremail = ""
    private var Str_pickup_date = ""
    private var Str_phoneno = ""
    private var Str_pickup_time = ""
    private var Str_userimg = ""
    private var Str_userrating = ""
    private var Str_vehiclenumber = ""
    private var Str_rideid = ""
    private var Str_pickuplocation = ""
    private var Str_pickup_lat = ""
    private var Str_pickup_long = ""
    private var sUserID = ""
    private var Str_drop_lat = ""
    private var Str_drop_lon = ""
    private var Str_drop_location = ""
    private var Cancelreason_arraylist: ArrayList<CancelReasonPojo>? = null
    private var Str_currency = ""
    private var Str_payment_type = ""
    private var iv_trip_summary_menu: ImageView? = null
    private var iv_rider_img: ImageView? = null
    private var tv_rider_cab_type: TextView? = null
    private var tv_rider_name: TextView? = null
    private var tv_rider_vehicle_no: TextView? = null
    private var tv_rider_vehicle_type: TextView? = null
    private var iv_route_map: ImageView? = null
    private var trip_detail_view_tips: TextView? = null
    private var trip_detail_view_wallet: TextView? = null
    private var ll_tips: LinearLayout? = null
    private var ll_wallet: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_trip_summary_detailed)
        initialize()
        // initilizeMap()
        iv_trip_summary_menu!!.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
        Bt_Cancel_ride!!.setOnClickListener {
            val intent = Intent(this@TripSummaryDetail, CancelTrip::class.java)
            intent.putExtra("RideId", Str_rideId)
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
        Bt_RequestPayment!!.setOnClickListener { showfaresummerydetails() }

        Bt_Continue_Ride!!.setOnClickListener {
            if (Str_continue_ridedetail.equals("arrived", ignoreCase = true)) {
                val intent = Intent(this@TripSummaryDetail, ArrivedTrip::class.java)
                intent.putExtra("address", Str_loctionaddress)
                intent.putExtra("rideId", Str_rideId)
                intent.putExtra("pickuplat", Str_pickup_lat)
                intent.putExtra("pickup_long", Str_pickup_long)
                intent.putExtra("username", Str_Username)
                intent.putExtra("userrating", Str_userrating)
                intent.putExtra("phoneno", Str_phoneno)
                intent.putExtra("userimg", Str_userimg)
                intent.putExtra("UserId", sUserID)
                intent.putExtra("drop_lat", Str_drop_lat)
                intent.putExtra("drop_lon", Str_drop_lon)
                intent.putExtra("drop_location", Str_drop_location)
                intent.putExtra("bundle_notification_id", "")
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            } else if (Str_continue_ridedetail.equals("begin", ignoreCase = true)) {
                val intent = Intent(this@TripSummaryDetail, BeginTrip::class.java)
                intent.putExtra("user_name", Str_Username)
                intent.putExtra("user_phoneno", Str_phoneno)
                intent.putExtra("user_image", Str_userimg)
                intent.putExtra("rideid", Str_rideId)
                intent.putExtra("UserId", sUserID)
                intent.putExtra("DropLatitude", Str_drop_lat)
                intent.putExtra("DropLongitude", Str_drop_lon)
                intent.putExtra("drop_location", Str_drop_location)
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            } else if (Str_continue_ridedetail.equals("end", ignoreCase = true)) {
                val intent = Intent(this@TripSummaryDetail, EndTrip_EnterDetails::class.java)
                intent.putExtra("user_name", Str_Username)
                intent.putExtra("user_phoneno", Str_phoneno)
                intent.putExtra("user_image", Str_userimg)
                intent.putExtra("rideid", Str_rideId)
                intent.putExtra("pickuptime", Str_pickup_date)
                intent.putExtra("pickup_location", Str_pickuplocation)
                intent.putExtra("pickup_lat", Str_pickup_lat)
                intent.putExtra("pickup_long", Str_pickup_long)
                intent.putExtra("drop_location", Str_drop_location)
                intent.putExtra("DropLatitude", Str_drop_lat)
                intent.putExtra("DropLongitude", Str_drop_lon)
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            } else {
                val intent = Intent(this@TripSummaryDetail, EndTrip::class.java)
                intent.putExtra("name", Str_Username)
                intent.putExtra("rideid", Str_rideId)
                intent.putExtra("mobilno", Str_phoneno)
                intent.putExtra("UserId", sUserID)
                intent.putExtra("drop_lat", Str_drop_lat)
                intent.putExtra("drop_lon", Str_drop_lon)
                intent.putExtra("drop_location", Str_drop_location)
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }
        }

    }

    private fun initialize() {
        session = SessionManager(this@TripSummaryDetail)
        // get user data from session
        val user = session!!.userDetails
        driver_id = user[SessionManager.KEY_DRIVERID]
        Cancelreason_arraylist = ArrayList()
        val i = intent
        Str_rideId = i.getStringExtra("ride_id")
        println("rideid----------------" + Str_rideId!!)
        Tv_trip_paid_status = findViewById(R.id.payment_paid_Textview_tripdetail)
        ll_payment_data = findViewById(R.id.ll_receipt)
        Tv_trip_status = findViewById(R.id.tripdetail_status)
        Tv_tripdetail_total_amount_paid =
            findViewById(R.id.tripdetail_view_total_paid_amount)
        Tv_tripdetail_total_paid = findViewById(R.id.trip_detail_view_total_amount)
        Tv_tripdetail_payment_type = findViewById(R.id.trip_detail_view_paymenttype)
        Tv_trip_detail_view_discount = findViewById(R.id.trip_detail_view_discount)
        Tv_trip_detail_view_servicetax = findViewById(R.id.trip_detail_view_servicetax)
        Tv_tripdetail_ride_distance = findViewById(R.id.trip_detail_distancekm)
        Tv_tripdetail_timetaken = findViewById(R.id.tripdetail_timetaken_value)
        Tv_tripdetail_waitingtime = findViewById(R.id.tripdetail_wait_time_value)
        Bt_Continue_Ride = findViewById(R.id.trip_summerydetail_continue_ride_button)
        Bt_Cancel_ride = findViewById(R.id.trip_summerydetail_cancelride_button)
        Bt_RequestPayment = findViewById(R.id.trip_summerydetail_requestpayment_button)
        Tv_tripdetail_pickup = findViewById(R.id.trip_view_pickupdates)
        Tv_trip_view_rating = findViewById(R.id.trip_view_rating)
        ridedetail_ratingbar = findViewById(R.id.ridedetail_ratingbar)
        Tv_tripdetail_vehicle_number = findViewById(R.id.trip_view_vehicle_number)
        Tv_tripdetail_address = findViewById(R.id.Tv_tripsummery_view_address)
        Tv_tripdetail_dropaddress = findViewById(R.id.Tv_tripsummery_view_dropaddress)
        Rl_layout_amount_status =
            findViewById(R.id.layout_tripdetail_payment_status)
        iv_trip_summary_menu = findViewById(R.id.iv_trip_summary_menu)
        iv_rider_img = findViewById(R.id.iv_rider_img)
        tv_rider_cab_type = findViewById(R.id.tv_rider_cab_type)
        tv_rider_name = findViewById(R.id.tv_rider_name)
        tv_rider_vehicle_no = findViewById(R.id.tv_rider_vehicle_no)
        tv_rider_vehicle_type = findViewById(R.id.tv_rider_vehicle_type)
        iv_route_map = findViewById(R.id.iv_route_map)
        trip_detail_view_tips = findViewById(R.id.trip_detail_view_tips)
        ll_tips = findViewById(R.id.ll_tips)
        ll_wallet = findViewById(R.id.ll_wallet)
        trip_detail_view_wallet = findViewById(R.id.trip_detail_view_wallet)

        cd = ConnectionDetector(this@TripSummaryDetail)
        isInternetPresent = cd!!.isConnectingToInternet

        if (isInternetPresent!!) {
            postRequest_tripdetail(ServiceConstant.tripsummery_view_url)
        } else {
            Alert(
                resources.getString(R.string.alert_sorry_label_title),
                resources.getString(R.string.alert_nointernet)
            )
        }
    }


    //-------------------Show Summery fare  Method--------------------
    private fun showfaresummerydetails() {

        val dialog = Dialog(this@TripSummaryDetail)
        val view = LayoutInflater.from(this@TripSummaryDetail)
            .inflate(R.layout.fare_summary, null)

        val Tv_reqest = view.findViewById(R.id.requst) as TextView
        val tv_fare_totalamount = view.findViewById(R.id.fare_summery_total_amount) as TextView
        val tv_ridedistance = view.findViewById(R.id.fare_summery_ride_distance_value) as TextView
        val tv_timetaken = view.findViewById(R.id.fare_summery_ride_timetaken_value) as TextView
        val tv_waittime = view.findViewById(R.id.fare_summery_wait_time_value) as TextView
        val layout_request_payment =
            view.findViewById(R.id.layout_faresummery_requstpayment) as RelativeLayout
        val layout_receive_cash =
            view.findViewById(R.id.fare_summery_receive_cash_layout) as RelativeLayout

        tv_fare_totalamount.text = Str_totalbill
        tv_ridedistance.text = Str_ride_distance
        tv_timetaken.text = Str_time_taken
        tv_waittime.text = Str_wait_time
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setContentView(view)
        dialog.show()

        layout_receive_cash.visibility = View.VISIBLE
        layout_request_payment.visibility = View.VISIBLE
        Tv_reqest.text =
            this@TripSummaryDetail.resources.getString(R.string.lbel_fare_summery_requestpayment)

        layout_receive_cash.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            layout_receive_cash.startAnimation(buttonClick)
            val intent = Intent(this@TripSummaryDetail, OtpPage::class.java)
            intent.putExtra("rideid", Str_rideId)
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        layout_request_payment.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            layout_request_payment.startAnimation(buttonClick)
            cd = ConnectionDetector(this@TripSummaryDetail)
            isInternetPresent = cd!!.isConnectingToInternet
            if (isInternetPresent!!) {

                if (Tv_reqest.text.toString().equals(
                        this@TripSummaryDetail.resources.getString(R.string.lbel_fare_summery_requestpayment),
                        ignoreCase = true
                    )
                ) {
                    postRequest_Reqqustpayment_TripDetail(ServiceConstant.request_paymnet_url)
                    println("arrived------------------" + ServiceConstant.request_paymnet_url)
                } else {
                    val intent = Intent(this@TripSummaryDetail, RatingsPage::class.java)
                    startActivity(intent)
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                }
            } else {
                Alert(
                    resources.getString(R.string.alert_sorry_label_title),
                    resources.getString(R.string.alert_nointernet)
                )
            }
        }

    }

    private fun initilizeMap() {
        /*if (googleMap == null) {
            googleMap =
                (this@TripSummaryDetail.fragmentManager.findFragmentById(R.id.tripsummery_view_map) as MapFragment).getMap()
            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(
                    this@TripSummaryDetail,
                    "Sorry! unable to create maps",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }*/
        if (mMap == null) {
            val mMap = supportFragmentManager
                .findFragmentById(R.id.tripsummery_view_map) as SupportMapFragment
            mMap.getMapAsync(this)

            if (mMap == null) {
            }
        }
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(this@TripSummaryDetail)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            resources.getString(R.string.alert_label_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    //-----------------------Change Post Request-----------------
    private fun postRequest_tripdetail(Url: String) {
        dialog = Dialog(this@TripSummaryDetail)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------dashboard----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id!!
        jsonParams["ride_id"] = Str_rideId!!

        println("driver_id--------------" + driver_id!!)
        println("ride_id--------------" + Str_rideId!!)

        mRequest = ServiceRequest(this@TripSummaryDetail)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {

                override fun onCompleteListener(response: String) {

                    println("-------------tripdetail----------------$response")
                    var Sstatus = ""
                    var Str_ridestatus = ""
                    var Str_cab_type = ""
                    var Str_coupon_code = ""
                    var Str_cancel = ""
                    var Str_wallet_usage = ""
                    var trip_paid_status = ""
                    var Str_continu_ride = ""
                    var Str_service_tax = ""
                    var Distance_unit = ""
                    val currencycode: Currency? = null
                    var Str_tipStatus = ""
                    var Str_tipAmount = ""
                    var Str_image = ""
                    var Str_driver_rating = ""

                    try {
                        val `object` = JSONObject(response)

                        Sstatus = `object`.getString("status")
                        println("status-----------$Sstatus")

                        val jobject = `object`.getJSONObject("response")
                        if (jobject.length() > 0) {
                            val jsonObject = jobject.getJSONObject("details")

                            if (jsonObject.length() > 0) {
                                Str_currency = jsonObject.getString("currency")
                                Str_payment_type = jsonObject.getString("payment_type")
                                //   currencycode = Currency.getInstance(getLocale(Str_currency));

                                sCurrencySymbol =
                                    CurrencySymbolConverter.getCurrencySymbol(Str_currency)

                                Str_ridestatus = jsonObject.getString("ride_status")
                                Str_rideid = jsonObject.getString("ride_id")
                                Str_cab_type = jsonObject.getString("cab_type")
                                trip_paid_status = jsonObject.getString("pay_status")
                                Str_cancel = jsonObject.getString("do_cancel_action")
                                Str_continue_ridedetail = jsonObject.getString("continue_ride")
                                Distance_unit = jsonObject.getString("distance_unit")

                                sUserID = jsonObject.getString("user_id")


                                println("sUserID------------$sUserID" + "---------" + Str_ridestatus)

                                println("BEFORE--------------$Str_ridestatus")
                                if (Str_ridestatus.equals("Cancelled", ignoreCase = true)) {
                                    println("AFTER--------------$Str_ridestatus")
                                    Tv_trip_paid_status!!.text = "Cancelled"
                                    Tv_trip_paid_status!!.setBackgroundResource(R.drawable.curve_bg_red_rhub)
                                    Tv_trip_paid_status!!.setTextColor(resources.getColor(R.color.white))
                                    ll_payment_data!!.visibility = View.GONE
                                    Tv_tripdetail_ride_distance!!.text = "0"
                                    Tv_tripdetail_timetaken!!.text = "0"
                                    Tv_tripdetail_waitingtime!!.text = "0"

                                }

                            }

                            val jobject2 = jsonObject.getJSONObject("pickup")

                            if (jobject2.length() > 0) {
                                Str_loctionaddress = jobject2.getString("location")

                                val jobject3 = jobject2.getJSONObject("latlong")
                                Str_lattitude = jobject3.getString("lat")
                                Str_logitude = jobject3.getString("lon")

                                println("lat---------$Str_lattitude")
                                println("lon---------$Str_logitude")

                                strlat = java.lang.Double.parseDouble(Str_lattitude)
                                strlon = java.lang.Double.parseDouble(Str_logitude)

                                println("strlat---------$strlat")
                                println("strlat---------$strlat")

                                //-------------------code for set marker-------------------------
                                /*mMap!!.addMarker(
                                    MarkerOptions()
                                        .position(LatLng(strlat, strlon))
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_icon_green))
                                )
                                // Move the camera to last position with a zoom level
                                val cameraPosition =
                                    CameraPosition.Builder().target(LatLng(strlat, strlon))
                                        .zoom(12f).build()
                                mMap!!.animateCamera(
                                    CameraUpdateFactory.newCameraPosition(
                                        cameraPosition
                                    )
                                )*/

                            }


                            val dropobject = jsonObject.getJSONObject("drop")

                            if (dropobject.length() > 0) {
                                Str_drop = dropobject.getString("location")

                                val dropobject3 = dropobject.getJSONObject("latlong")
                                Str_lattitude = dropobject3.getString("lat")
                                Str_logitude = dropobject3.getString("lon")

                                println("lat---------$Str_lattitude")
                                println("lon---------$Str_logitude")

                                strlat = java.lang.Double.parseDouble(Str_lattitude)
                                strlon = java.lang.Double.parseDouble(Str_logitude)

                                println("strlat---------$strlat")
                                println("strlat---------$strlat")
                            }

                            Tv_tripdetail_address!!.text = Str_loctionaddress
                            Tv_tripdetail_dropaddress!!.text = Str_drop

                            Str_pickup_date = jsonObject.getString("pickup_date")

                            if (Str_ridestatus.equals(
                                    "Completed",
                                    ignoreCase = true
                                ) || Str_ridestatus.equals("Finished", ignoreCase = true)
                            ) {

                                val jobject4 = jsonObject.getJSONObject("summary")
                                if (jobject4.length() > 0) {
                                    Str_ride_distance = jobject4.getString("ride_distance")
                                    Str_time_taken = jobject4.getString("ride_duration")
                                    Str_wait_time = jobject4.getString("waiting_duration")
                                }

                                val jobject5 = jsonObject.getJSONObject("fare")
                                if (jobject5.length() > 0) {
                                    Strtotalbill =
                                        sCurrencySymbol!! + jobject5.getString("total_bill")
                                    Str_totalbill =
                                        sCurrencySymbol!! + jobject5.getString("grand_bill")
                                    Str_service_tax =
                                        sCurrencySymbol!! + jobject5.getString("service_tax")
                                    Str_totalpaid =
                                        sCurrencySymbol!! + jobject5.getString("total_paid")
                                    Str_coupon_code = jobject5.getString("coupon_discount")
                                    Str_wallet_usage =
                                        sCurrencySymbol!! + jobject5.getString("wallet_usage")
                                }


                                val tips_object = jsonObject.getJSONObject("tips")
                                if (tips_object.length() > 0) {
                                    Str_tipStatus = tips_object.getString("tips_status")
                                    Str_tipAmount =
                                        sCurrencySymbol!! + tips_object.getString("tips_amount")
                                }

                            }


                            val data = jobject.getString("user_profile")
                            if (data.equals("") || data.equals(null) || data.equals("null") || data.equals(
                                    "[]"
                                )
                            ) {
                                dialog.dismiss()
                            } else {
                                val json = JSONTokener(data).nextValue()
                                if (json is JSONObject) {
                                    val jobject_profile = JSONObject(data)
                                    Str_useremail = jobject_profile.getString("user_email")
                                    Str_Username = jobject_profile.getString("user_name")
                                    Str_phoneno = jobject_profile.getString("phone_number")
                                    Str_userimg = jobject_profile.getString("user_image")
                                    Str_userrating = jobject_profile.getString("user_review")
                                    Str_vehiclenumber = jobject_profile.getString("vehicle_number")
                                    Str_rideid = jobject_profile.getString("ride_id")
                                    Str_pickuplocation =
                                        jobject_profile.getString("pickup_location")
                                    Str_pickup_lat = jobject_profile.getString("pickup_lat")
                                    Str_pickup_long = jobject_profile.getString("pickup_lon")
                                    Str_pickup_time = jobject_profile.getString("pickup_time")

                                    Str_drop_location = jobject_profile.getString("drop_loc")
                                    Str_drop_lat = jobject_profile.getString("drop_lat")
                                    Str_drop_lon = jobject_profile.getString("drop_lon")
                                    Str_image = jobject_profile.getString("image")
                                    Str_driver_rating = jobject_profile.getString("driver_rating")

                                    println("phone-----------------$Str_phoneno")
                                    println("pickup_long-----------------$Str_pickup_long")
                                    println("pickup_lat-----------------$Str_pickup_lat")
                                    println("username-----------------$Str_Username")

                                } else if (json is JSONArray) {
                                }
                            }

                            Str_continu_ride = jsonObject.getString("continue_ride")
                        }

                        if (Sstatus.equals("1", ignoreCase = true)) {
                            //Tv_tripdetail_address!!.text = Str_loctionaddress
                            Tv_tripdetail_payment_type!!.text = Str_payment_type
                            //Tv_tripdetail_dropaddress!!.text = Str_drop_location
                            Tv_tripdetail_pickup!!.text = Str_pickup_date
                            if (Str_driver_rating == "null" || Str_driver_rating == "") {
                                ridedetail_ratingbar!!.visibility = View.GONE
                            } else {
                                ridedetail_ratingbar!!.visibility = View.VISIBLE
                                Tv_trip_view_rating!!.text = Str_driver_rating
                                ridedetail_ratingbar!!.rating =
                                    java.lang.Float.parseFloat(Str_driver_rating)
                            }

                            Tv_tripdetail_vehicle_number!!.text = Str_vehiclenumber
                            Tv_tripdetail_total_paid!!.text = Strtotalbill
                            Tv_trip_detail_view_discount!!.text = Str_coupon_code
                            Tv_trip_detail_view_servicetax!!.text = Str_service_tax
                            Tv_tripdetail_total_amount_paid!!.text = Str_totalpaid

                            trip_detail_view_tips!!.text = Str_tipAmount
                            trip_detail_view_wallet!!.text = Str_wallet_usage

                            Picasso.with(context)
                                .load(Str_userimg)
                                .placeholder(R.drawable.no_profile_image_avatar_icon)
                                .into(iv_rider_img)

                            if (Str_image != "null") {
                                Picasso.with(context)
                                    .load("https://ridehub.co/$Str_image")
                                    .placeholder(R.drawable.bg_payment_map_2)
                                    .into(iv_route_map)
                            }

                            tv_rider_cab_type!!.text = Str_cab_type
                            tv_rider_name!!.text = Str_Username
                            tv_rider_vehicle_no!!.text = Str_vehiclenumber
                            tv_rider_vehicle_type!!.text = ""

                            /*  Tv_tripdetail_rideId!!.text = Str_rideid*/
                            Tv_trip_status!!.text =
                                resources.getString(R.string.tripsummery_add_Ride_label) + " " + Str_ridestatus
                            Tv_trip_paid_status!!.text =
                                resources.getString(R.string.tripsummery_add_Payment_label) + " " + trip_paid_status

                            Tv_tripdetail_ride_distance!!.text = "$Str_ride_distance $Distance_unit"
                            Tv_tripdetail_timetaken!!.text =
                                Str_time_taken + " " + resources.getString(R.string.tripsummery_add_mins_label)
                            Tv_tripdetail_waitingtime!!.text =
                                Str_wait_time + " " + resources.getString(R.string.tripsummery_add_mins_label)

                            //----------------------code for ride details---------------------
                            if (Str_ridestatus.equals(
                                    "Completed",
                                    ignoreCase = true
                                ) || Str_ridestatus.equals("Finished", ignoreCase = true)
                            ) {
                                // layout_address_and_loction_details!!.visibility = View.VISIBLE
                                //  layout_completed_details!!.visibility = View.VISIBLE
                                Rl_layout_amount_status!!.visibility = View.VISIBLE
                                if (Str_tipStatus == "1") {
                                    ll_tips!!.visibility = View.VISIBLE
                                } else {
                                    ll_tips!!.visibility = View.GONE
                                }

                                if (Str_wallet_usage != "0") {
                                    ll_wallet!!.visibility = View.VISIBLE
                                } else {
                                    ll_wallet!!.visibility = View.GONE
                                }
//                                Rl_layout_pickup_details!!.visibility = View.VISIBLE

                                //--------------code for discount and wallet usage------------
                                /* if (Str_wallet_usage.length > 0) {
                                     Tv_wallet_uage!!.visibility = View.GONE
                                     Tv_wallet_uage!!.text =
                                         resources.getString(R.string.cabily_wallet_used_label) + "  " + Str_wallet_usage
                                 } else {
                                     Tv_wallet_uage!!.visibility = View.GONE
                                 }

                                 if (Str_coupon_code.length > 0) {
                                     Tv_coupon_discount!!.visibility = View.VISIBLE
                                     Tv_coupon_discount!!.visibility = View.GONE
                                     Tv_coupon_discount!!.text =
                                         "Coupon discount used$Str_coupon_code"
                                 }

                                 if (Str_tipStatus.equals("0", ignoreCase = true)) {
                                     Tv_driverTip!!.visibility = View.GONE
                                 } else {
                                     Tv_driverTip!!.visibility = View.GONE
                                     Tv_driverTip!!.text =
                                         resources.getString(R.string.cabily_driver_tip_amount) + " " + Str_tipAmount
                                 }*/

                            } else {
                                Rl_layout_amount_status!!.visibility = View.GONE
                                // layout_address_and_loction_details!!.visibility = View.VISIBLE
//                                Rl_layout_pickup_details!!.visibility = View.VISIBLE
                            }

                            //-----------------code to visible Request payment button---------------
                            if (Str_ridestatus.equals("Finished", ignoreCase = true)) {
                                println("ridestatus--------------$Str_ridestatus")
                                Bt_RequestPayment!!.visibility = View.VISIBLE
                            } else {
                                Bt_RequestPayment!!.visibility = View.GONE
                            }

                            //------------code for continue ride---------------
                            if (Str_continue_ridedetail.equals("arrived", ignoreCase = true)) {
                                Bt_Continue_Ride!!.visibility = View.VISIBLE
                                Bt_Cancel_ride!!.visibility = View.VISIBLE
                            } else if (Str_continue_ridedetail.equals("begin", ignoreCase = true)) {
                                Bt_Continue_Ride!!.visibility = View.VISIBLE
                                Bt_Cancel_ride!!.visibility = View.VISIBLE
                            } else if (Str_continue_ridedetail.equals("end", ignoreCase = true)) {
                                Bt_Continue_Ride!!.visibility = View.VISIBLE
                                Bt_Cancel_ride!!.visibility = View.GONE
                            } else {
                                Bt_Continue_Ride!!.visibility = View.GONE
                                // Rl_layout_drop_details.setVisibility(View.GONE);
                            }

                            //---------code for cancel ride----------
                            if (Str_cancel.equals("1", ignoreCase = true)) {
                                Bt_Cancel_ride!!.visibility = View.VISIBLE
                            } else {
                                Bt_Cancel_ride!!.visibility = View.GONE
                            }

                        } else {
                            Alert(
                                resources.getString(R.string.alert_sorry_label_title),
                                resources.getString(R.string.fetchdatatoast)
                            )
                        }

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    dialog.dismiss()
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }

            })

    }

    //-----------------------Code for post request-----------------
    private fun postRequest_Reqqustpayment_TripDetail(Url: String) {
        dialog = Dialog(this@TripSummaryDetail)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()
        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)
        println("-------------trip----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id!!
        jsonParams["ride_id"] = Str_rideId!!
        println("--------------driver_id-------------------" + driver_id!!)
        println("--------------ride_id-------------------" + Str_rideId!!)
        mRequest = ServiceRequest(this@TripSummaryDetail)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    Log.e("requestpayment", response)
                    println("response---------$response")
                    var Str_status = ""
                    var Str_response = ""
                    try {
                        val `object` = JSONObject(response)
                        Str_response = `object`.getString("response")
                        Str_status = `object`.getString("status")
                    } catch (e: Exception) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }
                    dialog.dismiss()
                    if (Str_status.equals("0", ignoreCase = true)) {
                        Alert(resources.getString(R.string.alert_sorry_label_title), Str_response)
                    } else {
                        Alert(
                            resources.getString(R.string.label_pushnotification_cashreceived),
                            Str_response
                        )
                    }
                }

                override fun onErrorListener() {
                    dialog.dismiss()
                }
            })

    }

    //method to convert currency code to currency symbol
    private fun getLocale(strCode: String): Locale? {
        for (locale in NumberFormat.getAvailableLocales()) {
            val code = NumberFormat.getCurrencyInstance(locale).currency!!.currencyCode
            if (strCode == code) {
                return locale
            }
        }
        return null
    }
}
