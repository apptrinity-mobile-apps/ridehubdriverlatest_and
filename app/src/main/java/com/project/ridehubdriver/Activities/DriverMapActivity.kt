package com.project.ridehubdriver.Activities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.PictureInPictureParams
import android.content.*
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Point
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.util.Rational
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AlphaAnimation
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.fragment.app.FragmentActivity
import com.android.volley.Request
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.card.MaterialCardView
import com.project.ridehubdriver.Fragments.DashBoardDriver
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceManager
import com.project.ridehubdriver.Helper.xmpp.ChatingService
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.GPSTracker
import com.project.ridehubdriver.Utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class DriverMapActivity : FragmentActivity(), View.OnClickListener, LocationListener,
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
    OnMapReadyCallback {


    // Google Map
    private var mMap: GoogleMap? = null
    private var session: SessionManager? = null
    private var rl_pip: RelativeLayout? = null
    private var dialog: Dialog? = null
    private var Rl_layout_available_status: RelativeLayout? = null
    private var Str_rideId = ""
    private var driver_id: String? = ""
    private var receiver: BroadcastReceiver? = null
    private var gps: GPSTracker? = null
    private var mLocationRequest: LocationRequest? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var result: PendingResult<LocationSettingsResult>? = null
    private val mapHandler = Handler()
    var drivermarker: Marker? = null
    private var MyCurrent_lat = 0.0
    private var MyCurrent_long = 0.0
    var layout_verify_status: RelativeLayout? = null
    var mapFragment: SupportMapFragment? = null
    var go_offline: LinearLayout? = null
    var cv_refresh: MaterialCardView? = null
    var fromPictureInPictureMode:Boolean = false

    /** The arguments to be used for Picture-in-Picture mode.  */
    private var mPictureInPictureParamsBuilder: PictureInPictureParams.Builder? = null

    private val mapRunnable = object : Runnable {
        override fun run() {
            gps = GPSTracker(this@DriverMapActivity)
            if (gps != null && gps!!.canGetLocation()) {
                println("======map handler===========")
                postRequest(ServiceConstant.UPDATE_CURRENT_LOCATION)
            } else {
                enableGpsService()
            }
            mapHandler.postDelayed(this, 30000)
        }
    }

    private val mServiceListener = object : ServiceManager.ServiceListener {
        private var Str_status = ""
        private var Str_availablestaus = ""
        private var Str_message = ""

        override fun onCompleteListener(`object`: Any) {
            try {
                val response = `object` as String
                val jobject = JSONObject(response)

                Str_status = jobject.getString("status")
                if ("1".equals(Str_status, ignoreCase = true)) {
                    val jobject2 = jobject.getJSONObject("response")
                    Str_availablestaus = jobject2.getString("availability")
                    Str_message = jobject2.getString("message")
                    Str_rideId = jobject2.getString("ride_id")
                    println("rideIDDresponse----------$Str_rideId")
                    println("online----------$response")
                    if (Str_availablestaus.equals("Unavailable", ignoreCase = true)) {
                        Rl_layout_available_status!!.visibility = View.VISIBLE
                    } else {
                        Rl_layout_available_status!!.visibility = View.GONE
                    }
                    showVerifyStatus(jobject2)
                }
            } catch (e: Exception) {
            }

        }

        override fun onErrorListener(obj: Any) {

        }
    }

    private val updateAvailabilityServiceListener = object : ServiceManager.ServiceListener {
        override fun onCompleteListener(`object`: Any) {
            try {
                dismissDialog()
                val response = `object` as String
                println("goofflineresponse---------$response")

                val object1 = JSONObject(response)
                if (object1.length() > 0) {
                    val status = object1.getString("status")
                    if (status.equals("1", ignoreCase = true)) {
//                        finish()
                        if (!fromPictureInPictureMode){
                            val intent = Intent(this@DriverMapActivity,NavigationDrawer::class.java)
                            finish()
                            startActivity(intent)
                        }else{
                            finish()
                        }
                    }
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        override fun onErrorListener(obj: Any) {
            dismissDialog()
            finish()
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        val config = resources.configuration
        if (config.smallestScreenWidthDp >= 600) {
            setContentView(R.layout.roadmap)
            layout_verify_status = findViewById<RelativeLayout>(R.id.layout_verify_status)
        } else {
            setContentView(R.layout.roadmap)
        }

        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        session = SessionManager(this@DriverMapActivity)
        gps = GPSTracker(this)
        val user = session!!.userDetails
        driver_id = user[SessionManager.KEY_DRIVERID]
        val refresh_button = findViewById<ImageView>(R.id.refresh)
        refresh_button.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            refresh_button.startAnimation(buttonClick)
            val i = Intent(this@DriverMapActivity, DriverMapActivity::class.java)
            finish()
            startActivity(i)
        }
        val filter = IntentFilter()
        filter.addAction("com.finish.canceltrip.DriverMapActivity")
        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.action == "com.finish.canceltrip.DriverMapActivity") {
                    Rl_layout_available_status!!.visibility = View.GONE
                }
            }
        }
        try {
            registerReceiver(receiver, filter)
        } catch (e: Exception) {

        }

        go_offline = findViewById<LinearLayout>(R.id.go_offline)
        cv_refresh = findViewById<MaterialCardView>(R.id.cv_refresh)
        rl_pip = findViewById<RelativeLayout>(R.id.rl_pip)
        Rl_layout_available_status = findViewById<RelativeLayout>(R.id.layout_available_status)
        Rl_layout_available_status!!.setOnClickListener {
            val intent = Intent(this@DriverMapActivity, TripSummaryDetail::class.java)
            intent.putExtra("ride_id", Str_rideId)
            println("StrRideID---------$Str_rideId")
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        go_offline!!.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            go_offline!!.startAnimation(buttonClick)
            goOffLine()
        }

        try {
            session = SessionManager(this)
            setLocationRequest()
            buildGoogleApiClient()
            initilizeMap()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        initView()

        if (gps != null && gps!!.canGetLocation() && gps!!.isgpsenabled()) {
        } else {
            enableGpsService()
        }
        mapHandler.post(mapRunnable)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mPictureInPictureParamsBuilder = PictureInPictureParams.Builder()
        } else {
            //TODO("VERSION.SDK_INT < O")
        }

    }


    override fun onBackPressed() {
        showBackPressedDialog()
    }

    private fun showBackPressedDialog() {
        val builder = android.app.AlertDialog.Builder(this)
        builder.setMessage(R.string.label_sure_go_offline)
            .setPositiveButton(android.R.string.ok) { dialog, id ->
                if (!fromPictureInPictureMode){
                    val intent = Intent(this,NavigationDrawer::class.java)
                    finish()
                    startActivity(intent)
                }else{
                    finish()
                }
            }
            .setNegativeButton(android.R.string.cancel) { dialog, id ->
                dialog.dismiss()
            }
        val dialog = builder.create()
        dialog.show()
    }

    override fun onStart() {
        super.onStart()

        if (mGoogleApiClient != null)
            mGoogleApiClient!!.connect()
    }

    override fun onStop() {
        super.onStop()
        mapHandler.removeCallbacks(mapRunnable)
        if (mGoogleApiClient != null)
            mGoogleApiClient!!.disconnect()
    }

    override fun onResume() {
        super.onResume()
        mapRunnable.run()
        ChatingService.startDriverAction(this@DriverMapActivity)
        startLocationUpdates()
        if (myLocation == null) {
            myLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient
            )
        }
    }

    private fun initView() {}

    private fun showDialog(message: String) {
        dialog = Dialog(this)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
    }

    fun dismissDialog() {
        dialog!!.dismiss()
    }

    private fun postRequest(url: String) {
        if (myLocation != null) {
            val jsonParams = HashMap<String, String>()
            val userDetails = session!!.userDetails
            val driverId = userDetails["driverid"]
            println("driverId-------------" + driverId!!)
            println("latitude-------------" + myLocation!!.latitude)
            println("longitude-------------" + myLocation!!.longitude)
            jsonParams["driver_id"] = "" + driverId
            jsonParams["latitude"] = "" + myLocation!!.latitude
            jsonParams["longitude"] = "" + myLocation!!.longitude
            val manager = ServiceManager(this@DriverMapActivity, mServiceListener)
            manager.makeServiceRequest(url, Request.Method.POST, jsonParams)
        } else {
            // do nothing
        }
    }


    private fun showVerifyStatus(`object`: JSONObject) {
        try {
            val verify_status = `object`.getString("verify_status")
            if ("No".equals(verify_status, ignoreCase = true)) {
                layout_verify_status!!.visibility = View.VISIBLE
            } else {
                layout_verify_status!!.visibility = View.GONE
            }
        } catch (e: JSONException) {
            layout_verify_status!!.visibility = View.GONE
        }

    }

    private fun initilizeMap() {

        if (mMap == null) {
            mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment?
            mapFragment!!.getMapAsync(this)

            if (mMap == null) {
            }
        }

    }


    override fun onMapReady(googleMap: GoogleMap) {

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            mMap = googleMap
            mMap!!.mapType = R.raw.map_silver_json
            Log.e("MapsActivityRaw", "Can't find style.")

            mMap!!.uiSettings.setAllGesturesEnabled(true)
            mMap!!.uiSettings.isMapToolbarEnabled = false
            mMap!!.isIndoorEnabled = true
            mMap!!.isBuildingsEnabled = true
            mMap!!.uiSettings.isZoomControlsEnabled = true
            mMap!!.isIndoorEnabled = true
            mMap!!.isMyLocationEnabled = false
            mMap!!.uiSettings.isMyLocationButtonEnabled = false
            mMap!!.isBuildingsEnabled = true
            mMap!!.uiSettings.isZoomControlsEnabled = false
            if (gps != null && gps!!.canGetLocation()) {
                val Dlatitude = gps!!.getLatitude()
                val Dlongitude = gps!!.getLongitude()
                MyCurrent_lat = Dlatitude
                MyCurrent_long = Dlongitude
                val cameraPosition =
                    CameraPosition.Builder().target(LatLng(Dlatitude, Dlongitude)).zoom(18f).build()
                mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

            } else {
                /*if (alert_layout != null && alert_textview != null) {
                    alert_layout.visibility = View.VISIBLE
                    alert_textview.text = resources.getString(R.string.alert_gpsEnable)
                }*/
            }

        } catch (e: Resources.NotFoundException) {
            Log.e("MapsActivityRaw", "Can't find style.", e)
        }

    }


    private fun goOffLine() {

        try {
            DashBoardDriver.isOnline = false
            showDialog("")
            val jsonParams = HashMap<String, String>()
            val userDetails = session!!.userDetails
            val LoginDetails = session!!.sessionlogin_details

            val loginid = LoginDetails["loginid"]
            val logintime = LoginDetails["logintime"]
            println("loginid-------------" + loginid!!)
            println("logintime-------------" + logintime!!)

            val driverId = userDetails["driverid"]
            jsonParams["driver_id"] = "" + driverId!!
            jsonParams["availability"] = "" + "No"
            jsonParams["login_id"] = "" + loginid
            jsonParams["login_time"] = "" + logintime
            println("availability-------------" + "No")
            println("offline driver_id-------------$driverId")
            val manager = ServiceManager(this, updateAvailabilityServiceListener)
            // ChatingService.closeConnection();
            manager.makeServiceRequest(
                ServiceConstant.UPDATE_AVAILABILITY,
                Request.Method.POST,
                jsonParams
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = 10000
        mLocationRequest!!.fastestInterval = 5000
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    protected fun startLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient!!.isConnected) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this
            )
        }
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
    }


    override fun onConnected(bundle: Bundle?) {
        if (gps != null && gps!!.canGetLocation() && gps!!.isgpsenabled()) {
        }
        myLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        LocationServices.FusedLocationApi.requestLocationUpdates(
            mGoogleApiClient,
            mLocationRequest,
            this
        )
        if (myLocation != null) {

            drivermarker = mMap!!.addMarker(
                MarkerOptions()
                    .position(LatLng(myLocation!!.latitude, myLocation!!.longitude))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location))
            )
            mMap!!.moveCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(myLocation!!.latitude, myLocation!!.longitude),
                    17f
                )
            )

            postRequest(ServiceConstant.UPDATE_CURRENT_LOCATION)
            println("online------------------" + ServiceConstant.UPDATE_CURRENT_LOCATION)
        }
    }

    override fun onConnectionSuspended(i: Int) {}


    override fun onLocationChanged(location: Location) {

        myLocation = location
        if (myLocation != null) {
            try {
                val latLng = LatLng(myLocation!!.latitude, myLocation!!.longitude)
                if (drivermarker != null) {
                    drivermarker!!.remove()
                }
                drivermarker = mMap!!.addMarker(
                    MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location))
                )
                val zoom = mMap!!.cameraPosition.zoom
                val cameraPosition = CameraPosition.Builder().target(latLng).zoom(zoom).build()
                val camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
                mMap!!.moveCamera(camUpdate)

            } catch (e: Exception) {
            }

        }
    }

    override fun onClick(view: View) {

    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}

    //Enabling Gps Service
    private fun enableGpsService() {
        mLocationRequest = LocationRequest.create()
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest!!.interval = (30 * 1000).toLong()
        mLocationRequest!!.fastestInterval = (5 * 1000).toLong()
        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest!!)
        builder.setAlwaysShow(true)
        result =
            LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())
        result!!.setResultCallback { result ->
            val status = result.status
            //final LocationSettingsStates state = result.getLocationSettingsStates();
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->

                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(this@DriverMapActivity, REQUEST_LOCATION)
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }

                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }
        }
    }


    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_LOCATION -> when (resultCode) {
                Activity.RESULT_OK -> {
                }
                Activity.RESULT_CANCELED -> {
                    enableGpsService()
                }
                else -> {
                }
            }
        }
    }

    override fun onDestroy() {
        // Unregister the logout receiver
        mapHandler.removeCallbacks(mapRunnable)
        unregisterReceiver(receiver)
        super.onDestroy()
    }

    companion object {
        var myLocation: Location? = null
        private val REQUEST_LOCATION = 199
    }


    override fun onRestart() {
        super.onRestart()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                !isInPictureInPictureMode
            }

    }


    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        adjustFullScreen(newConfig)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            adjustFullScreen(resources.configuration)
        }
    }

    override fun onPictureInPictureModeChanged(
        isInPictureInPictureMode: Boolean, newConfig: Configuration
    ) {
        super.onPictureInPictureModeChanged(isInPictureInPictureMode, newConfig)
        if (isInPictureInPictureMode) {
            fromPictureInPictureMode = isInPictureInPictureMode
            // Show the video controls if the video is not playing
            Log.e("INSIDE", "" + isInPictureInPictureMode)
            go_offline!!.visibility = View.GONE
            cv_refresh!!.visibility = View.GONE

        } else {
            fromPictureInPictureMode = isInPictureInPictureMode
            Log.e("INSIDE", "" + isInPictureInPictureMode)
            go_offline!!.visibility = View.VISIBLE
            cv_refresh!!.visibility = View.VISIBLE
        }
    }

    private fun adjustFullScreen(config: Configuration) {
        val decorView = window.decorView
        if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            //mScrollView.visibility = View.GONE
            //rl_signupsignin.setAdjustViewBounds(false)
        } else {
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            // mScrollView.visibility = View.VISIBLE
            // rl_signupsignin.setAdjustViewBounds(true)
        }
    }


    override fun onUserLeaveHint() {
        Log.d("onUserLeaveHint", "Home button pressed")
        println("KEYCODE_HOME")
        minimize()
    }


    internal fun minimize() {
        // Hide the controls in picture-in-picture mode.
        // Calculate the aspect ratio of the PiP screen.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            try {
                val display = windowManager.defaultDisplay
                val point =
                    Point() // Pointer captures height & width of the current screen & reduces the aspect ratio as per required
                display.getSize(point)
                //mPictureInPictureParamsBuilder!!.setAspectRatio(Rational(point.x, point.y))
                val aspectRatio = Rational(rl_pip!!.width, rl_pip!!.height)
                mPictureInPictureParamsBuilder!!.setAspectRatio(aspectRatio).build()
                enterPictureInPictureMode(mPictureInPictureParamsBuilder!!.build())
            } catch (e: Exception) {
                Log.e("PIPEXCEPTION", e.toString())
            }
        } else {
            //TODO("VERSION.SDK_INT < O")
        }


    }
}



