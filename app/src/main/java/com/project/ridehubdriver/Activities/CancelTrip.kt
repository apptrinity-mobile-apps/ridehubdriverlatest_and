package com.project.ridehubdriver.Activities

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.project.ridehubdriver.Adapters.CancelReasonAdapter
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.Hockeyapp.ActivityHockeyApp
import com.project.ridehubdriver.PojoResponse.CancelReasonPojo
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.ConnectionDetector
import com.project.ridehubdriver.Utils.PkDialog
import com.project.ridehubdriver.Utils.SessionManager
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * Created by user88 on 10/28/2015.
 */
class CancelTrip : ActivityHockeyApp() {

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private val context: Context? = null
    private var session: SessionManager? = null
    private var cancel_listview: ListView? = null
    private val listAdapter: ArrayAdapter<String>? = null
    private var Cancelreason_arraylist: ArrayList<CancelReasonPojo>? = null
    private var adapter: CancelReasonAdapter? = null
    private val canceltrip_postrequest: StringRequest? = null
    private var dialog: Dialog? = null
    private var driver_id: String? = null
    private var Tv_Emtytxt: TextView? = null
    private var show_progress_status = false
    private var Str_rideId: String? = null

    private var mRequest: ServiceRequest? = null

    private var Rl_layout_cancel_back: ImageView? = null

    private var Str_reason: String? = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.ridecancel_reasons_dialog)
        initialize()

        Rl_layout_cancel_back!!.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        cancel_listview!!.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                Str_reason = Cancelreason_arraylist!![position].cancelreason_id
                println("reasonm-----------" + Cancelreason_arraylist!![position].cancelreason_id!!)
                cancelTripAlert()
            }

    }

    private fun initialize() {
        session = SessionManager(this@CancelTrip)
        // get user data from session
        val user = session!!.userDetails
        driver_id = user[SessionManager.KEY_DRIVERID]
        Cancelreason_arraylist = ArrayList()
        cancel_listview = findViewById(R.id.cancelreason_listView) as ListView
        Tv_Emtytxt = findViewById(R.id.emtpy_cancelreason) as TextView
        Rl_layout_cancel_back = findViewById(R.id.layouts_cancel_reasons) as ImageView

        val i = intent
        Str_rideId = i.getStringExtra("RideId")


        cd = ConnectionDetector(this@CancelTrip)
        isInternetPresent = cd!!.isConnectingToInternet

        if (isInternetPresent!!) {
            postRequest_Cancelreason(ServiceConstant.ridecancel_reason_url)
        } else {
            Alert(
                resources.getString(R.string.alert_sorry_label_title),
                resources.getString(R.string.alert_nointernet)
            )
        }
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(this@CancelTrip)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            resources.getString(R.string.alert_label_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }

    //--------------------code for cancel reason diaolg--------------------
    fun cancelTripAlert() {
        val cd = ConnectionDetector(this@CancelTrip)
        val isInternetPresent = cd.isConnectingToInternet
        val mDialog = PkDialog(this@CancelTrip)
        mDialog.setDialogTitle(resources.getString(R.string.confirmdelete))
        mDialog.setDialogMessage(resources.getString(R.string.surewanttodelete))

        mDialog.setPositiveButton(resources.getString(R.string.label_yes), View.OnClickListener {
            if (isInternetPresent) {
                postRequest_Cancelride(ServiceConstant.ridecancel_url)
            } else {
                Alert(
                    resources.getString(R.string.alert_sorry_label_title),
                    resources.getString(R.string.alert_nointernet)
                )
            }
            mDialog.dismiss()
        })

        mDialog.setNegativeButton(
            resources.getString(R.string.label_no),
            View.OnClickListener { mDialog.dismiss() })

        mDialog.show()

    }


    //---------------------code for cancel ride-----------------
    private fun postRequest_Cancelreason(Url: String) {
        dialog = Dialog(this@CancelTrip)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------cancel----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id!!

        println("driver_id-------------" + driver_id!!)

        mRequest = ServiceRequest(this@CancelTrip)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {

                override fun onCompleteListener(response: String) {
                    println("-------------cancelreason Response----------------$response")
                    var Sstatus = ""
                    var Str_response = ""
                    try {

                        val jobject = JSONObject(response)
                        Sstatus = jobject.getString("status")

                        if (Sstatus.equals("1", ignoreCase = true)) {
                            val `object` = jobject.getJSONObject("response")
                            val jarry = `object`.getJSONArray("reason")

                            if (jarry.length() > 0) {
                                for (i in 0 until jarry.length()) {

                                    val object1 = jarry.getJSONObject(i)

                                    val items = CancelReasonPojo()

                                    items.reason = object1.getString("reason")
                                    items.cancelreason_id = object1.getString("id")

                                    println("reason----------" + object1.getString("reason"))

                                    Cancelreason_arraylist!!.add(items)

                                }
                                show_progress_status = true

                            } else {
                                show_progress_status = false
                            }
                        } else {
                            Str_response = jobject.getString("response")
                        }

                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    dialog!!.dismiss()

                    if (Sstatus.equals("1", ignoreCase = true)) {
                        println("secnd-----------" + Cancelreason_arraylist!![0].reason!!)
                        adapter = CancelReasonAdapter(this@CancelTrip, Cancelreason_arraylist!!)
                        cancel_listview!!.adapter = adapter

                        if (show_progress_status) {
                            Tv_Emtytxt!!.visibility = View.GONE
                        } else {
                            Tv_Emtytxt!!.visibility = View.VISIBLE
                            cancel_listview!!.emptyView = Tv_Emtytxt
                        }
                    } else {

                        Alert(resources.getString(R.string.alert_sorry_label_title), Str_response)
                    }

                }

                override fun onErrorListener() {

                    dialog!!.dismiss()
                }

            })

    }


    //---------------------code for cancel ride-----------------
    private fun postRequest_Cancelride(Url: String) {
        dialog = Dialog(this@CancelTrip)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------cancelling----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id!!
        jsonParams["ride_id"] = Str_rideId!!
        jsonParams["reason"] = Str_reason!!

        println("ride_id-------------" + Str_rideId!!)
        println("driver_id-------------" + driver_id!!)
        println("reason-------------" + Str_reason!!)

        mRequest = ServiceRequest(this@CancelTrip)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {

                override fun onCompleteListener(response: String) {
                    println("------------- Response----------------$response")
                    var Str_status = ""
                    var Str_message = ""
                    var Str_Id = ""
                    try {
                        val `object` = JSONObject(response)
                        Str_status = `object`.getString("status")
                        val jobject = `object`.getJSONObject("response")
                        Str_message = jobject.getString("message")
                        Str_Id = jobject.getString("ride_id")

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    if (Str_status.equals("1", ignoreCase = true)) {
                        val mdialog = PkDialog(this@CancelTrip)
                        mdialog.setDialogTitle(resources.getString(R.string.action_loading_sucess))
                        mdialog.setDialogMessage(Str_message)
                        mdialog.setPositiveButton(
                            resources.getString(R.string.lbel_notification_ok),
                            View.OnClickListener {
                                mdialog.dismiss()

                                val broadcastIntent = Intent()
                                broadcastIntent.action = "com.finish.ArrivedTrip"
                                sendBroadcast(broadcastIntent)

                                val broadcastIntent_userinfo = Intent()
                                broadcastIntent_userinfo.action = "com.finish.UserInfo"
                                sendBroadcast(broadcastIntent_userinfo)

                                val broadcastIntent_tripdetail = Intent()
                                broadcastIntent_tripdetail.action = "com.finish.tripsummerydetail"
                                sendBroadcast(broadcastIntent_tripdetail)

                                val broadcastIntent_drivermap = Intent()
                                broadcastIntent_drivermap.action =
                                    "com.finish.canceltrip.DriverMapActivity"
                                sendBroadcast(broadcastIntent_drivermap)

                                finish()
                                onBackPressed()
                            }
                        )
                        mdialog.show()

                    } else {
                        Alert(resources.getString(R.string.alert_sorry_label_title), Str_message)
                    }
                    dialog!!.dismiss()

                }

                override fun onErrorListener() {

                    dialog!!.dismiss()
                }

            })

    }

}
