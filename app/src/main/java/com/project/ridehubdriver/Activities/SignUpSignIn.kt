package com.project.ridehubdriver.Activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.Point
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.Log
import android.util.Rational
import android.view.*
import android.view.animation.AlphaAnimation
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.location.*
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.xmpp.ChatingService
import com.project.ridehubdriver.Hockeyapp.ActivityHockeyApp
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.GPSTracker
import com.project.ridehubdriver.Utils.SessionManager
import org.jsoup.Jsoup
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

class SignUpSignIn : ActivityHockeyApp(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {
    override fun onConnected(bundle: Bundle?) {
    }

    private lateinit var mSignIn: LinearLayout
    private lateinit var rl_signupsignin: RelativeLayout
    private lateinit var iv_google: ImageView
    private lateinit var mRegister: LinearLayout
    private var session: SessionManager? = null
    private var gps: GPSTracker? = null
    private var mLocationRequest: LocationRequest? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var result: PendingResult<LocationSettingsResult>? = null
    private val package_name = "com.indobytes.ridehubdriver"
    private val MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 2313
    internal lateinit var dialog: Dialog


    /** The arguments to be used for Picture-in-Picture mode.  */
    private var mPictureInPictureParamsBuilder: PictureInPictureParams.Builder? = null


    /* private void onRequestRunTimePermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }*/
    internal val PERMISSION_REQUEST_CODE = 111


    @SuppressLint("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.activity_signup_register)

        handleSSLHandshake()

        homePage = this@SignUpSignIn

        // onRequestRunTimePermission();
        rl_signupsignin = findViewById(R.id.rl_signupsignin)
        iv_google = findViewById(R.id.iv_google)
        mSignIn = findViewById(R.id.btn_signin)
        mRegister = findViewById(R.id.btn_register)
        val SDK_INT = Build.VERSION.SDK_INT
        if (SDK_INT > 8) {
            val policy = StrictMode.ThreadPolicy.Builder()
                .permitAll().build()
            StrictMode.setThreadPolicy(policy)
        }
        web_update()

        Log.d("update", "stutd===" + web_update())
        if (web_update()) {
            Alert("Update Available", "Do you want to update.?")
        }
        gps = GPSTracker(applicationContext)
        mGoogleApiClient = GoogleApiClient.Builder(this@SignUpSignIn)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this).build()
        mGoogleApiClient!!.connect()
        if (gps!!.isgpsenabled() && gps!!.canGetLocation()) {
            //do nothing
        } else {
            enableGpsService()
        }
        try {

        } catch (e: Exception) {

        }

        session = SessionManager(this@SignUpSignIn)
        session!!.createSessionOnline("0")
        session!!.setRequestCount(0)

        /*iv_google.setOnClickListener {
            minimize()
        }*/

        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkAccessFineLocationPermission() || !checkAccessCoarseLocationPermission() || !checkWriteExternalStoragePermission()) {
                requestPermission()
            } else {
                if (session!!.isLoggedIn) {
                    ChatingService.startDriverAction(this@SignUpSignIn)
                    val i = Intent(applicationContext, NavigationDrawer::class.java)
                    startActivity(i)
                    finish()
                }
            }
        } else {
            if (session!!.isLoggedIn) {
                ChatingService.startDriverAction(this@SignUpSignIn)
                val i = Intent(applicationContext, NavigationDrawer::class.java)
                startActivity(i)
                finish()
            }
        }

        mSignIn.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            mSignIn.startAnimation(buttonClick)
            val i = Intent(applicationContext, LoginPage::class.java)
            startActivity(i)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
        mRegister.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            mRegister.startAnimation(buttonClick)
            if (ContextCompat.checkSelfPermission(
                    this@SignUpSignIn,
                    Manifest.permission.READ_PHONE_STATE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestCameraAndExternalPermission()
            } else {
                val telephonyManager =
                    getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
//                val android_device_id = telephonyManager.deviceId
                val android_device_id =
                    Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
                Log.d("IMEI NUmber", android_device_id)
                val browserIntent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(ServiceConstant.Register_URL + "?device_id=" + android_device_id)
                )
                startActivity(browserIntent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            }
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mPictureInPictureParamsBuilder = PictureInPictureParams.Builder()
        } else {
            //TODO("VERSION.SDK_INT < O")
        }

    }


    internal fun minimize() {
        // Hide the controls in picture-in-picture mode.
        // Calculate the aspect ratio of the PiP screen.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val display = windowManager.defaultDisplay
            val point =
                Point() // Pointer captures height & width of the current screen & reduces the aspect ratio as per required
            display.getSize(point)
            mPictureInPictureParamsBuilder!!.setAspectRatio(Rational(point.x, point.y))

            /* val aspectRatio = Rational(rl_signupsignin.width, rl_signupsignin.height)
             mPictureInPictureParamsBuilder!!.setAspectRatio(aspectRatio).build()*/
            enterPictureInPictureMode(mPictureInPictureParamsBuilder!!.build())
        } else {
            //TODO("VERSION.SDK_INT < O")
        }


    }


    private fun value(_string: String): Long {
        var string = _string
        string = string.trim { it <= ' ' }
        if (string.contains(".")) {
            val index = string.lastIndexOf(".")
            return value(string.substring(0, index)) * 100 + value(string.substring(index + 1))
        } else {
            return java.lang.Long.valueOf(string)
        }
    }

    //--------------------------code to update checker------------------
    private fun web_update(): Boolean {
        try {
            val curVersion =
                this@SignUpSignIn.getPackageManager().getPackageInfo(package_name, 0).versionName

            println("currentversion-----------$curVersion")

            var newVersion = curVersion

            newVersion =
                Jsoup.connect("https://play.google.com/store/apps/details?id=$package_name&hl=en")
                    .timeout(30000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get()
                    .select("div[itemprop=softwareVersion]")
                    .first()
                    .ownText()

            println("Newversion-----------$newVersion")

            return if (value(curVersion) < value(newVersion)) {
                true
            } else {
                false
            }

        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }

    }


    private fun Alert(title: String, message: String) {
        val builder = AlertDialog.Builder(this)
        val inflater = this.getLayoutInflater()
        val dialogView = inflater.inflate(R.layout.custom_dialog_library, null)
        val alert_title =
            dialogView.findViewById(R.id.custom_dialog_library_title_textview) as TextView
        val alert_message =
            dialogView.findViewById(R.id.custom_dialog_library_message_textview) as TextView
        val Bt_action = dialogView.findViewById(R.id.custom_dialog_library_ok_button) as TextView
        Bt_action.text = "Update now"
        val Bt_dismiss =
            dialogView.findViewById(R.id.custom_dialog_library_cancel_button) as TextView
        Bt_dismiss.visibility = View.GONE
        builder.setView(dialogView)
        alert_title.text = title
        alert_message.text = message
        builder.setCancelable(false)
        Bt_action.setOnClickListener {
            dialog.dismiss()
            finish()
            try {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("market://details?id=$package_name")
                    )
                )
            } catch (anfe: android.content.ActivityNotFoundException) {
                startActivity(
                    Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=$package_name")
                    )
                )
            }
        }

        builder.setOnKeyListener { dialog, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP)
                finish()
            false
        }

        dialog = builder.create()
        dialog.show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        return if (id == R.id.action_settings) {
            true
        } else super.onOptionsItemSelected(item)
    }

    override fun onConnectionSuspended(i: Int) {

    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {

    }

    //Enabling Gps Service
    private fun enableGpsService() {
        mLocationRequest = LocationRequest.create()
        mLocationRequest!!.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        mLocationRequest!!.setInterval(30 * 1000)
        mLocationRequest!!.setFastestInterval(5 * 1000)

        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest!!)
        builder.setAlwaysShow(true)

        result =
            LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())

        result!!.setResultCallback(object : ResultCallback<LocationSettingsResult> {
            override fun onResult(result: LocationSettingsResult) {
                val status = result.getStatus()
                //final LocationSettingsStates state = result.getLocationSettingsStates();
                when (status.getStatusCode()) {
                    LocationSettingsStatusCodes.SUCCESS -> {
                    }
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(this@SignUpSignIn, REQUEST_LOCATION)
                        } catch (e: IntentSender.SendIntentException) {
                            // Ignore the error.
                        }

                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                    }
                }// All location settings are satisfied. The client can initialize location
                // requests here.
                //...
                // Location settings are not satisfied. However, we have no way to fix the
                // settings so we won't show the dialog.
                //...
            }
        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_LOCATION -> when (resultCode) {
                Activity.RESULT_OK -> {
                }
                Activity.RESULT_CANCELED -> {
                    finish()
                }
                else -> {
                }
            }
        }
    }


    private fun checkAccessFineLocationPermission(): Boolean {
        val result =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun checkAccessCoarseLocationPermission(): Boolean {
        val result =
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun checkWriteExternalStoragePermission(): Boolean {
        val result =
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ),
            PERMISSION_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (session!!.isLoggedIn) {
                    ChatingService.startDriverAction(this@SignUpSignIn)
                    val i = Intent(applicationContext, NavigationDrawer::class.java)
                    startActivity(i)
                    finish()
                }

            } else {
                finish()
            }
        }
    }

    private fun requestCameraAndExternalPermission() {
        ActivityCompat.requestPermissions(this, PERMISSIONS_SELFIE, REQUEST_SELFIE)
    }

    companion object {
        private val REQUEST_LOCATION = 199
        private val REQUEST_SELFIE = 123
        private val PERMISSIONS_SELFIE = arrayOf(Manifest.permission.READ_PHONE_STATE)

        lateinit var homePage: SignUpSignIn

        /**
         * Enables https connections
         */
        @SuppressLint("TrulyRandom")
        fun handleSSLHandshake() {
            try {
                val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                    override fun getAcceptedIssuers(): Array<X509Certificate?> {
                        return arrayOfNulls(0)
                    }

                    override fun checkClientTrusted(
                        certs: Array<X509Certificate>,
                        authType: String
                    ) {
                    }

                    override fun checkServerTrusted(
                        certs: Array<X509Certificate>,
                        authType: String
                    ) {
                    }
                })

                val sc = SSLContext.getInstance("SSL")
                sc.init(null, trustAllCerts, SecureRandom())
                HttpsURLConnection.setDefaultSSLSocketFactory(sc.socketFactory)
                /*  HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String arg0, SSLSession arg1) {
                    Log.e("arg1",arg0+""+arg1);
                    return true;
                }
            });*/
            } catch (ignored: Exception) {
            }

        }
    }


    override fun onRestart() {
        super.onRestart()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                !isInPictureInPictureMode
            }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        adjustFullScreen(newConfig)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            adjustFullScreen(resources.configuration)
        }
    }

    override fun onPictureInPictureModeChanged(
        isInPictureInPictureMode: Boolean, newConfig: Configuration
    ) {
        super.onPictureInPictureModeChanged(isInPictureInPictureMode, newConfig)
        if (!isInPictureInPictureMode) {
            // Show the video controls if the video is not playing
            Log.e("INSIDE", "" + isInPictureInPictureMode)
        } else {
            Log.e("INSIDE", "" + isInPictureInPictureMode)
        }
    }

    private fun adjustFullScreen(config: Configuration) {
        val decorView = window.decorView
        if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            //mScrollView.visibility = View.GONE
            //rl_signupsignin.setAdjustViewBounds(false)
        } else {
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            // mScrollView.visibility = View.VISIBLE
            // rl_signupsignin.setAdjustViewBounds(true)
        }
    }


    /* override fun onUserLeaveHint() {
         Log.d("onUserLeaveHint", "Home button pressed")
         println("KEYCODE_HOME")
         minimize()
     }*/


    // custom notification
    /*var BUNDLE_NOTIFICATION_ID = 100
    val BUNDLE_CHANNEL_ID = "RIDEHUB_DRIVER_APP"
    val BUNDLE_CHANNEL_NAME = "RIDEHUB_DRIVER"
    private fun showNotification(message: String, bundle_notification_id: String) {
        val contentView =  RemoteViews(packageName, R.layout.custom_notification)
        contentView.setTextViewText(R.id.textView1,"RideHub")
        contentView.setTextViewText(R.id.textView2,"RideHub")
        contentView.setTextViewText(R.id.textView3,"Jason is arriving in WheelChair Taxi Cab555")
        contentView.setImageViewResource(R.id.imageView,R.mipmap.ic_launcher_blue_round)
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val groupChannel = NotificationChannel(
                BUNDLE_CHANNEL_ID,
                BUNDLE_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(groupChannel)
        }
        val intent = Intent()
        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
        val resultPendingIntent =
            PendingIntent.getActivity(
                this@SignUpSignIn,
                BUNDLE_NOTIFICATION_ID,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
        val notificationBuilder = NotificationCompat.Builder(this@SignUpSignIn, BUNDLE_CHANNEL_ID)
            .setGroup(bundle_notification_id)
            .setGroupSummary(true)
            .setSmallIcon(R.mipmap.ic_launcher_blue_round)
            *//*.setContentTitle("Message from rider")
            .setContentText(message)*//*
            .setDefaults(NotificationCompat.DEFAULT_ALL)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setAutoCancel(true)
            .setContent(contentView)
            .setContentIntent(resultPendingIntent)
//            .setTimeoutAfter(1000)

        notificationManager.notify(BUNDLE_NOTIFICATION_ID, notificationBuilder.build())
    }*/


}