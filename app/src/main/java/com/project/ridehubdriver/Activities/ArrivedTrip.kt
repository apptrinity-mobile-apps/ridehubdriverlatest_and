package com.project.ridehubdriver.Activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.*
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.view.animation.AlphaAnimation
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.project.ridehubdriver.Adapters.ContinuousRequestAdapter
import com.project.ridehubdriver.Helper.DBHelper
import com.project.ridehubdriver.Helper.GMapV2GetRouteDirection
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.Helper.xmpp.ChatingService
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.ConnectionDetector
import com.project.ridehubdriver.Utils.GPSTracker
import com.project.ridehubdriver.Utils.PkDialog
import com.project.ridehubdriver.Utils.SessionManager
import com.project.ridehubdriver.subclass.RealTimeActivity
import org.jivesoftware.smack.chat.Chat
import org.json.JSONException
import org.json.JSONObject
import org.w3c.dom.Document
import java.util.*


/**
 * Created by user88 on 10/28/2015.
 */
class ArrivedTrip : RealTimeActivity(), LocationListener, GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, SeekBar.OnSeekBarChangeListener,
    OnMapReadyCallback {


    private var context: Context? = null
    private var session: SessionManager? = null
    private var driver_id: String? = ""
    private var Str_RideId: String? = ""
    private var Str_address: String? = ""
    private var Str_pickUp_Lat: String? = ""
    private var Str_pickUp_Long: String? = ""
    private var Str_username: String? = ""
    private var Str_user_rating: String? = ""
    private var Str_user_phoneno: String? = ""
    private var Str_user_img: String? = ""
    private var Str_droplat: String? = ""
    private var Str_droplon: String? = ""
    private var str_drop_location: String? = ""
    private var str_note_for_driver: String? = ""
    private var str_bundle_notification_id: String? = ""
    private var Tv_Address: TextView? = null
    private val Tv_RideId: TextView? = null
    private var Tv_usename: TextView? = null
    private var Iv_chat_driverinfo: ImageView? = null
    private val ERROR_TAG = "Unknown Error Occured"
    private var Rl_layout_enable_voicenavigation: ImageView? = null
    internal var results: FloatArray? = null
    internal var initialX: Float = 0.toFloat()
    internal var initialY: Float = 0.toFloat()
    // List<Overlay> mapOverlays;
    private val locManager: LocationManager? = null
    internal var drawable: Drawable? = null
    internal lateinit var document: Document
    internal lateinit var v2GetRouteDirection: GMapV2GetRouteDirection
    internal var fromPosition: LatLng? = null
    internal var toPosition: LatLng? = null
    internal lateinit var markerOptions: MarkerOptions
    internal var location: Location? = null
    private val postrequest: StringRequest? = null
    private var dialog: Dialog? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var mMap: GoogleMap? = null
    private var gps: GPSTracker? = null
    private var MyCurrent_lat = 0.0
    private var MyCurrent_long = 0.0
    private var alert_layout: RelativeLayout? = null
    private var alert_textview: TextView? = null
    private var phone_call: ImageView? = null

    private var Str_Latitude = ""
    private var Str_longitude = ""
    internal lateinit var mHandler: Handler
    private var mRequest: ServiceRequest? = null
    private var Suser_Id: String? = ""
    private var mLocationRequest: LocationRequest? = null
    private var mGoogleApiClient: GoogleApiClient? = null

    internal lateinit var result: PendingResult<LocationSettingsResult>
    private var currentMarker: Marker? = null
    internal lateinit var marker: MarkerOptions
    private val latLng: LatLng? = null
    internal var previous_lat: Double = 0.toDouble()
    internal var previous_lon: Double = 0.toDouble()
    internal var current_lat: Double = 0.toDouble()
    internal var current_lon: Double = 0.toDouble()


    //Slider Design Declaration
    internal var sliderSeekBar: SeekBar? = null
    //ShimmerButton Bt_slider; chaitanya
    internal lateinit var Bt_slider: TextView
    internal lateinit var btn_cancel: TextView

    internal val PERMISSION_REQUEST_CODE = 111

    //-----------------------------code for car moving handler------------
    private val arrivedTripHandler = Handler()
    private val count = 0

    val markerWidth = 350
    val markerHeight = 250

    private val arrivedTripRunnable = object : Runnable {
        override fun run() {
            gps = GPSTracker(this@ArrivedTrip)
            if (gps != null && gps!!.canGetLocation()) {
            } else {
                enableGpsService()
            }
            arrivedTripHandler.postDelayed(this, 600)
        }
    }
    internal var chat: Chat? = null


    //--------------code for location updatre--------
    internal var mHandlerTask: Runnable = object : Runnable {
        override fun run() {

            gps = GPSTracker(this@ArrivedTrip)
            cd = ConnectionDetector(this@ArrivedTrip)
            isInternetPresent = cd!!.isConnectingToInternet
            if (isInternetPresent!!) {
                if (gps != null && gps!!.canGetLocation() && gps!!.isgpsenabled()) {

                    Str_Latitude = gps!!.getLatitude().toString()
                    Str_longitude = gps!!.getLongitude().toString()

                    postRequest_UpdateProviderLocation(ServiceConstant.UPDATE_CURRENT_LOCATION)
                }
            } else {
                Toast.makeText(
                    this@ArrivedTrip,
                    resources.getString(R.string.no_internet_connection),
                    Toast.LENGTH_SHORT
                ).show()
            }

            mHandler.postDelayed(this, INTERVAL.toLong())
        }
    }

    internal var mm = MarkerOptions()
    internal var drivermarker: Marker? = null
    internal var job: JSONObject? = JSONObject()
    internal var isDrawnOnMap = false
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    class MessageHandler : Handler() {
        override fun handleMessage(message: Message) {}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // setContentView(R.layout.arrivedtrip);
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        val config = resources.configuration
        if (config.smallestScreenWidthDp >= 600) {
            setContentView(R.layout.arrivedtrip)
        } else {
            setContentView(R.layout.arrivedtrip)
        }

        arrivedTrip_class = this@ArrivedTrip
        initialize()
        try {
            setLocationRequest()
            buildGoogleApiClient()
            initilizeMap()
        } catch (e: Exception) {
        }

        //Starting Xmpp service
        ChatingService.startDriverAction(this@ArrivedTrip)

        Tv_usename!!.setOnClickListener {
            val intent = Intent(this@ArrivedTrip, UserInfo::class.java)
            intent.putExtra("user_name", Str_username)
            intent.putExtra("user_phoneno", Str_user_phoneno)
            intent.putExtra("user_rating", Str_user_rating)
            intent.putExtra("user_image", Str_user_img)
            intent.putExtra("RideId", Str_RideId)
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }


        phone_call!!.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            phone_call!!.startAnimation(buttonClick)
            if (Str_user_phoneno != null) {

                if (Build.VERSION.SDK_INT >= 23) {
                    // Marshmallow+
                    if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                        requestPermission()
                    } else {
                        val callIntent = Intent(Intent.ACTION_CALL)
                        callIntent.data = Uri.parse("tel:" + Str_user_phoneno!!)
                        startActivity(callIntent)
                    }
                } else {
                    val callIntent = Intent(Intent.ACTION_CALL)
                    callIntent.data = Uri.parse("tel:" + Str_user_phoneno!!)
                    startActivity(callIntent)
                }
            } else {
                Alert(
                    this@ArrivedTrip.resources.getString(R.string.alert_sorry_label_title),
                    this@ArrivedTrip.resources.getString(R.string.arrived_alert_content1)
                )
            }
        }


        Rl_layout_enable_voicenavigation!!.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            Rl_layout_enable_voicenavigation!!.startAnimation(buttonClick)
            val voice_curent_lat_long = "$MyCurrent_lat,$MyCurrent_long"
            val voice_destination_lat_long = "$Str_pickUp_Lat,$Str_pickUp_Long"
            println("----------fromPosition---------------$voice_curent_lat_long")
            println("----------toPosition---------------$voice_destination_lat_long")
            val locationUrl =
                "http://maps.google.com/maps?saddr=$voice_curent_lat_long&daddr=$voice_destination_lat_long"
            println("----------locationUrl---------------$locationUrl")
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(locationUrl))
            startActivity(intent)
        }


    }


    private fun initialize() {
        context = this.applicationContext
        session = SessionManager(this@ArrivedTrip)
        gps = GPSTracker(this@ArrivedTrip)
        v2GetRouteDirection = GMapV2GetRouteDirection()

        mHandler = Handler()
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        arrivedTripHandler.post(arrivedTripRunnable)
        val user = session!!.userDetails
        driver_id = user[SessionManager.KEY_DRIVERID]
        alert_textview = findViewById<TextView>(R.id.arrivd_Tripaccpt_alert_textView)
        alert_layout = findViewById<RelativeLayout>(R.id.arrivd_Tripaccpt_alert_layout)
        phone_call = findViewById<ImageView>(R.id.user_phonecall)
        val i = intent
        Str_address = i.getStringExtra("address")
        Str_RideId = i.getStringExtra("rideId")
        Str_pickUp_Lat = i.getStringExtra("pickuplat")
        Str_pickUp_Long = i.getStringExtra("pickup_long")
        Str_username = i.getStringExtra("username")
        Str_user_rating = i.getStringExtra("userrating")
        Str_user_phoneno = i.getStringExtra("phoneno")
        Str_user_img = i.getStringExtra("userimg")
        Str_droplat = i.getStringExtra("drop_lat")
        Str_droplon = i.getStringExtra("drop_lon")
        str_drop_location = i.getStringExtra("drop_location")
        str_note_for_driver = i.getStringExtra("note_for_driver")
        str_bundle_notification_id = i.getStringExtra("bundle_notification_id")
        println("KKKKKKK---------" + Str_droplat!!)
        Suser_Id = i.getStringExtra("UserId")

        if (str_note_for_driver == "" || str_note_for_driver.equals(null) || str_note_for_driver == "null") {

        } else {
            showNoteToDriver(str_note_for_driver!!)
        }

        ContinuousRequestAdapter.userID = Suser_Id.toString()

        println("UserId---------" + Suser_Id!!)
        println("adres---------" + Str_address!!)
        println("id---------" + Str_RideId!!)
        Tv_Address = findViewById<TextView>(R.id.trip_arrived_user_address)
        // Tv_RideId = (TextView) findViewById(R.id.trip_arrived_user_id);
        Tv_usename = findViewById<TextView>(R.id.trip_arrived_usernameTxt)
        Iv_chat_driverinfo = findViewById<ImageView>(R.id.trip_arrived_driverchattextview)
        Rl_layout_enable_voicenavigation = findViewById<ImageView>(R.id.enable_voice_img)

        Bt_slider = findViewById(R.id.arrived_Trip_slider_button)
        btn_cancel = findViewById(R.id.cancel_Trip_slider_button)
        Bt_slider.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            Bt_slider.startAnimation(buttonClick)
            if (str_bundle_notification_id == "") {

            } else {
                // dismiss ongoing notification, if any.
                val manager =
                    applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                manager.cancel(str_bundle_notification_id!!.toInt())
            }

            cd = ConnectionDetector(this@ArrivedTrip)
            isInternetPresent = cd!!.isConnectingToInternet
            if (isInternetPresent!!) {
                PostRequest(ServiceConstant.arrivedtrip_url)
                println("arrived------------------" + ServiceConstant.arrivedtrip_url)
            } else {
                Alert(
                    resources.getString(R.string.alert_sorry_label_title),
                    resources.getString(R.string.alert_nointernet)
                )
            }
        }
        //sliderSeekBar.setOnSeekBarChangeListener(this); chaitanya
        btn_cancel.setOnClickListener {
            val dbHelper = DBHelper(this@ArrivedTrip)
            dbHelper.clearData()
            val buttonClick = AlphaAnimation(1F, 0.8f)
            btn_cancel.startAnimation(buttonClick)
            val intent = Intent(this@ArrivedTrip, CancelTrip::class.java)
            intent.putExtra("RideId", Str_RideId)
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        Tv_Address!!.text = Str_address
        // Tv_RideId.setText(Str_RideId);
        Tv_usename!!.text = Str_username

        cd = ConnectionDetector(this@ArrivedTrip)
        isInternetPresent = cd!!.isConnectingToInternet
        if (isInternetPresent!!) {
            mHandlerTask.run()
        } else {
            Alert(
                resources.getString(R.string.alert_sorry_label_title),
                resources.getString(R.string.alert_nointernet)
            )
        }

        Iv_chat_driverinfo!!.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            Iv_chat_driverinfo!!.startAnimation(buttonClick)
            cd = ConnectionDetector(this@ArrivedTrip)
            isInternetPresent = cd!!.isConnectingToInternet
            if (isInternetPresent!!) {

                val intent = Intent(applicationContext, MainActivity::class.java)
                intent.putExtra("address", Str_address)
                intent.putExtra("rideId", Str_RideId)
                intent.putExtra("pickuplat", Str_pickUp_Lat)
                intent.putExtra("pickup_long", Str_pickUp_Long)
                intent.putExtra("username", Str_username)
                intent.putExtra("userrating", Str_user_rating)
                intent.putExtra("phoneno", Str_user_phoneno)
                intent.putExtra("userimg", Str_user_img)
                intent.putExtra("UserId", Suser_Id)
                intent.putExtra("drop_lat", Str_droplat)
                intent.putExtra("drop_lon", Str_droplon)
                intent.putExtra("drop_location", str_drop_location)
                startActivity(intent)

            } else {
                Alert(
                    resources.getString(R.string.alert_sorry_label_title),
                    resources.getString(R.string.alert_nointernet)
                )
            }
        }
    }

    override fun onBackPressed() {
        //super.onBackPressed();
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(this@ArrivedTrip)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            resources.getString(R.string.alert_label_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }

    private fun showNoteToDriver(message: String) {

        val dialogView: View
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater

        dialogView = inflater.inflate(R.layout.custom_dialog_library, null)
        dialogBuilder.setView(dialogView)
        val tv_yes = dialogView.findViewById(R.id.custom_dialog_library_ok_button) as TextView
        val tv_no = dialogView.findViewById(R.id.custom_dialog_library_cancel_button) as TextView
        val tv_message =
            dialogView.findViewById(R.id.custom_dialog_library_message_textview) as TextView
        val tv_title =
            dialogView.findViewById(R.id.custom_dialog_library_title_textview) as TextView
        val iv_header_img = dialogView.findViewById(R.id.iv_custom_dialog_library) as ImageView
        iv_header_img.visibility = View.GONE
        tv_no.visibility = View.GONE
        tv_message.text = message
        tv_title.text = "Note from Rider"
        val b = dialogBuilder.create()
        b.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        b.show()
        tv_yes.setOnClickListener { b.dismiss() }
        tv_no.setOnClickListener { b.dismiss() }

    }

    private fun initilizeMap() {

        /* val mapFragment = this!!.supportFragmentManager.findFragmentById(R.id.arrived_trip_view_map)
         mapFragment!!.getMapAsync(this)*/

        /* if (mMap == null) {
             val mMap = supportFragmentManager
                 .findFragmentById(R.id.arrived_trip_view_map)
             //mMap!!.getMapAsync(this)

             if (mMap == null) {
             }
         }
 */
        if (mMap == null) {
            val mMap = supportFragmentManager
                .findFragmentById(R.id.arrived_trip_view_map) as SupportMapFragment?
            mMap!!.getMapAsync(this)

            if (mMap == null) {
            }
        }
    }


    override fun onMapReady(googlemap: GoogleMap?) {

        mMap = googlemap
        mMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL
        mMap!!.isMyLocationEnabled = false
        mMap!!.uiSettings.isZoomControlsEnabled = false
        mMap!!.uiSettings.isMyLocationButtonEnabled = false
        mMap!!.uiSettings.isCompassEnabled = false
        mMap!!.uiSettings.isRotateGesturesEnabled = true
        mMap!!.uiSettings.isZoomGesturesEnabled = true

        if (gps != null && gps!!.canGetLocation()) {
            val Dlatitude = gps!!.getLatitude()
            val Dlongitude = gps!!.getLongitude()
            MyCurrent_lat = Dlatitude
            MyCurrent_long = Dlongitude
            val cameraPosition =
                CameraPosition.Builder().target(LatLng(Dlatitude, Dlongitude)).zoom(18f).build()
            mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            println("currntlat----------$MyCurrent_lat")
            println("currntlon----------$MyCurrent_long")
            drawRouteInMap()
        } else {
            alert_layout!!.visibility = View.VISIBLE
            alert_textview!!.text = resources.getString(R.string.alert_gpsEnable)
        }
        markerOptions = MarkerOptions()
    }


    public override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null)
            mGoogleApiClient!!.connect()
    }


    override fun onConnected(bundle: Bundle?) {

        if (gps != null && gps!!.canGetLocation() && gps!!.isgpsenabled()) {
        }
        getLastLocation()
        if (myLocation != null) {
            mMap!!.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(myLocation!!.latitude, myLocation!!.longitude),
                    16f
                )
            )
        }

    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        mFusedLocationClient!!.lastLocation
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful && task.result != null) {
                    myLocation = task.result
                    Log.w(
                        "mLastLocation",
                        "" + myLocation!!.latitude + "--" + myLocation!!.longitude
                    )
                    /*  mLatitudeText!!.setText(
                          mLatitudeLabel+":   "+
                                  (mLastLocation )!!.latitude)
                      mLongitudeText!!.setText(mLongitudeLabel+":   "+
                              (mLastLocation )!!.longitude)*/
                } else {
                    Log.w("TAG", "getLastLocation:exception", task.exception)
                    //showMessage(getString(R.string.no_location_detected))
                }
            }
    }

    override fun onLocationChanged(location: Location) {
        myLocation = location
        println("locatbegintrip-----------$location")
        if (myLocation != null) {
            try {
                val latLng = LatLng(myLocation!!.latitude, myLocation!!.longitude)
                drawRouteInMap()
                sendLocationToTheUser(myLocation!!)
                if (drivermarker != null) {
                    drivermarker!!.remove()
                }
                drivermarker = mMap!!.addMarker(
                    MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.dash_car))
                )
                val zoom = mMap!!.cameraPosition.zoom
                val cameraPosition = CameraPosition.Builder().target(latLng).zoom(zoom).build()
                val camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
                mMap!!.moveCamera(camUpdate)

            } catch (e: Exception) {
            }
        }
    }

    private fun drawRouteInMap() {
        try {
            // println("drawRouteInMap----------${myLocation!!.latitude}")

            if (!isDrawnOnMap) {

                fromPosition = LatLng(MyCurrent_lat, MyCurrent_long)
                //fromPosition = LatLng(myLocation!!.latitude, myLocation!!.longitude)
                toPosition = LatLng(
                    java.lang.Double.parseDouble(Str_pickUp_Lat!!),
                    java.lang.Double.parseDouble(Str_pickUp_Long!!)
                )
                marker = MarkerOptions().position(LatLng(MyCurrent_lat, MyCurrent_long))
                marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location))
                currentMarker = mMap!!.addMarker(marker)
                if (fromPosition != null && toPosition != null) {
                    val getRoute = GetRouteTask()
                    getRoute.execute()
                }
            } else {
            }
        } catch (e: Exception) {
            Log.e("DRAWROUTEEXCEPTION", e.toString())
            e.printStackTrace()
            Toast.makeText(applicationContext, ERROR_TAG, Toast.LENGTH_SHORT).show()
        }

    }

    @Throws(JSONException::class)
    private fun sendLocationToTheUser(location: Location) {

        println("userid-------------$chatID")

        val sendlat = java.lang.Double.valueOf(location.latitude).toString()
        val sendlng = java.lang.Double.valueOf(location.longitude).toString()
        if (job == null) {
            job = JSONObject()
        }
        job!!.put("action", "driver_loc")
        job!!.put("latitude", sendlat)
        job!!.put("longitude", sendlng)
        job!!.put("device_type", "android")
        job!!.put("background", "no")
        job!!.put("ride_id", Str_RideId)
        builder!!.sendMessage(chatID, job!!.toString())

    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}
    override fun onStop() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient!!.disconnect()
        }
        super.onStop()
        mHandler.removeCallbacks(mHandlerTask)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (chat != null) {
            chat!!.close()
        }
        mHandler.removeCallbacks(mHandlerTask)
    }

    private inner class GetRouteTask : AsyncTask<String, Void, String>() {

        internal var response = ""

        override fun onPreExecute() {}

        override fun doInBackground(vararg urls: String): String {
            //Get All Route values
            document = v2GetRouteDirection.getDocument(
                fromPosition!!,
                toPosition!!,
                GMapV2GetRouteDirection.MODE_DRIVING
            )!!
            response = "Success"
            return response
        }

        override fun onPostExecute(result: String) {
            // googleMap.clear();
            try {
                val directionPoint = v2GetRouteDirection.getDirection(document)
                val rectLine =
                    PolylineOptions().width(15f).color(resources.getColor(R.color.app_color))

                for (i in directionPoint.indices) {
                    rectLine.add(directionPoint.get(i))
                }
                // Adding route on the map
                mMap!!.addPolyline(rectLine)
                markerOptions.position(fromPosition!!)
                markerOptions.position(toPosition!!)
                markerOptions.draggable(true)
                //mMap.addMarker(markerOptions);
                isDrawnOnMap = true

                // destination marker
                val destn_marker_view =
                    (context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                        R.layout.custom_drop_marker,
                        null
                    )
                val tv_marker_drop =
                    destn_marker_view.findViewById<TextView>(R.id.tv_marker_drop)
                tv_marker_drop.text = str_drop_location!!
                val destn_bitmap = createBitmapFromLayout(destn_marker_view)
                val destn_small_bitmap =
                    Bitmap.createScaledBitmap(destn_bitmap, markerWidth, markerHeight, false)

                mMap!!.addMarker(
                    MarkerOptions()
                        .position(toPosition!!)
//                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_icon_green))
                        .icon(BitmapDescriptorFactory.fromBitmap(destn_bitmap))
                )
                val builder = LatLngBounds.Builder()
                builder.include(toPosition!!)
                builder.include(fromPosition!!)
                val bounds = builder.build()
                mMap!!.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        //startLocationUpdates()
        mHandlerTask.run()
    }

    private fun setLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = 1000
        mLocationRequest!!.fastestInterval = 1000
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    /*  protected fun startLocationUpdates() {
          if (mGoogleApiClient != null && mGoogleApiClient!!.isConnected) {
              LocationServices.FusedLocationApi.requestLocationUpdates(
                  mGoogleApiClient,
                  mLocationRequest,
                  this
              )
          }
      }
  */

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
    }

    override fun onConnectionSuspended(i: Int) {

    }

    //-----------------------Code for arrived post request-----------------
    private fun PostRequest(Url: String) {
        dialog = Dialog(this@ArrivedTrip)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()
        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)
        println("-------------dashboard----------------$Url")
        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id!!
        jsonParams["ride_id"] = Str_RideId!!
        println("--------------driver_id-------------------" + driver_id!!)
        println("--------------ride_id-------------------" + Str_RideId!!)


        mRequest = ServiceRequest(this@ArrivedTrip)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {

                override fun onCompleteListener(response: String) {

                    Log.e("arrived", response)
                    println("response---------$response")

                    var Str_status = ""
                    var Str_response = ""

                    try {
                        val `object` = JSONObject(response)
                        Str_status = `object`.getString("status")
                        Str_response = `object`.getString("response")
                    } catch (e: Exception) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    dialog!!.dismiss()
                    if (Str_status.equals("1", ignoreCase = true)) {
                        if (Str_droplat != null && !Str_droplon!!.equals(
                                "",
                                ignoreCase = true
                            ) && Str_droplat != null && !Str_droplon!!.equals("", ignoreCase = true)
                        ) {
                            val intent = Intent(this@ArrivedTrip, BeginTrip::class.java)
                            intent.putExtra("user_name", Str_username)
                            intent.putExtra("rideid", Str_RideId)
                            intent.putExtra("user_image", Str_user_img)
                            intent.putExtra("user_phoneno", Str_user_phoneno)
                            intent.putExtra("drop_location", str_drop_location)
                            intent.putExtra("DropLatitude", Str_droplat)
                            intent.putExtra("DropLongitude", Str_droplon)
                            intent.putExtra("UserId", Suser_Id)
                            intent.putExtra("user_rating", Str_user_rating)
                            val locationaddressstartingpoint = "$MyCurrent_lat,$MyCurrent_long"
                            Log.d("myloc", locationaddressstartingpoint)
                            intent.putExtra("pickuplatlng", "$Str_droplat,$Str_droplon")
                            intent.putExtra("startpoint", locationaddressstartingpoint)
                            startActivity(intent)
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        } else {
                            val intent = Intent(this@ArrivedTrip, BeginTrip::class.java)
                            intent.putExtra("user_name", Str_username)
                            intent.putExtra("user_phoneno", Str_user_phoneno)
                            intent.putExtra("user_image", Str_user_img)
                            intent.putExtra("rideid", Str_RideId)
                            intent.putExtra("UserId", Suser_Id)
                            startActivity(intent)
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                            finish()
                        }
                    } else {
                        Alert(resources.getString(R.string.alert_sorry_label_title), Str_response)
                    }
                }

                override fun onErrorListener() {

                    dialog!!.dismiss()
                }
            })
    }


    //Enabling Gps Service
    private fun enableGpsService() {
        mLocationRequest = LocationRequest.create()
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest!!.interval = (30 * 1000).toLong()
        mLocationRequest!!.fastestInterval = (5 * 1000).toLong()

        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest!!)
        builder.setAlwaysShow(true)

        result =
            LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())

        result.setResultCallback { result ->
            val status = result.status
            //final LocationSettingsStates state = result.getLocationSettingsStates();
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                    // Location settings are not satisfied. But could be fixed by showing the user
                    // a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        status.startResolutionForResult(this@ArrivedTrip, REQUEST_LOCATION)
                    } catch (e: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }

                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }// All location settings are satisfied. The client can initialize location
            // requests here.
            //...
            // Location settings are not satisfied. However, we have no way to fix the
            // settings so we won't show the dialog.
            //...
        }
    }

    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
        if (progress > 95) {
            seekBar.thumb = resources.getDrawable(R.drawable.slidetounlock_arrow)
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar) {
        Bt_slider.visibility = View.INVISIBLE
    }

    override fun onStopTrackingTouch(seekBar: SeekBar) {

        if (seekBar.progress < 80) {
            seekBar.progress = 0
            sliderSeekBar!!.setBackgroundResource(R.drawable.blue_slide_to_unlock_bg)
            Bt_slider.visibility = View.VISIBLE
            Bt_slider.text = resources.getString(R.string.arrivedtrip_arrivedtriptv_label)

        } else if (seekBar.progress > 90) {
            seekBar.progress = 100
            Bt_slider.visibility = View.VISIBLE
            Bt_slider.text = resources.getString(R.string.arrivedtrip_arrivedtriptv_label)
            sliderSeekBar!!.visibility = View.VISIBLE

            cd = ConnectionDetector(this@ArrivedTrip)
            isInternetPresent = cd!!.isConnectingToInternet
            if (isInternetPresent!!) {
                PostRequest(ServiceConstant.arrivedtrip_url)
                println("arrived------------------" + ServiceConstant.arrivedtrip_url)
            } else {
                Alert(
                    resources.getString(R.string.alert_sorry_label_title),
                    resources.getString(R.string.alert_nointernet)
                )
            }
        }
    }

    //-----------------------Update current Location for stion  Post Request-----------------
    private fun postRequest_UpdateProviderLocation(Url: String) {

        val jsonParams = HashMap<String, String>()
        jsonParams["ride_id"] = Str_RideId!!
        jsonParams["latitude"] = Str_Latitude!!
        jsonParams["longitude"] = Str_longitude
        jsonParams["driver_id"] = driver_id!!

        println("-------------Arrivedsendrequestride_id----------------" + Str_RideId!!)
        println("-------------Arrivedsendrequestlatitude----------------$Str_Latitude")
        println("-------------Arrivedsendrequestlongitude----------------$Str_longitude")

        println("-------------latlongupdate----------------$Url")
        mRequest = ServiceRequest(this@ArrivedTrip)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    Log.e("updatelocation", response)
                    println("-------------latlongupdate----------------$response")
                }

                override fun onErrorListener() {}
            })
    }

    override fun onPause() {
        super.onPause()
        System.gc()
        if (chat != null) {
            chat!!.close()
        }
        mHandler.removeCallbacks(mHandlerTask)
    }

    private fun checkCallPhonePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun checkReadStatePermission(): Boolean {
        val result = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
        return if (result == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            false
        }
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE),
            PERMISSION_REQUEST_CODE
        )
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val callIntent = Intent(Intent.ACTION_CALL)
                callIntent.data = Uri.parse("tel:" + Str_user_phoneno!!)
                startActivity(callIntent)
            }
        }
    }

    fun createBitmapFromLayout(view: View): Bitmap {

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)

        view.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.buildDrawingCache()
        val bitmap =
            Bitmap.createBitmap(view.measuredWidth, view.measuredHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)

        return bitmap
    }

    companion object {
        private val TAG = "swipe"
        internal val REQUEST_LOCATION = 199
        private val INTERVAL = 45000
        lateinit var arrivedTrip_class: ArrivedTrip
        var myLocation: Location? = null
    }

}
