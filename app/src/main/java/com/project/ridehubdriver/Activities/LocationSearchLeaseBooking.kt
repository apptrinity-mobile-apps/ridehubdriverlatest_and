package com.project.ridehubdriver.Activities

import android.app.Dialog
import android.content.*
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import com.android.volley.Request
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.location.*
import com.project.ridehubdriver.Adapters.PlaceSearchAdapter
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class LocationSearchLeaseBooking : Fragment(), ProgressListener,
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    //private var back: LinearLayout? = null
    private var listview: ListView? = null
    private var et_username: EditText? = null
    private var ll_destination: LinearLayout? = null
    private var ll_source: LinearLayout? = null
    private var et_mobilenumber: EditText? = null
    private var ll_submit: LinearLayout? = null
    private var tv_source: TextView? = null
    private var tv_destination: TextView? = null
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null

    private var mRequest: ServiceRequest? = null
    internal lateinit var context: Context
    internal var itemList_location = ArrayList<String>()
    internal var itemList_placeId = ArrayList<String>()

    internal lateinit var adapter: PlaceSearchAdapter
    private var isdataAvailable = false


    internal var dialog: Dialog? = null
    internal lateinit var setpickuploc: String
    internal var identi = 0
    internal lateinit var session: SessionManager
    private var UserID: String? = null

    lateinit var avd: AnimatedVectorDrawableCompat
    lateinit var iv_line: ImageView
    internal lateinit var gps: GPSTracker

    lateinit var location_search_dialog: Dialog
    var location_stg = ""
    var lat_stg = ""
    var lon_stg = ""
    lateinit var et_source: EditText
    var source = ""
    var destination = ""
    var destination_lat = ""
    var destination_lon = ""
    var source_lat = ""
    var source_lon = ""
    private var mGoogleApiClient: GoogleApiClient? = null
    internal lateinit var mLocationRequest: LocationRequest
    internal lateinit var result: PendingResult<LocationSettingsResult>
    internal val REQUEST_LOCATION = 299
    private var receiver: BroadcastReceiver? = null
    private var drawer_layout: ImageView? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        try {
            if (rootview != null) {
                val parent = rootview!!.parent as ViewGroup?
                parent?.removeView(rootview)
            }
            rootview = inflater.inflate(R.layout.place_search_leasebooking, container, false)

        } catch (e: InflateException) {
            /* map is already there, just return view as it is */
        }

        initialize(rootview!!)

        val filter = IntentFilter()
        filter.addAction("com.finish.canceltrip.DriverMapActivity")
        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.action == "com.finish.canceltrip.DriverMapActivity") {
                    //Rl_layout_available_status!!.visibility = View.GONE
                }
            }
        }
        drawer_layout!!.setOnClickListener {
            NavigationDrawer.openDrawer()
        }
        CustomProgress.getInstance().setListener(this)

        return rootview

    }


    private fun initialize(rootview: View) {
        context = activity!!
        tv_source = rootview.findViewById(R.id.tv_source) as TextView
        tv_destination = rootview.findViewById(R.id.tv_destination) as TextView
        et_username = rootview.findViewById(R.id.et_username) as EditText
        ll_source = rootview.findViewById(R.id.ll_source) as LinearLayout
        ll_destination = rootview.findViewById(R.id.ll_destination) as LinearLayout
        et_mobilenumber = rootview.findViewById(R.id.et_mobilenumber) as EditText
        ll_submit = rootview.findViewById(R.id.ll_submit) as LinearLayout

        //back = rootview.findViewById(R.id.location_search_back_layout) as LinearLayout
        drawer_layout = rootview.findViewById(R.id.ham_home)
        session = SessionManager(activity!!)

        val userDetails = session!!.userDetails
        UserID = userDetails["driverid"]
        gps = GPSTracker(activity!!)
        if (gps.canGetLocation() && gps.isgpsenabled()) {
            source_lat = gps.getLatitude().toString()
            source_lon = gps.getLongitude().toString()
            source = getCompleteAddressString(gps.getLatitude(), gps.getLongitude())
            tv_source!!.text = source
        } else {
            enableGpsService()
        }


        ll_source!!.setOnClickListener {
            showLocationSearchDialog(tv_source!!, "source")
        }
        ll_destination!!.setOnClickListener {
            showLocationSearchDialog(tv_destination!!, "destination")
        }
        tv_source!!.setOnClickListener {
            showLocationSearchDialog(tv_source!!, "source")
        }
        tv_destination!!.setOnClickListener {
            showLocationSearchDialog(tv_destination!!, "destination")
        }


        ll_submit!!.setOnClickListener {
            when {
                tv_source!!.text.toString() == "" -> {
                    Toast.makeText(
                        activity!!,
                        "Pick up location must not be empty!",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                tv_destination!!.text.toString() == "" -> {
                    Toast.makeText(
                        activity!!,
                        "Drop location must not be empty!",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                et_username!!.text.toString() == "" -> {
                    Toast.makeText(
                        activity!!,
                        "Enter Username",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                et_mobilenumber!!.text.toString() == "" -> {
                    Toast.makeText(
                        activity!!,
                        "Enter User MobileNumber",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                else -> {
                    book_offline_ride(
                        ServiceConstant.book_offline_ride_url,
                        UserID!!,
                        tv_source!!.text.toString(),
                        tv_destination!!.text.toString(),
                        source_lon,
                        source_lat,
                        destination_lon,
                        destination_lat,
                        et_username!!.text.toString(),
                        et_mobilenumber!!.text.toString()
                    )
                }
            }
        }


    }


    private fun CloseKeyboard(edittext: EditText) {
        val `in` = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(
            edittext.applicationWindowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, alert: String) {

        val mDialog = PkDialog(activity!!)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(alert)
        mDialog.setPositiveButton(
            getResources().getString(R.string.action_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()

    }

    //Enabling Gps Service
     private fun enableGpsService() {
         mGoogleApiClient = GoogleApiClient.Builder(activity!!)
             .addApi(LocationServices.API)
             .addConnectionCallbacks(this)
             .addOnConnectionFailedListener(this).build()
         mGoogleApiClient!!.connect()
         mLocationRequest = LocationRequest.create()
         mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
         mLocationRequest.interval = (30 * 1000).toLong()
         mLocationRequest.fastestInterval = (5 * 1000).toLong()
         val builder = LocationSettingsRequest.Builder()
             .addLocationRequest(mLocationRequest)
         builder.setAlwaysShow(true)
         result =
             LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())
         result.setResultCallback { result ->
             val status = result.status
             when (status.statusCode) {
                 LocationSettingsStatusCodes.SUCCESS -> {
                 }
                 LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                     try {
                         status.startResolutionForResult(activity!!, REQUEST_LOCATION)
                     } catch (e: IntentSender.SendIntentException) {
                     }
                 LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                 }
             }
         }
     }
    override fun onDestroy() {
        super.onDestroy()
        if (dialog != null) {
            dialog!!.dismiss()
            dialog = null
        }
    }

    private fun startAnimation(isStart: Boolean) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (isStart) {
                repeatAnimation()
            } else {
                handler.postDelayed({
                    iv_line.removeCallbacks(action)
                    avd.stop()
                    iv_line.visibility = View.GONE
                    iv_line.invalidate()
                }, 800)

            }
        }
    }


    private val action = Runnable { repeatAnimation() }
    internal var handler = Handler()
    private fun repeatAnimation() {
        if (avd != null) {
            Thread(Runnable {

                handler.post {
                    iv_line.visibility = View.VISIBLE
                    avd.start()
                    iv_line.postDelayed(action, 1000)
                    iv_line.invalidate()
                }
            }).start()

        }
    }

    override fun onProgressChanged(state: Boolean) {
        startAnimation(state)
    }

    companion object {
        private var rootview: View? = null
    }

    //-------------Method to get Complete Address------------
    private fun getCompleteAddressString(LATITUDE: Double, LONGITUDE: Double): String {
        var strAdd = ""
        val geocoder = Geocoder(context, Locale.getDefault())
        try {
            val addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1)
            if (addresses != null) {
                val returnedAddress = addresses[0]
                val strReturnedAddress = StringBuilder("")
                if (returnedAddress.maxAddressLineIndex > 0) {
                    for (i in 0 until returnedAddress.maxAddressLineIndex) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n")
                    }
                } else {
                    try {
                        strReturnedAddress.append(returnedAddress.getAddressLine(0))
                    } catch (ignored: Exception) {
                    }

                }
                strAdd = strReturnedAddress.toString()
            } else {
                Log.e("Current loction address", "No Address returned!")
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Current loction address", "Canont get Address!")
        }

        return strAdd
    }


    private fun showLocationSearchDialog(textView: TextView, source_or_destination: String) {
        location_search_dialog = Dialog(activity!!)
        location_search_dialog.window
        location_search_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        location_search_dialog.setContentView(R.layout.fav_place_search_dialog)
        location_search_dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        location_search_dialog.setCanceledOnTouchOutside(false)
        location_search_dialog.window!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        location_search_dialog.show()

        iv_line = location_search_dialog.findViewById(R.id.iv_line)
        avd = AnimatedVectorDrawableCompat.create(activity!!, R.drawable.avd_line)!!
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            iv_line.background = avd
        }
        iv_line.visibility = View.GONE
        val iv_close = location_search_dialog!!.findViewById(R.id.iv_close) as ImageView
        et_source = location_search_dialog!!.findViewById(R.id.tv_source) as EditText
        listview = location_search_dialog!!.findViewById(R.id.location_search_listView) as ListView

        iv_close.setOnClickListener { location_search_dialog.dismiss() }
        listview!!.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                location_stg = itemList_location[position]
                textView.text = location_stg
                location_search_dialog.dismiss()
                cd = ConnectionDetector(activity!!)
                isInternetPresent = cd!!.isConnectingToInternet
                if (isInternetPresent!!) {
                    latLongRequest(
                        ServiceConstant.GetAddressFrom_LatLong_url + itemList_placeId[position],
                        source_or_destination
                    )
                } else {
                    Toast.makeText(
                        activity!!,
                        resources.getString(R.string.alert_nointernet),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
            }

        et_source.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {

                cd = ConnectionDetector(activity!!)
                isInternetPresent = cd!!.isConnectingToInternet

                if (isInternetPresent!!) {
                    if (mRequest != null) {
                        mRequest!!.cancelRequest()
                    }
                    val data = et_source.text.toString().toLowerCase(Locale.getDefault())
                        .replace(" ", "%20")
                    citySearchRequest(ServiceConstant.place_search_url + data)
                } else {
                    Toast.makeText(
                        activity!!,
                        resources.getString(R.string.alert_nointernet),
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }

            }
        })

        et_source.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event!!.keyCode === KeyEvent.KEYCODE_ENTER) {
                closeKeyboard(et_source)
            }
            false
        }

    }

    private fun latLongRequest(Url: String, source_or_destination: String) {

        dialog = Dialog(activity!!)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_processing)

        println("--------------LatLong url-------------------$Url")

        mRequest = ServiceRequest(activity!!)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.GET,
            null,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("--------------LatLong  reponse-------------------$response")
                    var status = ""
                    try {
                        val `object` = JSONObject(response)
                        if (`object`.length() > 0) {

                            status = `object`.getString("status")
                            val place_object = `object`.getJSONObject("result")
                            if (status.equals("OK", ignoreCase = true)) {
                                if (place_object.length() > 0) {
                                    val geometry_object = place_object.getJSONObject("geometry")
                                    if (geometry_object.length() > 0) {
                                        val location_object =
                                            geometry_object.getJSONObject("location")
                                        if (location_object.length() > 0) {
                                            lat_stg = location_object.getString("lat")
                                            lon_stg = location_object.getString("lng")
                                            isdataAvailable = true
                                        } else {
                                            isdataAvailable = false
                                        }
                                    } else {
                                        isdataAvailable = false
                                    }
                                } else {
                                    isdataAvailable = false
                                }
                            } else {
                                isdataAvailable = false
                            }
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    if (isdataAvailable) {
                        dialog!!.dismiss()
                        if (source_or_destination == "source") {
                            source_lat = lat_stg
                            source_lon = lon_stg
                        } else if (source_or_destination == "destination") {
                            destination_lat = lat_stg
                            destination_lon = lon_stg
                        }
                    } else {
                        dialog!!.dismiss()
                        Toast.makeText(activity!!, status, Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }
            })
    }

    private fun citySearchRequest(Url: String) {

        println("--------------Search city url-------------------$Url")

        mRequest = ServiceRequest(activity!!)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.GET,
            null,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    println("--------------Search city  reponse-------------------$response")
                    var status = ""
                    try {
                        val `object` = JSONObject(response)
                        if (`object`.length() > 0) {

                            status = `object`.getString("status")
                            val place_array = `object`.getJSONArray("predictions")
                            if (status.equals("OK", ignoreCase = true)) {
                                if (place_array.length() > 0) {
                                    itemList_location.clear()
                                    itemList_placeId.clear()
                                    for (i in 0 until place_array.length()) {
                                        val place_object = place_array.getJSONObject(i)
                                        itemList_location.add(place_object.getString("description"))
                                        itemList_placeId.add(place_object.getString("place_id"))
                                    }
                                    isdataAvailable = true
                                } else {
                                    itemList_location.clear()
                                    itemList_placeId.clear()
                                    isdataAvailable = false
                                }
                            } else {
                                itemList_location.clear()
                                itemList_placeId.clear()
                                isdataAvailable = false
                            }
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    /*if (!isdataAvailable) {
                        ll_location_search.visibility = View.GONE
                    } else {
                        ll_location_search.visibility = View.VISIBLE
                    }*/
                    adapter = PlaceSearchAdapter(activity!!, itemList_location)
                    listview!!.adapter = adapter!!
                    adapter.setProgress(iv_line)
                    adapter!!.notifyDataSetChanged()
                }

                override fun onErrorListener() {
                    // close keyboard
                    closeKeyboard(et_source)
                }
            })
    }

    private fun closeKeyboard(editText: EditText) {
        val `in` = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        `in`.hideSoftInputFromWindow(
            editText.applicationWindowToken,
            InputMethodManager.HIDE_NOT_ALWAYS
        )
    }

    private fun book_offline_ride(
        Url: String,
        user_id: String,
        pickup_location: String,
        drop_location: String,
        pickup_lon: String,
        pickup_lat: String,
        drop_lon: String,
        drop_lat: String,
        user_name: String,
        mobile_number: String

    ) {

        dialog = Dialog(activity!!)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_processing)

        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = user_id
        jsonParams["pickup_location"] = pickup_location
        jsonParams["drop_location"] = drop_location
        jsonParams["pickup_lon"] = pickup_lon
        jsonParams["pickup_lat"] = pickup_lat
        jsonParams["drop_lon"] = drop_lon
        jsonParams["drop_lat"] = drop_lat
        jsonParams["user_name"] = user_name
        jsonParams["phone_number"] = mobile_number
        println("--------------book offline-------------------$Url-------$jsonParams")
        mRequest = ServiceRequest(activity!!)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    println("--------------offline ride response-------------------$response")
                    try {
                        val `object` = JSONObject(response)

                        Log.d("response_test", response)

                        if (`object`.length() > 0) {
                            var status = `object`.getString("status")
                            Log.d("OFFLINESTATUS", status)
                            if (status.equals("1", ignoreCase = true)) {
                                val jsonObject = `object`.getJSONObject("response")
                                val user_name = jsonObject.getString("user_name")
                                val phone_number = jsonObject.getString("phone_number")
                                val ride_id = jsonObject.getString("ride_id")
                                val driver_id = jsonObject.getString("driver_id")
                                val pickup_location = jsonObject.getString("pickup_location")
                                val pickup_lon = jsonObject.getString("pickup_lon")
                                val pickup_lat = jsonObject.getString("pickup_lat")
                                val drop_location = jsonObject.getString("drop_location")
                                val drop_lon = jsonObject.getString("drop_lon")
                                val drop_lat = jsonObject.getString("drop_lat")

                                Log.e(
                                    "OFFLINERESPONSE",
                                    user_name + "" + phone_number + "" + ride_id
                                )

                                val intent = Intent(activity!!, BeginTrip::class.java)
                                intent.putExtra("user_name", user_name)
                                intent.putExtra("rideid", ride_id)
                                intent.putExtra("user_image", "")
                                intent.putExtra("user_phoneno", phone_number)
                                intent.putExtra("drop_location", drop_location)
                                intent.putExtra("DropLatitude", drop_lat)
                                intent.putExtra("DropLongitude", drop_lon)
                                intent.putExtra("UserId", UserID)
                                intent.putExtra("user_rating", "")
                                val locationaddressstartingpoint = "$source_lat,$source_lon"
                                Log.d("myloc", locationaddressstartingpoint)
                                intent.putExtra("pickuplatlng", "$drop_lat,$drop_lon")
                                intent.putExtra("startpoint", locationaddressstartingpoint)
                                startActivity(intent)
                                activity!!.overridePendingTransition(
                                    R.anim.fade_in,
                                    R.anim.fade_out
                                )

                                /*Toast.makeText(this@OfflineActivity, "Call sent successfully ", Toast.LENGTH_SHORT)
                                    .show()*/

                            } else {
                                val response_stg = `object`.getString("response")
                                Toast.makeText(activity!!, response_stg, Toast.LENGTH_SHORT)
                                    .show()
                            }
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    dialog!!.dismiss()
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }
            })
    }

    override fun onConnected(p0: Bundle?) {
    }

    override fun onConnectionSuspended(p0: Int) {
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }


}
