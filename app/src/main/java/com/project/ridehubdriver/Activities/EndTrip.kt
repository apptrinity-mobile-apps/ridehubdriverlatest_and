package com.project.ridehubdriver.Activities

import android.animation.ValueAnimator
import android.app.Activity
import android.app.ActivityManager
import android.app.Dialog
import android.app.PictureInPictureParams
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Point
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.*
import android.util.DisplayMetrics
import android.util.Log
import android.util.Rational
import android.view.*
import android.view.animation.AlphaAnimation
import android.view.animation.LinearInterpolator
import android.widget.*
import com.android.volley.Request
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.project.ridehubdriver.Adapters.ContinuousRequestAdapter
import com.project.ridehubdriver.Helper.GMapV2GetRouteDirection
import com.project.ridehubdriver.Helper.SensorService
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.Helper.xmpp.ChatingService
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.*
import com.project.ridehubdriver.customRatingBar.RotationRatingBar
import com.project.ridehubdriver.latlnginterpolation.LatLngInterpolator
import com.project.ridehubdriver.subclass.RealTimeActivity
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso
import org.jivesoftware.smack.chat.Chat
import org.json.JSONException
import org.json.JSONObject
import org.w3c.dom.Document
import java.text.NumberFormat
import java.util.*

/**
 * Created by user88 on 10/29/2015.
 */
class EndTrip : RealTimeActivity(), com.google.android.gms.location.LocationListener,
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
    SeekBar.OnSeekBarChangeListener, OnMapReadyCallback {

    override fun onMapReady(googlemap: GoogleMap?) {
        mMap = googlemap
        // Changing map type
        mMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL
        // Showing / hiding your current location
        mMap!!.isMyLocationEnabled = false
        // Enable / Disable zooming controls
        mMap!!.uiSettings.isZoomControlsEnabled = false
        // Enable / Disable my location button
        mMap!!.uiSettings.isMyLocationButtonEnabled = false
        // Enable / Disable Compass icon
        mMap!!.uiSettings.isCompassEnabled = false
        // Enable / Disable Rotate gesture
        mMap!!.uiSettings.isRotateGesturesEnabled = true
        // Enable / Disable zooming functionality
        mMap!!.uiSettings.isZoomGesturesEnabled = true
        mMap!!.isMyLocationEnabled = false

        if (gps!!.canGetLocation()) {
            val Dlatitude = gps!!.getLatitude()
            val Dlongitude = gps!!.getLongitude()

            MyCurrent_lat = Dlatitude
            MyCurrent_long = Dlongitude

            previous_lat = MyCurrent_lat
            previous_lon = MyCurrent_long
            // Move the camera to last position with a zoom level
            val cameraPosition =
                CameraPosition.Builder().target(LatLng(Dlatitude, Dlongitude)).zoom(17f).build()
            mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

            //----------------------set marker------------------
            marker = MarkerOptions().position(LatLng(Dlatitude, Dlongitude))
            marker!!.icon(BitmapDescriptorFactory.fromResource(R.drawable.dash_car))
            currentMarker = mMap!!.addMarker(marker)

        } else {
            alert_layout!!.visibility = View.VISIBLE
            alert_textview!!.text = resources.getString(R.string.alert_gpsEnable)
        }
    }

    private var result: PendingResult<LocationSettingsResult>? = null
    private var driver_id: String? = ""
    private val category_type = ""
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    private var layout_end_trip_details: RelativeLayout? = null
    private var layout_end_trip_header: RelativeLayout? = null
    private var Tv_name: TextView? = null
    private var Tv_userimage: ImageView? = null
    private var Tv_mobilno: TextView? = null
    private var Str_profilpic: String? = ""
    private var Tv_start_wait: TextView? = null
    private var Tv_stop_wait: TextView? = null
    private val Rl_layout_back: RelativeLayout? = null
    private val Bt_Endtrip: Button? = null
    private var Str_name: String? = ""
    private var Str_mobilno: String? = ""
    private var Str_rideid: String? = ""
    private var Str_user_rating: String? = ""
    private var Str_User_Id: String? = ""
    internal var Str_otp = ""
    internal var Str_amount = ""
    internal var totalDistanceTravel = ""
    private var alert_layout: RelativeLayout? = null
    private var alert_textview: TextView? = null
    private var droplocation: Array<String>? = null
    private var startlocation: Array<String>? = null
    private var marker: MarkerOptions? = null
    private var previous_lat: Double = 0.toDouble()
    private var previous_lon: Double = 0.toDouble()
    private var current_lat: Double = 0.toDouble()
    private var current_lon: Double = 0.toDouble()
    private var dis = 0.0
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLocationRequest: LocationRequest? = null
    private var currentMarker: Marker? = null
    private var mRequest: ServiceRequest? = null
    private var mMap: GoogleMap? = null
    private var dialog: Dialog? = null
    private var v2GetRouteDirection: GMapV2GetRouteDirection? = null
    private val document: Document? = null
    private var mins: Int = 0
    private var secs: Int = 0
    private var hours: Int = 0

    private var milliseconds: Int = 0
    private val Bt_Enable_voice: Button? = null
    private var Str_status = ""
    private var Str_response = ""
    private var Str_ridefare = ""
    private var Str_timetaken = ""
    private var Str_waitingtime = ""
    private var Str_need_payment = ""
    private var Str_ride_type = ""
    private var Str_currency = ""
    private var Str_ride_distance = ""
    private var str_recievecash = ""
    private var gps: GPSTracker? = null
    private var destlatlng: LatLng? = null
    private var startlatlng: LatLng? = null
    private var MyCurrent_lat = 0.0
    private var MyCurrent_long = 0.0
    private var timerValue: TextView? = null
    private var layout_timer: RelativeLayout? = null
    private var startTime = 0L
    private val customHandler = Handler()
    private var timeInMilliseconds = 0L
    private var timeSwapBuff = 0L
    private var updatedTime = 0L
    internal var results: FloatArray? = null
    internal var locationManager: LocationManager? = null
    internal var location: Double = 0.toDouble()
    private var beginAddress: String? = null
    private val endAddress: String? = null
    private var sCurrencySymbol: String? = ""
    private val distance = ""
    private val latLng: LatLng? = null
    private var mPolylineOptions: PolylineOptions? = null
    private val sliderSeekBar: SeekBar? = null
    //private ShimmerButton Bt_slider; chaitanya
    private var Bt_slider: TextView? = null
    private var distance_to = 0f
    internal var startlatitude = 0.0
    internal var startlongitude = 0.0
    private var v: Float = 0.toFloat()
    private val startPositionn: LatLng? = null
    private var Str_Latitude = ""
    private var Str_longitude = ""
    internal lateinit var mHandler: Handler

    private var markerOptions: MarkerOptions? = null
    private var Rl_layout_enable_voicenavigation: ImageView? = null

    internal lateinit var mServiceIntent: Intent
    private var mSensorService: SensorService? = null

    private var isFirstPosition = true
    private var startPosition: LatLng? = null
    private var endPosition: LatLng? = null
    private var lat: Double = 0.toDouble()
    private var lng: Double = 0.toDouble()
    private var firsttimepolyline: Boolean? = true
    private var polyline: Polyline? = null
    internal var job: JSONObject? = null
    internal var chat: Chat? = null

    val markerWidth = 350
    val markerHeight = 250
    private var context: Context? = null
    private var str_drop_location: String = ""
    private var end_user_rating: RotationRatingBar? = null

    var fromPictureInPictureMode:Boolean = false

    /** The arguments to be used for Picture-in-Picture mode.  */
    private var mPictureInPictureParamsBuilder: PictureInPictureParams.Builder? = null


    private val updateTimerThread = object : Runnable {

        override fun run() {

            timeInMilliseconds = SystemClock.uptimeMillis() - startTime

            updatedTime = timeSwapBuff + timeInMilliseconds
            secs = (updatedTime / 1000).toInt()
            hours = secs / (60 * 60)
            mins = secs / 60
            secs = secs % 60
            if (mins >= 60) {
                mins = 0
            }
            milliseconds = (updatedTime % 1000).toInt()
            timerValue!!.text = (hours.toString() + ":" + mins + ":"
                    + String.format("%02d", secs))

            println("thread----------------" + timerValue!!)

            customHandler.postDelayed(this, 0)
        }
    }


    internal var mHandlerTask: Runnable = object : Runnable {
        override fun run() {

            gps = GPSTracker(this@EndTrip)
            cd = ConnectionDetector(this@EndTrip)
            isInternetPresent = cd!!.isConnectingToInternet
            if (isInternetPresent!!) {
                if (gps != null && gps!!.canGetLocation() && gps!!.isgpsenabled()) {

                    Str_Latitude = gps!!.getLatitude().toString()
                    Str_longitude = gps!!.getLongitude().toString()

                    postRequest_UpdateProviderLocation(ServiceConstant.UPDATE_CURRENT_LOCATION)
                }
            } else {
                Toast.makeText(
                    this@EndTrip,
                    resources.getString(R.string.no_internet_connection),
                    Toast.LENGTH_SHORT
                ).show()
            }

            mHandler.postDelayed(this, INTERVAL.toLong())
        }
    }

    internal var mLatLngInterpolator: LatLngInterpolator? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.endtrip)

        initialize()
        mSensorService = SensorService(applicationContext)
        mServiceIntent = Intent(applicationContext, mSensorService!!.javaClass)

        try {
            setLocationRequest()
            buildGoogleApiClient()
            initializeMap()
        } catch (e: Exception) {
        }

        ChatingService.startDriverAction(this@EndTrip)

        Tv_start_wait!!.setOnClickListener {
            Tv_stop_wait!!.visibility = View.VISIBLE
            Tv_start_wait!!.visibility = View.GONE
            layout_timer!!.visibility = View.VISIBLE
            startTime = SystemClock.uptimeMillis()
            customHandler.postDelayed(updateTimerThread, 0)
        }
        Tv_stop_wait!!.setOnClickListener {
            Tv_start_wait!!.visibility = View.VISIBLE
            Tv_stop_wait!!.visibility = View.GONE
            timeSwapBuff += timeInMilliseconds
            customHandler.removeCallbacks(updateTimerThread)
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mPictureInPictureParamsBuilder = PictureInPictureParams.Builder()
        } else {
            //TODO("VERSION.SDK_INT < O")
        }

        if (Str_user_rating == "null" || Str_user_rating == "") {
            end_user_rating!!.visibility = View.GONE
        } else {
            end_user_rating!!.visibility = View.VISIBLE
            end_user_rating!!.rating =
                java.lang.Float.parseFloat(Str_user_rating!!)
        }
    }


    private fun initialize() {
        context = this.applicationContext
        session = SessionManager(this@EndTrip)
        gps = GPSTracker(this@EndTrip)
        mHandler = Handler()
        markerOptions = MarkerOptions()

        // get user data from session
        val user = session!!.userDetails
        driver_id = user[SessionManager.KEY_DRIVERID]
        //category_type = user.get(SessionManager.KEY_CATEGORY_TYPE);

        v2GetRouteDirection = GMapV2GetRouteDirection()
        val b = intent.extras

        if (b != null && b.containsKey("pickuplatlng")) {
            droplocation =
                b.getString("pickuplatlng")!!.split(",".toRegex()).dropLastWhile { it.isEmpty() }
                    .toTypedArray()
        }
        if (b != null && b.containsKey("startpoint")) {
            beginAddress = b.getString("startpoint")
            startlocation =
                b.getString("startpoint")!!.split(",".toRegex()).dropLastWhile { it.isEmpty() }
                    .toTypedArray()
            try {
                val latitude = java.lang.Double.parseDouble(droplocation!![0])
                val longitude = java.lang.Double.parseDouble(droplocation!![1])
                startlatitude = java.lang.Double.parseDouble(startlocation!![0])
                startlongitude = java.lang.Double.parseDouble(startlocation!![1])
                destlatlng = LatLng(latitude, longitude)
                startlatlng = LatLng(startlatitude, startlongitude)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        if (b != null && b.containsKey("drop_location")) {
            str_drop_location = b.getString("drop_location").toString()
        }

        //---------------set polyline color and width----------------
        mPolylineOptions = PolylineOptions()
        mPolylineOptions!!.color(Color.BLUE).width(10f)
        val i = intent
        Str_rideid = i.getStringExtra("rideid")
        Str_name = i.getStringExtra("name")
        Str_mobilno = i.getStringExtra("mobilno")
        Str_User_Id = i.getStringExtra("user_id")
        Str_profilpic = i.getStringExtra("user_img")
        Str_user_rating = i.getStringExtra("user_rating")

        ContinuousRequestAdapter.userID = Str_User_Id!!

        Tv_name = findViewById<TextView>(R.id.end_trip_name)
        Tv_userimage = findViewById<ImageView>(R.id.profile_image_endtrip)
        Tv_mobilno = findViewById<TextView>(R.id.end_trip_mobilno)
        Tv_start_wait = findViewById<TextView>(R.id.begin_waitingtime_tv_start)
        Tv_stop_wait = findViewById<TextView>(R.id.begin_waitingtime_tv_stop)

        layout_end_trip_details = findViewById<RelativeLayout>(R.id.layout_end_trip_details)
        layout_end_trip_header = findViewById<RelativeLayout>(R.id.layout_end_trip_header)
        /*if(category_type.equalsIgnoreCase("Jcb")){
            Tv_start_wait.setVisibility(View.GONE);
            Tv_stop_wait.setVisibility(View.GONE);
        }*/
        timerValue = findViewById<TextView>(R.id.timerValue)
        layout_timer = findViewById<RelativeLayout>(R.id.layout_timer)
        alert_layout = findViewById<RelativeLayout>(R.id.end_trip_alert_layout)
        alert_textview = findViewById<TextView>(R.id.end_trip_alert_textView)
        Rl_layout_enable_voicenavigation =
            findViewById<ImageView>(R.id.enable_voice_img)

        Rl_layout_enable_voicenavigation!!.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            Rl_layout_enable_voicenavigation!!.startAnimation(buttonClick)
            //minimize()

            val voice_curent_lat_long = "$MyCurrent_lat,$MyCurrent_long"
            val voice_destination_lat_long = droplocation!![0] + "," + droplocation!![1]
            println("----------fromPosition---------------$voice_curent_lat_long")
            println("----------toPosition---------------$voice_destination_lat_long")
            val locationUrl =
                "http://maps.google.com/maps?saddr=$voice_curent_lat_long&daddr=$voice_destination_lat_long"
            println("----------locationUrl---------------$locationUrl")
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(locationUrl))
            startActivity(intent)

            if (!isMyServiceRunning(mSensorService!!.javaClass)) {
                mServiceIntent.putExtra("rideid", Str_rideid)
                mServiceIntent.putExtra("driverid", driver_id)
                mServiceIntent.putExtra("chatID", chatID)
                Log.d("send service", "$Str_rideid-$driver_id")
                startService(mServiceIntent)
            }
        }

        //  shimmer = new Shimmer();
        //sliderSeekBar = (SeekBar) findViewById(R.id.end_Trip_seek);
        //Bt_slider = (ShimmerButton) findViewById(R.id.end_Trip_slider_button);
        //shimmer.start(Bt_slider);  chaitanya
        end_user_rating = findViewById(R.id.end_user_rating)
        Bt_slider = findViewById(R.id.end_Trip_slider_button)
        Bt_slider!!.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            Bt_slider!!.startAnimation(buttonClick)
            println("------------------sliding completed----------------")
            mSensorService!!.stoptimertask()
            cd = ConnectionDetector(this@EndTrip)
            isInternetPresent = cd!!.isConnectingToInternet

            if (isInternetPresent!!) {
                PostRequest(ServiceConstant.endtrip_url)
                println("end------------------" + ServiceConstant.endtrip_url)
            } else {
                Alert(
                    resources.getString(R.string.alert_sorry_label_title),
                    resources.getString(R.string.alert_nointernet)
                )
            }
        }
        //sliderSeekBar.setOnSeekBarChangeListener(this); chaitanya

        timerValue!!.text = "00:00:00"

        Tv_name!!.text = Str_name
        Tv_mobilno!!.text = Str_mobilno
        if(Str_profilpic.toString().equals("")){

        }else{
            Picasso.with(this@EndTrip).load(Str_profilpic.toString())
                .placeholder(R.drawable.no_profile_image_avatar_icon)
                .memoryPolicy(MemoryPolicy.NO_CACHE).into(Tv_userimage)
        }




        cd = ConnectionDetector(this@EndTrip)
        isInternetPresent = cd!!.isConnectingToInternet
        if (isInternetPresent!!) {
            mHandlerTask.run()
        } else {
            Alert(
                resources.getString(R.string.alert_sorry_label_title),
                resources.getString(R.string.alert_nointernet)
            )
        }
    }

    private fun initializeMap() {

        if (mMap == null) {
            val mMap = supportFragmentManager
                .findFragmentById(R.id.arrived_trip_view_map) as SupportMapFragment
            mMap!!.getMapAsync(this)
            if (mMap == null) {
            }
        }
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(this@EndTrip)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            resources.getString(R.string.alert_label_ok),
            View.OnClickListener {
                mDialog.dismiss()

                val broadcastIntent_begintrip = Intent()
                broadcastIntent_begintrip.action = "com.finish.com.finish.BeginTrip"
                sendBroadcast(broadcastIntent_begintrip)

                val broadcastIntent_arrivedtrip = Intent()
                broadcastIntent_arrivedtrip.action = "com.finish.ArrivedTrip"
                sendBroadcast(broadcastIntent_arrivedtrip)

                val broadcastIntent_endtrip = Intent()
                broadcastIntent_endtrip.action = "com.finish.EndTrip"
                sendBroadcast(broadcastIntent_endtrip)
                val intent = Intent(this@EndTrip, LoadingPage::class.java)
                intent.putExtra("Driverid", driver_id)
                intent.putExtra("RideId", Str_rideid)
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                finish()
            })
        mDialog.show()
    }

    //------------------------------code for distance----------------------------
    override fun onResume() {
        super.onResume()
        mHandlerTask.run()
        Log.e("ENDTRIP_ONRESUME", "ONRESUME")
        startLocationUpdates()
        //stopService(mServiceIntent);
        // Log.i("MAINACT","onDestroy!");
    }

    private fun setLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = 2000
        mLocationRequest!!.fastestInterval = 2000
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    protected fun startLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient!!.isConnected) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest,
                this
            )
        }
    }


    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API)
            .build()
    }

    public override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null)
            mGoogleApiClient!!.connect()
    }

    public override fun onStop() {
        super.onStop()
        if (mGoogleApiClient != null)
            mGoogleApiClient!!.disconnect()
        mHandler.removeCallbacks(mHandlerTask)
    }


    override fun onConnected(bundle: Bundle?) {

        if (gps != null && gps!!.canGetLocation() && gps!!.isgpsenabled()) {
        }
        myLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        LocationServices.FusedLocationApi.requestLocationUpdates(
            mGoogleApiClient,
            mLocationRequest,
            this as LocationListener
        )

        if (myLocation != null) {

            if (mMap == null) {
                val mMap = supportFragmentManager
                    .findFragmentById(R.id.arrived_trip_view_map) as SupportMapFragment
                mMap!!.getMapAsync(this)
                if (mMap == null) {
                }
            }

            mMap!!.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    LatLng(
                        myLocation!!.latitude,
                        myLocation!!.longitude
                    ), 16f
                )
            )

            if (startlatlng != null && destlatlng != null) {
                val getRoute = GetRouteTask()
                getRoute.execute()
            }
        }
    }

    override fun onConnectionSuspended(i: Int) {}
    override fun onLocationChanged(location: Location) {

        if (myLocation != null) {
            distance_to = location.distanceTo(myLocation)
            println("---------distance to-----------" + location.distanceTo(myLocation))
        }
        myLocation = location
        if (myLocation != null) {
            //-------------Updating Marker-------
            try {

                MyCurrent_lat = myLocation!!.latitude
                MyCurrent_long = myLocation!!.longitude

                val latLng = LatLng(myLocation!!.latitude, myLocation!!.longitude)
                if (currentMarker != null) {
                    currentMarker!!.remove()
                }
                currentMarker = mMap!!.addMarker(
                    MarkerOptions().position(latLng).icon(BitmapDescriptorFactory.fromResource(R.drawable.dash_car))
                )
                //updateDriverOnMap(myLocation.getLatitude(),myLocation.getLongitude());
                val zoom = mMap!!.cameraPosition.zoom
                val cameraPosition = CameraPosition.Builder().target(latLng).zoom(zoom).build()
                val camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
                mMap!!.moveCamera(camUpdate)

                sendLocationToUser(myLocation!!)
                updateDriverOnMap(MyCurrent_lat, MyCurrent_long)
            } catch (e: Exception) {
            }

            //------Calculating Distance------
            val f = FloatArray(1)
            current_lat = location.latitude
            current_lon = location.longitude
            if (current_lat != previous_lat || current_lon != previous_lon) {

                if (distance_to >= 1.0) {
                    Location.distanceBetween(
                        previous_lat,
                        previous_lon,
                        current_lat,
                        current_lon,
                        f
                    )
                    dis += java.lang.Double.parseDouble(f[0].toString())
                }
                previous_lat = current_lat
                previous_lon = current_lon
                println("distance inside----------------------$dis")
            } else {
                previous_lat = current_lat
                previous_lon = current_lon
                dis = dis
            }
            previous_lat = current_lat
            previous_lon = current_lon
        }
    }

    @Throws(JSONException::class)
    private fun sendLocationToUser(location: Location) {

        val sendLat = java.lang.Double.valueOf(location.latitude).toString()
        val sendLng = java.lang.Double.valueOf(location.longitude).toString()

        println("endtripchatID--------------$chatID--$sendLat--$sendLat--$Str_rideid")
        if (job == null) {
            job = JSONObject()
        }
        job!!.put("action", "driver_loc")
        job!!.put("latitude", sendLat)
        job!!.put("longitude", sendLng)
        job!!.put("device_type", "android")
        job!!.put("background", "no")
        job!!.put("ride_id", Str_rideid)
        builder!!.sendMessage(chatID, job!!.toString())
    }

    private inner class GetRouteTask : AsyncTask<String, Void, String>() {

        internal var response = ""
        internal var v2GetRouteDirection = GMapV2GetRouteDirection()
        internal lateinit var document: Document

        override fun onPreExecute() {}

        override fun doInBackground(vararg urls: String): String {
            //Get All Route values
            document = v2GetRouteDirection.getDocument(
                startlatlng!!,
                destlatlng!!,
                GMapV2GetRouteDirection.MODE_DRIVING
            )!!
            response = "Success"
            return response
        }

        override fun onPostExecute(result: String) {

            try {
                if (polyline != null) {
                    polyline!!.remove()
                }
                if (result.equals("Success", ignoreCase = true)) {
                    val directionPoint = v2GetRouteDirection.getDirection(document)
                    val rectLine =
                        PolylineOptions().width(10f).color(resources.getColor(R.color.app_color))
                    for (i in directionPoint.indices) {
                        rectLine.add(directionPoint.get(i))
                    }

                    if (firsttimepolyline!!) {
                        // destination marker
                        val destn_marker_view =
                            (context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(
                                R.layout.custom_drop_marker,
                                null
                            )
                        val tv_marker_drop =
                            destn_marker_view.findViewById<TextView>(R.id.tv_marker_drop)
                        tv_marker_drop.text = str_drop_location
                        val destn_bitmap = createBitmapFromLayout(destn_marker_view)
                        val destn_small_bitmap =
                            Bitmap.createScaledBitmap(
                                destn_bitmap,
                                markerWidth,
                                markerHeight,
                                false
                            )
                        val m = arrayOfNulls<Marker>(2)
                        m[0] = mMap!!.addMarker(
                            MarkerOptions().position(startlatlng!!).icon(
                                BitmapDescriptorFactory.fromResource(
                                    R.drawable.current_location
                                )
                            )
                        )
                        m[1] = mMap!!.addMarker(
                            MarkerOptions().position(destlatlng!!)
//                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.current_location)
                                .icon(
                                    BitmapDescriptorFactory.fromBitmap(destn_bitmap)
                                )
                        )
                        val builder = LatLngBounds.Builder()
                        for (marker in m) {
                            builder.include(marker!!.getPosition())
                        }
                        val bounds = builder.build()
                        val padding = 100 // offset from edges of the map in pixels
                        val cu = CameraUpdateFactory.newLatLngBounds(bounds, padding)
                        mMap!!.moveCamera(cu)
                        mMap!!.animateCamera(cu)
                        markerOptions!!.position(startlatlng!!)
                    }

                    markerOptions!!.position(destlatlng!!)

                    // Adding route on the map
                    polyline = mMap!!.addPolyline(rectLine)
                    markerOptions!!.draggable(true)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getDistance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double {
        val latA = Math.toRadians(lat1)
        val lonA = Math.toRadians(lon1)
        val latB = Math.toRadians(lat2)
        val lonB = Math.toRadians(lon2)
        val cosAng =
            Math.cos(latA) * Math.cos(latB) * Math.cos(lonB - lonA) + Math.sin(latA) * Math.sin(latB)
        val ang = Math.acos(cosAng)
        return ang * 6371
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}

    //-------------------Show Summery fare  Method--------------------
    private fun showfaresummerydetails() {

        val dialog = Dialog(this@EndTrip)
        val view =
            LayoutInflater.from(this@EndTrip).inflate(R.layout.fare_summary, null)
        val Tv_reqest = view.findViewById<View>(R.id.requst) as TextView
        val tv_fare_totalamount =
            view.findViewById<View>(R.id.fare_summery_total_amount) as TextView
        val tv_ridedistance =
            view.findViewById<View>(R.id.fare_summery_ride_distance_value) as TextView
        val tv_timetaken =
            view.findViewById<View>(R.id.fare_summery_ride_timetaken_value) as TextView
        val tv_waittime = view.findViewById<View>(R.id.fare_summery_wait_time_value) as TextView
        val layout_request_payment =
            view.findViewById<View>(R.id.layout_faresummery_requstpayment) as RelativeLayout
        val layout_receive_cash =
            view.findViewById<View>(R.id.fare_summery_receive_cash_layout) as RelativeLayout
        tv_fare_totalamount.text = Str_ridefare
        tv_ridedistance.text = Str_ride_distance
        tv_timetaken.text = Str_timetaken
        tv_waittime.text = Str_waitingtime
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setContentView(view)
        dialog.show()
        //if (Str_need_payment.equalsIgnoreCase("YES")){
        layout_receive_cash.visibility = View.VISIBLE
        Log.e("RIDETYPE",Str_ride_type)
        if(Str_ride_type.equals("offline")){
            layout_request_payment.visibility = View.GONE
        }else{
            layout_request_payment.visibility = View.VISIBLE
        }
        Tv_reqest.text = this@EndTrip.resources.getString(R.string.lbel_fare_summery_requestpayment)


        layout_receive_cash.setOnClickListener {
            ////chaitanya////
            val buttonClick = AlphaAnimation(1F, 0.8f)
            layout_receive_cash.startAnimation(buttonClick)
            cd = ConnectionDetector(this@EndTrip)
            isInternetPresent = cd!!.isConnectingToInternet
            if (isInternetPresent!!) {
                PostRequest_payment(ServiceConstant.receivecash_url)
                System.out.println("end------------------" + ServiceConstant.receivecash_url)
            } else {

                Alert(
                    resources.getString(R.string.alert_sorry_label_title),
                    resources.getString(R.string.alert_nointernet)
                )
            }
        }

        layout_request_payment.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            layout_request_payment.startAnimation(buttonClick)
            cd = ConnectionDetector(this@EndTrip)
            isInternetPresent = cd!!.isConnectingToInternet
            if (isInternetPresent!!) {

                if (Tv_reqest.text.toString().equals(
                        this@EndTrip.resources.getString(R.string.lbel_fare_summery_requestpayment),
                        ignoreCase = true
                    )
                ) {
                    postRequest_Reqqustpayment(ServiceConstant.request_paymnet_url)
                    System.out.println("arrived------------------" + ServiceConstant.request_paymnet_url)
                } else {
                    val intent = Intent(this@EndTrip, RatingsPage::class.java)
                    startActivity(intent)
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                }
            } else {
                Alert(
                    resources.getString(R.string.alert_sorry_label_title),
                    resources.getString(R.string.alert_nointernet)
                )
            }
        }
    }

    private fun showfaresummerydetails1() {

        val dialog = Dialog(this@EndTrip)
        val view =
            LayoutInflater.from(this@EndTrip).inflate(R.layout.fare_summary, null)
        val Tv_reqest = view.findViewById<View>(R.id.requst) as TextView
        val tv_fare_totalamount =
            view.findViewById<View>(R.id.fare_summery_total_amount) as TextView
        val tv_ridedistance =
            view.findViewById<View>(R.id.fare_summery_ride_distance_value) as TextView
        val tv_timetaken =
            view.findViewById<View>(R.id.fare_summery_ride_timetaken_value) as TextView
        val tv_waittime = view.findViewById<View>(R.id.fare_summery_wait_time_value) as TextView
        val layout_request_payment =
            view.findViewById<View>(R.id.layout_faresummery_requstpayment) as RelativeLayout
        val layout_receive_cash =
            view.findViewById<View>(R.id.fare_summery_receive_cash_layout) as RelativeLayout
        tv_fare_totalamount.text = Str_ridefare
        tv_ridedistance.text = Str_ride_distance
        tv_timetaken.text = Str_timetaken
        tv_waittime.text = Str_waitingtime
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setContentView(view)
        dialog.show()
        // if (Str_need_payment.equalsIgnoreCase("YES")){
        layout_receive_cash.visibility = View.GONE

        Log.e("RIDETYPE",Str_ride_type)
        if(Str_ride_type.equals("offline")){
            layout_request_payment.visibility = View.GONE
        }else{
            layout_request_payment.visibility = View.VISIBLE
        }


        Tv_reqest.text = this@EndTrip.resources.getString(R.string.lbel_fare_summery_requestpayment)

        layout_request_payment.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            layout_request_payment.startAnimation(buttonClick)
            cd = ConnectionDetector(this@EndTrip)
            isInternetPresent = cd!!.isConnectingToInternet
            if (isInternetPresent!!) {
                if (Tv_reqest.text.toString().equals(
                        this@EndTrip.resources.getString(R.string.lbel_fare_summery_requestpayment),
                        ignoreCase = true
                    )
                ) {
                    postRequest_Reqqustpayment(ServiceConstant.request_paymnet_url)
                    println("arrived------------------" + ServiceConstant.request_paymnet_url)
                } else {
                    val intent = Intent(this@EndTrip, RatingsPage::class.java)
                    startActivity(intent)
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                }
            } else {
                Alert(
                    resources.getString(R.string.alert_sorry_label_title),
                    resources.getString(R.string.alert_nointernet)
                )
            }
        }
    }

    private fun showfaresummerydetails2() {

        val dialog = Dialog(this@EndTrip)
        val view =
            LayoutInflater.from(this@EndTrip).inflate(R.layout.fare_summary, null)
        val Tv_reqest = view.findViewById<View>(R.id.requst) as TextView
        val tv_fare_totalamount =
            view.findViewById<View>(R.id.fare_summery_total_amount) as TextView
        val tv_ridedistance =
            view.findViewById<View>(R.id.fare_summery_ride_distance_value) as TextView
        val tv_timetaken =
            view.findViewById<View>(R.id.fare_summery_ride_timetaken_value) as TextView
        val tv_waittime = view.findViewById<View>(R.id.fare_summery_wait_time_value) as TextView
        val layout_request_payment =
            view.findViewById<View>(R.id.layout_faresummery_requstpayment) as RelativeLayout
        val layout_receive_cash =
            view.findViewById<View>(R.id.fare_summery_receive_cash_layout) as RelativeLayout
        tv_fare_totalamount.text = Str_ridefare
        tv_ridedistance.text = Str_ride_distance
        tv_timetaken.text = Str_timetaken
        tv_waittime.text = Str_waitingtime
        dialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setContentView(view)
        dialog.show()
        // if (Str_need_payment.equalsIgnoreCase("YES")){
        layout_receive_cash.visibility = View.GONE
        layout_request_payment.visibility = View.VISIBLE
        Tv_reqest.text = this@EndTrip.resources.getString(R.string.lbel_notification_ok)

        layout_request_payment.setOnClickListener {
            val buttonClick = AlphaAnimation(1F, 0.8f)
            layout_request_payment.startAnimation(buttonClick)
            cd = ConnectionDetector(this@EndTrip)
            isInternetPresent = cd!!.isConnectingToInternet
            if (isInternetPresent!!) {

                val intent = Intent(this@EndTrip, RatingsPage::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)

            } else {
                Alert(
                    resources.getString(R.string.alert_sorry_label_title),
                    resources.getString(R.string.alert_nointernet)
                )
            }
        }
    }

    //-----------------------Code for begin trip post request-----------------
    private fun PostRequest(Url: String) {
        dialog = Dialog(this@EndTrip)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------endtrip----------------$Url")
        println("-------------wait_time----------------$mins")

        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id!!
        jsonParams["ride_id"] = Str_rideid!!
        jsonParams["drop_lat"] = MyCurrent_lat.toString()
        jsonParams["drop_lon"] = MyCurrent_long.toString()
        jsonParams["distance"] = (dis / 1000).toString()
        jsonParams["wait_time"] = timerValue!!.text.toString()

        println("--------------driver_id-------------------" + driver_id!!)
        println("--------------drop_lat-------------------$MyCurrent_lat")
        println("--------------drop_lon-------------------$MyCurrent_long")
        println("--------------postdistance-------------------" + (dis / 1000).toString())
        println("--------------wait_time-------------------" + timerValue!!.text.toString())

        mRequest = ServiceRequest(this@EndTrip)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    Log.e("end", response)
                    println("endtrip---------$response")
                    //  String Str_status = "",Str_response="",Str_ridefare="",Str_timetaken="",Str_waitingtime="",Str_currency="",Str_ride_distance="";
                    try {
                        val `object` = JSONObject(response)
                        Str_status = `object`.getString("status")
                        Str_response = `object`.getString("response")

                        val jsonObject = `object`.getJSONObject("response")
                        val jobject = jsonObject.getJSONObject("fare_details")
                        Str_need_payment = jsonObject.getString("need_payment")
                        str_recievecash = jsonObject.getString("receive_cash")
                        Str_currency = jobject.getString("currency")

                        //Currency currencycode = Currency.getInstance(getLocale(Str_currency));
                        sCurrencySymbol = CurrencySymbolConverter.getCurrencySymbol(Str_currency)

                        Str_ridefare = sCurrencySymbol!! + jobject.getString("ride_fare")
                        Str_timetaken = jobject.getString("ride_duration")
                        Str_waitingtime = jobject.getString("waiting_duration")
                        Str_ride_distance = jobject.getString("ride_distance")
                        Str_need_payment = jobject.getString("need_payment")
                        Str_ride_type = jsonObject.getString("ride_type")


                        Log.d("RECEIVE", str_recievecash+"------"+Str_ride_type)

                    } catch (e: Exception) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }
                    dialog!!.dismiss()
                    if (Str_status.equals("1", ignoreCase = true)) {
                        //  endTripHandler.removeCallbacks(endTripRunnable);
                        if (Str_need_payment.equals("YES", ignoreCase = true)) {
                            println("sucess------------$Str_need_payment")
                            if (str_recievecash.matches("Enable".toRegex())) {
                                showfaresummerydetails()
                            } else {
                                showfaresummerydetails1()
                            }
                        } else {
                            showfaresummerydetails2()
                        }
                    } else {
                        Alert(resources.getString(R.string.alert_sorry_label_title), Str_response)
                    }
                    dialog!!.dismiss()
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }
            })
    }

    //-----------------------Code for arrived post request-----------------chaitanya
    private fun PostRequest_payment(Url: String) {
        dialog = Dialog(this@EndTrip)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------otp----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id!!
        jsonParams["ride_id"] = Str_rideid!!

        println("otp-------driver_id---------" + driver_id!!)
        println("otp-------ride_id---------" + Str_rideid!!)
        mRequest = ServiceRequest(this@EndTrip)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    Log.e("otp", response)
                    println("otp---------$response")

                    var Str_status = ""
                    var Str_response = ""
                    var Str_otp_status = ""
                    var Str_ride_id = ""
                    var Str_currency = ""

                    try {
                        val `object` = JSONObject(response)
                        Str_status = `object`.getString("status")

                        if (Str_status.equals("1", ignoreCase = true)) {

                            Str_response = `object`.getString("response")
                            Str_currency = `object`.getString("currency")
                            //Currency currencycode = Currency.getInstance(getLocale(Str_currency));

                            sCurrencySymbol =
                                CurrencySymbolConverter.getCurrencySymbol(Str_currency)

                            Str_otp_status = `object`.getString("otp_status")
                            Str_otp = `object`.getString("otp")
                            Str_ride_id = `object`.getString("ride_id")
                            Str_amount = sCurrencySymbol!! + `object`.getString("amount")

                            println("otp--------$Str_otp")

                            println("Str_otp_status--------$Str_otp_status")
                            val intent = Intent(this@EndTrip, PaymentPage::class.java)
                            intent.putExtra("amount", Str_amount)
                            intent.putExtra("rideid", Str_rideid)
                            startActivity(intent)
                            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
                        } else {
                            Str_response = `object`.getString("response")
                        }
                    } catch (e: Exception) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }
                    dialog!!.dismiss()
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }
            })
    }

    //-----------------------Code for arrived post request-----------------
    private fun postRequest_Reqqustpayment(Url: String) {
        dialog = Dialog(this@EndTrip)
        dialog!!.window
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog!!.setCanceledOnTouchOutside(false)
        dialog!!.show()

        val dialog_title = dialog!!.findViewById<View>(R.id.custom_loading_textview) as TextView

        dialog_title.text = resources.getString(R.string.action_loading)
        println("-------------endtrip----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["driver_id"] = driver_id!!
        jsonParams["ride_id"] = Str_rideid!!

        println("--------------driver_id-------------------" + driver_id!!)
        println("--------------ride_id-------------------" + Str_rideid!!)

        mRequest = ServiceRequest(this@EndTrip)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    Log.e("requestpayment", response)
                    println("response---------$response")

                    var Str_status = ""
                    var Str_response = ""
                    val Str_currency = ""
                    val Str_rideid = ""
                    val Str_action = ""

                    try {
                        val `object` = JSONObject(response)
                        Str_response = `object`.getString("response")
                        Str_status = `object`.getString("status")
                    } catch (e: Exception) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }
                    if (Str_status.equals("0", ignoreCase = true)) {
                        Alert(resources.getString(R.string.alert_sorry_label_title), Str_response)
                    } else {
                        Alert(
                            resources.getString(R.string.label_pushnotification_cashreceived),
                            Str_response
                        )
                    }
                }

                override fun onErrorListener() {
                    dialog!!.dismiss()
                }
            })
    }

    fun onRoutingSuccess(mPolyOptions: PolylineOptions) {
        val polyoptions = PolylineOptions()
        polyoptions.color(Color.BLUE)
        polyoptions.width(10f)
        polyoptions.addAll(mPolyOptions.points)
        mMap!!.addPolyline(polyoptions)
    }

    //-----------------Move Back on  phone pressed  back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            // nothing
            true
        } else false
    }

    //Enabling Gps Service
    private fun enableGpsService() {
        mLocationRequest = LocationRequest.create()
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest!!.interval = (30 * 1000).toLong()
        mLocationRequest!!.fastestInterval = (5 * 1000).toLong()

        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(mLocationRequest!!)
        builder.setAlwaysShow(true)

        result =
            LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build())

        result!!.setResultCallback { result ->
            val status = result.status
            //final LocationSettingsStates state = result.getLocationSettingsStates();
            when (status.statusCode) {
                LocationSettingsStatusCodes.SUCCESS -> {
                }
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                    try {

                        status.startResolutionForResult(this@EndTrip, REQUEST_LOCATION)
                    } catch (e: IntentSender.SendIntentException) {
                    }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_LOCATION) {
            println("----------inside request location------------------")

            when (resultCode) {
                Activity.RESULT_OK -> {
                    Toast.makeText(this@EndTrip, "Location enabled!", Toast.LENGTH_LONG).show()
                }
                Activity.RESULT_CANCELED -> {
                    enableGpsService()
                }
                else -> {
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
        if (progress > 95) {
            seekBar.thumb = resources.getDrawable(R.drawable.slidetounlock_arrow)
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar) {
        Bt_slider!!.visibility = View.INVISIBLE
    }

    override fun onStopTrackingTouch(seekBar: SeekBar) {
        //shimmer = new Shimmer();
        if (seekBar.progress < 80) {
            seekBar.progress = 0
            sliderSeekBar!!.setBackgroundResource(R.drawable.red_slide_to_unlock_bg)
            Bt_slider!!.visibility = View.VISIBLE
            Bt_slider!!.text = resources.getString(R.string.lbel_endtrip)
            //shimmer.start(Bt_slider); chaitanya
        } else if (seekBar.progress > 90) {
            seekBar.progress = 100
            Bt_slider!!.visibility = View.VISIBLE
            Bt_slider!!.text = resources.getString(R.string.lbel_endtrip)
            //shimmer.start(Bt_slider); chaitanya
            sliderSeekBar!!.visibility = View.VISIBLE
            println("------------------sliding completed----------------")

            cd = ConnectionDetector(this@EndTrip)
            isInternetPresent = cd!!.isConnectingToInternet

            if (isInternetPresent!!) {
                PostRequest(ServiceConstant.endtrip_url)
                println("end------------------" + ServiceConstant.endtrip_url)
            } else {
                Alert(
                    resources.getString(R.string.alert_sorry_label_title),
                    resources.getString(R.string.alert_nointernet)
                )
            }
        }
    }

    //-----------------------Update current Location for notification  Post Request-----------------
    private fun postRequest_UpdateProviderLocation(Url: String) {

        val jsonParams = HashMap<String, String>()
        jsonParams["ride_id"] = Str_rideid!!
        jsonParams["latitude"] = Str_Latitude
        jsonParams["longitude"] = Str_longitude
        jsonParams["driver_id"] = driver_id!!

        println("-------------Endtripride_id----------------$Str_longitude")
        println("-------------Endtriplatitude----------------$Str_Latitude")
        println("-------------Endtriplongitude----------------$Str_longitude")
        println("-------------latlongupdate----------------$Url")

        mRequest = ServiceRequest(this@EndTrip)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {
                    Log.e("updatelocation", response)
                    println("-------------latlongupdate----------------$response")
                    // updateDriverOnMap(Double.parseDouble(Str_Latitude),Double.parseDouble(Str_longitude));
                }

                override fun onErrorListener() {}
            })
    }

    override fun onPause() {
        super.onPause()
        System.gc()
        if (chat != null) {
            chat!!.close()
        }
        mHandler.removeCallbacks(mHandlerTask)
        // stopService(mServiceIntent);
        // Log.i("MAINACT","onDestroy!");
    }

    override fun onDestroy() {
        super.onDestroy()
        mHandler.removeCallbacks(mHandlerTask)
        //stopService(mServiceIntent);
        ///mSensorService.onDestroy();
        Log.i("MAINACT", "onDestroy!")
    }

    private fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                Log.i("isMyServiceRunning?", true.toString() + "")
                return true
            }
        }
        Log.i("isMyServiceRunning?", false.toString() + "")
        return false
    }

    private fun updateDriverOnMap(lat_decimal: Double, lng_decimal: Double) {
        val latLng = LatLng(lat_decimal, lng_decimal)
        Log.d("tess", "inside run ")
        if (currentMarker != null) {
            Log.d("UPDATEDRIVERONMAP", "inside run IF")
            // moveVechile(latLng, currentMarker);
            if (isFirstPosition) {
                startPosition = marker!!.position
                currentMarker!!.setAnchor(0.5f, 0.5f)
                mMap!!.moveCamera(
                    CameraUpdateFactory
                        .newCameraPosition(
                            CameraPosition.Builder()
                                .target(startPosition)
                                .zoom(15.5f)
                                .build()
                        )
                )
                isFirstPosition = false
            } else {
                endPosition = LatLng(latLng.latitude, latLng.longitude)

                Log.d(
                    "dhasgdh",
                    startPosition!!.latitude.toString() + "--" + endPosition!!.latitude + "--Check --" + startPosition!!.longitude + "--" + endPosition!!.longitude
                )

                if (startPosition!!.latitude != endPosition!!.latitude || startPosition!!.longitude != endPosition!!.longitude) {
                    Log.e("jhdhasd", "NOT SAME")
                    startBikeAnimation(startPosition!!, endPosition!!)
                } else {
                    Log.e("hdgahsdg", "SAMME")
                }
            }
        } else {
            Log.d("UPDATEDRIVERONMAP", "inside run ELSE")
            /* mLatLngInterpolator = new LatLngInterpolator.Linear();*/
            currentMarker = mMap!!.addMarker(
                MarkerOptions()
                    .position(latLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.dash_car))
                    .anchor(0.5f, 0.5f)
                    .flat(true)
            )

            val cameraPosition = CameraPosition.Builder().target(latLng).zoom(18f).build()
            val camUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
            mMap!!.moveCamera(camUpdate)
            mMap!!.animateCamera(
                CameraUpdateFactory.newLatLngZoom(
                    latLng, 15.5f
                )
            )
        }
    }

    private fun startBikeAnimation(start: LatLng, end: LatLng) {
        val handler = Handler()
        Log.i("Inside", "startBikeAnimation called...")

        val valueAnimator = ValueAnimator.ofFloat(0F, 1F)
        valueAnimator.duration = 3000
        valueAnimator.interpolator = LinearInterpolator()
        valueAnimator.addUpdateListener { valueAnimator ->
            //LogMe.i(TAG, "Car Animation Started...");
            v = valueAnimator.animatedFraction
            lng = v * end.longitude + (1 - v) * start.longitude
            lat = v * end.latitude + (1 - v) * start.latitude

            val newPos = LatLng(lat, lng)
            currentMarker!!.setPosition(newPos)
            currentMarker!!.setAnchor(0.5f, 0.5f)
            currentMarker!!.rotation = getBearing(start, end)

            // todo : Shihab > i can delay here
            mMap!!.moveCamera(
                CameraUpdateFactory
                    .newCameraPosition(
                        CameraPosition.Builder()
                            .target(newPos)
                            .zoom(15.5f)
                            .build()
                    )
            )

            startPosition = currentMarker!!.position
            /*Polyline line = googleMap.addPolyline(new PolylineOptions().add(startPosition,endPosition)
                        .width(5).color(Color.BLUE));*/
        }
        valueAnimator.start()
        firsttimepolyline = false

        //to add new polyline
        val runnable = Runnable {
            startlatlng = start
            if (startlatlng != null) {
                val getRoute = GetRouteTask()
                getRoute.execute()
            }
        }
        handler.postDelayed(runnable, 3000)
    }

    //Method for finding bearing between two points
    private fun getBearing(begin: LatLng, end: LatLng): Float {
        Log.e("getbearing_inside", "bearing")
        val lat = Math.abs(begin.latitude - end.latitude)
        val lng = Math.abs(begin.longitude - end.longitude)

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return Math.toDegrees(Math.atan(lng / lat)).toFloat()
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 90).toFloat()
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (Math.toDegrees(Math.atan(lng / lat)) + 180).toFloat()
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 270).toFloat()

        return 1f

    }

    companion object {
        private val REQUEST_LOCATION = 199
        var myLocation: Location? = null
        private val movingMarker: Marker? = null

        private val INTERVAL = 10000
        fun calculateDistanceTo(fromLocation: Location, toLocation: Location): FloatArray {
            val results = FloatArray(0)
            val startLatitude = fromLocation.latitude
            val startLongitude = fromLocation.longitude
            val endLatitude = toLocation.latitude
            val endLongitude = toLocation.longitude
            Location.distanceBetween(
                startLatitude,
                startLongitude,
                endLatitude,
                endLongitude,
                results
            )
            return results
        }

        //method to convert currency code to currency symbol
        private fun getLocale(strCode: String): Locale? {

            for (locale in NumberFormat.getAvailableLocales()) {
                val code = NumberFormat.getCurrencyInstance(locale).currency!!.currencyCode
                if (strCode == code) {
                    return locale
                }
            }
            return null
        }
    }

    fun createBitmapFromLayout(view: View): Bitmap {

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)

        view.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
        view.buildDrawingCache()
        val bitmap =
            Bitmap.createBitmap(view.measuredWidth, view.measuredHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        view.draw(canvas)

        return bitmap
    }


    override fun onRestart() {
        super.onRestart()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                !isInPictureInPictureMode
            }
    }


    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        adjustFullScreen(newConfig)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            adjustFullScreen(resources.configuration)
        }
    }

    override fun onPictureInPictureModeChanged(
        isInPictureInPictureMode: Boolean, newConfig: Configuration
    ) {
        super.onPictureInPictureModeChanged(isInPictureInPictureMode, newConfig)
        if (isInPictureInPictureMode) {
            // Show the video controls if the video is not playing

            fromPictureInPictureMode = isInPictureInPictureMode
            Log.e("INSIDE", "" + isInPictureInPictureMode)
            Bt_slider!!.visibility = View.GONE
            layout_end_trip_details!!.visibility = View.GONE
            layout_end_trip_header!!.visibility = View.GONE

        } else {
            fromPictureInPictureMode = isInPictureInPictureMode
            Log.e("INSIDE", "" + isInPictureInPictureMode)
            Bt_slider!!.visibility = View.VISIBLE
            layout_end_trip_details!!.visibility = View.VISIBLE
            layout_end_trip_header!!.visibility = View.VISIBLE
        }
    }

    private fun adjustFullScreen(config: Configuration) {
        val decorView = window.decorView
        if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            //mScrollView.visibility = View.GONE
            //rl_signupsignin.setAdjustViewBounds(false)
        } else {
            decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            // mScrollView.visibility = View.VISIBLE
            // rl_signupsignin.setAdjustViewBounds(true)
        }
    }


    override fun onUserLeaveHint() {
        Log.d("onUserLeaveHint", "Home button pressed")
        println("KEYCODE_HOME")
        //minimize()
    }


    internal fun minimize() {
        // Hide the controls in picture-in-picture mode.
        // Calculate the aspect ratio of the PiP screen.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            try {
                val display = windowManager.defaultDisplay
                val point =
                    Point() // Pointer captures height & width of the current screen & reduces the aspect ratio as per required
                display.getSize(point)
                mPictureInPictureParamsBuilder!!.setAspectRatio(Rational(point.x, point.y))
                //val aspectRatio = Rational(rl_pip!!.width, rl_pip!!.height)
                //mPictureInPictureParamsBuilder!!.setAspectRatio(aspectRatio).build()
                enterPictureInPictureMode(mPictureInPictureParamsBuilder!!.build())
            } catch (e: Exception) {
                Log.e("PIPEXCEPTION", e.toString())
            }
        } else {
            //TODO("VERSION.SDK_INT < O")
        }


    }


}
