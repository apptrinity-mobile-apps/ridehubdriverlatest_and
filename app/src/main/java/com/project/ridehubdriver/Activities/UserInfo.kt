package com.project.ridehubdriver.Activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.ConnectionDetector
import com.project.ridehubdriver.Utils.RoundedImageView
import com.project.ridehubdriver.Utils.SessionManager
import com.project.ridehubdriver.customRatingBar.RotationRatingBar
import com.project.ridehubdriver.subclass.SubclassActivity
import com.squareup.picasso.MemoryPolicy
import com.squareup.picasso.Picasso

/**
 * Created by user88 on 10/30/2015.
 */
class UserInfo : SubclassActivity() {

    private var cd: ConnectionDetector? = null
    private var isInternetPresent: Boolean? = false
    private var session: SessionManager? = null

    private var Tv_userName: TextView? = null
    private var Tv_usermobile_no: TextView? = null
    private var Tv_username_header: TextView? = null
    private var ratingBar: RotationRatingBar? = null
    private var Rl_layout_back: ImageView? = null
    private var Bt_canceltrip: LinearLayout? = null
    private var userimage: RoundedImageView? = null

    private var Str_username: String? = ""
    private var Str_userrating: String? = ""
    private var Str_usermobilno: String? = ""
    private var Str_rideId: String? = ""
    private var Str_user_img: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.user_info)
        userInfo_class = this@UserInfo
        initialize()
        Rl_layout_back!!.setOnClickListener {
            onBackPressed()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }
        Bt_canceltrip!!.setOnClickListener {
            val intent = Intent(this@UserInfo, CancelTrip::class.java)
            intent.putExtra("RideId", Str_rideId)
            startActivity(intent)
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

    }

    private fun initialize() {
        cd = ConnectionDetector(this@UserInfo)
        isInternetPresent = cd!!.isConnectingToInternet
        session = SessionManager(this@UserInfo)
        Tv_userName = findViewById<View>(R.id.userinfo_usernamedetail) as TextView
        Tv_usermobile_no = findViewById<View>(R.id.userinfo_user_mobileno) as TextView
        ratingBar = findViewById(R.id.user_ratings)
        Tv_username_header = findViewById<View>(R.id.user_info_user_name_head) as TextView
        Rl_layout_back = findViewById<View>(R.id.layout_user_info_back) as ImageView
        Bt_canceltrip = findViewById<View>(R.id.userinfo_canceltrip) as LinearLayout
        userimage = findViewById<View>(R.id.userinfo_user_image) as RoundedImageView
        val i = intent
        Str_username = i.getStringExtra("user_name")
        Str_userrating = i.getStringExtra("user_rating")
        Str_usermobilno = i.getStringExtra("user_phoneno")
        Str_user_img = i.getStringExtra("user_image")
        Str_rideId = i.getStringExtra("RideId")
        Tv_userName!!.text = Str_username
        Tv_usermobile_no!!.text = Str_usermobilno
        ratingBar!!.rating = java.lang.Float.parseFloat(Str_userrating!!)
        Tv_username_header!!.text = Str_username
        Picasso.with(this@UserInfo).load(Str_user_img.toString()).placeholder(R.drawable.no_profile_image_avatar_icon)
            .memoryPolicy(MemoryPolicy.NO_CACHE).into(userimage)
    }

    companion object {

        lateinit var userInfo_class: UserInfo
    }


}
