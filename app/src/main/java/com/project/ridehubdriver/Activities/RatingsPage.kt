package com.project.ridehubdriver.Activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.animation.AlphaAnimation
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.project.ridehubdriver.Adapters.Reviews_adapter_2
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Helper.ServiceRequest
import com.project.ridehubdriver.Hockeyapp.ActivityHockeyApp
import com.project.ridehubdriver.PojoResponse.Reviwes_Pojo
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.*
import org.json.JSONObject
import java.util.*

/**
 * Created by user88 on 11/4/2015.
 */
class RatingsPage : ActivityHockeyApp() {
    private var driver_id: String? = ""
    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null
    private var session: SessionManager? = null
    internal lateinit var postrequest: StringRequest
    private var empty_Tv: TextView? = null
    internal lateinit var adapter: Reviews_adapter_2
    private var reivweslist: ArrayList<Reviwes_Pojo>? = null
    private var show_progress_status = false
    internal lateinit var dialog: Dialog
    private var mRequest: ServiceRequest? = null

    private var Tv_skip: TextView? = null
    internal lateinit var listview: RecyclerView
    private var Str_rideid: String? = ""

    internal lateinit var Bt_rate_rider: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.activity_rating_final)

        /*val config = resources.configuration
        if (config.smallestScreenWidthDp >= 600) {
            setContentView(R.layout.activity_rating)
        } else {
            setContentView(R.layout.reviwes_list)
        }*/

        initialize()

        Tv_skip!!.setOnClickListener {
            val broadcastIntent_tripdetail = Intent()
            broadcastIntent_tripdetail.action = "com.finish.endtripenterdetail"
            sendBroadcast(broadcastIntent_tripdetail)

            val broadcastIntent_begintrip = Intent()
            broadcastIntent_begintrip.action = "com.finish.com.finish.BeginTrip"
            sendBroadcast(broadcastIntent_begintrip)

            val broadcastIntent = Intent()
            broadcastIntent.action = "com.finish.EndTrip"
            sendBroadcast(broadcastIntent)
            finish()

            val broadcastIntent_drivermap = Intent()
            broadcastIntent_drivermap.action = "com.finish.canceltrip.DriverMapActivity"
            sendBroadcast(broadcastIntent_drivermap)
            finish()


            val broadcastIntent_loading = Intent()
            broadcastIntent_loading.action = "com.finish.loadingpage"
            sendBroadcast(broadcastIntent_loading)
            finish()
        }

        Bt_rate_rider.setOnClickListener {
            var isRatingEmpty = false
            val buttonClick = AlphaAnimation(1F, 0.8f)
            Bt_rate_rider.startAnimation(buttonClick)
            println("btnclick------------------")

            if (reivweslist != null) {
                for (i in reivweslist!!.indices) {
                    if (reivweslist!![i].ratings_count!!.length == 0 || reivweslist!![i].ratings_count!!.equals(
                            "0.0",
                            ignoreCase = true
                        )
                    ) {
                        isRatingEmpty = true
                    }

                    println("btnclick2------------------")
                }

                if (!isRatingEmpty) {
                    if (isInternetPresent!!) {

                        println("btnclick3------------------")
                        println("------------ride_id-------------" + Str_rideid!!)

                        val jsonParams = HashMap<String, String>()
                        jsonParams["ratingsFor"] = "rider"
                        jsonParams["ride_id"] = Str_rideid!!

                        println("ride_id------------------" + Str_rideid!!)

                        println("ratingsFor------------------" + "rider")

                        for (i in reivweslist!!.indices) {
                            jsonParams["ratings[$i][option_id]"] = reivweslist!![i].options_id!!
                            jsonParams["ratings[$i][option_title]"] = reivweslist!![i].options_title!!
                            jsonParams["ratings[$i][rating]"] = reivweslist!![i].ratings_count!!

                            println("option_id------------------" + reivweslist!![i].options_id!!)
                            println("option_title------------------" + reivweslist!![i].options_title!!)
                            println("rating------------------" + reivweslist!![i].ratings_count!!)
                        }
                        Post_RequestReviwes(ServiceConstant.submit_reviwes_url, jsonParams)

                    } else {
                        Alert(
                            resources.getString(R.string.alert_sorry_label_title),
                            resources.getString(R.string.alert_nointernet)
                        )
                    }
                } else {
                    Alert(
                        resources.getString(R.string.lbel_notification),
                        resources.getString(R.string.lbel_notification_selectrating)
                    )
                }

            }
        }

    }


    private fun initialize() {
        session = SessionManager(this@RatingsPage)
        // get user data from session
        val user = session!!.userDetails
        driver_id = user[SessionManager.KEY_DRIVERID]
        reivweslist = ArrayList()

        listview = findViewById(R.id.listView_rating) as RecyclerView
        empty_Tv = findViewById(R.id.reviwes_no_textview) as TextView
        Bt_rate_rider = findViewById(R.id.btn_submit_reviwes) as TextView


        val i = intent
        Str_rideid = i.getStringExtra("rideid")

        Tv_skip = findViewById(R.id.review_skip) as TextView

        cd = ConnectionDetector(this@RatingsPage)
        isInternetPresent = cd!!.isConnectingToInternet

        if (isInternetPresent!!) {
            PostRequest(ServiceConstant.reviwes_options_list_url)
            System.out.println("raatingslist------------------" + ServiceConstant.reviwes_options_list_url)
        } else {
            Alert(
                resources.getString(R.string.alert_sorry_label_title),
                resources.getString(R.string.alert_nointernet)
            )

        }
    }

    //--------------Alert Method-----------
    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(this@RatingsPage)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            resources.getString(R.string.alert_label_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    //-----------------------Code for reviwes options list post request-----------------
    private fun PostRequest(Url: String) {
        dialog = Dialog(this@RatingsPage)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.custom_loading)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById<View>(R.id.custom_loading_textview) as TextView
        dialog_title.text = resources.getString(R.string.action_loading)

        println("-------------dashboard----------------$Url")

        val jsonParams = HashMap<String, String>()
        jsonParams["optionsFor"] = "rider"
        jsonParams["ride_id"] = Str_rideid!!


        mRequest = ServiceRequest(this@RatingsPage)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    Log.e("reviwes", response)

                    var ride_ratting_status = ""

                    var status = ""
                    var Str_total = ""
                    val Str_Rating = ""

                    try {
                        val `object` = JSONObject(response)
                        status = `object`.getString("status")
                        Str_total = `object`.getString("total")
                        ride_ratting_status = `object`.getString("ride_ratting_status")

                        if (ride_ratting_status.equals("1", ignoreCase = true)) {
                            val i = Intent(this@RatingsPage, SignUpSignIn::class.java)
                            startActivity(i)
                            finish()


                        }

                        println("status---------" + `object`.getString("status"))

                        val jarry = `object`.getJSONArray("review_options")

                        if (jarry.length() > 0) {

                            for (i in 0 until jarry.length()) {

                                val jobject = jarry.getJSONObject(i)

                                val item = Reviwes_Pojo()

                                item.options_title = jobject.getString("option_title")
                                item.ratings_count = ""
                                item.options_id = jobject.getString("option_id")

                                reivweslist!!.add(item)

                            }
                            show_progress_status = true

                        } else {
                            show_progress_status = false
                        }
                    } catch (e: Exception) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }





                    if (status.equals("1", ignoreCase = true)) {
                        val layoutManager = LinearLayoutManager(
                            (this@RatingsPage)
                            , LinearLayoutManager.VERTICAL, false
                        )
                        listview!!.layoutManager = layoutManager
                        listview!!.itemAnimator = DefaultItemAnimator()
                        adapter = Reviews_adapter_2(this@RatingsPage, reivweslist!!)
                        listview.adapter = adapter
                        dialog.dismiss()
                    } else {
                        Toast.makeText(
                            applicationContext,
                            resources.getString(R.string.fetchdatatoast),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    if (show_progress_status) {
                        empty_Tv!!.visibility = View.GONE
                    } else {
                        empty_Tv!!.visibility = View.VISIBLE
                        //listview.emptyView = empty_Tv
                    }

                }

                override fun onErrorListener() {

                    dialog.dismiss()
                }

            })
    }


    private fun Post_RequestReviwes(Url: String, jsonParams: Map<String, String>) {
        dialog = Dialog(this@RatingsPage)
        dialog.window
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.rating_loading_layout)
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()

        val dialog_title = dialog.findViewById(R.id.custom_loading_textview_rating) as TextView
        dialog_title.text = resources.getString(R.string.action_loading_rating)


        postrequest = object : StringRequest(Request.Method.POST, Url,
            Response.Listener { response ->
                Log.e("reviwes", response)

                println("reviwes------------$response")

                var status = ""
                var Str_response = ""

                try {
                    val `object` = JSONObject(response)
                    status = `object`.getString("status")
                    Str_response = `object`.getString("response")

                    println("status------$status")

                } catch (e: Exception) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }

                if (status.equals("1", ignoreCase = true)) {
                    val mdialog = PkDialog2(this@RatingsPage)
                    mdialog.setDialogTitle(resources.getString(R.string.action_loading_sucess))
                    mdialog.setDialogMessage(Str_response)
                    mdialog.setPositiveButton(
                        resources.getString(R.string.alert_label_ok),
                        View.OnClickListener {
                            mdialog.dismiss()

                            val broadcastIntent_tripdetail = Intent()
                            broadcastIntent_tripdetail.action = "com.finish.endtripenterdetail"
                            sendBroadcast(broadcastIntent_tripdetail)

                            val broadcastIntent_drivermap = Intent()
                            broadcastIntent_drivermap.action =
                                "com.finish.canceltrip.DriverMapActivity"
                            sendBroadcast(broadcastIntent_drivermap)
                            finish()

                            val broadcastIntent_loading = Intent()
                            broadcastIntent_loading.action = "com.finish.loadingpage"
                            sendBroadcast(broadcastIntent_loading)
                            finish()
                        }
                    )
                    mdialog.show()

                } else {
                    Alert(resources.getString(R.string.alert_sorry_label_title), Str_response)
                }
                dialog.dismiss()
            }, Response.ErrorListener { error ->
                VolleyErrorResponse.VolleyError(this@RatingsPage, error)
                dialog.dismiss()
            }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["User-agent"] = ServiceConstant.useragent
                headers["isapplication"] = ServiceConstant.isapplication
                headers["applanguage"] = ServiceConstant.applanguage
                return headers
            }

            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                return jsonParams
            }
        }
        AppController.instance!!.addToRequestQueue(postrequest)
    }


    //-----------------Move Back on  phone pressed  back button------------------
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK && event.repeatCount == 0) {
            // nothing
            true
        } else false
    }


}
