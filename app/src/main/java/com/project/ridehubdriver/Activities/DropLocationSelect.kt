package com.project.ridehubdriver.Activities

import android.app.Activity
import android.content.Intent
import android.location.Geocoder
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.GooglePlayServicesUtil
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.ConnectionDetector
import com.project.ridehubdriver.Utils.GPSTracker
import com.project.ridehubdriver.Utils.PkDialog
import java.util.*

/**
 * Created by Prem Kumar and Anitha on 2/26/2016.
 */
class DropLocationSelect : AppCompatActivity(), OnMapReadyCallback {
    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap
        // Changing map type
        mMap!!.mapType = GoogleMap.MAP_TYPE_NORMAL
        // Showing / hiding your current location
        mMap!!.isMyLocationEnabled = false
        // Enable / Disable zooming controls
        mMap!!.uiSettings.isZoomControlsEnabled = false
        // Enable / Disable my location button
        mMap!!.uiSettings.isMyLocationButtonEnabled = false
        // Enable / Disable Compass icon
        mMap!!.uiSettings.isCompassEnabled = false
        // Enable / Disable Rotate gesture
        mMap!!.uiSettings.isRotateGesturesEnabled = true
        // Enable / Disable zooming functionality
        mMap!!.uiSettings.isZoomGesturesEnabled = true
        mMap!!.isMyLocationEnabled = false
        if (gps!!.canGetLocation() && gps!!.isgpsenabled()) {

            val Dlatitude = gps!!.getLatitude()
            val Dlongitude = gps!!.getLongitude()

            // Move the camera to last position with a zoom level
            val cameraPosition =
                CameraPosition.Builder().target(LatLng(Dlatitude, Dlongitude)).zoom(17f).build()
            mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

        } else {
            Alert(
                resources.getString(R.string.alert_sorry_label_title),
                resources.getString(R.string.alert_gpsEnable)
            )
        }


        if (CheckPlayService()) {
            mMap!!.setOnCameraChangeListener(mOnCameraChangeListener)
            mMap!!.setOnMarkerClickListener { marker ->
                val tittle = marker.title
                true
            }
        } else {
            Toast.makeText(
                this@DropLocationSelect,
                "Install Google Play service To View Location !!!",
                Toast.LENGTH_LONG
            ).show()
        }

    }

    internal lateinit var Rl_done: RelativeLayout
    internal lateinit var Rl_back: ImageView
    internal lateinit var Rl_selectDrop: RelativeLayout
    private var Tv_dropLocation: TextView? = null

    private var isInternetPresent: Boolean? = false
    private var cd: ConnectionDetector? = null

    private var mMap: GoogleMap? = null
    private var gps: GPSTracker? = null
    internal lateinit var progressBar: ProgressBar

    private var sLatitude: String? = ""
    private var sLongitude: String? = ""


    //-------------------------------code for map marker moving-------------------------------
    internal var mOnCameraChangeListener: GoogleMap.OnCameraChangeListener =
        GoogleMap.OnCameraChangeListener { cameraPosition ->
            val latitude = cameraPosition.target.latitude
            val longitude = cameraPosition.target.longitude

            cd = ConnectionDetector(this@DropLocationSelect)
            isInternetPresent = cd!!.isConnectingToInternet

            Log.e("camerachange lat-->", "" + latitude)
            Log.e("on_camera_change lon-->", "" + longitude)

            if (mMap != null) {
                mMap!!.clear()

                if (isInternetPresent!!) {

                    sLatitude = latitude.toString()
                    sLongitude = longitude.toString()

                    val asynTask = Map_movingTask(latitude, longitude)
                    asynTask.execute()
                } else {
                    Alert(
                        resources.getString(R.string.alert_sorry_label_title),
                        resources.getString(R.string.no_internet_connection)
                    )
                }
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(R.layout.droplocation_select)
        initialize()
        initializeMap()

        Rl_done.setOnClickListener {
            val returnIntent = Intent()
            returnIntent.putExtra("Selected_Latitude", sLatitude)
            returnIntent.putExtra("Selected_Longitude", sLongitude)
            returnIntent.putExtra("Selected_Location", Tv_dropLocation!!.text.toString())
            setResult(Activity.RESULT_OK, returnIntent)
            onBackPressed()
            finish()
        }

        Rl_back.setOnClickListener {
            onBackPressed()
            finish()
            overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        }

        Rl_selectDrop.setOnClickListener {
            val gps = GPSTracker(this@DropLocationSelect)
            if (gps.canGetLocation()) {
                val i = Intent(this@DropLocationSelect, LocationSearch::class.java)
                startActivityForResult(i, ActivityDropRequestCode)
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
            } else {
                Toast.makeText(this@DropLocationSelect, "Enable Gps", Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun initialize() {
        cd = ConnectionDetector(this@DropLocationSelect)
        isInternetPresent = cd!!.isConnectingToInternet
        gps = GPSTracker(this@DropLocationSelect)

        Rl_done = findViewById<View>(R.id.drop_location_select_done_layout) as RelativeLayout
        Rl_back = findViewById<View>(R.id.drop_location_select_back_layout) as ImageView
        Rl_selectDrop =
            findViewById<View>(R.id.drop_location_select_dropLocation_layout) as RelativeLayout
        Tv_dropLocation = findViewById<View>(R.id.drop_location_select_drop_address) as TextView
        progressBar = findViewById<View>(R.id.drop_location_select_progress_bar) as ProgressBar

    }

    private fun initializeMap() {
        if (mMap == null) {
            val mapFragment = this@DropLocationSelect.supportFragmentManager
                .findFragmentById(R.id.drop_location_select_view_map) as SupportMapFragment
            mapFragment!!.getMapAsync(this)

            if (mMap == null) {
            }
        }


    }

    //--------------Alert Method-----------
    private fun Alert(title: String, message: String) {
        val mDialog = PkDialog(this@DropLocationSelect)
        mDialog.setDialogTitle(title)
        mDialog.setDialogMessage(message)
        mDialog.setPositiveButton(
            resources.getString(R.string.alert_label_ok),
            View.OnClickListener { mDialog.dismiss() })
        mDialog.show()
    }


    //-----------Check Google Play Service--------
    private fun CheckPlayService(): Boolean {
        val connectionStatusCode =
            GooglePlayServicesUtil.isGooglePlayServicesAvailable(this@DropLocationSelect)
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode)
            return false
        }
        return true
    }


    internal fun showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode: Int) {
        this@DropLocationSelect.runOnUiThread {
            val dialog = GooglePlayServicesUtil.getErrorDialog(
                connectionStatusCode, this@DropLocationSelect, REQUEST_CODE_RECOVER_PLAY_SERVICES
            )
            if (dialog == null) {
                Toast.makeText(
                    this@DropLocationSelect,
                    "incompatible version of Google Play Services",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }


    private inner class Map_movingTask internal constructor(lat: Double, lng: Double) :
        AsyncTask<String, Void, String>() {

        internal var response = ""
        private var dLatitude = 0.0
        private var dLongitude = 0.0

        init {
            dLatitude = lat
            dLongitude = lng
        }

        override fun onPreExecute() {
            progressBar.visibility = View.VISIBLE
        }

        override fun doInBackground(vararg urls: String): String {
            return getCompleteAddressString(dLatitude, dLongitude)
        }

        override fun onPostExecute(result: String?) {
            progressBar.visibility = View.GONE
            if (result != null) {
                Tv_dropLocation!!.text = result
                Rl_done.visibility = View.VISIBLE
            } else {
                Rl_done.visibility = View.GONE
            }
        }
    }


    //-------------Method to get Complete Address------------
    private fun getCompleteAddressString(LATITUDE: Double, LONGITUDE: Double): String {
        var strAdd = ""
        val geocoder = Geocoder(this@DropLocationSelect, Locale.getDefault())
        try {
            val addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1)
            if (addresses != null) {
                val returnedAddress = addresses[0]
                val strReturnedAddress = StringBuilder("")
                if (returnedAddress.maxAddressLineIndex > 0) {
                    for (i in 0 until returnedAddress.maxAddressLineIndex) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n")
                    }
                } else {
                    try {
                        strReturnedAddress.append(returnedAddress.getAddressLine(0))
                    } catch (ignored: Exception) {
                    }

                }
                /*for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }*/
                strAdd = strReturnedAddress.toString()
            } else {
                Log.e("Current loction address", "No Address returned!")
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Current loction address", "Canont get Address!")
        }

        return strAdd
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // check if the request code is same as what is passed  here it is 2
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == ActivityDropRequestCode) {
                val sAddress = data!!.getStringExtra("Selected_Location")
                sLatitude = data!!.getStringExtra("Selected_Latitude")
                sLongitude = data!!.getStringExtra("Selected_Longitude")
                Tv_dropLocation!!.text = sAddress

                // Move the camera to last position with a zoom level
                val cameraPosition = CameraPosition.Builder().target(
                    LatLng(
                        java.lang.Double.parseDouble(sLatitude!!),
                        java.lang.Double.parseDouble(sLongitude!!)
                    )
                ).zoom(17f).build()
                mMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))

                Rl_done.visibility = View.VISIBLE
            }
        }
    }

    companion object {

        val ActivityDropRequestCode = 6000
        private val REQUEST_CODE_RECOVER_PLAY_SERVICES = 1001
    }

}
