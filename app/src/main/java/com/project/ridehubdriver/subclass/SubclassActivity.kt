package com.project.ridehubdriver.subclass

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import com.project.ridehubdriver.Hockeyapp.ActivityHockeyApp

/**
 */
open class SubclassActivity : ActivityHockeyApp() {
    internal lateinit var receiver: BroadcastReceiver
     override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val filter = IntentFilter()
        filter.addAction("com.finish.ArrivedTrip")
        filter.addAction("com.finish.UserInfo")
        filter.addAction("com.finish.BeginTrip")
        filter.addAction("com.finish.EndTrip")
        filter.addAction("com.finish.OtpPage")
        filter.addAction("com.finish.PaymentPage")
        filter.addAction("com.finish.tripsummerydetail")
        filter.addAction("com.finish.endtripenterdetail")
        filter.addAction("com.finish.tripsummerylist")
        filter.addAction("com.finish.loadingpage")



        receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                if (intent.action == "com.finish.ArrivedTrip") {
                    finish()
                } else if (intent.action == "com.finish.UserInfo") {
                    finish()
                } else if (intent.action == "com.finish.BeginTrip") {
                    finish()
                } else if (intent.action == "com.finish.OtpPage") {
                    finish()
                } else if (intent.action == "com.finish.EndTrip") {
                    finish()
                } else if (intent.action == "com.finish.PaymentPage") {
                    finish()
                } else if (intent.action == "com.finish.tripsummerydetail") {
                    finish()
                } else if (intent.action == "com.finish.endtripenterdetail") {
                    finish()
                } else if (intent.action == "com.finish.tripsummerylist") {
                    finish()
                } else if (intent.action == "com.finish.loadingpage") {
                    finish()
                }
            }
        }
        registerReceiver(receiver, filter)
    }

    override fun onDestroy() {
        unregisterReceiver(receiver)
        super.onDestroy()
    }

}
