package com.project.ridehubdriver.subclass

import android.os.Bundle
import com.project.ridehubdriver.Adapters.ContinuousRequestAdapter
import com.project.ridehubdriver.Helper.ServiceConstant

import com.project.ridehubdriver.Helper.xmpp.ChatConfigurationBuilder

/**
 * Created by Administrator on 3/17/2016.
 */
open class RealTimeActivity : SubclassActivity() {

    protected var builder: ChatConfigurationBuilder? = null
    protected lateinit var chatID: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        chatID = ContinuousRequestAdapter.userID + "@" + ServiceConstant.XMPP_SERVICE_NAME
        builder = ChatConfigurationBuilder(this)
        builder!!.createConnection()
    }

    override fun onPause() {
        super.onPause()
        if (builder != null)
            builder!!.closeConnection()
    }

    public override fun onDestroy() {
        super.onDestroy()
        if (builder != null)
            builder!!.closeConnection()
    }
}
