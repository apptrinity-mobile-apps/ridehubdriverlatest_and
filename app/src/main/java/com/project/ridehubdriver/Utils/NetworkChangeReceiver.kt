package com.project.ridehubdriver.Utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.widget.Toast
import com.project.ridehubdriver.Helper.xmpp.ChatingService


/**
 * Created by user88 on 1/6/2016.
 */
class NetworkChangeReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (isOnline(context)) {
            ChatingService.startDriverAction(context)
            Toast.makeText(context, "Internet connected", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(context, "Internet not connected", Toast.LENGTH_SHORT).show()
        }
    }

    fun isOnline(context: Context): Boolean {

        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val netInfo = cm.activeNetworkInfo
        //should check null because in air plan mode it will be null
        return netInfo != null && netInfo.isConnected

    }

}