package com.project.ridehubdriver.Utils

import android.content.Context
import android.widget.Toast

import com.android.volley.AuthFailureError
import com.android.volley.NetworkError
import com.android.volley.NoConnectionError
import com.android.volley.ParseError
import com.android.volley.ServerError
import com.android.volley.TimeoutError
import com.android.volley.VolleyError

/**
 * Created by user88 on 10/15/2015.
 */
object VolleyErrorResponse {

    fun VolleyError(context: Context, error: VolleyError) {
        if (error is TimeoutError || error is NoConnectionError) {
            Toast.makeText(context, "Unable to fetch data from server", Toast.LENGTH_LONG).show()
        } else if (error is AuthFailureError) {
            Toast.makeText(context, "AuthFailureError", Toast.LENGTH_LONG).show()
        } else if (error is ServerError) {
            Toast.makeText(context, "ServerError", Toast.LENGTH_LONG).show()
        } else if (error is NetworkError) {
            Toast.makeText(context, "NetworkError", Toast.LENGTH_LONG).show()
        } else if (error is ParseError) {
            Toast.makeText(context, "ParseError", Toast.LENGTH_LONG).show()
        }
    }
}
