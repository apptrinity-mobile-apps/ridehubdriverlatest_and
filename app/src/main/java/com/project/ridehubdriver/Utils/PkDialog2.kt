package com.project.ridehubdriver.Utils

import android.app.Dialog
import android.content.Context
import android.graphics.Typeface
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import android.widget.TextView
import com.project.ridehubdriver.R


/**
 * Created by Prem Kumar and Anitha on 11/27/2015.
 */
class PkDialog2(private val mContext: Context) {
    private val Bt_action: TextView
    private val Bt_dismiss: TextView
    private val alert_title: TextView
    private val alert_message: TextView
    private val Rl_button: LinearLayout
    private val dialog: Dialog
    private val view: View
    private var isPositiveAvailable = false
    private var isNegativeAvailable = false


    init {

        //--------Adjusting Dialog width-----
        val metrics = mContext.resources.displayMetrics
        val screenWidth = (metrics.widthPixels * 0.85).toInt()//fill only 85% of the screen

        view = View.inflate(mContext, R.layout.custom_dialog_success, null)
        dialog = Dialog(mContext)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(view)
        dialog.setCanceledOnTouchOutside(false)
        dialog.window!!.setLayout(screenWidth, LinearLayout.LayoutParams.WRAP_CONTENT)
        dialog!!.window!!.setBackgroundDrawableResource(android.R.color.transparent)

        alert_title = view.findViewById<View>(R.id.custom_dialog_library_title_textview) as TextView
        alert_message =
            view.findViewById<View>(R.id.custom_dialog_library_message_textview) as TextView
        Bt_action = view.findViewById<View>(R.id.custom_dialog_library_ok_button) as TextView
        Bt_dismiss = view.findViewById<View>(R.id.custom_dialog_library_cancel_button) as TextView
        Rl_button =
            view.findViewById<View>(R.id.custom_dialog_library_button_layout) as LinearLayout
    }


    fun show() {

        /*Enable or Disable positive Button*/
        if (isPositiveAvailable) {
            Bt_action.visibility = View.VISIBLE
        } else {
            Bt_action.visibility = View.GONE
        }

        /*Enable or Disable Negative Button*/
        if (isNegativeAvailable) {
            Bt_dismiss.visibility = View.VISIBLE
        } else {
            Bt_dismiss.visibility = View.GONE
        }

        /*Changing color for Button Layout*/
        if (isPositiveAvailable && isNegativeAvailable) {
           // Rl_button.setBackgroundColor(mContext.resources.getColor(R.color.white))
        } else {
           // Rl_button.setBackgroundColor(mContext.resources.getColor(R.color.white))
        }

        dialog.show()
    }


    fun dismiss() {
        dialog.dismiss()
    }


    fun setDialogTitle(title: String) {
        alert_title.text = title
    }


    fun setDialogMessage(message: String) {
        alert_message.text = message
    }


    fun setCancelOnTouchOutside(value: Boolean) {
        dialog.setCanceledOnTouchOutside(value)
    }


    /*Action Button for Dialog*/
    fun setPositiveButton(text: String, listener: View.OnClickListener) {

        isPositiveAvailable = true
        Bt_action.typeface = Typeface.createFromAsset(mContext.assets, "fonts/Poppins-Medium.ttf")
        Bt_action.text = text
        Bt_action.setOnClickListener(listener)
    }

    fun setNegativeButton(text: String, listener: View.OnClickListener) {
        isNegativeAvailable = true
        Bt_dismiss.typeface = Typeface.createFromAsset(mContext.assets, "fonts/Poppins-Medium.ttf")
        Bt_dismiss.text = text
        Bt_dismiss.setOnClickListener(listener)
    }


}
