package com.project.ridehubdriver.Utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import java.util.*

/**
 * Created by user88 on 4/26/2016.
 */
class SessionManager_Applaunch// Constructor
@SuppressLint("CommitPrefEdits")
constructor(// Context
    internal var _context: Context
) {
    // Shared pref mode
    internal var PRIVATE_MODE = 0


    init {
        if (pref == null && editor == null) {
            pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
            editor = pref!!.edit()
        }

    }

    fun createSessionPhoneMask(phonemask: String) {
        editor!!.putString(KEY_PHONE_MASK, phonemask)
        editor!!.commit()
    }


    fun getphonemaskDetails(): HashMap<String, String> {
        val online = HashMap<String, String>()
        online[KEY_PHONE_MASK] = pref!!.getString(KEY_PHONE_MASK, "")!!
        return online
    }

    companion object {
        // Shared Preferences
        internal var pref: SharedPreferences? = null
        // Editor for Shared preferences
        internal var editor: SharedPreferences.Editor? = null
        // Sharedpref file name
        private val PREF_NAME = "Ride247"

        val KEY_PHONE_MASK = "phonemask"
    }

}
