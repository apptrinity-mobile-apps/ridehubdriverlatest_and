package com.project.ridehubdriver.Utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.project.ridehubdriver.Activities.SignUpSignIn
import java.util.*

class SessionManager// Constructor
@SuppressLint("CommitPrefEdits")
constructor(// Context
    internal var _context: Context
) {
    // Shared pref mode
    internal var PRIVATE_MODE = 0

    val sessionlogin_details: HashMap<String, String>
        get() {
            val login_details = HashMap<String, String>()
            login_details[KEY_LOGINID] = pref.getString(KEY_LOGINID, "")!!
            login_details[KEY_LOGINTIME] = pref.getString(KEY_LOGINTIME, "")!!
            return login_details
        }

    /**
     */
    val userDetails: HashMap<String, String>
        get() {
            val user = HashMap<String, String>()
            user[KEY_DRIVER_IMAGE] = pref.getString(KEY_DRIVER_IMAGE, null).toString()
            user[KEY_DRIVERID] = pref.getString(KEY_DRIVERID, null).toString()
            user[KEY_DRIVER_NAME] = pref.getString(KEY_DRIVER_NAME, null).toString()
            user[KEY_EMAIL] = pref.getString(KEY_EMAIL, null).toString()
            user[KEY_VEHICLENO] = pref.getString(KEY_VEHICLENO, null).toString()
            user[KEY_VEHICLE_MODEL] = pref.getString(KEY_VEHICLE_MODEL, null).toString()
            user[KEY] = pref.getString(KEY, null).toString()
            user[KEY_GCM_ID] = pref.getString(KEY_GCM_ID, null).toString()
            user[KEY_LOGINTIME] = pref.getString(KEY_LOGINTIME, null).toString()
            user[KEY_LOGINID] = pref.getString(KEY_LOGINID, null).toString()
            user[KEY_CHAUFFL_NO] = pref.getString(KEY_CHAUFFL_NO, null).toString()
            return user
        }

    val onlineDetails: HashMap<String, String>
        get() {
            val online = HashMap<String, String>()
            online[KEY_ONLINE] = pref.getString(KEY_ONLINE, "")!!
            return online
        }

    val requestCount: HashMap<String, Int>
        get() {
            val count = HashMap<String, Int>()
            count[KEY_COUNT] = pref.getInt(KEY_COUNT, 0)
            return count
        }

    var userVehicle: String
        get() = pref.getString(KEY_VEHICLE_NAME, "")!!
        set(name) {
            editor.putString(KEY_VEHICLE_NAME, name)
            editor.commit()
        }

    /**
     * Quick check for login
     * *
     */
    // Get Login State
    val isLoggedIn: Boolean
        get() = pref.getBoolean(IS_LOGIN, false)


    init {
        /*if(pref == null && editor == null) {
            pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
            editor = pref.edit();
        }*/
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref.edit()
    }

    /**
     * Create login session
     */
    fun createLoginSession(
        image: String,
        driverid: String,
        name: String,
        email: String,
        vehicleno: String,
        vechilemodel: String,
        key: String,
        sec_key: String,
        gcmID: String,
        login_time: String,
        login_id: String,
        chauffl_no: String
    ) {
        editor.putBoolean(IS_LOGIN, true)
        editor.putString(KEY_DRIVER_IMAGE, image)
        editor.putString(KEY_DRIVERID, driverid)
        editor.putString(KEY_DRIVER_NAME, name)
        editor.putString(KEY_EMAIL, email)
        editor.putString(KEY_VEHICLENO, vehicleno)
        editor.putString(KEY_VEHICLE_MODEL, vechilemodel)
        editor.putString(KEY_SEC_KEY, sec_key)
        editor.putString(KEY, key)
        editor.putString(KEY_GCM_ID, gcmID)
        editor.putString(KEY_LOGINTIME, login_time)
        editor.putString(KEY_LOGINID, login_id)
        editor.putString(KEY_CHAUFFL_NO, chauffl_no)
        editor.commit()
    }

    fun createSessionOnline(online: String) {
        editor.putString(KEY_ONLINE, online)
        editor.commit()
    }

    fun createSessionlogin_details(login_id: String, login_time: String) {
        editor.putString(KEY_LOGINID, login_id)
        editor.putString(KEY_LOGINTIME, login_time)
        editor.commit()
    }


    fun setRequestCount(count: Int) {
        editor.putInt(KEY_COUNT, count)
        editor.commit()
    }

    /**
     * Clear session details
     */
    fun logoutUser() {
        editor.clear()
        editor.commit()
        val i = Intent(_context, SignUpSignIn::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        _context.startActivity(i)
    }

    companion object {
        // Shared Preferences
        internal lateinit var pref: SharedPreferences
        // Editor for Shared preferences
        internal lateinit var editor: SharedPreferences.Editor
        // Sharedpref file name
        private val PREF_NAME = "AndroidHivePref"
        // All Shared Preferences Keys
        private val IS_LOGIN = "IsLoggedIn"
        // Email address (make variable public to access from outside)
        val KEY_EMAIL = "email"
        val KEY = "key"
        val KEY_DRIVER_NAME = "drivername"
        var KEY_DRIVERID = "driverid"
        val KEY_DRIVER_IMAGE = "driverimage"
        val KEY_VEHICLENO = "vechicleno"
        val KEY_VEHICLE_MODEL = "vehiclemodel"
        val KEY_SEC_KEY = "vehiclemodel"
        val KEY_VEHICLE_NAME = "VEHICLE_NAME"
        val KEY_ONLINE = "online"
        val KEY_COUNT = "keycount"
        val KEY_GCM_ID = "gcmId"
        val KEY_LOGINTIME = "logintime"
        val KEY_LOGINID = "loginid"
        val KEY_CHAUFFL_NO = "chauffl"
    }
}