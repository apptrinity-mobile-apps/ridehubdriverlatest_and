package com.project.ridehubdriver.Utils;

public interface ProgressListener {
    public void onProgressChanged(boolean state);
}
