package com.project.ridehubdriver.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;


import com.project.ridehubdriver.Activities.SignUpSignIn;

import java.util.HashMap;

public class SessionManagerChat {
    // Shared Preferences
    static SharedPreferences pref;
    // Editor for Shared preferences
    static Editor editor;
    // Context
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;
    // Sharedpref file name
    private static final String PREF_NAME = "AndroidHivePref";
    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";
    public static final String KEY = "key";
    public static final String KEY_DRIVER_NAME = "drivername";
    public static final String KEY_DRIVERID = "driverid";
    public static final String KEY_DRIVER_IMAGE = "driverimage";
    public static final String KEY_VEHICLENO = "vechicleno";
    public static final String KEY_VEHICLE_MODEL = "vehiclemodel";
    public static final String KEY_SEC_KEY = "vehiclemodel";
    public static final String KEY_VEHICLE_NAME = "VEHICLE_NAME";
    public static final String KEY_ONLINE = "online";
    public static final String KEY_COUNT = "keycount";
    public static final String KEY_GCM_ID = "gcmId";
    public static final String KEY_LOGINTIME = "logintime";
    public static final String KEY_LOGINID = "loginid";


    // Constructor
    @SuppressLint("CommitPrefEdits")
    public SessionManagerChat(Context context) {
        this._context = context;
        /*if(pref == null && editor == null) {
            pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
            editor = pref.edit();
        }*/
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     */
    public void createLoginSession(String image, String driverid, String name, String email, String vehicleno,
                                   String vechilemodel, String key, String sec_key, String gcmID, String login_time, String login_id) {
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_DRIVER_IMAGE, image);
        editor.putString(KEY_DRIVERID, driverid);
        editor.putString(KEY_DRIVER_NAME, name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_VEHICLENO, vehicleno);
        editor.putString(KEY_VEHICLE_MODEL, vechilemodel);
        editor.putString(KEY_SEC_KEY, sec_key);
        editor.putString(KEY, key);
        editor.putString(KEY_GCM_ID, gcmID);
        editor.putString(KEY_LOGINTIME, login_time);
        editor.putString(KEY_LOGINID, login_id);
        editor.commit();
    }

    public void createSessionOnline(String online){
        editor.putString(KEY_ONLINE, online);
        editor.commit();
    }

    public void createSessionlogin_details(String login_id, String login_time){
        editor.putString(KEY_LOGINID, login_id);
        editor.putString(KEY_LOGINTIME, login_time);
        editor.commit();
    }

    public HashMap<String, String> getSessionlogin_details() {
        HashMap<String, String> login_details = new HashMap<String, String>();
        login_details.put(KEY_LOGINID, pref.getString(KEY_LOGINID, ""));
        login_details.put(KEY_LOGINTIME, pref.getString(KEY_LOGINTIME, ""));
        return login_details;
    }

    /**
     * */
    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_DRIVER_IMAGE, pref.getString(KEY_DRIVER_IMAGE, null));
        user.put(KEY_DRIVERID, pref.getString(KEY_DRIVERID, null));
        user.put(KEY_DRIVER_NAME, pref.getString(KEY_DRIVER_NAME, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_VEHICLENO, pref.getString(KEY_VEHICLENO, null));
        user.put(KEY_VEHICLE_MODEL, pref.getString(KEY_VEHICLE_MODEL, null));
        user.put(KEY, pref.getString(KEY, null));
        user.put(KEY_GCM_ID, pref.getString(KEY_GCM_ID, null));
        user.put(KEY_LOGINTIME, pref.getString(KEY_LOGINTIME, null));
        user.put(KEY_LOGINID, pref.getString(KEY_LOGINID, null));
        return user;
    }

    public HashMap<String, String> getOnlineDetails() {
        HashMap<String, String> online = new HashMap<String, String>();
        online.put(KEY_ONLINE, pref.getString(KEY_ONLINE, ""));
        return online;
    }



    public void setRequestCount(int count){
        editor.putInt(KEY_COUNT, count);
        editor.commit();
    }

    public HashMap<String, Integer> getRequestCount(){
        HashMap<String, Integer> count = new HashMap<String, Integer>();
        count.put(KEY_COUNT, pref.getInt(KEY_COUNT, 0));
        return count;
    }



    public void setUserVehicle(String name){
        editor.putString(KEY_VEHICLE_NAME, name);
        editor.commit();
    }

    public String getUserVehicle(){
        return pref.getString(KEY_VEHICLE_NAME, "");
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        editor.clear();
        editor.commit();
        Intent i = new Intent(_context, SignUpSignIn.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     * *
     */
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }
}