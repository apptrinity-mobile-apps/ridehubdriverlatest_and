package com.project.ridehubdriver.PojoResponse

/**
 * Created by user14 on 9/23/2015.
 */
class TripSummaryPojo {
    private var ride_id: String? = null
    private var ride_time: String? = null
    private var ride_date: String? = null
    private var pickup: String? = null
    private var drop: String? = null
    private var group: String? = null
    private var datetime: String? = null


    fun getride_id(): String? {
        return ride_id
    }

    fun setride_id(ride_id: String) {
        this.ride_id = ride_id
    }

    fun getride_time(): String? {
        return ride_time
    }

    fun setride_time(ride_time: String) {
        this.ride_time = ride_time
    }

    fun getride_date(): String? {
        return ride_date
    }

    fun setride_date(ride_date: String) {
        this.ride_date = ride_date
    }

    fun getpickup(): String? {
        return pickup
    }

    fun setpickup(pickup: String) {
        this.pickup = pickup
    }

    fun getdrop(): String? {
        return drop
    }

    fun setdrop(drop: String) {
        this.drop = drop
    }

    fun getgroup(): String? {
        return group
    }

    fun setgroup(group: String) {
        this.group = group
    }

    fun getdatetime(): String? {
        return datetime
    }

    fun setdatetime(datetime: String) {
        this.datetime = datetime
    }
}
