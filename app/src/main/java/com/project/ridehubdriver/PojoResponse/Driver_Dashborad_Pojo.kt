package com.project.ridehubdriver.PojoResponse

/**
 * Created by user88 on 12/22/2015.
 */
class Driver_Dashborad_Pojo {

    var dashboard_last_trip: String? = null
    var dashboard_today_earnings: String? = null
    var dashboard_today_tips: String? = null

    var dashboard_last_trip_ride_time: String? = null
    var dashboard_last_trip_ride_date: String? = null
    var dashboard_last_trip_earnings: String? = null
    var dashboard_last_trip_currency_code: String? = null
    var dashboard_today_earning_online_hour: String? = null
    var dashboard_today_earning_trips: String? = null
    var dashboard_today_earnings_amount: String? = null
    var dashboard_today_tips_trips: String? = null
    var dashboard_today_tips_tipsAmount: String? = null

    var dashboard_lasttrip_currrency_code: String? = null

    var dashboard_today_earnings_currency_code: String? = null
    var dashboard_today_tips_currency_code: String? = null


}
