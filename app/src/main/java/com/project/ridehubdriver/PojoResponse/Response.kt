package com.project.ridehubdriver.PojoResponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class Response {

    /**
     *
     * @return
     * The totalRides
     */
    /**
     *
     * @param totalRides
     * The total_rides
     */
    @SerializedName("total_rides")
    @Expose
    var totalRides: String? = null
    /**
     *
     * @return
     * The rides
     */
    /**
     *
     * @param rides
     * The rides
     */
    @SerializedName("rides")
    @Expose
    var rides: List<Ride> = ArrayList()

}
