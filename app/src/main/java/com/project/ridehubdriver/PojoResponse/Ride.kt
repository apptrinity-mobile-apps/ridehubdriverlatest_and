package com.project.ridehubdriver.PojoResponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Ride {

    /**
     *
     * @return
     * The rideId
     */
    /**
     *
     * @param rideId
     * The ride_id
     */
    @SerializedName("ride_id")
    @Expose
    var rideId: String? = null
    /**
     *
     * @return
     * The rideTime
     */
    /**
     *
     * @param rideTime
     * The ride_time
     */
    @SerializedName("ride_time")
    @Expose
    var rideTime: String? = null
    /**
     *
     * @return
     * The rideDate
     */
    /**
     *
     * @param rideDate
     * The ride_date
     */
    @SerializedName("ride_date")
    @Expose
    var rideDate: String? = null
    /**
     *
     * @return
     * The pickup
     */
    /**
     *
     * @param pickup
     * The pickup
     */
    @SerializedName("pickup")
    @Expose
    var pickup: String? = null

}
