package com.project.ridehubdriver.PojoResponse

/**
 * Created by user14 on 9/24/2015.
 */
class ReferralPointsPojo {
    var refer_from: String? = null
    var refer_to: String? = null
    var points: String? = null
    var referrer_name: String? = null
    var type: String? = null
    var ride_id: String? = null
    var trans_type: String? = null
    var level: String? = null
    var date: String? = null
}
