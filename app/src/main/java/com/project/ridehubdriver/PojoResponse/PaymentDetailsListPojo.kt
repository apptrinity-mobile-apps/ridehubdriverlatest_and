package com.project.ridehubdriver.PojoResponse

/**
 * Created by user14 on 9/24/2015.
 */
class PaymentDetailsListPojo {
    private var ride_id: String? = null
    private var amount: String? = null
    private var ride_date: String? = null

    fun getride_id(): String? {
        return ride_id
    }

    fun setride_id(ride_id: String) {
        this.ride_id = ride_id
    }

    fun getamount(): String? {
        return amount
    }

    fun setamount(amount: String) {
        this.amount = amount
    }

    fun getride_date(): String? {
        return ride_date
    }

    fun setride_date(ride_date: String) {
        this.ride_date = ride_date
    }
}
