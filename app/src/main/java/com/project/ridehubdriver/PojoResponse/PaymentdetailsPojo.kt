package com.project.ridehubdriver.PojoResponse

/**
 * Created by user14 on 9/24/2015.
 */
class PaymentdetailsPojo {
    private var pay_id: String? = null
    private var pay_duration_from: String? = null
    private var pay_duration_to: String? = null
    private var amount: String? = null
    private var pay_date: String? = null

    var currency: String? = null


    fun getpay_id(): String? {
        return pay_id
    }

    fun setpay_id(pay_id: String) {
        this.pay_id = pay_id
    }

    fun getpay_duration_to(): String? {
        return pay_duration_to
    }

    fun setpay_duration_to(pay_duration_to: String) {
        this.pay_duration_to = pay_duration_to
    }

    fun getpay_duration_from(): String? {
        return pay_duration_from
    }

    fun setpay_duration_from(pay_duration_from: String) {
        this.pay_duration_from = pay_duration_from
    }

    fun getamount(): String? {
        return amount
    }

    fun setamount(amount: String) {
        this.amount = amount
    }

    fun getpay_date(): String? {
        return pay_date
    }

    fun setpay_date(pay_date: String) {
        this.pay_date = pay_date
    }
}
