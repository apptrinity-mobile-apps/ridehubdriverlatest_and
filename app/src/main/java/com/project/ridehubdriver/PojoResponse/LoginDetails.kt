package com.project.ridehubdriver.PojoResponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 */


class LoginDetails : ServiceResponse() {

    /**
     * @return The status
     */
    /**
     * @param status The status
     */
    @SerializedName("status")
    @Expose
    override var status: String? = null
    /**
     * @return The response
     */
    /**
     * @param response The response
     */
    @SerializedName("response")
    @Expose
    var response: String? = null
    /**
     * @return The driverImage
     */
    /**
     * @param driverImage The driver_image
     */
    @SerializedName("driver_image")
    @Expose
    var driverImage: String? = null
    /**
     * @return The driverId
     */
    /**
     * @param driverId The driver_id
     */
    @SerializedName("driver_id")
    @Expose
    var driverId: String? = null
    /**
     * @return The driverName
     */
    /**
     * @param driverName The driver_name
     */
    @SerializedName("driver_name")
    @Expose
    var driverName: String? = null

    @SerializedName("chauffl_no")
    @Expose
    var chaufflNo: String? = null
    /**
     * @return The email
     */
    /**
     * @param email The email
     */
    @SerializedName("email")
    @Expose
    var email: String? = null
    /**
     * @return The vehicleNumber
     */
    /**
     * @param vehicleNumber The vehicle_number
     */
    @SerializedName("vehicle_number")
    @Expose
    var vehicleNumber: String? = null
    /**
     * @return The vehicleModel
     */
    /**
     * @param vehicleModel The vehicle_model
     */
    @SerializedName("vehicle_model")
    @Expose
    var vehicleModel: String? = null
    /**
     * @return The key
     */
    /**
     * @param key The key
     */
    @SerializedName("key")
    @Expose
    var key: String? = null
    /**
     * @param login_time The login_time
     */
    @SerializedName("login_time")
    @Expose
    var login_time: String? = null
    /**
     * @param login_id The login_id
     */
    @SerializedName("login_id")
    @Expose
    var login_id: String? = null

    @SerializedName("is_alive_other")
    @Expose
    var is_alive_other: String? = null

    @SerializedName("sec_key")
    @Expose
    var sec_key: String? = null

}
