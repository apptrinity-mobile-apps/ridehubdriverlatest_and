package com.project.ridehubdriver.PojoResponse

class ChatResponsePojo {

    var RIDER_ID = "rider_id"
    var RECEIVED_STATUS = "chat_received_status"
    var RECEIVED_MESSAGE = "chat_received"
    var SENT_STATUS = "chat_sent_status"
    var SENT_MESSAGE = "chat_sent"
    
}