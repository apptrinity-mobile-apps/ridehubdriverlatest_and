package com.project.ridehubdriver.PojoResponse

import java.io.Serializable
import java.util.*

/**
 * Created by Prem Kumar and Anitha on 10/9/2015.
 */
class EstimateDetailPojo : Serializable {
    var rate_cartype: String? = null
    var rate_note: String? = null
    var minfare_amt: String? = null
    var minfare_km: String? = null
    var afterfare_amt: String? = null
    var afterfare_km: String? = null
    var otherfare_amt: String? = null
    var otherfare_km: String? = null
    var currencyCode: String? = null

    val estimatePojo: ArrayList<EstimateDetailPojo>

    constructor(estimatePojo: ArrayList<EstimateDetailPojo>) {
        this.estimatePojo = estimatePojo
    }

    constructor(
        dummy: String,
        data: ArrayList<EstimateDetailPojo>,
        estimatePojo: ArrayList<EstimateDetailPojo>
    ) {
        this.estimatePojo = data
    }
}
