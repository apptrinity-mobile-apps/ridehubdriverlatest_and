package com.project.ridehubdriver.PojoResponse

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TripDetails {

    /**
     *
     * @return
     * The status
     */
    /**
     *
     * @param status
     * The status
     */
    @SerializedName("status")
    @Expose
    var status: String? = null
    /**
     *
     * @return
     * The response
     */
    /**
     *
     * @param response
     * The response
     */
    @SerializedName("response")
    @Expose
    var response: Response? = null

}
