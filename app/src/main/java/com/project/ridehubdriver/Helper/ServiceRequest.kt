package com.project.ridehubdriver.Helper

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.project.ridehubdriver.Activities.SignUpSignIn
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.AppController
import com.project.ridehubdriver.Utils.PkDialog
import com.project.ridehubdriver.Utils.SessionManager
import org.json.JSONObject
import java.util.*

/**
 * Created by Prem Kumar and Anitha on 11/26/2015.
 */
class ServiceRequest(private val context: Context) {
    private var mServiceListener: ServiceListener? = null
    private var stringRequest: StringRequest? = null
    private val sessionManager: SessionManager
    private var userID = ""
    private var gcmID = ""

    interface ServiceListener {
        fun onCompleteListener(response: String)

        fun onErrorListener()
    }

    init {
        sessionManager = SessionManager(context)

        val user = sessionManager.userDetails
        userID = user[SessionManager.KEY_DRIVERID]!!
        gcmID = user[SessionManager.KEY_GCM_ID]!!

        //Log.d("DRIVERID@@@@@@@", user.get(SessionManager.KEY_DRIVERID));
        //Log.d("GCMID", user.get(SessionManager.KEY_GCM_ID));
    }

    fun cancelRequest() {
        if (stringRequest != null) {
            stringRequest!!.cancel()
        }
    }

    fun makeServiceRequest(
        url: String,
        method: Int,
        param: HashMap<String, String>?,
        listener: ServiceListener
    ) {

        this.mServiceListener = listener

        stringRequest = object : StringRequest(method, url, Response.Listener { response ->
            Log.e("ServiceRequest_response", response)
            try {

                println("-----------service request response--------------$response")

                mServiceListener!!.onCompleteListener(response)

                val `object` = JSONObject(response)
                if (`object`.has("is_dead")) {
                    println("-----------is dead----------------")
                    val mDialog = PkDialog(context)
                    mDialog.setDialogTitle(context.resources.getString(R.string.action_session_expired_title))
                    mDialog.setDialogMessage(context.resources.getString(R.string.action_session_expired_message))
                    mDialog.setPositiveButton(
                        context.resources.getString(R.string.lbel_notification_ok),
                        View.OnClickListener {
                            mDialog.dismiss()
                            sessionManager.logoutUser()
                            val intent = Intent(context, SignUpSignIn::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                            context.startActivity(intent)
                        })
                    mDialog.show()
                }

            } catch (e: Exception) {
            }
        }, Response.ErrorListener { error ->
            try {
                if (error is TimeoutError || error is NoConnectionError) {
                    Toast.makeText(
                        context,
                        "Network connection is slow.Please try again.",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (error is AuthFailureError) {
                    Toast.makeText(context, "AuthFailureError", Toast.LENGTH_SHORT).show()
                } else if (error is ServerError) {
                    Toast.makeText(context, "ServerError", Toast.LENGTH_SHORT).show()
                } else if (error is NetworkError) {
                    Toast.makeText(context, "NetworkError", Toast.LENGTH_SHORT).show()
                } else if (error is ParseError) {
                    Toast.makeText(context, "ParseError", Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
            }

            mServiceListener!!.onErrorListener()
        }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                return param!!
            }

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                System.out.println("-----------User-agent----------------" + ServiceConstant.useragent)
                System.out.println("-----------apptype----------------" + ServiceConstant.cabily_AppType)
                println("-----------driverid----------------$userID")
                println("-----------apptoken----------------$gcmID")
                val headers = HashMap<String, String>()
                headers["User-agent"] = ServiceConstant.useragent
                //headers.put("isapplication", ServiceConstant.isapplication);
                //headers.put("applanguage", ServiceConstant.applanguage);
                headers["apptype"] = ServiceConstant.cabily_AppType
                headers["driverid"] = userID
                headers["apptoken"] = gcmID
                return headers
            }
        }

        //to avoid repeat request Multiple Time
        val retryPolicy = DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest!!.retryPolicy = retryPolicy
        stringRequest!!.retryPolicy = DefaultRetryPolicy(
            30000,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        stringRequest!!.setShouldCache(false)
        //AppController.instance!!.addToRequestQueue(stringRequest!!)
        AppController.instance!!.addToRequestQueue(stringRequest!!)
    }


}
