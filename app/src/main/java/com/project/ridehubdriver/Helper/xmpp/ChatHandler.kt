package com.project.ridehubdriver.Helper.xmpp

import android.app.IntentService
import android.content.Context
import android.content.Intent
import com.project.ridehubdriver.Activities.DriverAlertActivity
import com.project.ridehubdriver.Activities.PushNotificationAlert

import com.project.ridehubdriver.Helper.ServiceConstant

import org.jivesoftware.smack.packet.Message
import org.json.JSONObject

import java.net.URLDecoder

/**
 * Created by user88 on 11/4/2015.
 */
class ChatHandler(private val context: Context, private val service: IntentService) {

    fun onHandleChatMessage(message: Message) {
        try {
            val data = URLDecoder.decode(message.body, "UTF-8")
            println("-------------Xmpp response---------------------$data")
            val messageObject = JSONObject(data)
            val action = messageObject.get(ServiceConstant.ACTION_LABEL) as String
            if (ServiceConstant.ACTION_TAG_RIDE_REQUEST.equals(action)) {
                rideRequest(data)
            } else if (ServiceConstant.ACTION_TAG_RIDE_CANCELLED.equals(action)) {
                rideCancelled(messageObject)
            } else if (ServiceConstant.ACTION_TAG_RECEIVE_CASH.equals(action)) {
                receiveCash(messageObject)
            } else if (ServiceConstant.ACTION_TAG_PAYMENT_PAID.equals(action)) {
                paymentPaid(messageObject)
            } else if (ServiceConstant.ACTION_TAG_NEW_TRIP.equals(action)) {
                newTipAlert(messageObject)
            } else if (ServiceConstant.pushNotification_Ads.equals(action)) {
                display_Ads(messageObject)
            }

        } catch (e: Exception) {
        }

    }

    private fun rideRequest(message: String) {
        val intent = Intent(context, DriverAlertActivity::class.java)
        intent.putExtra(DriverAlertActivity.EXTRA, message)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        service.startActivity(intent)
    }

    @Throws(Exception::class)
    private fun rideCancelled(messageObject: JSONObject) {
        val broadcastIntent = Intent()
        broadcastIntent.action = "com.finish.BeginTrip"
        context.sendOrderedBroadcast(broadcastIntent, null)

        val intent = Intent(context, PushNotificationAlert::class.java)
        intent.putExtra("Message", messageObject.getString("message"))
        intent.putExtra("Action", messageObject.getString("action"))
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        service.startActivity(intent)
    }

    @Throws(Exception::class)
    private fun receiveCash(messageObject: JSONObject) {
        val broadcastIntent = Intent()
        broadcastIntent.action = "com.finish.OtpPage"
        context.sendOrderedBroadcast(broadcastIntent, null)
        println("rideId----------------xmpp" + messageObject.getString("key1"))

        val intent = Intent(context, PushNotificationAlert::class.java)
        intent.putExtra("Message", messageObject.getString("message"))
        intent.putExtra("Action", messageObject.getString("action"))
        intent.putExtra("amount", messageObject.getString("key3"))
        intent.putExtra("RideId", messageObject.getString("key1"))
        intent.putExtra("Currencycode", messageObject.getString("key4"))

        println("currncyputex---------------" + messageObject.getString("key4"))
        println("amountputex---------------" + messageObject.getString("key3"))


        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        service.startActivity(intent)
    }


    @Throws(Exception::class)
    private fun paymentPaid(messageObject: JSONObject) {
        val broadcastIntent = Intent()
        broadcastIntent.action = "com.finish.OtpPage"
        context.sendOrderedBroadcast(broadcastIntent, null)

        val broadcastIntent_payment = Intent()
        broadcastIntent_payment.action = "com.finish.PaymentPage"
        context.sendOrderedBroadcast(broadcastIntent_payment, null)

        val broadcastIntent_paymenttrip = Intent()
        broadcastIntent_paymenttrip.action = "com.finish.tripsummerydetail"
        context.sendOrderedBroadcast(broadcastIntent_paymenttrip, null)


        val intent = Intent(context, PushNotificationAlert::class.java)
        intent.putExtra("Message", messageObject.getString("message"))
        intent.putExtra("Action", messageObject.getString("action"))
        intent.putExtra("RideId", messageObject.getString("key1"))


        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        service.startActivity(intent)
    }

    @Throws(Exception::class)
    private fun newTipAlert(messageObject: JSONObject) {

        println("---------------inside new trip------------------")

        /*val intent = Intent(context, NewTripAlert::class.java)
        intent.putExtra("Message", messageObject.getString("message"))
        intent.putExtra("Action", messageObject.getString("action"))
        intent.putExtra("Username", messageObject.getString("key1"))
        intent.putExtra("Mobilenumber", messageObject.getString("key3"))
        intent.putExtra("UserImage", messageObject.getString("key4"))
        intent.putExtra("RideId", messageObject.getString("key6"))
        intent.putExtra("UserPickuplocation", messageObject.getString("key7"))
        intent.putExtra("UserPickupTime", messageObject.getString("key10"))
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        service.startActivity(intent)*/

    }


    @Throws(Exception::class)
    private fun display_Ads(messageObject: JSONObject) {
        /*val i1 = Intent(context, AdsPage::class.java)
        i1.putExtra("AdsTitle", messageObject.getString(ServiceConstant.Ads_title))
        i1.putExtra("AdsMessage", messageObject.getString(ServiceConstant.Ads_Message))
        if (messageObject.has(ServiceConstant.Ads_image)) {
            i1.putExtra("AdsBanner", messageObject.getString(ServiceConstant.Ads_image))
        }
        i1.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(i1)*/
    }


}

