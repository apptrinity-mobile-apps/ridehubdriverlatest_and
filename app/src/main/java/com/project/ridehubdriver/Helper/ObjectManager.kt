package com.project.ridehubdriver.Helper

import com.google.gson.Gson
import com.project.ridehubdriver.Helper.ServiceConstant.Companion.ARRIVED_REQUEST
import com.project.ridehubdriver.Helper.ServiceConstant.Companion.BEGIN_RIDE_REQUEST
import com.project.ridehubdriver.Helper.ServiceConstant.Companion.CANCELLATION_REQUEST
import com.project.ridehubdriver.Helper.ServiceConstant.Companion.CANCEL_RIDE_REQUEST
import com.project.ridehubdriver.Helper.ServiceConstant.Companion.END_RIDE_REQUEST
import com.project.ridehubdriver.Helper.ServiceConstant.Companion.LOGIN_URL
import com.project.ridehubdriver.Helper.ServiceConstant.Companion.TRIP_VIEW_REQUEST
import com.project.ridehubdriver.Helper.ServiceConstant.Companion.UPDATE_AVAILABILITY
import com.project.ridehubdriver.Helper.ServiceConstant.Companion.UPDATE_CURRENT_LOCATION
import com.project.ridehubdriver.PojoResponse.LoginDetails
import com.project.ridehubdriver.parser.ViewTripParser

class ObjectManager : ServiceConstant {
    internal var gson: Gson

    init {
        gson = Gson()
    }

    fun getObjectForUrl(url: String, response: String): Any {
        if (url.contains(LOGIN_URL)) {
            return gson.fromJson(response, LoginDetails::class.java)
        } else if (url.contains(UPDATE_CURRENT_LOCATION)) {
            return response
        } else if (url.contains(UPDATE_AVAILABILITY)) {
            return response
        } else if (url.contains(CANCELLATION_REQUEST)) {
            return response
        } else if (url.contains(CANCEL_RIDE_REQUEST)) {
            return response
        } else if (url.contains(BEGIN_RIDE_REQUEST)) {
            return response
        } else if (url.contains(ARRIVED_REQUEST)) {
            return response
        } else if (url.contains(END_RIDE_REQUEST)) {
            return response
        } else if (url.contains(TRIP_VIEW_REQUEST)) {
            val parser = ViewTripParser()
            return parser.parse(response) as LoginDetails
        } else {
            return response
        }
    }
}
