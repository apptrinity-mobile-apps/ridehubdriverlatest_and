package com.project.ridehubdriver.Helper.xmpp

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.os.Messenger
import android.os.RemoteException
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Utils.SessionManager
import org.jivesoftware.smack.*
import org.jivesoftware.smack.chat.Chat
import org.jivesoftware.smack.chat.ChatManager
import org.jivesoftware.smack.chat.ChatManagerListener
import org.jivesoftware.smack.chat.ChatMessageListener
import org.jivesoftware.smack.packet.Message
import org.jivesoftware.smack.tcp.XMPPTCPConnection
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration
import java.io.IOException
import java.security.KeyManagementException
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.X509TrustManager

/**
 */
class ChatingService : IntentService("ChatingService"), ChatManagerListener, ChatMessageListener {
    private var isConnected: Boolean = false

    override fun onHandleIntent(intent: Intent?) {
        if (intent != null) {
            try {
                handleActionFoo()
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */

    private fun handleActionFoo() {
        if (!shouldContinue) {
            stopSelf()
            return
        }
        val configBuilder = XMPPTCPConnectionConfiguration.builder()
        configBuilder.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
        var context: SSLContext? = null
        try {
            context = SSLContext.getInstance("TLS")
            context!!.init(null, arrayOf<X509TrustManager>(object : X509TrustManager {
                @Throws(CertificateException::class)
                override fun checkClientTrusted(
                    chain: Array<X509Certificate>,
                    authType: String
                ) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(
                    chain: Array<X509Certificate>,
                    authType: String
                ) {
                }

                override fun getAcceptedIssuers(): Array<X509Certificate?> {
                    return arrayOfNulls(0)
                }
            }), SecureRandom())
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: KeyManagementException) {
        }

        configBuilder.setHost(ServiceConstant.XMPP_HOST_URL)//http://192.168.1.116/67.219.149.186
        configBuilder.setServiceName(ServiceConstant.XMPP_SERVICE_NAME)
        //sec_key
        connection = XMPPTCPConnection(configBuilder.build())
        connection!!.addConnectionListener(object : ConnectionListener {
            override fun connected(connection: XMPPConnection) {
                isConnected = true

                //Code to send request to disable chat
                /*  ChatAvailabilityCheck chatAvailability = new ChatAvailabilityCheck(ChatingService.this, "available");
                chatAvailability.postChatRequest();*/
            }

            override fun authenticated(connection: XMPPConnection, resumed: Boolean) {

            }

            override fun connectionClosed() {
                isConnected = false
                /* ChatAvailabilityCheck chatAvailability = new ChatAvailabilityCheck(ChatingService.this, "available");
                chatAvailability.postChatRequest();
*/
            }

            override fun connectionClosedOnError(e: Exception) {
                isConnected = false
                /* ChatAvailabilityCheck chatAvailability = new ChatAvailabilityCheck(ChatingService.this, "available");
                chatAvailability.postChatRequest();*/

            }

            override fun reconnectionSuccessful() {
                isConnected = true
                /* ChatAvailabilityCheck chatAvailability = new ChatAvailabilityCheck(ChatingService.this, "available");
                chatAvailability.postChatRequest();*/

            }

            override fun reconnectingIn(seconds: Int) {}

            override fun reconnectionFailed(e: Exception) {
                isConnected = false

                /* ChatAvailabilityCheck chatAvailability = new ChatAvailabilityCheck(ChatingService.this, "available");
                chatAvailability.postChatRequest();*/

            }
        })
        connection!!.packetReplyTimeout = 30000
        try {
            connection!!.connect()
        } catch (e: SmackException) {
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: XMPPException) {
            e.printStackTrace()
        }

        try {
            var userName: String? = ""
            var password: String? = ""
            if (session != null && session!!.userDetails != null) {
                userName = session!!.userDetails[SessionManager.KEY_DRIVERID]
                password = session!!.userDetails[SessionManager.KEY_SEC_KEY]
            }
            if (userName!!.length > 0 && password!!.length > 0) {
                connection!!.login(userName, password)
                chatManager = ChatManager.getInstanceFor(connection)
                chatManager!!.addChatListener(this)
                println("-------------Xmpp response conectd---------------------")
            }
        } catch (e: XMPPException) {
            e.printStackTrace()
        } catch (e: SmackException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        if (!shouldContinue) {
            stopSelf()
            return
        }
    }

    override fun processMessage(chat: Chat, message: Message) {
        try {
            if (chatHandler == null) {
                chatHandler = ChatHandler(applicationContext, this)
            }
            val online = session!!.onlineDetails
            val checkonline = online[SessionManager.KEY_ONLINE]
            if (checkonline!!.equals("1", ignoreCase = true)) {
                chatHandler!!.onHandleChatMessage(message)
            }
        } catch (e: Exception) {
        }

    }

    fun processMessage(message: Message) {
        if (chatMessenger != null) {
            val chatMessage = android.os.Message.obtain()
            chatMessage.obj = message.body
            try {
                chatMessenger!!.send(chatMessage)
            } catch (e: RemoteException) {
                e.printStackTrace()
            }

        }
    }

    override fun chatCreated(chat: Chat, createdLocally: Boolean) {
        chat.addMessageListener(this)
    }

    override fun onDestroy() {
        isConnected = false
        println("Chating Service is Destroyed")
        super.onDestroy()
    }

    companion object {

        private val ACTION_FOO = "com.casperon.smackclient.action.FOO"
        private var connection: AbstractXMPPConnection? = null
        private var session: SessionManager? = null
        private var chatHandler: ChatHandler? = null
        private var chat: Chat? = null
        private var chatManager: ChatManager? = null
        internal var isChatEnabled: Boolean = false
        private var chatMessenger: Messenger? = null
        var shouldContinue = true

        fun startDriverAction(context: Context) {
             val intent = Intent(context, ChatingService::class.java)
             intent.action = ACTION_FOO
             intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
             session = SessionManager(context)
             context.startService(intent)
         }

        fun createChat(chatID: String?): Chat? {
            if (chatID != null && chatManager != null) {
                chat = chatManager!!.createChat(chatID)
            }
            return chat
        }

        fun setChatMessenger(messenger: Messenger) {
            chatMessenger = messenger
        }

        fun enableChat() {
            isChatEnabled = true
        }

        fun disableChat() {
            isChatEnabled = false
        }
    }
}
