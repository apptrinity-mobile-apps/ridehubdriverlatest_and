package com.project.ridehubdriver.Helper.xmpp

import android.content.Context
import android.util.Log
import com.project.ridehubdriver.Helper.ServiceConstant
import com.project.ridehubdriver.Utils.SessionManager
import org.jivesoftware.smack.*
import org.jivesoftware.smack.chat.Chat
import org.jivesoftware.smack.chat.ChatManager
import org.jivesoftware.smack.chat.ChatManagerListener
import org.jivesoftware.smack.chat.ChatMessageListener
import org.jivesoftware.smack.packet.Message
import org.jivesoftware.smack.tcp.XMPPTCPConnection
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration
import java.io.IOException
import java.security.KeyManagementException
import java.security.NoSuchAlgorithmException
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.X509TrustManager

/**
 */
class ChatConfigurationBuilder(private val context1: Context) : ChatManagerListener,
    ChatMessageListener {
    private var connection: AbstractXMPPConnection? = null
    private val TOTAL_TIME_OUT = 30000
    private val session: SessionManager?
    private var chatManager: ChatManager? = null
    private var chat: Chat? = null


    internal var mConnectionListener: ConnectionListener = object : ConnectionListener {
        override fun connected(connection: XMPPConnection) {}

        override fun authenticated(connection: XMPPConnection, resumed: Boolean) {}

        override fun connectionClosed() {}

        override fun connectionClosedOnError(e: Exception) {}

        override fun reconnectionSuccessful() {}

        override fun reconnectingIn(seconds: Int) {}

        override fun reconnectionFailed(e: Exception) {

        }
    }

    init {
        session = SessionManager(context1)
    }

    fun createConnection() {
        val thread = Thread(Runnable {
            val configBuilder = XMPPTCPConnectionConfiguration.builder()
            configBuilder.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
            createSSLContext()
            configBuilder.setHost(ServiceConstant.XMPP_HOST_URL)//http://192.168.1.116/67.219.149.186
            configBuilder.setServiceName(ServiceConstant.XMPP_SERVICE_NAME)
            createConnection(configBuilder)
        })
        thread.start()

    }

    private fun createConnection(configBuilder: XMPPTCPConnectionConfiguration.Builder) {
        connection = XMPPTCPConnection(configBuilder.build())
        connection!!.packetReplyTimeout = TOTAL_TIME_OUT.toLong()
        try {
            connection!!.connect()
        } catch (e: SmackException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: XMPPException) {
            e.printStackTrace()
        }

        var userName: String? = ""
        var password: String? = ""
        if (session != null && session.userDetails != null) {
            userName = session.userDetails[SessionManager.KEY_DRIVERID]
            password = session.userDetails[SessionManager.KEY_SEC_KEY]
        }
        try {
            connection!!.login(userName, password)
            chatManager = ChatManager.getInstanceFor(connection)
            chatManager!!.addChatListener(this)
        } catch (e: XMPPException) {
            e.printStackTrace()
        } catch (e: SmackException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    fun createChat(chatID: String?): Chat? {
        synchronized(ChatConfigurationBuilder::class.java) {
            if (chatID != null && chatManager != null) {
                chat = ChatingService.createChat(chatID)
            }
            return chat
        }
    }

    fun sendMessage(chatID: String, message: String) {
        if (chat == null) {
            chat = createChat(chatID)
        } else {
            try {

                //  Toast.makeText(context1,chatID,Toast.LENGTH_SHORT).show();

                chat!!.sendMessage(message)
            } catch (e: SmackException.NotConnectedException) {
                try {
                    chat = ChatingService.createChat(chatID)
                    chat!!.sendMessage(message)
                } catch (e1: SmackException.NotConnectedException) {
                    Log.d(TAG, "Not able to Send Live Stream")
                }

            }

        }
    }


    fun closeConnection() {
        if (connection != null) {
            connection!!.disconnect()
        }
        connection = null
        chatManager = null
    }


    private fun createSSLContext() {
        var context: SSLContext? = null
        try {
            context = SSLContext.getInstance("TLS")
            context!!.init(null, arrayOf<X509TrustManager>(object : X509TrustManager {
                @Throws(CertificateException::class)
                override fun checkClientTrusted(
                    chain: Array<X509Certificate>,
                    authType: String
                ) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(
                    chain: Array<X509Certificate>,
                    authType: String
                ) {
                }

                override fun getAcceptedIssuers(): Array<X509Certificate?> {
                    return arrayOfNulls(0)
                }
            }), SecureRandom())
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: KeyManagementException) {
        }

    }

    override fun chatCreated(chat: Chat, createdLocally: Boolean) {}

    override fun processMessage(chat: Chat, message: Message) {}

    companion object {

        private val TAG = "Chat Constant"
    }
}
