package com.project.ridehubdriver.Helper

import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.IBinder
import android.util.Log
import androidx.annotation.Nullable
import com.android.volley.Request
import com.project.ridehubdriver.Helper.xmpp.ChatConfigurationBuilder
import com.project.ridehubdriver.Utils.GPSTracker
import org.json.JSONException
import org.json.JSONObject
import java.util.*

/**
 * Created by fabio on 30/01/2016.
 */
class SensorService : Service {

    var counter = 0
    internal var gps: GPSTracker? = null
    internal lateinit var builder: ChatConfigurationBuilder
    private var Str_Latitude = ""
    private var Str_longitude = ""
    private var Str_rideid = ""
    private var driver_id = ""
    private var chatID = ""
    private var mRequest: ServiceRequest? = null

    private var timer: Timer? = null
    private var timerTask: TimerTask? = null
    internal var oldTime: Long = 0

    internal var job: JSONObject? = null

    constructor(applicationContext: Context) : super() {
        Log.i("HERE", "here I am!")
    }

    constructor() {}

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        gps = GPSTracker(applicationContext)
        builder = ChatConfigurationBuilder(this)

        Str_rideid = (intent.extras!!.get("rideid") as String?)!!
        driver_id = (intent.extras!!.get("driverid") as String?)!!
        chatID = (intent.extras!!.get("chatID") as String?)!!

        Log.d("receive service", "$Str_rideid-$driver_id")

        startTimer()
        return Service.START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i("EXIT", "ondestroy!")
        val broadcastIntent = Intent("com.app.service.ActivityRecognition.RestartSensor")
        sendBroadcast(broadcastIntent)
        stoptimertask()
    }

    fun startTimer() {
        //set a new Timer
        timer = Timer()

        //initialize the TimerTask's job
        initializeTimerTask()

        //schedule the timer, to wake up every 1 second
        timer!!.schedule(timerTask, 1000, 10000) //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    fun initializeTimerTask() {
        timerTask = object : TimerTask() {
            override fun run() {
                Log.i("in timer", "in timer ++++  " + counter++)




                if (gps != null && gps!!.canGetLocation() && gps!!.isgpsenabled()) {

                    Str_Latitude = gps!!.getLatitude().toString()
                    Str_longitude = gps!!.getLongitude().toString()


                    try {
                        sendLocationToUser(gps!!.getLocation()!!)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    postRequest_UpdateProviderLocation(ServiceConstant.UPDATE_CURRENT_LOCATION)
                }


                /* Intent intent = new Intent(getApplicationContext(), GoogleService.class);
                        startService(intent);*/


            }
        }
    }

    /**
     * not needed
     */
    fun stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer!!.cancel()
            timer = null
        }
    }

    @Nullable
    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    //-----------------------Update current Location for notification  Post Request-----------------
    private fun postRequest_UpdateProviderLocation(Url: String) {

        val jsonParams = HashMap<String, String>()
        jsonParams["ride_id"] = Str_rideid
        jsonParams["latitude"] = Str_Latitude
        jsonParams["longitude"] = Str_longitude
        jsonParams["driver_id"] = driver_id

        println("-------------service Endtripride_id----------------$Str_longitude")
        println("-------------service Endtriplatitude----------------$Str_Latitude")
        println("-------------service Endtriplongitude----------------$Str_longitude")

        println("-------------service latlongupdate----------------$Url")
        mRequest = ServiceRequest(applicationContext)
        mRequest!!.makeServiceRequest(
            Url,
            Request.Method.POST,
            jsonParams,
            object : ServiceRequest.ServiceListener {
                override fun onCompleteListener(response: String) {

                    Log.e("service updatelocation", response)

                    println("-------------service latlongupdate----------------$response")

                }

                override fun onErrorListener() {

                }
            })
    }

    @Throws(JSONException::class)
    private fun sendLocationToUser(location: Location) {


        val sendLat = java.lang.Double.valueOf(location.latitude).toString()
        val sendLng = java.lang.Double.valueOf(location.longitude).toString()

        println("endtripchatID--------------$chatID--$sendLat--$sendLat--$Str_rideid")
        if (job == null) {
            job = JSONObject()
        }
        job!!.put("action", "driver_loc")
        job!!.put("latitude", sendLat)
        job!!.put("longitude", sendLng)
        job!!.put("device_type", "android")
        job!!.put("background", "no")
        job!!.put("ride_id", Str_rideid)
        builder.sendMessage(chatID, job!!.toString())


        /* String sToID = ContinuousRequestAdapter.userID + "@" + ServiceConstant.XMPP_SERVICE_NAME;
        try {
            if(chat  != null){
                chat.sendMessage(job.toString());
            }else{
                chat = ChatingService.createChat(sToID);
                chat.sendMessage(job.toString());
            }
        } catch (SmackException.NotConnectedException e) {
            try {
                chat = ChatingService.createChat(sToID);
                chat.sendMessage(job.toString());
            }catch (SmackException.NotConnectedException e1){
                Toast.makeText(this,"Not Able to send data to the user Network Error",Toast.LENGTH_SHORT).show();
            }
        }*/

    }


}