package com.project.ridehubdriver.Helper

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.project.ridehubdriver.PojoResponse.ChatResponsePojo
import java.lang.Exception

class DBHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, VERSION) {
    override fun onCreate(sqLiteDatabase: SQLiteDatabase?) {
        sqLiteDatabase!!.execSQL(
            "create table $CHAT_TABLE_NAME ($CHAT_ID integer primary key AUTOINCREMENT, $CHAT_RIDER_ID text, $CHAT_RECEIVED_STATUS text, $CHAT_RECEIVED_MESSAGE text, $CHAT_SENT_STATUS text, $CHAT_SENT_MESSAGE text)"
        )
    }

    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        sqLiteDatabase!!.execSQL(
            "DROP TABLE IF EXISTS $CHAT_TABLE_NAME"
        )
        onCreate(sqLiteDatabase)
    }

    fun insert(data: ChatResponsePojo): Long {
        val db = this.writableDatabase
        val cv = ContentValues()
        cv.put(CHAT_RIDER_ID, data.RIDER_ID)
        cv.put(CHAT_RECEIVED_STATUS, data.RECEIVED_STATUS)
        cv.put(CHAT_RECEIVED_MESSAGE, data.RECEIVED_MESSAGE)
        cv.put(CHAT_SENT_STATUS, data.SENT_STATUS)
        cv.put(CHAT_SENT_MESSAGE, data.SENT_MESSAGE)
        val success = db!!.insert(CHAT_TABLE_NAME, null, cv)

        db.close()
        return success
    }

    fun checkDataAvailability(): Boolean {
        var isAvailable = false
        val db = this.writableDatabase
        val cursor = db.rawQuery("SELECT * FROM $CHAT_TABLE_NAME", null)
        try {
            isAvailable = cursor.count <= 0
        }catch (e:Exception){
            e.printStackTrace()
        }finally {
            cursor.close()
        }
        return isAvailable
    }

    fun getData(): ArrayList<ChatResponsePojo> {
        val list: ArrayList<ChatResponsePojo> = ArrayList()
        val db = this.writableDatabase
        val cursor = db.rawQuery("SELECT * FROM $CHAT_TABLE_NAME", null)
        try {
            cursor.moveToFirst()
            while (!cursor.isAfterLast) {
                val pojo = ChatResponsePojo()
                pojo.RIDER_ID = cursor.getString(cursor.getColumnIndex(CHAT_RIDER_ID))
                pojo.RECEIVED_STATUS = cursor.getString(cursor.getColumnIndex(CHAT_RECEIVED_STATUS))
                pojo.RECEIVED_MESSAGE = cursor.getString(cursor.getColumnIndex(CHAT_RECEIVED_MESSAGE))
                pojo.SENT_STATUS = cursor.getString(cursor.getColumnIndex(CHAT_SENT_STATUS))
                pojo.SENT_MESSAGE = cursor.getString(cursor.getColumnIndex(CHAT_SENT_MESSAGE))
                list.add(pojo)
                cursor.moveToNext()
            }
        }catch (e:Exception){
            e.printStackTrace()
        }finally {
            cursor.close()
        }

        return list
    }

    fun clearData() {
        val db = this.writableDatabase
        db.execSQL("DELETE FROM $CHAT_TABLE_NAME")
        db.execSQL("VACUUM")
        db!!.close()
    }

    companion object {
        val VERSION = 1
        val DATABASE_NAME = "RideHubChatHistory.db"
        val CHAT_ID = "id"
        val CHAT_TABLE_NAME = "chat_history"
        val CHAT_RIDER_ID = "rider_id"
        val CHAT_RECEIVED_STATUS = "chat_received_status"
        val CHAT_RECEIVED_MESSAGE = "chat_received"
        val CHAT_SENT_STATUS = "chat_sent_status"
        val CHAT_SENT_MESSAGE = "chat_sent"
    }

}
