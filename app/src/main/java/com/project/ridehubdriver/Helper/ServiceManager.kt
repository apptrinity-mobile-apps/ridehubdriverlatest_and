package com.project.ridehubdriver.Helper

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.project.ridehubdriver.Activities.SignUpSignIn
import com.project.ridehubdriver.PojoResponse.ServiceResponse
import com.project.ridehubdriver.R
import com.project.ridehubdriver.Utils.PkDialog
import com.project.ridehubdriver.Utils.SessionManager
import org.json.JSONObject
import java.util.*

/**
 */
class ServiceManager(private val context: Context, private val mServiceListener: ServiceListener) {
    private var mRequestQueue: RequestQueue? = null
    private var stringRequest: StringRequest? = null
    private var manager: ObjectManager? = null
    private val sessionManager: SessionManager
    private var userID = ""
    private var gcmID = ""
    val requestQueue: RequestQueue?
        get() {
            if (mRequestQueue == null) {
                mRequestQueue = Volley.newRequestQueue(context)
            }
            return mRequestQueue
        }

    interface ServiceListener {
        fun onCompleteListener(`object`: Any)
        fun onErrorListener(error: Any)
    }

    init {
        init()
        sessionManager = SessionManager(context)

        val user = sessionManager.userDetails
        userID = user[SessionManager.KEY_DRIVERID]!!
        gcmID = user[SessionManager.KEY_GCM_ID]!!

    }

    private fun init() {
        manager = ObjectManager()
    }

    fun makeServiceRequest(url: String, method: Int, param: HashMap<String, String>) {
        stringRequest = object : StringRequest(method, url, Response.Listener { response ->
            println("----------test response------------------$response")
            Log.e("goOnline_response", response)

            try {
                val `object` = JSONObject(response)
                val returnvalue = `object`.getString("response")
                if (returnvalue.equals(
                        "Please check the phone number and password and try again",
                        ignoreCase = true
                    )
                ) {
                    val mDialog = PkDialog(context)
                    mDialog.setDialogTitle(context.resources.getString(R.string.lbel_notification))
                    mDialog.setDialogMessage(returnvalue)
                    mDialog.setPositiveButton(
                        context.resources.getString(R.string.lbel_notification_ok),
                        View.OnClickListener { mDialog.dismiss() })
                    mDialog.show()
                } else if (returnvalue.equals(
                        "Your account not yet activated. Please contact to admin.",
                        ignoreCase = true
                    )
                ) {
                    val mDialog = PkDialog(context)
                    mDialog.setDialogTitle(context.resources.getString(R.string.lbel_notification))
                    mDialog.setDialogMessage(returnvalue)
                    mDialog.setPositiveButton(
                        context.resources.getString(R.string.lbel_notification_ok),
                        View.OnClickListener { mDialog.dismiss() })
                    mDialog.show()
                } else if (returnvalue.equals(
                        "Your account not yet verified. Please contact to admin.",
                        ignoreCase = true
                    )
                ) {
                    val mDialog = PkDialog(context)
                    mDialog.setDialogTitle(context.resources.getString(R.string.lbel_notification))
                    mDialog.setDialogMessage(returnvalue)
                    mDialog.setPositiveButton(
                        context.resources.getString(R.string.lbel_notification_ok),
                        View.OnClickListener { mDialog.dismiss() })
                    mDialog.show()
                }

                if (`object`.has("is_dead")) {
                    println("-----------is dead----------------")
                    val mDialog = PkDialog(context)
                    mDialog.setDialogTitle(context.resources.getString(R.string.action_session_expired_title))
                    mDialog.setDialogMessage(context.resources.getString(R.string.action_session_expired_message))
                    mDialog.setPositiveButton(
                        context.resources.getString(R.string.lbel_notification_ok),
                        View.OnClickListener {
                            mDialog.dismiss()
                            sessionManager.logoutUser()
                            val intent = Intent(context, SignUpSignIn::class.java)
                            intent.flags =
                                Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP
                            context.startActivity(intent)
                        })
                    mDialog.show()
                }

                val obj = manager!!.getObjectForUrl(url, response)
                if (obj is ServiceResponse) {
                    val mResponse = obj as ServiceResponse
                    if ("1".equals(mResponse.status, ignoreCase = true)) {
                        mServiceListener.onCompleteListener(obj)
                    } else {
                        if (obj is ServiceResponse) {
                            val sr = obj as ServiceResponse
                        }
                        mServiceListener.onErrorListener(obj)
                    }
                } else {
                    mServiceListener.onCompleteListener(obj)
                }
            } catch (e: Exception) {
                Toast.makeText(context, "Unknown error occurred", Toast.LENGTH_LONG).show()
            }
        }, Response.ErrorListener { error ->
            Log.e("error", error.toString())
            try {
                if (error is TimeoutError || error is NoConnectionError) {
                    Toast.makeText(
                        context,
                        "Network connection is slow.Please try again.",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (error is AuthFailureError) {
                    Toast.makeText(context, "AuthFailureError", Toast.LENGTH_SHORT).show()
                } else if (error is ServerError) {
                    Toast.makeText(context, "ServerError", Toast.LENGTH_SHORT).show()
                } else if (error is NetworkError) {
                    Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show()
                } else if (error is ParseError) {
                    Toast.makeText(context, "ParseError", Toast.LENGTH_SHORT).show()
                }
            } catch (e: Exception) {
            }

            mServiceListener.onErrorListener(error)
        }) {
            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                return param
            }

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                headers["User-agent"] = ServiceConstant.useragent
                headers["isapplication"] = ServiceConstant.isapplication
                headers["applanguage"] = ServiceConstant.applanguage
                headers["apptype"] = ServiceConstant.cabily_AppType
                headers["driverid"] = userID
                headers["apptoken"] = gcmID
                System.out.println("isapplication------------" + ServiceConstant.isapplication)
                System.out.println("applanguage------------" + ServiceConstant.applanguage)


                return headers


            }
        }
        val retryPolicy = DefaultRetryPolicy(0, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        stringRequest!!.retryPolicy = retryPolicy
        stringRequest!!.retryPolicy = DefaultRetryPolicy(
            30000,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue!!.add(stringRequest!!)
    }

    fun <T> addToRequestQueue(req: Request<T>, tag: String) {
        req.tag = if (TextUtils.isEmpty(tag)) TAG else tag
        requestQueue!!.add(req)
    }

    fun <T> addToRequestQueue(req: Request<T>) {
        req.tag = TAG
        requestQueue!!.add(req)
    }

    fun cancelPendingRequests(tag: Any) {
        if (mRequestQueue != null) {
            mRequestQueue!!.cancelAll(tag)
        }
    }

    companion object {
        private val TAG = "ServiceManager TAG"
    }
}
